import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    OnDestroy,
    ChangeDetectorRef,
    TemplateRef
} from '@angular/core';
import { screen } from "tns-core-modules/platform";
import { ScrollView, ScrollEventData } from "ui/scroll-view";
import { LoadOnDemandListViewEventData, RadListView } from 'nativescript-ui-listview';
import {OrientationChangedEventData} from "application"
import * as app from "application";
import {Router} from "@angular/router";


@Component({
    selector: "tns-platform-dialog",
    moduleId: module.id,
    templateUrl: "./dialog-default.component.html",
    styleUrls: [
        "./dialog-default.common.scss",
        "./dialog-default.component.scss"
    ]
})

export class PlatformDialogDefaultComponent implements OnInit, OnDestroy {
    public result: string;
    public screenRationWidth: any;
    public spanCount: any;
    public changeOrientation: boolean;

    @Input() dialogItemTemplate: TemplateRef<any>;
    @Input() data: any;
    @Output() loadDataEvent = new EventEmitter<any>();

    loadingData: boolean;

    constructor(private ref: ChangeDetectorRef, private router: Router,) {
        
    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }
}
