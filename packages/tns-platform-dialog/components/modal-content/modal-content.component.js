"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var platform_gdpr_service_1 = require("./../../services/platform-gdpr.service");
var ModalContentComponent = /** @class */ (function () {
    function ModalContentComponent(params, gdprService) {
        this.params = params;
        this.gdprService = gdprService;
        this.agreeEvent = new core_1.EventEmitter();
        this.prompt = params.context.promptMsg;
        this.template = params.context.template;
        this.gdprConfiguration = params.context.gdprConfiguration;
    }
    ModalContentComponent.prototype.disagree = function (result) {
        var _this = this;
        this.params.context.functions.cancel(function (result) {
            if (result) {
                _this.params.closeCallback();
                _this.gdprService.setGdprEvent(result);
            }
        });
    };
    ModalContentComponent.prototype.agree = function (result) {
        var _this = this;
        this.params.context.functions.confirm(function (result) {
            if (result) {
                _this.params.closeCallback();
                _this.gdprService.setGdprEvent(result);
            }
        });
    };
    ModalContentComponent.prototype.ngAfterViewChecked = function () {
    };
    ModalContentComponent.prototype.ngOnInit = function () {
        this.vcr.createEmbeddedView(this.template);
    };
    __decorate([
        core_1.ViewChild('vcr', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], ModalContentComponent.prototype, "vcr", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ModalContentComponent.prototype, "agreeEvent", void 0);
    ModalContentComponent = __decorate([
        core_1.Component({
            selector: 'ns-modal-content',
            templateUrl: './modal-content.component.html',
            styleUrls: ['./modal-content.component.scss'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams, platform_gdpr_service_1.PlatformGdprService])
    ], ModalContentComponent);
    return ModalContentComponent;
}());
exports.ModalContentComponent = ModalContentComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtY29udGVudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtb2RhbC1jb250ZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEySTtBQUMzSSxrRUFBc0U7QUFDdEUsZ0ZBQTZFO0FBUzdFO0lBVUksK0JBQW9CLE1BQXlCLEVBQVUsV0FBZ0M7UUFBbkUsV0FBTSxHQUFOLE1BQU0sQ0FBbUI7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBcUI7UUFGN0UsZUFBVSxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO1FBR3RDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7UUFDdkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztRQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztJQUM5RCxDQUFDO0lBRU0sd0NBQVEsR0FBZixVQUFnQixNQUFjO1FBQTlCLGlCQU9DO1FBTkcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFDLE1BQU07WUFDeEMsSUFBSSxNQUFNLEVBQUU7Z0JBQ1IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDNUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDekM7UUFDTCxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFTSxxQ0FBSyxHQUFaLFVBQWEsTUFBYztRQUEzQixpQkFPQztRQU5HLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNO1lBQ3pDLElBQUksTUFBTSxFQUFFO2dCQUNSLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3pDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBSUQsa0RBQWtCLEdBQWxCO0lBQ0EsQ0FBQztJQUVELHdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBbkMyQztRQUEzQyxnQkFBUyxDQUFDLEtBQUssRUFBRSxFQUFDLElBQUksRUFBRSx1QkFBZ0IsRUFBQyxDQUFDO2tDQUFNLHVCQUFnQjtzREFBQztJQUV4RDtRQUFULGFBQU0sRUFBRTs7NkRBQWlDO0lBUmpDLHFCQUFxQjtRQVBqQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixXQUFXLEVBQUUsZ0NBQWdDO1lBQzdDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO1lBQzdDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDO3lDQVk4QixnQ0FBaUIsRUFBdUIsMkNBQW1CO09BVjlFLHFCQUFxQixDQTJDakM7SUFBRCw0QkFBQztDQUFBLEFBM0NELElBMkNDO0FBM0NZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q29udGFpbmVyUmVmLCBJbnB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3Q2hlY2tlZCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dQYXJhbXMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2cnO1xyXG5pbXBvcnQgeyBQbGF0Zm9ybUdkcHJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zZXJ2aWNlcy9wbGF0Zm9ybS1nZHByLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25zLW1vZGFsLWNvbnRlbnQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL21vZGFsLWNvbnRlbnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbW9kYWwtY29udGVudC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBNb2RhbENvbnRlbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0NoZWNrZWQge1xyXG4gICAgcHVibGljIHRlbXBsYXRlIDogVGVtcGxhdGVSZWY8YW55PjtcclxuICAgIHB1YmxpYyBwcm9tcHQ6IHN0cmluZztcclxuICAgIHB1YmxpYyBnZHByQ29uZmlndXJhdGlvbiA6ICBhbnk7XHJcblxyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3ZjcicsIHtyZWFkOiBWaWV3Q29udGFpbmVyUmVmfSkgdmNyOiBWaWV3Q29udGFpbmVyUmVmO1xyXG5cclxuICAgIEBPdXRwdXQoKSBhZ3JlZUV2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcywgcHJpdmF0ZSBnZHByU2VydmljZTogUGxhdGZvcm1HZHByU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMucHJvbXB0ID0gcGFyYW1zLmNvbnRleHQucHJvbXB0TXNnO1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGUgPSBwYXJhbXMuY29udGV4dC50ZW1wbGF0ZTtcclxuICAgICAgICB0aGlzLmdkcHJDb25maWd1cmF0aW9uID0gcGFyYW1zLmNvbnRleHQuZ2RwckNvbmZpZ3VyYXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRpc2FncmVlKHJlc3VsdDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wYXJhbXMuY29udGV4dC5mdW5jdGlvbnMuY2FuY2VsKChyZXN1bHQpPT57XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2soKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2RwclNlcnZpY2Uuc2V0R2RwckV2ZW50KHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhZ3JlZShyZXN1bHQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMucGFyYW1zLmNvbnRleHQuZnVuY3Rpb25zLmNvbmZpcm0oKHJlc3VsdCk9PntcclxuICAgICAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjaygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZHByU2VydmljZS5zZXRHZHByRXZlbnQocmVzdWx0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICBuZ0FmdGVyVmlld0NoZWNrZWQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy52Y3IuY3JlYXRlRW1iZWRkZWRWaWV3KHRoaXMudGVtcGxhdGUpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=