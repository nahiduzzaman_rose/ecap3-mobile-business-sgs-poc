import { Component, OnInit, ViewContainerRef, Input, TemplateRef, ViewChild, AfterViewChecked, Output, EventEmitter } from '@angular/core';
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { PlatformDialogService } from './../../services/platform-dialog.service';

@Component({
    selector: 'ns-modal-content',
    templateUrl: './modal-content.component.html',
    styleUrls: ['./modal-content.component.scss'],
    moduleId: module.id,
})

export class ModalContentComponent implements OnInit, AfterViewChecked {
    public template : TemplateRef<any>;
    public prompt: string;
    public configOptions: ModalDialogParams;
    public modalWidth: string;
    public data: modalHeaderData;

    @ViewChild('vcr', {read: ViewContainerRef, static: true }) vcr: ViewContainerRef;

    @Output() agreeEvent = new EventEmitter();

    constructor(private params?: ModalDialogParams, private dialogService?: PlatformDialogService) {
        this.configOptions = this.params;
        this.prompt = params.context.promptMsg;
        this.template = params.context.template;
        this.data = params.context.data;
        this.setWidth();
    }

    public setWidth(){
        if(!this.configOptions.context.isFullscreen){
            this.modalWidth = this.configOptions.context.width
        }else{
            this.modalWidth = "auto"
        }
    }

    public disagree(result?: string) {
        this.params.context.functions.cancel((result)=>{
            if (result) {
                this.params.closeCallback(result);
            }
        })
    }

    public agree(result?: string) {
        this.params.context.functions.confirm((result)=>{
            if (result) {
                this.params.closeCallback(result);
            }
        })
    }

    ngAfterViewChecked() {
        
    }

    ngOnInit() {
        setTimeout(()=>{
            this.vcr.createEmbeddedView(this.template);
        },0)
    }

}

export interface modalHeaderData{
    headerTitle: string;
    description?: string;
}
