export * from './platform-dialog.module';
export * from './services/platform-dialog.service';

export * from './components/modal-content/modal-content.component';
export * from './components/dialog-default/dialog-default.component';
