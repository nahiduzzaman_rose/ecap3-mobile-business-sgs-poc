import { ModalContentComponent } from './components/modal-content/modal-content.component';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { PlatformDialogDefaultComponent } from "./components/dialog-default/dialog-default.component";
import { PlatformDialogService } from "./services/platform-dialog.service";



@NgModule({
    providers: [
        PlatformDialogService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        PlatformDialogDefaultComponent,
        ModalContentComponent
    ],
    entryComponents: [
        ModalContentComponent
    ],
    exports: [
        PlatformDialogDefaultComponent,
        ModalContentComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformDialogModule {}
