import {Component, OnInit, ChangeDetectorRef, Input, ViewContainerRef, ViewChild, TemplateRef} from '@angular/core';
import {TNSPlayer, TNSRecorder, AudioPlayerOptions} from 'nativescript-audio';
import {ModalDialogOptions, ModalDialogService} from 'nativescript-angular/modal-dialog';
import {screen} from "tns-core-modules/platform";
import {AudioPlayerModalComponent} from '../audio-player-modal/audio-player-modal.component';


@Component({
    selector: 'ns-audio-gallery',
    templateUrl: './audio-gallery.component.html',
    styleUrls: ['./audio-gallery.component.scss'],
    moduleId: module.id,
})
export class AudioGalleryComponent implements OnInit {
    @Input() audioData: any;
    @Input() configuration: any;
    _player: TNSPlayer;
    _recorder: TNSRecorder;
    flag: boolean;
    timer: boolean;
    millisecondString: string;
    fileName: any;

    @ViewChild('modalContainer', {static: false}) modalContainer: TemplateRef<any>;
    screenRationWidth: number;
    screenRationHeight: number;

    constructor(private cdrf: ChangeDetectorRef,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) {
        this._player = new TNSPlayer();
        this._player.debug = true; // set true for tns_player logs
        this._recorder = new TNSRecorder();
        this._recorder.debug = true; // set true for tns_recorder log
    }

    openAudioPlayer(audio) {
        audio['audioDirectory'] = this.configuration && this.configuration.fileDirectoryName || null;
        let options: ModalDialogOptions = {
            context: {
                promptMsg: "This is the prompt message!!!",
                audioData: audio
            },
            fullscreen: false,
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(AudioPlayerModalComponent, options)
            .then((dialogResult: string) => {
                console.log(dialogResult);
            })
    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
    }

}
