import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from "@angular/core";
import { AudioPlayerOptions, AudioRecorderOptions, TNSPlayer, TNSRecorder } from 'nativescript-audio';
import * as app from 'tns-core-modules/application';
import { File, knownFolders } from 'tns-core-modules/file-system';
import * as platform from 'tns-core-modules/platform';
import { screen } from "tns-core-modules/platform";
import { clearInterval, setInterval } from "tns-core-modules/timer";
import * as dialogs from 'tns-core-modules/ui/dialogs';
import { Page } from 'tns-core-modules/ui/page';

@Component({
    selector: "tns-platform-audio",
    moduleId: module.id,
    templateUrl: "./audio-default.component.html",
    styleUrls: [
        "./audio-default.common.scss",
        "./audio-default.component.scss"
    ],
    changeDetection: ChangeDetectionStrategy.Default
})

export class PlatformAudioDefaultComponent implements OnInit, OnDestroy {
    public isPlaying: boolean;
    public player: boolean = false;
    public recordingPath;
    public isRecording: boolean = false;
    public audioMeter = '0';
    public recordedAudioFile: any;
    public audioPath;
    public saveFlag;
    public saveFlagForContainer: boolean = false;
    public remainingDuration; // used to show the remaining time of the audio track
    private _recorder;
    public tempAudioFile: any;
    private _player: TNSPlayer;
    @Input() containerFlag;
    @Input() configuration: any;
    public flagFromContainer = this.containerFlag;
    @Output() audioFile: EventEmitter<any> = new EventEmitter<any>();
    @Output() flagtoContainer: EventEmitter<any> = new EventEmitter<any>();
    public flag: boolean = false;
    minuteString: any = "00";
    secondString: any = "00"; 
    millisecondString: any = "00";
    startTime: any;
    pauseTime: any;
    totalMinutes: any;
    totalSeconds: any;
    totalMilliseconds: any;
    destroy: boolean = false;
    public timer: boolean = false;
    isStarted;
    isPaused;
    interval;
    fileName;
    screenRationWidth: any;
    screenRationHeight: any;
    isTabWidthIsNineSixty: any;
    myInterval:any;

    recordTimeInterval: any;


    constructor(page: Page, private cdrf: ChangeDetectorRef) {
        this._player = new TNSPlayer();
        this._player.debug = false; // set true for tns_player logs
        this._recorder = new TNSRecorder();
        this._recorder.debug = false; // set true for tns_recorder log
    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
    }
    public async startRecord() {
        this.destroy = true;
        this.player = false;
        this.saveFlag = false;
        this.stateClick();
        try {
            if (!TNSRecorder.CAN_RECORD()) {
                dialogs.alert('This device cannot record audio.');
                return;
            }
            const audioFolder = knownFolders.currentApp().getFolder(this.configuration.fileDirectoryName);
            // console.log(JSON.stringify(audioFolder));
            let androidFormat;
            let androidEncoder;
            if (platform.isAndroid) {
                androidFormat = 2;
                androidEncoder = 3;
            }
            this.recordingPath = `${audioFolder.path}/recording_${new Date().getTime().toString()}.${this.platformExtension()}`;
            const lastIndex = this.recordingPath.lastIndexOf("/");
            const finalIndex = this.recordingPath.lastIndexOf("3");
            this.fileName = this.recordingPath.slice(lastIndex + 1, finalIndex + 1);
            //console.log("recordingPath::", this.recordingPath);
            //console.log("filename::", this.fileName);
            //console.log("flag from container::", this.containerFlag);

            const recorderOptions: AudioRecorderOptions = {
                filename: this.recordingPath,
                format: androidFormat,
                encoder: androidEncoder,
                metering: true,
                infoCallback: infoObject => {
                },
                errorCallback: errorObject => {
                }
            };
            await this._recorder.start(recorderOptions).catch(ex => {
                // TODO
            });
            this.isRecording = true;
            this.updateDetectChange();

        } catch (err) {
            this.isRecording = false;
            dialogs.alert(err);
        }
    }
    public async stopRecord() {
        this.destroy = false;
        this.onReset();
        await this._recorder.stop().catch(ex => {
            console.log(ex);
            this.updateDetectChange();
        });
        this.isRecording = false;
        this.saveFlag = true;
        this.flag = true;
        this.player = true;
    }
    public saveFile() {
        try {
            const audioFolder = knownFolders.currentApp().getFolder(this.configuration.fileDirectoryName);
            const recordedFile = audioFolder.getFile(this.fileName);
            // console.log('recordFile', recordedFile);
            // console.log('recording exists: ' + File.exists(recordedFile.path));
            this.recordedAudioFile = this.recordingPath;
            this.saveFlagForContainer = true;
            this.audioFile.emit(this.recordedAudioFile);
            this.flagtoContainer.emit(this.saveFlagForContainer);
            this.saveFlag = false;
            this.player = false;
            this.isRecording = false;
        } catch (ex) {
            console.log('exception: ', ex);
        }
    }
    removeFile() {
        File.fromPath(this.recordingPath).remove().then(value => {
            console.log("Remove Status ", value);
        });
        this.saveFlagForContainer = true;
        this.flagtoContainer.emit(this.saveFlagForContainer);
        this.saveFlag = false;
        this.player = false;
        this.isRecording = false;
    }
    public async playRecordedFile() {
        this.stateClick();
        this.flag = false;
        this.player = true;
        console.log("flag just after playing", this.flag);
        //const audioFolder = knownFolders.currentApp().getFolder('audio');
        //const recordedFile = audioFolder.getFile(this.fileName);
        // console.log('RECORDED FILE : ' + JSON.stringify(recordedFile));
        this.isPlaying = true;
        const playerOptions: AudioPlayerOptions = {
            audioFile: "~/" + this.configuration.fileDirectoryName + "/" + this.fileName,
            loop: false,
            completeCallback: async () => {
                this.stopPlaying();
                this.updateDetectChange();
            },
            errorCallback: errorObject => {
            },
            infoCallback: infoObject => {
            }
        };
        await this._player.playFromFile(playerOptions).catch(err => {
            console.log('error playFromFile');
        });
    }
    public async pauseAudio() {
        try {
            await this._player.pause();
            this.isPlaying = false;
            this.isPaused = true;
        } catch (error) {
            console.log(error);
            this.isPlaying = true;
        }
    }
    public async stopPlaying() {
        console.log("player stopped");
        this.flag = true;
        this.isPlaying = false;
        this.player = true;
        await this._player.dispose();
        this.onReset();

    }
    public async resumePlaying(args) {
        console.log('START!');
        await this._player.play();
    }
    private platformExtension() {
        return `${app.android ? 'mp3' : 'caf'}`;
    }
    setDefaults() {
        this.startTime = 0;
        this.pauseTime = 0;
        this.totalMinutes = 0;
        this.totalSeconds = 0;
        this.totalMilliseconds = 0;
        this.minuteString = "00";
        this.secondString = "00";
        this.millisecondString = "00";
        this.isStarted = false;
        this.isPaused = false;

        this.updateDetectChange();
    }
    setUpdate() {
        let totalTime = Date.now() - this.startTime;
        this.totalMinutes = Math.floor(totalTime / 60000);
        this.totalSeconds = Math.floor((totalTime % 60000) / 1000);
        this.totalMilliseconds = (totalTime / 1000).toFixed(2);
        this.minuteString = (this.totalMinutes < 10) ? ("0" + this.totalMinutes) : this.totalMinutes;
        this.secondString = (this.totalSeconds < 10) ? ("0" + this.totalSeconds) : this.totalSeconds;
        this.millisecondString = this.totalMilliseconds.toString().slice(-2);

        this.updateDetectChange();
    }
    stateClick() {
        this.timer = true;
        console.log("state clocked");
        if (!this.isStarted) {
            this.startTime = Date.now();
            this.isStarted = true;
            console.log("state", "PAUSE");
            this.interval = setInterval(() => this.isPaused ? null : this.setUpdate(), 10);
        } else {
            if (!this.isPaused) {
                this.pauseTime = Date.now();
                this.isPaused = true;
                console.log("state", "RESUME");
            } else {
                this.startTime += (Date.now() - this.pauseTime);
                this.isPaused = false;
                console.log("state", "PAUSE");
            }
        }

        this.updateDetectChange();
    }

    onReset() {
        this.timer = false;
        clearInterval(this.interval);
        this.setDefaults();
    }

    updateDetectChange() {
        /*if (!this.cdrf['destroyed']) {
            this.cdrf.detectChanges();
            this.cdrf.markForCheck();
        }*/
    }

    ngOnDestroy() {
        console.log("aksdhasd");
        if (this.destroy == true) {
            this.stopRecord();
            //this.removeFile();
        }
        /* if (this.saveFlag == true) {
            this.removeFile();
        } */
        if (this.timer == true) {
            this.stopPlaying();
            //this.removeFile();
        }

        // this.cdrf.detach();
    }
}
