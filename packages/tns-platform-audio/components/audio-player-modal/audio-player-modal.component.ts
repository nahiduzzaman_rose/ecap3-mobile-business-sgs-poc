import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { TNSPlayer, TNSRecorder, AudioPlayerOptions } from 'nativescript-audio';
import { screen } from "tns-core-modules/platform";
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { Progress } from "tns-core-modules/ui/progress";
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { FileLocal } from '../../model/file-local';


@Component({
    selector: 'ns-audio-player-modal',
    templateUrl: './audio-player-modal.component.html',
    styleUrls: ['./audio-player-modal.component.scss'],
    moduleId: module.id,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AudioPlayerModalComponent implements OnInit {
    public progressValue: number;
    _player: TNSPlayer;
    _recorder: TNSRecorder;
    flag: boolean;
    player: boolean;
    isPlaying: boolean;
    timer: boolean;
    isStarted: any;
    startTime: number;
    interval: any;
    isPaused: any;
    pauseTime: number;
    totalMinutes: number;
    totalSeconds: number;
    totalMilliseconds: number; 
    minuteString: any = "00";
    secondString: any = "00"; 
    millisecondString: any = "00";
    fileName: any;
    configuration: any;
    screenRationWidth: number;
    screenRationHeight: number;
    audioData: any;
    duration: number;
    public durationInSeconds: number;
    totalTime: number;

    sumMilliseconds: number;
    constructor(private cdrf: ChangeDetectorRef,
        private localPlatformDataService : LocalPlatformDataService,
        private params: ModalDialogParams) { 
        this._player = new TNSPlayer();
        this._player.debug = true; // set true for tns_player logs
        this._recorder = new TNSRecorder();
        this._recorder.debug = true; // set true for tns_recorder log
    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
        console.log('screenRationHeight',screen.mainScreen.heightDIPs);
        //console.log('screenRationHeight',this.screenRationHeight);
        this.progressValue = 0;

        console.log("shohan");

        /* setInterval(() => {
            this.progressValue += 1;
        }, 300); */
    }

    stateClick() {
        this.timer = true;
        this.sumMilliseconds = 0;
        console.log("state clocked");
        if (!this.isStarted) {
            this.startTime = Date.now();
            this.isStarted = true;
            console.log("state", "PAUSE");
            this.interval = setInterval(() => this.setUpdate(), 10);
        } else {
            if (!this.isPaused) {
                this.pauseTime = Date.now();
                this.isPaused = true;
                console.log("state", "RESUME");
            } else {
                this.startTime += (Date.now() - this.pauseTime);
                this.isPaused = false;
                console.log("state", "PAUSE");
            }
        }
    }

    public getFileName(recordingPath) {
        const lastIndex = recordingPath.lastIndexOf("/");
        const fnalIndex = recordingPath.lastIndexOf("g");
        const audio = recordingPath.slice(lastIndex + 1, fnalIndex + 1);
        this.fileName = audio;
        return this.fileName;
    }

    public async playRecordedFile() {
        this.audioData = this.params.context.audioData;
        this.flag = false;
        this.player = true;
        console.log("flag just after playing", this.flag);
        console.log('duration',this._player.duration);
        //const audioFolder = knownFolders.currentApp().getFolder('audio');
        //const recordedFile = audioFolder.getFile(this.fileName);
        // console.log('RECORDED FILE : ' + JSON.stringify(recordedFile));
        this.isPlaying = true;

        let audioFile = this.audioData.audioDirectory && "~/" + this.audioData.audioDirectory + "/" + this.getFileName(this.audioData.LocalPath) ||
            this.getLocalPathFromLocalDb(this.audioData.ItemId);
        debugger
        const playerOptions: AudioPlayerOptions = {
            audioFile: audioFile,
            loop: false,
            completeCallback: async () => {
                this.stopPlaying();
                this.cdrf.markForCheck();
                this.cdrf.detectChanges();
            },
            errorCallback: errorObject => {
                console.log('errorObject',errorObject);
            },
            infoCallback: infoObject => {
                console.log('infoObject',infoObject);
            }
        };
        
        await this._player.playFromFile(playerOptions).then((res)=>{
            this._player.getAudioTrackDuration().then(duration => {
                // iOS: duration is in seconds
                // Android: duration is in milliseconds
                this.duration = parseInt(duration);
                this.durationInSeconds = Math.floor(this.duration/1000);
                console.log(`song duration:`, duration);
                this.stateClick();
            });
        })
    }

    getLocalPathFromLocalDb(fileId){
        const file = <FileLocal>this.localPlatformDataService.getById(fileId);
        let filePath = '';
        if (file && file.LocalPath) {
            return file.LocalPath
        }
    }

    public async stopPlaying() {
        console.log('duration',this.duration);
        console.log('sumMilliseconds',this.sumMilliseconds);
        //if(this.sumMilliseconds == this.duration) {    
            this.flag = true;
            this.isPlaying = false;
            this.player = true;
            
            await this._player.dispose();
            this.onReset();
       // }
    }

    setUpdate() {
        this.totalTime = Date.now() - this.startTime;
        this.totalMinutes = Math.floor(this.totalTime / 60000);
        this.totalSeconds = Math.floor((this.totalTime % 60000) / 1000);
        this.totalMilliseconds = (this.totalTime % 1000)

        this.minuteString = (this.totalMinutes < 10) ? ("0" + this.totalMinutes) : this.totalMinutes;
        this.secondString = (this.totalSeconds < 10) ? ("0" + this.totalSeconds) : this.totalSeconds;
        this.sumMilliseconds =  this.secondString*1000 + this.totalMilliseconds;

        this.millisecondString = (this.totalMilliseconds < 10) ? ("0" + this.totalMilliseconds.toString().substring(0,2)) : this.totalMilliseconds.toString().substring(0,2);
        console.log('this.millisecondString',this.millisecondString);

        //

        /* console.log('millisecondString',parseInt(this.millisecondString));
        console.log('sumMilliseconds',this.sumMilliseconds);
        console.log(this.minuteString,this.secondString,this.millisecondString); */

        console.log('sumOfMilliseconds',this.sumMilliseconds);

        if(this.sumMilliseconds >= this.duration) {
            clearInterval(this.interval);
            console.log('timer stopped');
        }
       
        console.log('secondString',this.secondString);
        console.log('durationInSeconds',this.durationInSeconds);
        console.log('secondString/durationInSeconds',this.secondString/this.durationInSeconds);
        this.progressValue = (this.secondString/this.durationInSeconds)*100;
        console.log('progressValue',this.progressValue) 
    }

    onReset() {
        console.log('reset');
        this.timer = false;
        clearInterval(this.interval);
        this.setDefaults();
    }

    setDefaults() {
        this.startTime = 0;
        this.pauseTime = 0;
        this.totalMinutes = 0;
        this.totalSeconds = 0;
        this.totalMilliseconds = 0;
        this.minuteString = "00";
        this.secondString = "00";
        this.millisecondString = "00";
        this.isStarted = false;
        this.isPaused = false;
    }

    public closePlayer(result?: string) {
        /* this.params.context.functions.cancel((result)=>{
            if (result) { */
                this.params.closeCallback('close');
            /* }
        }) */
    }

    onValueChanged(args) {
        let progressBar = <Progress>args.object;

        console.log("Value changed for " + progressBar);
        console.log("New value: " + progressBar.value);
    }

    

}
