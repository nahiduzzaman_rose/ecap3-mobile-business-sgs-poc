import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {PlatformAudioService} from "./services/platform-audio.service";
import { PlatformAudioDefaultComponent } from "./components/audio-default/audio-default.component";
import { AudioGalleryComponent } from './components/audio-gallery/audio-gallery.component';
import { AudioPlayerModalComponent } from './components/audio-player-modal/audio-player-modal.component';

@NgModule({
    providers: [
        PlatformAudioService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        PlatformAudioDefaultComponent,
        AudioGalleryComponent,
        AudioPlayerModalComponent
    ],
    entryComponents: [
        AudioPlayerModalComponent
    ],
    exports: [
        PlatformAudioDefaultComponent,
        AudioGalleryComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})

export class PlatformAudioModule {}
