import {
    Component, OnInit, ViewEncapsulation, Input, EventEmitter, ViewChild,
    TemplateRef, Output, Inject, HostListener, ElementRef, AfterContentChecked, Optional, ViewContainerRef, AfterViewInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDialogService, ModalDialogOptions } from 'nativescript-angular/modal-dialog'
import { ModalContentComponent } from '../modal-content/modal-content.component';
import { PlatformGdprService } from './../../services/platform-gdpr.service';
import * as _ from 'lodash';
import {forEach} from 'lodash';

@Component({
    selector: "tns-platform-gdpr",
    moduleId: module.id,
    templateUrl: "./gdpr-default.component.html",
    styleUrls: [
        "./gdpr-default.common.scss",
        "./gdpr-default.component.scss"
    ]
})

export class PlatformGdprDefaultComponent implements OnInit,AfterViewInit {
    public result: string;
    @Input() requiredConditionsDetails: any;
    @Input() getGdprContentUsingInterface: boolean;
    @Input() userEmail : string;
    @Input() gdprConfiguration : any;
    @Output() OnGDPRacceptanceSuccessFull = new EventEmitter<any>();
    @ViewChild('gdprContainer',{ static: false }) gdprContainer : TemplateRef<any>;
    // personIdRecieved = new Subject<boolean>();
    // getPersonId = this.personIdRecieved.asObservable();
    allGdprContents: any[]; // all gdpr contents store
    currentGdprType: any; // current gdpr type
    currentGdprIndex: any; // current gdpr index
    allGdprArrived: EventEmitter<boolean> = new EventEmitter(); // eventemmiter for all gdpr content loader
    gdprModalHeaderTitle: any; // gdpr modal header title
    gdprModalAgreeText: any; // gdpr modal agree text
    gdprModalDisAgreeText: any; // gdpr modal disagree text
    termAndCondition: any; // term and condition store
    requiredConditions: any; // required conditions array
    contextId: any; // store context Id of GDPR
    public termsConditionForm: FormGroup; // Declaration of T&C form

    readFullDocument: boolean = true;
    @HostListener('scroll', ['$event'])
    onScroll(event: any) {
        if (!this.readFullDocument && (event.target.offsetHeight + event.target.scrollTop) >= event.target.scrollHeight) {
            this.readFullDocument = true;
            this.updateGdprForm();
        }
    }

    constructor(private modalService: ModalDialogService, 
        private viewContainerRef: ViewContainerRef,
        private gdprService: PlatformGdprService,
       @Optional() @Inject("IAppPlatfromGdpr") private IAppPlatfromGdpr,
        private formBuilder: FormBuilder) {

        //  console.log("gdpr component loaded");
        this.termAndCondition = {}; // reseting at component load
        // =========================================
        // waiting for all gdpr contents arrival
        // =========================================
        
        this.allGdprArrived.subscribe(response => {
            if (response) {
                this.denormalizeGdprContents();
            }
        });
    }

    loadGdprContent(){
        if(this.getGdprContentUsingInterface){
            this.IAppPlatfromGdpr.getGdprContents(this.requiredConditions, this.userEmail).subscribe((response:any) => {
                if (response) {
                    this.allGdprContents = this.getDenomGdprContent(response) ;
                    this.allGdprArrived.emit(true); // emitting event after all gdpr content arrived
                }
            });
        }else{
            console.log('Without Interface !!');
            this.gdprService.getAllGdprContent().subscribe((response: any) => {
                // =================================================
                // if gdpr content loads from database
                // =================================================
                if (response) {
                    let gdprContentList = this.getDenomGdprContent(response) ;
                    
                    let sortOrder = this.requiredConditions;
                    gdprContentList.sort(function(a:any, b:any){
                        return sortOrder.indexOf(a.TermAndConditionType) - sortOrder.indexOf(b.TermAndConditionType);
                    });
                    this.allGdprContents = gdprContentList ;
                    console.log('allGdprContents',this.allGdprContents);
                    this.allGdprArrived.emit(true); // emitting event after all gdpr content arrived
                }
            });
        }
    }

    ngAfterContentChecked() {
        
    }

    updateGdprForm() {
        this.termsConditionForm.controls['termValue'].setValue(this.readFullDocument);
        this.termsConditionForm.controls['termValue'].updateValueAndValidity();
    }

    getDenomGdprContent(gdprDataList){
       return forEach(gdprDataList, (data)=> new DTOGDPR(data));
    }

    private denormalizeGdprContents(): void {
        //  console.log('gdpr information arrived from mongo');
       //   console.log(this.allGdprContents);
        this.setCurrentGpdrOptions(0); // setting initial gdpr option to first element of array
        this.openGdprConditionModal(); // openning GDPR conditional modal
    }

    public setCurrentGpdrOptions(index?: number): void {
        this.currentGdprIndex = index ? index : 0;
        this.currentGdprType = this.requiredConditions[this.currentGdprIndex];
        console.log('currentGdprType',this.currentGdprType);
        //this.readFullDocument = false;
        //this.updateGdprForm();
    }

    public storeInitialData(): void {
        this.requiredConditions = this.requiredConditionsDetails.RequiredTermsAndConditions; // storing required conditions array
        this.contextId = this.requiredConditionsDetails.ContextId; // storing contextID supplied by login app as INPUT
    }

    public openGdprConditionModal() {

        let confirm = (callback) => {
            console.log('call confirm !!');
            // console.log(this.termsConditionForm.value);
            //this.termAndCondition[this.currentGdprType] = this.termsConditionForm.value.termValue;
            this.termAndCondition[this.currentGdprType] = true;
            const totalConditionCount = this.requiredConditions.length - 1;
            if (totalConditionCount > this.currentGdprIndex) {
                this.setCurrentGpdrOptions(this.currentGdprIndex + 1);
                callback(false);
            } else {
                const GdprData = {
                    "grant_type": "authenticate_terms_and_conditions",
                    "context_id": this.contextId,
                    "accepted_terms_and_conditions": JSON.stringify(this.termAndCondition),
                    "required_conditions": JSON.stringify(this.requiredConditions)
                }
                this.gdprService.StoreGDPRdata(GdprData).subscribe(response => {
                    this.OnGDPRacceptanceSuccessFull && this.OnGDPRacceptanceSuccessFull.emit({GdprData: GdprData, Email: this.userEmail, Status: true});
                    callback(response);
                }, error => {
                    this.OnGDPRacceptanceSuccessFull && this.OnGDPRacceptanceSuccessFull.emit({GdprData: GdprData,Email: this.userEmail, Error: error.error, Status: false});
                    callback(true);
                });
            }
        };

        let cancel = (callback) => {
            console.log('call cenceled');
            this.termAndCondition[this.currentGdprType] = false;
            const GdprData = {
                "grant_type": "authenticate_terms_and_conditions",
                "context_id": this.contextId,
                "accepted_terms_and_conditions": JSON.stringify(this.termAndCondition),
                "required_conditions": JSON.stringify(this.requiredConditions)
            }
            this.gdprService.StoreGDPRdata(GdprData).subscribe(response => {
                callback(true);
            }, error => {
                callback(true);
                this.OnGDPRacceptanceSuccessFull && this.OnGDPRacceptanceSuccessFull.emit({GdprData: GdprData,Email: this.userEmail,  Error: error.error, Status: false});
            });
        };

        let options: ModalDialogOptions = {
            context: { 
                promptMsg: "This is the prompt message!!!",  
                template: this.gdprContainer,
                gdprConfiguration: this.gdprConfiguration,
                functions: {
                    confirm: confirm,
                    cancel: cancel
                }
            },
            fullscreen: false,
            viewContainerRef: this.viewContainerRef
        };
    
        this.modalService.showModal(ModalContentComponent, options)
            .then((dialogResult: string) => {
                this.result = dialogResult
            })
    }

    ngAfterViewInit() {
        this.storeInitialData(); // storing initial value

        // ================================================
        // get all gdpr content at first call
        // ================================================

        this.loadGdprContent();
        
        /* this.termsConditionForm = this.formBuilder.group({
            "termValue": [this.readFullDocument, Validators.requiredTrue]
        });

        this.gdprService.gdprReloadControl.subscribe(termsConditionDetails=>{
            if(termsConditionDetails){
                this.termAndCondition = termsConditionDetails;
                this.loadGdprContent();
            }
        }); */
    }

    ngOnInit() {
        
    }
}

export class DTOGDPR{
    ItemId: string;
    Language: string;
    TermAndConditionContent: string;
    TermAndConditionType: string;
    constructor(data?){
        data = data || {};
        this.ItemId = data && data.ItemId || "";
        this.Language = data && data.Language || "en-US";
        this.TermAndConditionContent = data && data.TermAndConditionContent || "";
        this.TermAndConditionType = data && data.TermAndConditionType || "";
    }
}