import { Component, OnInit, ViewContainerRef, Input, TemplateRef, ViewChild, AfterViewChecked, Output, EventEmitter } from '@angular/core';
import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { PlatformGdprService } from './../../services/platform-gdpr.service';

@Component({
    selector: 'ns-modal-content',
    templateUrl: './modal-content.component.html',
    styleUrls: ['./modal-content.component.scss'],
    moduleId: module.id,
})

export class ModalContentComponent implements OnInit, AfterViewChecked {
    public template : TemplateRef<any>;
    public prompt: string;
    public gdprConfiguration :  any;


    @ViewChild('vcr', {read: ViewContainerRef, static: false }) vcr: ViewContainerRef;

    @Output() agreeEvent = new EventEmitter();

    constructor(private params: ModalDialogParams, private gdprService: PlatformGdprService) {
        this.prompt = params.context.promptMsg;
        this.template = params.context.template;
        this.gdprConfiguration = params.context.gdprConfiguration;
    }

    public disagree(result: string) {
        this.params.context.functions.cancel((result)=>{
            if (result) {
                this.params.closeCallback();
                this.gdprService.setGdprEvent(result);
            }
        })
    }

    public agree(result: string) {
        this.params.context.functions.confirm((result)=>{
            if (result) {
                this.params.closeCallback();
                this.gdprService.setGdprEvent(result);
            }
        })
    }



    ngAfterViewChecked() {
    }

    ngOnInit() {
        console.log('this.template', this.template);
        this.vcr.createEmbeddedView(this.template);
    }

}
