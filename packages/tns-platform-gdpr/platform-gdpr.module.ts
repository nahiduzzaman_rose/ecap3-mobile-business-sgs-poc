import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {PlatformGdprService} from "./services/platform-gdpr.service";
import { PlatformGdprDefaultComponent } from "./components/gdpr-default/gdpr-default.component";
import { ModalContentComponent } from './components/modal-content/modal-content.component';

@NgModule({
    providers: [
        PlatformGdprService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        PlatformGdprDefaultComponent,
        ModalContentComponent
    ],
    entryComponents: [
        ModalContentComponent
    ],
    exports: [
        PlatformGdprDefaultComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformGdprModule {}
