import {Observable} from "rxjs";
import {map} from 'rxjs/operators';

import {Injectable, OnInit, EventEmitter} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {TokenProvider, AuthGuard, EcapLoginService, AppSettingsProvider, UtilityService, PlatformDataService} from '@ecap3/tns-core';


@Injectable()
export class PlatformGdprService {
    header: any;

    personaConfig: any;
    notFoundStatus: number = 404;
    public gdprReloadControl: EventEmitter<any> = new EventEmitter<any>();
    public gdprEvent: EventEmitter<any> = new EventEmitter<any>();


    commonOptions: any = {headers: this.header, observe: 'response'};

    constructor(private http: HttpClient,
                public ecapLoginService: EcapLoginService,
                private tokenProvider: TokenProvider,
                private featureActivator: AuthGuard,
                private appSettingsProvider: AppSettingsProvider,
                private platformDataService: PlatformDataService) {

        this.header = new HttpHeaders({
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + this.tokenProvider.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
            
        }); 
    }
    
    public getAllGdprContent(): Observable<any> {
        console.log('getAllGdprContent');
        const fields = ['ItemId', 'TermAndConditionType', 'TermAndConditionContent', 'Language', 'PlatformType'];
        const sqlQuery = "select<" + fields.join(',') + "> from<GdprContent> where<PlatformType=__eql(Mobile)>";

        return this.platformDataService.getBySqlFilter('GdprContent', sqlQuery).pipe(map(response => {
            if (response['body'] && response['body'].ErrorMessages && !response['body'].ErrorMessages.length) {
                return response['body'].Results;
            } else {
                return null;
            }
        }));
    }

    public StoreGDPRdata(data: any): Observable<any> {

        const body = new HttpParams().set('grant_type', data.grant_type).set('context_id', data.context_id).set('accepted_terms_and_conditions', data.accepted_terms_and_conditions);
        const header = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });

        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), {
            headers: header,
            withCredentials: true
        }).pipe(map((response: any) => {
            
            this.tokenProvider.setRefreshToken(response.refresh_token);
            this.tokenProvider.setAccessToken(response.access_token);
            this.featureActivator.resetFeatures();
        
            return response;
        }, (error) => {
            console.log("ERROR FOUND");
            return error;
        }));
    }

    public reloadGdprContent(termsConditionDetails){
        this.gdprReloadControl.emit(termsConditionDetails);
    }

    public setGdprEvent(data) {
		this.gdprEvent.emit(data);
	}
}