"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var operators_1 = require("rxjs/operators");
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var tns_core_1 = require("@ecap3/tns-core");
var PlatformGdprService = /** @class */ (function () {
    function PlatformGdprService(http, ecapLoginService, tokenProvider, featureActivator, appSettingsProvider, platformDataService) {
        this.http = http;
        this.ecapLoginService = ecapLoginService;
        this.tokenProvider = tokenProvider;
        this.featureActivator = featureActivator;
        this.appSettingsProvider = appSettingsProvider;
        this.platformDataService = platformDataService;
        this.notFoundStatus = 404;
        this.gdprReloadControl = new core_1.EventEmitter();
        this.gdprEvent = new core_1.EventEmitter();
        this.commonOptions = { headers: this.header, observe: 'response' };
        this.header = new http_1.HttpHeaders({
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + this.tokenProvider.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
    }
    PlatformGdprService.prototype.getAllGdprContent = function () {
        console.log('getAllGdprContent');
        var fields = ['ItemId', 'TermAndConditionType', 'TermAndConditionContent', 'Language', 'PlatformType'];
        var sqlQuery = "select<" + fields.join(',') + "> from<GdprContent> where<PlatformType=__eql(Mobile)>";
        return this.platformDataService.getBySqlFilter('GdprContent', sqlQuery).pipe(operators_1.map(function (response) {
            if (response['body'] && response['body'].ErrorMessages && !response['body'].ErrorMessages.length) {
                return response['body'].Results;
            }
            else {
                return null;
            }
        }));
    };
    PlatformGdprService.prototype.StoreGDPRdata = function (data) {
        var _this = this;
        var body = new http_1.HttpParams().set('grant_type', data.grant_type).set('context_id', data.context_id).set('accepted_terms_and_conditions', data.accepted_terms_and_conditions);
        var header = new http_1.HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), {
            headers: header,
            withCredentials: true
        }).pipe(operators_1.map(function (response) {
            _this.tokenProvider.setRefreshToken(response.refresh_token);
            _this.tokenProvider.setAccessToken(response.access_token);
            _this.featureActivator.resetFeatures();
            return response;
        }, function (error) {
            console.log("ERROR FOUND");
            return error;
        }));
    };
    PlatformGdprService.prototype.reloadGdprContent = function (termsConditionDetails) {
        this.gdprReloadControl.emit(termsConditionDetails);
    };
    PlatformGdprService.prototype.setGdprEvent = function (data) {
        this.gdprEvent.emit(data);
    };
    PlatformGdprService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            tns_core_1.EcapLoginService,
            tns_core_1.TokenProvider,
            tns_core_1.AuthGuard,
            tns_core_1.AppSettingsProvider,
            tns_core_1.PlatformDataService])
    ], PlatformGdprService);
    return PlatformGdprService;
}());
exports.PlatformGdprService = PlatformGdprService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhdGZvcm0tZ2Rwci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGxhdGZvcm0tZ2Rwci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNENBQW1DO0FBRW5DLHNDQUErRDtBQUMvRCw2Q0FBeUU7QUFFekUsNENBQXFJO0FBSXJJO0lBV0ksNkJBQW9CLElBQWdCLEVBQ2pCLGdCQUFrQyxFQUNqQyxhQUE0QixFQUM1QixnQkFBMkIsRUFDM0IsbUJBQXdDLEVBQ3hDLG1CQUF3QztRQUx4QyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2pCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDakMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFXO1FBQzNCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVo1RCxtQkFBYyxHQUFXLEdBQUcsQ0FBQztRQUN0QixzQkFBaUIsR0FBc0IsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFDL0QsY0FBUyxHQUFzQixJQUFJLG1CQUFZLEVBQU8sQ0FBQztRQUc5RCxrQkFBYSxHQUFRLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDO1FBUzdELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzFCLGNBQWMsRUFBRSxrQkFBa0I7WUFDbEMsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFBRTtZQUNoRSxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU07U0FFN0QsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLCtDQUFpQixHQUF4QjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNqQyxJQUFNLE1BQU0sR0FBRyxDQUFDLFFBQVEsRUFBRSxzQkFBc0IsRUFBRSx5QkFBeUIsRUFBRSxVQUFVLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDekcsSUFBTSxRQUFRLEdBQUcsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsdURBQXVELENBQUM7UUFFeEcsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBRyxDQUFDLFVBQUEsUUFBUTtZQUNyRixJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsYUFBYSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7Z0JBQzlGLE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQzthQUNuQztpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQzthQUNmO1FBQ0wsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFTSwyQ0FBYSxHQUFwQixVQUFxQixJQUFTO1FBQTlCLGlCQXNCQztRQXBCRyxJQUFNLElBQUksR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFDN0ssSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzNCLGNBQWMsRUFBRSxtQ0FBbUM7WUFDbkQsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNO1NBQzdELENBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDcEYsT0FBTyxFQUFFLE1BQU07WUFDZixlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFDLFFBQWE7WUFFdEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzNELEtBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLENBQUM7WUFFdEMsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFFLFVBQUMsS0FBSztZQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDM0IsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFTSwrQ0FBaUIsR0FBeEIsVUFBeUIscUJBQXFCO1FBQzFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRU0sMENBQVksR0FBbkIsVUFBb0IsSUFBSTtRQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBdEVXLG1CQUFtQjtRQUQvQixpQkFBVSxFQUFFO3lDQVlpQixpQkFBVTtZQUNDLDJCQUFnQjtZQUNsQix3QkFBYTtZQUNWLG9CQUFTO1lBQ04sOEJBQW1CO1lBQ25CLDhCQUFtQjtPQWhCbkQsbUJBQW1CLENBdUUvQjtJQUFELDBCQUFDO0NBQUEsQUF2RUQsSUF1RUM7QUF2RVksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHtJbmplY3RhYmxlLCBPbkluaXQsIEV2ZW50RW1pdHRlcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXN9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuXHJcbmltcG9ydCB7VG9rZW5Qcm92aWRlciwgQXV0aEd1YXJkLCBFY2FwTG9naW5TZXJ2aWNlLCBBcHBTZXR0aW5nc1Byb3ZpZGVyLCBVdGlsaXR5U2VydmljZSwgUGxhdGZvcm1EYXRhU2VydmljZX0gZnJvbSAnQGVjYXAzL3Rucy1jb3JlJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBQbGF0Zm9ybUdkcHJTZXJ2aWNlIHtcclxuICAgIGhlYWRlcjogYW55O1xyXG5cclxuICAgIHBlcnNvbmFDb25maWc6IGFueTtcclxuICAgIG5vdEZvdW5kU3RhdHVzOiBudW1iZXIgPSA0MDQ7XHJcbiAgICBwdWJsaWMgZ2RwclJlbG9hZENvbnRyb2w6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBwdWJsaWMgZ2RwckV2ZW50OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuXHJcbiAgICBjb21tb25PcHRpb25zOiBhbnkgPSB7aGVhZGVyczogdGhpcy5oZWFkZXIsIG9ic2VydmU6ICdyZXNwb25zZSd9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBlY2FwTG9naW5TZXJ2aWNlOiBFY2FwTG9naW5TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB0b2tlblByb3ZpZGVyOiBUb2tlblByb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBmZWF0dXJlQWN0aXZhdG9yOiBBdXRoR3VhcmQsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFwcFNldHRpbmdzUHJvdmlkZXI6IEFwcFNldHRpbmdzUHJvdmlkZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHBsYXRmb3JtRGF0YVNlcnZpY2U6IFBsYXRmb3JtRGF0YVNlcnZpY2UpIHtcclxuXHJcbiAgICAgICAgdGhpcy5oZWFkZXIgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnYmVhcmVyICcgKyB0aGlzLnRva2VuUHJvdmlkZXIuZ2V0QWNjZXNzVG9rZW4oKSxcclxuICAgICAgICAgICAgJ09yaWdpbic6IHRoaXMuYXBwU2V0dGluZ3NQcm92aWRlci5nZXRBcHBTZXR0aW5ncygpLk9yaWdpblxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9KTsgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyBnZXRBbGxHZHByQ29udGVudCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdnZXRBbGxHZHByQ29udGVudCcpO1xyXG4gICAgICAgIGNvbnN0IGZpZWxkcyA9IFsnSXRlbUlkJywgJ1Rlcm1BbmRDb25kaXRpb25UeXBlJywgJ1Rlcm1BbmRDb25kaXRpb25Db250ZW50JywgJ0xhbmd1YWdlJywgJ1BsYXRmb3JtVHlwZSddO1xyXG4gICAgICAgIGNvbnN0IHNxbFF1ZXJ5ID0gXCJzZWxlY3Q8XCIgKyBmaWVsZHMuam9pbignLCcpICsgXCI+IGZyb208R2RwckNvbnRlbnQ+IHdoZXJlPFBsYXRmb3JtVHlwZT1fX2VxbChNb2JpbGUpPlwiO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5wbGF0Zm9ybURhdGFTZXJ2aWNlLmdldEJ5U3FsRmlsdGVyKCdHZHByQ29udGVudCcsIHNxbFF1ZXJ5KS5waXBlKG1hcChyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZVsnYm9keSddICYmIHJlc3BvbnNlWydib2R5J10uRXJyb3JNZXNzYWdlcyAmJiAhcmVzcG9uc2VbJ2JvZHknXS5FcnJvck1lc3NhZ2VzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlWydib2R5J10uUmVzdWx0cztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBTdG9yZUdEUFJkYXRhKGRhdGE6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcblxyXG4gICAgICAgIGNvbnN0IGJvZHkgPSBuZXcgSHR0cFBhcmFtcygpLnNldCgnZ3JhbnRfdHlwZScsIGRhdGEuZ3JhbnRfdHlwZSkuc2V0KCdjb250ZXh0X2lkJywgZGF0YS5jb250ZXh0X2lkKS5zZXQoJ2FjY2VwdGVkX3Rlcm1zX2FuZF9jb25kaXRpb25zJywgZGF0YS5hY2NlcHRlZF90ZXJtc19hbmRfY29uZGl0aW9ucyk7XHJcbiAgICAgICAgY29uc3QgaGVhZGVyID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLFxyXG4gICAgICAgICAgICAnT3JpZ2luJzogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5Ub2tlbiwgYm9keS50b1N0cmluZygpLCB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IGhlYWRlcixcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXHJcbiAgICAgICAgfSkucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMudG9rZW5Qcm92aWRlci5zZXRSZWZyZXNoVG9rZW4ocmVzcG9uc2UucmVmcmVzaF90b2tlbik7XHJcbiAgICAgICAgICAgIHRoaXMudG9rZW5Qcm92aWRlci5zZXRBY2Nlc3NUb2tlbihyZXNwb25zZS5hY2Nlc3NfdG9rZW4pO1xyXG4gICAgICAgICAgICB0aGlzLmZlYXR1cmVBY3RpdmF0b3IucmVzZXRGZWF0dXJlcygpO1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRVJST1IgRk9VTkRcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBlcnJvcjtcclxuICAgICAgICB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlbG9hZEdkcHJDb250ZW50KHRlcm1zQ29uZGl0aW9uRGV0YWlscyl7XHJcbiAgICAgICAgdGhpcy5nZHByUmVsb2FkQ29udHJvbC5lbWl0KHRlcm1zQ29uZGl0aW9uRGV0YWlscyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldEdkcHJFdmVudChkYXRhKSB7XHJcblx0XHR0aGlzLmdkcHJFdmVudC5lbWl0KGRhdGEpO1xyXG5cdH1cclxufSJdfQ==