"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("nativescript-angular/common");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var forms_2 = require("@angular/forms");
var angular_1 = require("nativescript-localize/angular");
var platform_gdpr_service_1 = require("./services/platform-gdpr.service");
var gdpr_default_component_1 = require("./components/gdpr-default/gdpr-default.component");
var modal_content_component_1 = require("./components/modal-content/modal-content.component");
var PlatformGdprModule = /** @class */ (function () {
    function PlatformGdprModule() {
    }
    PlatformGdprModule = __decorate([
        core_1.NgModule({
            providers: [
                platform_gdpr_service_1.PlatformGdprService
            ],
            imports: [
                forms_1.NativeScriptFormsModule,
                common_1.NativeScriptCommonModule,
                forms_2.ReactiveFormsModule,
                angular_1.NativeScriptLocalizeModule
            ],
            declarations: [
                gdpr_default_component_1.PlatformGdprDefaultComponent,
                modal_content_component_1.ModalContentComponent
            ],
            entryComponents: [
                modal_content_component_1.ModalContentComponent
            ],
            exports: [
                gdpr_default_component_1.PlatformGdprDefaultComponent
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], PlatformGdprModule);
    return PlatformGdprModule;
}());
exports.PlatformGdprModule = PlatformGdprModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhdGZvcm0tZ2Rwci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwbGF0Zm9ybS1nZHByLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNEQUFxRTtBQUNyRSxvREFBbUU7QUFDbkUsc0NBQXlEO0FBQ3pELHdDQUFtRDtBQUNuRCx5REFBeUU7QUFDekUsMEVBQXFFO0FBQ3JFLDJGQUFnRztBQUNoRyw4RkFBMkY7QUF3QjNGO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixrQkFBa0I7UUF0QjlCLGVBQVEsQ0FBQztZQUNOLFNBQVMsRUFBRTtnQkFDUCwyQ0FBbUI7YUFDdEI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsK0JBQXVCO2dCQUN2QixpQ0FBd0I7Z0JBQ3hCLDJCQUFtQjtnQkFDbkIsb0NBQTBCO2FBQzdCO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLHFEQUE0QjtnQkFDNUIsK0NBQXFCO2FBQ3hCO1lBQ0QsZUFBZSxFQUFFO2dCQUNiLCtDQUFxQjthQUN4QjtZQUNELE9BQU8sRUFBRTtnQkFDTCxxREFBNEI7YUFDL0I7WUFDRCxPQUFPLEVBQUUsQ0FBQyx1QkFBZ0IsQ0FBQztTQUM5QixDQUFDO09BQ1csa0JBQWtCLENBQUc7SUFBRCx5QkFBQztDQUFBLEFBQWxDLElBQWtDO0FBQXJCLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7TmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGV9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQge05nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1JlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHtOYXRpdmVTY3JpcHRMb2NhbGl6ZU1vZHVsZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1sb2NhbGl6ZS9hbmd1bGFyXCI7XHJcbmltcG9ydCB7UGxhdGZvcm1HZHByU2VydmljZX0gZnJvbSBcIi4vc2VydmljZXMvcGxhdGZvcm0tZ2Rwci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFBsYXRmb3JtR2RwckRlZmF1bHRDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL2dkcHItZGVmYXVsdC9nZHByLWRlZmF1bHQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1vZGFsQ29udGVudENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9tb2RhbC1jb250ZW50L21vZGFsLWNvbnRlbnQuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBQbGF0Zm9ybUdkcHJTZXJ2aWNlXHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcclxuICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdExvY2FsaXplTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgUGxhdGZvcm1HZHByRGVmYXVsdENvbXBvbmVudCxcclxuICAgICAgICBNb2RhbENvbnRlbnRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtcclxuICAgICAgICBNb2RhbENvbnRlbnRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgUGxhdGZvcm1HZHByRGVmYXVsdENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtOT19FUlJPUlNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGxhdGZvcm1HZHByTW9kdWxlIHt9XHJcbiJdfQ==