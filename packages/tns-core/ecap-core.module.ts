import { NgModule, ModuleWithProviders, APP_INITIALIZER, Inject } from "@angular/core";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { AppProvider } from "./service/provider/app.provider";
import { TokenProvider } from "./service/provider/token.provider";
import { FeatureProvider } from "./service/provider/feature.provider";
import { NavigationProvider } from "./service/provider/navigation.provider";
import { AppSettingsProvider } from "./service/provider/settings.provider";
import { AuthGuard } from "./service/auth/auth-guard.service";
import { UserInfoService } from "./service/auth/user-info.service";
import { EcapLoginService } from "./service/auth/ecap-login.service";
import { UtilityService } from "./service/utility/utility.service";
import { SpinnerService } from "./service/utility/spinner.service";
import { StorageDataService } from "./service/storage/storage-data.service";
import { PlatformDataService } from "./service/pds/platform-data.service";
import { SqlQueryBuilderService } from "./service/query-builder/sql-query-builder.service";
import { InterceptorModule } from "./service/interceptor/httpinterceptor.module";
import { TokenState } from "./service/interceptor/token.state";
import { LocalPlatformDataService } from "./service/local-pds/local-platform-data.service";
import { IfAndroidDirective, IfIosDirective } from "./directives/if-platform.directive";
import { SnackBarService } from "./service/utility/snackbar.service";
import { GqlQueryBuilderService } from "./service/query-builder/gql-query-builder.service";

export function StartupServiceFactory(http: HttpClient, tokenProvider: TokenProvider, appSettingsProvider: AppSettingsProvider) {
    return () => {
        if (appSettingsProvider.getAppSettings().IsMock) {
            console.log('is mock, no need to rebootstrap');
        } else {
            console.log('====== @Ecap3/StartupServiceFactory Called ========');
            return new Promise(function (resolve, reject) {
                if (!tokenProvider.getAccessToken() || !tokenProvider.getRefreshToken()) {
                    // const body = new HttpParams().set("grant_type", "client_credentials");
                    const body = new HttpParams().set("grant_type", "authenticate_site");
                    const header = new HttpHeaders({
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Origin": appSettingsProvider.getAppSettings().Origin
                    });
                    
                    http.post(
                        appSettingsProvider.getAppSettings().Token,
                        body.toString(),
                        {headers: header, observe: "response"}
                    )
                    .toPromise()
                    .then((response: any) => {
                        tokenProvider.setRefreshToken(response.body.refresh_token);
                        tokenProvider.setAccessToken(response.body.access_token);
                        resolve();
                    });
                    resolve();
                }
                else {
                    resolve();
                }
            });
        }
    };
}

@NgModule({
    imports: [
        NativeScriptCommonModule,
        InterceptorModule
    ], 
    declarations:[
        IfAndroidDirective,
        IfIosDirective
    ],
    exports: [
        IfAndroidDirective,
        IfIosDirective
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: StartupServiceFactory,
            deps: [
                HttpClient,
                TokenProvider,
                AppSettingsProvider
            ],
            multi: true
        }
    ]
})
export class EcapCoreModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: EcapCoreModule,
            providers: [
                AppProvider,
                TokenProvider,
                FeatureProvider,
                NavigationProvider,
                AppSettingsProvider,
                UtilityService,
                SpinnerService,
                SnackBarService,
                AuthGuard,
                UserInfoService,
                EcapLoginService,
                PlatformDataService,
                LocalPlatformDataService,
                SqlQueryBuilderService,
                StorageDataService,
                GqlQueryBuilderService,
                InterceptorModule,
                TokenState,
            ]
        };
    }
}
