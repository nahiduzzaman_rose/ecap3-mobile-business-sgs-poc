export class LocalStroageKeys {
    static APP_FEATURE_LIST: string = 'APP_FEATURE_LIST';
    static APP_SETTINGS: string = 'APP_SETTINGS';
    static ACCESS_TOKEN: string = 'ACCESS_TOKEN';
    static REFRESH_TOKEN: string = 'REFRESH_TOKEN';
    static USER_INFO: string = 'USER_INFO';
}