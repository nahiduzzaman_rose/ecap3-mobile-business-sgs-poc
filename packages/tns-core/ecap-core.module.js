"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var common_1 = require("nativescript-angular/common");
var app_provider_1 = require("./service/provider/app.provider");
var token_provider_1 = require("./service/provider/token.provider");
var feature_provider_1 = require("./service/provider/feature.provider");
var navigation_provider_1 = require("./service/provider/navigation.provider");
var settings_provider_1 = require("./service/provider/settings.provider");
var auth_guard_service_1 = require("./service/auth/auth-guard.service");
var user_info_service_1 = require("./service/auth/user-info.service");
var ecap_login_service_1 = require("./service/auth/ecap-login.service");
var utility_service_1 = require("./service/utility/utility.service");
var platform_data_service_1 = require("./service/pds/platform-data.service");
var sql_query_builder_service_1 = require("./service/query-builder/sql-query-builder.service");
var httpinterceptor_module_1 = require("./service/interceptor/httpinterceptor.module");
var token_state_1 = require("./service/interceptor/token.state");
function StartupServiceFactory(http, tokenProvider, appSettingsProvider) {
    return function () {
        if (appSettingsProvider.getAppSettings().IsMock) {
            console.log('is mock, no need to rebootstrap');
        }
        else {
            console.log('====== @Ecap3/StartupServiceFactory Called ========');
            return new Promise(function (resolve, reject) {
                if (!tokenProvider.getAccessToken() || !tokenProvider.getRefreshToken()) {
                    // const body = new HttpParams().set("grant_type", "client_credentials");
                    var body = new http_1.HttpParams().set("grant_type", "authenticate_site");
                    var header = new http_1.HttpHeaders({
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Origin": appSettingsProvider.getAppSettings().Origin
                    });
                    http.post(appSettingsProvider.getAppSettings().Token, body.toString(), { headers: header, observe: "response" })
                        .toPromise()
                        .then(function (response) {
                        tokenProvider.setRefreshToken(response.body.refresh_token);
                        tokenProvider.setAccessToken(response.body.access_token);
                        resolve();
                    });
                    resolve();
                }
                else {
                    resolve();
                }
            });
        }
    };
}
exports.StartupServiceFactory = StartupServiceFactory;
var EcapCoreModule = /** @class */ (function () {
    function EcapCoreModule() {
    }
    EcapCoreModule_1 = EcapCoreModule;
    EcapCoreModule.forRoot = function () {
        return {
            ngModule: EcapCoreModule_1,
            providers: [
                app_provider_1.AppProvider,
                token_provider_1.TokenProvider,
                feature_provider_1.FeatureProvider,
                navigation_provider_1.NavigationProvider,
                settings_provider_1.AppSettingsProvider,
                utility_service_1.UtilityService,
                auth_guard_service_1.AuthGuard,
                user_info_service_1.UserInfoService,
                ecap_login_service_1.EcapLoginService,
                platform_data_service_1.PlatformDataService,
                sql_query_builder_service_1.SqlQueryBuilderService,
                httpinterceptor_module_1.InterceptorModule,
                token_state_1.TokenState,
            ]
        };
    };
    var EcapCoreModule_1;
    EcapCoreModule = EcapCoreModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                httpinterceptor_module_1.InterceptorModule
            ],
            declarations: [],
            exports: [],
            providers: [
                {
                    provide: core_1.APP_INITIALIZER,
                    useFactory: StartupServiceFactory,
                    deps: [
                        http_1.HttpClient,
                        token_provider_1.TokenProvider,
                        settings_provider_1.AppSettingsProvider
                    ],
                    multi: true
                }
            ]
        })
    ], EcapCoreModule);
    return EcapCoreModule;
}());
exports.EcapCoreModule = EcapCoreModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNhcC1jb3JlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImVjYXAtY29yZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBdUY7QUFDdkYsNkNBQTJFO0FBQzNFLHNEQUF1RTtBQUV2RSxnRUFBOEQ7QUFDOUQsb0VBQWtFO0FBQ2xFLHdFQUFzRTtBQUN0RSw4RUFBNEU7QUFDNUUsMEVBQTJFO0FBQzNFLHdFQUE4RDtBQUM5RCxzRUFBbUU7QUFDbkUsd0VBQXFFO0FBQ3JFLHFFQUFtRTtBQUNuRSw2RUFBMEU7QUFDMUUsK0ZBQTJGO0FBQzNGLHVGQUFpRjtBQUNqRixpRUFBK0Q7QUFHL0QsU0FBZ0IscUJBQXFCLENBQUMsSUFBZ0IsRUFBRSxhQUE0QixFQUFFLG1CQUF3QztJQUMxSCxPQUFPO1FBQ0gsSUFBSSxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1NBQ2xEO2FBQU07WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7WUFDbkUsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFVLE9BQU8sRUFBRSxNQUFNO2dCQUN4QyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxFQUFFO29CQUNyRSx5RUFBeUU7b0JBQ3pFLElBQU0sSUFBSSxHQUFHLElBQUksaUJBQVUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDckUsSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO3dCQUMzQixjQUFjLEVBQUUsbUNBQW1DO3dCQUNuRCxRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTTtxQkFDeEQsQ0FBQyxDQUFDO29CQUVILElBQUksQ0FBQyxJQUFJLENBQ0wsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsS0FBSyxFQUMxQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQ2YsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FDekM7eUJBQ0EsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxVQUFDLFFBQWE7d0JBQ2hCLGFBQWEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDM0QsYUFBYSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN6RCxPQUFPLEVBQUUsQ0FBQztvQkFDZCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLEVBQUUsQ0FBQztpQkFDYjtxQkFDSTtvQkFDRCxPQUFPLEVBQUUsQ0FBQztpQkFDYjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDLENBQUM7QUFDTixDQUFDO0FBbENELHNEQWtDQztBQXNCRDtJQUFBO0lBc0JBLENBQUM7dUJBdEJZLGNBQWM7SUFDVCxzQkFBTyxHQUFyQjtRQUNJLE9BQU87WUFDSCxRQUFRLEVBQUUsZ0JBQWM7WUFDeEIsU0FBUyxFQUFFO2dCQUNQLDBCQUFXO2dCQUNYLDhCQUFhO2dCQUNiLGtDQUFlO2dCQUNmLHdDQUFrQjtnQkFDbEIsdUNBQW1CO2dCQUNuQixnQ0FBYztnQkFDZCw4QkFBUztnQkFDVCxtQ0FBZTtnQkFDZixxQ0FBZ0I7Z0JBQ2hCLDJDQUFtQjtnQkFDbkIsa0RBQXNCO2dCQUV0QiwwQ0FBaUI7Z0JBQ2pCLHdCQUFVO2FBQ2I7U0FDSixDQUFDO0lBQ04sQ0FBQzs7SUFyQlEsY0FBYztRQXBCMUIsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLGlDQUF3QjtnQkFDeEIsMENBQWlCO2FBQ3BCO1lBQ0QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFLEVBQUU7WUFDWCxTQUFTLEVBQUU7Z0JBQ1A7b0JBQ0ksT0FBTyxFQUFFLHNCQUFlO29CQUN4QixVQUFVLEVBQUUscUJBQXFCO29CQUNqQyxJQUFJLEVBQUU7d0JBQ0YsaUJBQVU7d0JBQ1YsOEJBQWE7d0JBQ2IsdUNBQW1CO3FCQUN0QjtvQkFDRCxLQUFLLEVBQUUsSUFBSTtpQkFDZDthQUNKO1NBQ0osQ0FBQztPQUNXLGNBQWMsQ0FzQjFCO0lBQUQscUJBQUM7Q0FBQSxBQXRCRCxJQXNCQztBQXRCWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBBUFBfSU5JVElBTElaRVIsIEluamVjdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEh0dHBIZWFkZXJzLCBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcclxuXHJcbmltcG9ydCB7IEFwcFByb3ZpZGVyIH0gZnJvbSBcIi4vc2VydmljZS9wcm92aWRlci9hcHAucHJvdmlkZXJcIjtcclxuaW1wb3J0IHsgVG9rZW5Qcm92aWRlciB9IGZyb20gXCIuL3NlcnZpY2UvcHJvdmlkZXIvdG9rZW4ucHJvdmlkZXJcIjtcclxuaW1wb3J0IHsgRmVhdHVyZVByb3ZpZGVyIH0gZnJvbSBcIi4vc2VydmljZS9wcm92aWRlci9mZWF0dXJlLnByb3ZpZGVyXCI7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25Qcm92aWRlciB9IGZyb20gXCIuL3NlcnZpY2UvcHJvdmlkZXIvbmF2aWdhdGlvbi5wcm92aWRlclwiO1xyXG5pbXBvcnQgeyBBcHBTZXR0aW5nc1Byb3ZpZGVyIH0gZnJvbSBcIi4vc2VydmljZS9wcm92aWRlci9zZXR0aW5ncy5wcm92aWRlclwiO1xyXG5pbXBvcnQgeyBBdXRoR3VhcmQgfSBmcm9tIFwiLi9zZXJ2aWNlL2F1dGgvYXV0aC1ndWFyZC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFVzZXJJbmZvU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvYXV0aC91c2VyLWluZm8uc2VydmljZVwiO1xyXG5pbXBvcnQgeyBFY2FwTG9naW5TZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9hdXRoL2VjYXAtbG9naW4uc2VydmljZVwiO1xyXG5pbXBvcnQgeyBVdGlsaXR5U2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvdXRpbGl0eS91dGlsaXR5LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUGxhdGZvcm1EYXRhU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvcGRzL3BsYXRmb3JtLWRhdGEuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTcWxRdWVyeUJ1aWxkZXJTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9xdWVyeS1idWlsZGVyL3NxbC1xdWVyeS1idWlsZGVyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgSW50ZXJjZXB0b3JNb2R1bGUgfSBmcm9tIFwiLi9zZXJ2aWNlL2ludGVyY2VwdG9yL2h0dHBpbnRlcmNlcHRvci5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVG9rZW5TdGF0ZSB9IGZyb20gXCIuL3NlcnZpY2UvaW50ZXJjZXB0b3IvdG9rZW4uc3RhdGVcIjtcclxuXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gU3RhcnR1cFNlcnZpY2VGYWN0b3J5KGh0dHA6IEh0dHBDbGllbnQsIHRva2VuUHJvdmlkZXI6IFRva2VuUHJvdmlkZXIsIGFwcFNldHRpbmdzUHJvdmlkZXI6IEFwcFNldHRpbmdzUHJvdmlkZXIpIHtcclxuICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgaWYgKGFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5Jc01vY2spIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2lzIG1vY2ssIG5vIG5lZWQgdG8gcmVib290c3RyYXAnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnPT09PT09IEBFY2FwMy9TdGFydHVwU2VydmljZUZhY3RvcnkgQ2FsbGVkID09PT09PT09Jyk7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRva2VuUHJvdmlkZXIuZ2V0QWNjZXNzVG9rZW4oKSB8fCAhdG9rZW5Qcm92aWRlci5nZXRSZWZyZXNoVG9rZW4oKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnN0IGJvZHkgPSBuZXcgSHR0cFBhcmFtcygpLnNldChcImdyYW50X3R5cGVcIiwgXCJjbGllbnRfY3JlZGVudGlhbHNcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYm9keSA9IG5ldyBIdHRwUGFyYW1zKCkuc2V0KFwiZ3JhbnRfdHlwZVwiLCBcImF1dGhlbnRpY2F0ZV9zaXRlXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGhlYWRlciA9IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiT3JpZ2luXCI6IGFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5PcmlnaW5cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICBodHRwLnBvc3QoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5Ub2tlbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgYm9keS50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7aGVhZGVyczogaGVhZGVyLCBvYnNlcnZlOiBcInJlc3BvbnNlXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRva2VuUHJvdmlkZXIuc2V0UmVmcmVzaFRva2VuKHJlc3BvbnNlLmJvZHkucmVmcmVzaF90b2tlbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRva2VuUHJvdmlkZXIuc2V0QWNjZXNzVG9rZW4ocmVzcG9uc2UuYm9keS5hY2Nlc3NfdG9rZW4pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcclxuICAgICAgICBJbnRlcmNlcHRvck1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW10sXHJcbiAgICBleHBvcnRzOiBbXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHJvdmlkZTogQVBQX0lOSVRJQUxJWkVSLFxyXG4gICAgICAgICAgICB1c2VGYWN0b3J5OiBTdGFydHVwU2VydmljZUZhY3RvcnksXHJcbiAgICAgICAgICAgIGRlcHM6IFtcclxuICAgICAgICAgICAgICAgIEh0dHBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBUb2tlblByb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgQXBwU2V0dGluZ3NQcm92aWRlclxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEVjYXBDb3JlTW9kdWxlIHtcclxuICAgIHB1YmxpYyBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBuZ01vZHVsZTogRWNhcENvcmVNb2R1bGUsXHJcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgICAgICAgICAgQXBwUHJvdmlkZXIsXHJcbiAgICAgICAgICAgICAgICBUb2tlblByb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgRmVhdHVyZVByb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgTmF2aWdhdGlvblByb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgQXBwU2V0dGluZ3NQcm92aWRlcixcclxuICAgICAgICAgICAgICAgIFV0aWxpdHlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgQXV0aEd1YXJkLFxyXG4gICAgICAgICAgICAgICAgVXNlckluZm9TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgRWNhcExvZ2luU2VydmljZSxcclxuICAgICAgICAgICAgICAgIFBsYXRmb3JtRGF0YVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBTcWxRdWVyeUJ1aWxkZXJTZXJ2aWNlLFxyXG5cclxuICAgICAgICAgICAgICAgIEludGVyY2VwdG9yTW9kdWxlLFxyXG4gICAgICAgICAgICAgICAgVG9rZW5TdGF0ZSxcclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cclxuIl19