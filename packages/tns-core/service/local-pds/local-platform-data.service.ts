import { Injectable } from '@angular/core';
import { AppSettingsProvider } from '../provider/settings.provider';
import {Couchbase, Query, QueryLogicalOperator, QueryWhereItem} from 'nativescript-couchbase-plugin';
import {Observable, of} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class LocalPlatformDataService {
    public database: Couchbase;
    public dbName: string;
    
    constructor() {}

    initDatabase(dbName : string) {
        this.dbName = dbName;
        this.database = new Couchbase(dbName);
    }

    getDatabase() {
        return this.database;
    }

    getDatabaseInfo() {
        return {
            dbName: this.dbName
        }
    }

    insert(data: any) : Observable<any> {
        // Check if document already exists, if Yes then update the document
        const existingDocument = this.database.getDocument(data.ItemId);
        if (existingDocument) {
            // Use existing SyncSetting
            if (existingDocument.SyncSetting) {
                data.SyncSetting = existingDocument.SyncSetting;
            }
            console.log('Update/Insert document');
            return of(this.database.updateDocument(data.ItemId, data));

        } else {
            // New Document
            console.log('Insert document');
            return of(this.database.createDocument(data, data.ItemId || null));
        }
    }

    update(documentId: string, data: any, skipUpdateDate = false): Observable<any> {
        return of(this.database.updateDocument(documentId, data));
    }

    delete(documentId: string) {
        return this.database.deleteDocument(documentId);
    }

    batchInsert(data: any[], entityName: string) : Observable<any>  {
        const resultIds = [];
        this.database.inBatch(() => {
            if(data && data.length > 0) {
                data.forEach((item) => {
                    if (entityName) {
                        item.EntityName = entityName;
                    }
                    // Check if document already exists
                    const existingDocument = this.database.getDocument(item.ItemId);
                    if (existingDocument) {
                        if (existingDocument.SyncSetting) {
                            item.SyncSetting = existingDocument.SyncSetting;
                        }
                        resultIds.push(
                            this.database.updateDocument(item.ItemId, item)
                        );
                    } else {
                        // New Document
                        resultIds.push(
                            this.database.createDocument(item, item.ItemId || null)
                        );
                    }
                })
            }
        });
        console.log('Batch Insert documents', resultIds);
        return of(resultIds);
    }

    batchUpdate(data: any[], entityName: string, skipUpdateDate = false) : Observable<any>  {
        const resultIds = [];
        this.database.inBatch(() => {
            if(data && data.length > 0) {
                data.forEach((item) => {
                    resultIds.push(
                        this.database.updateDocument(item.ItemId, item )
                    );
                })
            }
        });
        console.log('Batch update documents', resultIds);
        return of(resultIds);
    }

    getById(documentId: string) {
        return this.database.getDocument(documentId);
    }

    getBySqlFilter(query: Query) {
        /*
            QueryComparisonOperator = 'modulo' | 'is' | 'between' | 'isNot' | 'collate' | 'in' | 'add' | 'isNullOrMissing' |
            'greaterThan' | 'divide' | 'notEqualTo' | 'greaterThanOrEqualTo' | 'like' | 'subtract' | 'lessThanOrEqualTo' | 'lessThan'
            | 'notNullOrMissing' | 'regex' | 'equalTo' | 'multiply';
        */
        query['from'] = this.dbName;
        return this.database.query(query);
    }

    getPaginateData(query: Query) {
        let results = null;
        let totalRecordCount = 0;
        if (query['limit']) {
            // Get total record count
            const queryCount: Query = {
                select: [],
                where: query['where']
            };

            const resultsAll = this.database.query(queryCount);
            totalRecordCount = resultsAll.length;
            console.log('totalRecordCount', totalRecordCount);
            // Get Paginate data
            query['from'] = this.dbName;
            results = this.database.query(query);
        } else {
            query['from'] = this.dbName;
            results = this.database.query(query);
            totalRecordCount = results.length;
        }

        return {
            StatusCode: 0,
            ErrorMessages: [],
            TotalRecordCount: totalRecordCount,
            Results: results ? results : []
        };
    }

    clearData() {
        const query: Query = {
            select: [],
            from: this.dbName
            //where: filter
        };

        const results = this.database.query(query);
        if (results && results.length > 0) {
            results.forEach(x => {
                this.database.deleteDocument(x.id);
            })
        }

        console.log('All Data Cleared !');
    }

    dropDataBase() {
        this.database.destroyDatabase();
        console.log('Database dropped !');
    }

    chunkArray(array, size) {
        if (!array) return [];
        const firstChunk = array.slice(0, size); // create the first chunk of the given array
        if (!firstChunk.length) {
            return array; // this is the base case to terminal the recursive
        }
        return [firstChunk].concat(this.chunkArray(array.slice(size, array.length), size));
    }
    
    getGuid() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0, v = c === "x" ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
