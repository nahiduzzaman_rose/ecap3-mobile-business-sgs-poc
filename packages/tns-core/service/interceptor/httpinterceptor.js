"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/common/http");
var feature_provider_1 = require("../provider/feature.provider");
var token_provider_1 = require("../provider/token.provider");
var token_state_1 = require("./token.state");
var settings_provider_1 = require("../provider/settings.provider");
var HttpsRequestInterceptor = /** @class */ (function () {
    function HttpsRequestInterceptor(tokenProvider, injector, tokenState, appSettingsProvider) {
        this.tokenProvider = tokenProvider;
        this.injector = injector;
        this.tokenState = tokenState;
        this.appSettingsProvider = appSettingsProvider;
        console.log('=====HttpsRequestInterceptor INITIALIZED=====');
        this.gettingAcessToken = false;
        this.tokenStateObservable = this.tokenState.getTokenStateObservable();
    }
    HttpsRequestInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        var featureProvider = this.injector.get(feature_provider_1.FeatureProvider);
        console.log('intercepted for request');
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + this.tokenProvider.getAccessToken(),
                'Origin': this.appSettingsProvider.getAppSettings().Origin
            }
        });
        return next.handle(req).pipe(operators_1.catchError(function (err, caught) {
            console.log('Intercept got error');
            if (err.url && err.url.toLowerCase() === _this.appSettingsProvider.getAppSettings().Token.toLowerCase() && err.status === 400) {
                switch (err.error.error) {
                    case "term_and_condition_acceptance_require":
                        return rxjs_1.throwError(err);
                        break;
                    case "two_factor_code_require":
                        break;
                    case "incorrect_user_name_or_password":
                        break;
                    case "Session_Lock_Id":
                        break;
                    case "persona_require":
                        break;
                    default:
                        break;
                }
                console.log('ITS TIME TO LOGOUT FROM CLIENT');
                var that_1 = _this;
                _this.tokenProvider.getAnnonymousToken().then(function (response) {
                    that_1.tokenProvider.setAccessToken(response.access_token);
                    that_1.tokenProvider.setRefreshToken(response.refresh_token);
                    featureProvider.resetFeatures();
                    _this.tokenStateObservable.next('400_RECOVERED');
                    var router = _this.injector.get(router_1.Router);
                    router.navigate(['/']);
                });
            }
            else if (err.status === 401) {
                console.log('Intercept ERROR Code 401');
                return _this.tokenProvider.refreshTokenObservable().pipe(operators_1.switchMap(function (newToken) {
                    var httpHeaders = new http_1.HttpHeaders()
                        .set('Authorization', "Bearer " + _this.tokenProvider.getAccessToken())
                        .set('Origin', _this.appSettingsProvider.getAppSettings().Origin);
                    var authReq = req.clone({ headers: httpHeaders });
                    _this.tokenStateObservable.next('401_RECOVERED');
                    return next.handle(authReq);
                }));
            }
            else {
                console.log('faced unknown error, aborting bootstraping process...');
                return rxjs_1.throwError(err);
            }
        }));
    };
    HttpsRequestInterceptor.prototype.renewAccessToken = function (next, req) {
        next.handle(req);
    };
    HttpsRequestInterceptor = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [token_provider_1.TokenProvider, core_1.Injector, token_state_1.TokenState, settings_provider_1.AppSettingsProvider])
    ], HttpsRequestInterceptor);
    return HttpsRequestInterceptor;
}());
exports.HttpsRequestInterceptor = HttpsRequestInterceptor;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cGludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaHR0cGludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNkJBQTZEO0FBQzdELDRDQUFxRDtBQUNyRCxzQ0FBbUQ7QUFDbkQsMENBQXVDO0FBQ3ZDLDZDQUF1RztBQUV2RyxpRUFBNkQ7QUFDN0QsNkRBQXlEO0FBQ3pELDZDQUF5QztBQUN6QyxtRUFBa0U7QUFJbEU7SUFRSSxpQ0FBb0IsYUFBNEIsRUFBVSxRQUFrQixFQUFVLFVBQXNCLEVBQVUsbUJBQXdDO1FBQTFJLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUFVLGVBQVUsR0FBVixVQUFVLENBQVk7UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQzFKLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0NBQStDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDMUUsQ0FBQztJQUVNLDJDQUFTLEdBQWhCLFVBQWlCLEdBQXFCLEVBQUUsSUFBaUI7UUFBekQsaUJBMERDO1FBekRHLElBQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGtDQUFlLENBQUMsQ0FBQztRQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFFdkMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDWixVQUFVLEVBQUU7Z0JBQ1IsYUFBYSxFQUFFLFlBQVUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUk7Z0JBQzlELFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTTthQUM3RDtTQUNKLENBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQVUsQ0FBQyxVQUFDLEdBQUcsRUFBRSxNQUFrQztZQUM1RSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDbkMsSUFBSSxHQUFHLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssS0FBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtnQkFDMUgsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQkFDckIsS0FBSyx1Q0FBdUM7d0JBQ3hDLE9BQU8saUJBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDdkIsTUFBTTtvQkFDVixLQUFLLHlCQUF5Qjt3QkFDMUIsTUFBTTtvQkFDVixLQUFLLGlDQUFpQzt3QkFDbEMsTUFBTTtvQkFDVixLQUFLLGlCQUFpQjt3QkFDbEIsTUFBTTtvQkFDVixLQUFLLGlCQUFpQjt3QkFDbEIsTUFBTTtvQkFDVjt3QkFDSSxNQUFNO2lCQUNiO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztnQkFDOUMsSUFBTSxNQUFJLEdBQUcsS0FBSSxDQUFDO2dCQUNsQixLQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBYTtvQkFDdkQsTUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUN6RCxNQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzNELGVBQWUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDaEMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDaEQsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZUFBTSxDQUFDLENBQUM7b0JBQ3pDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixFQUFFLENBQUMsSUFBSSxDQUNuRCxxQkFBUyxDQUFDLFVBQUMsUUFBYTtvQkFFcEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxrQkFBVyxFQUFFO3lCQUNsQyxHQUFHLENBQUMsZUFBZSxFQUFFLFlBQVUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUksQ0FBQzt5QkFDckUsR0FBRyxDQUFDLFFBQVEsRUFBRSxLQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2pFLElBQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFFcEQsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDaEQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNoQyxDQUFDLENBQUMsQ0FDTCxDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1REFBdUQsQ0FBQyxDQUFDO2dCQUNyRSxPQUFPLGlCQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDMUI7UUFDTCxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQ1QsQ0FBQztJQUVNLGtEQUFnQixHQUF2QixVQUF3QixJQUFJLEVBQUUsR0FBRztRQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUE1RVEsdUJBQXVCO1FBRG5DLGlCQUFVLEVBQUU7eUNBUzBCLDhCQUFhLEVBQW9CLGVBQVEsRUFBc0Isd0JBQVUsRUFBK0IsdUNBQW1CO09BUnJKLHVCQUF1QixDQTZFbkM7SUFBRCw4QkFBQztDQUFBLEFBN0VELElBNkVDO0FBN0VZLDBEQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0LCB0aHJvd0Vycm9yfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHtjYXRjaEVycm9yLCBzd2l0Y2hNYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHtJbmplY3RhYmxlLCBJbmplY3Rvcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQge0h0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yLCBIdHRwSGFuZGxlciwgSHR0cFJlcXVlc3QsIEh0dHBIZWFkZXJzfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcblxyXG5pbXBvcnQge0ZlYXR1cmVQcm92aWRlcn0gZnJvbSAnLi4vcHJvdmlkZXIvZmVhdHVyZS5wcm92aWRlcic7XHJcbmltcG9ydCB7VG9rZW5Qcm92aWRlcn0gZnJvbSAnLi4vcHJvdmlkZXIvdG9rZW4ucHJvdmlkZXInO1xyXG5pbXBvcnQge1Rva2VuU3RhdGV9IGZyb20gJy4vdG9rZW4uc3RhdGUnO1xyXG5pbXBvcnQge0FwcFNldHRpbmdzUHJvdmlkZXJ9IGZyb20gJy4uL3Byb3ZpZGVyL3NldHRpbmdzLnByb3ZpZGVyJztcclxuaW1wb3J0IHsgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS9zcmMvcmVuZGVyMyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBIdHRwc1JlcXVlc3RJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBodHRwOiBhbnk7XHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyO1xyXG4gICAgcHJpdmF0ZSBnZXR0aW5nQWNlc3NUb2tlbjogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgZmFpbGVkUmVxU3RhY2s6IGFueSBbXTtcclxuICAgIHByaXZhdGUgdG9rZW5TdGF0ZU9ic2VydmFibGU6IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdG9rZW5Qcm92aWRlcjogVG9rZW5Qcm92aWRlciwgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsIHByaXZhdGUgdG9rZW5TdGF0ZTogVG9rZW5TdGF0ZSwgcHJpdmF0ZSBhcHBTZXR0aW5nc1Byb3ZpZGVyOiBBcHBTZXR0aW5nc1Byb3ZpZGVyKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJz09PT09SHR0cHNSZXF1ZXN0SW50ZXJjZXB0b3IgSU5JVElBTElaRUQ9PT09PScpO1xyXG4gICAgICAgIHRoaXMuZ2V0dGluZ0FjZXNzVG9rZW4gPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnRva2VuU3RhdGVPYnNlcnZhYmxlID0gdGhpcy50b2tlblN0YXRlLmdldFRva2VuU3RhdGVPYnNlcnZhYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgICAgIGNvbnN0IGZlYXR1cmVQcm92aWRlciA9IHRoaXMuaW5qZWN0b3IuZ2V0KEZlYXR1cmVQcm92aWRlcik7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ2ludGVyY2VwdGVkIGZvciByZXF1ZXN0Jyk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgcmVxID0gcmVxLmNsb25lKHtcclxuICAgICAgICAgICAgc2V0SGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3RoaXMudG9rZW5Qcm92aWRlci5nZXRBY2Nlc3NUb2tlbigpfWAsXHJcbiAgICAgICAgICAgICAgICAnT3JpZ2luJzogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcSkucGlwZShjYXRjaEVycm9yKChlcnIsIGNhdWdodDogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4pID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0ludGVyY2VwdCBnb3QgZXJyb3InKTtcclxuICAgICAgICAgICAgaWYgKGVyci51cmwgJiYgZXJyLnVybC50b0xvd2VyQ2FzZSgpID09PSB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5Ub2tlbi50b0xvd2VyQ2FzZSgpICYmIGVyci5zdGF0dXMgPT09IDQwMCkge1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChlcnIuZXJyb3IuZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwidGVybV9hbmRfY29uZGl0aW9uX2FjY2VwdGFuY2VfcmVxdWlyZVwiOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwidHdvX2ZhY3Rvcl9jb2RlX3JlcXVpcmVcIjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcImluY29ycmVjdF91c2VyX25hbWVfb3JfcGFzc3dvcmRcIjpcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIlNlc3Npb25fTG9ja19JZFwiOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwicGVyc29uYV9yZXF1aXJlXCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0lUUyBUSU1FIFRPIExPR09VVCBGUk9NIENMSUVOVCcpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRva2VuUHJvdmlkZXIuZ2V0QW5ub255bW91c1Rva2VuKCkudGhlbigocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoYXQudG9rZW5Qcm92aWRlci5zZXRBY2Nlc3NUb2tlbihyZXNwb25zZS5hY2Nlc3NfdG9rZW4pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoYXQudG9rZW5Qcm92aWRlci5zZXRSZWZyZXNoVG9rZW4ocmVzcG9uc2UucmVmcmVzaF90b2tlbik7XHJcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVByb3ZpZGVyLnJlc2V0RmVhdHVyZXMoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRva2VuU3RhdGVPYnNlcnZhYmxlLm5leHQoJzQwMF9SRUNPVkVSRUQnKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByb3V0ZXIgPSB0aGlzLmluamVjdG9yLmdldChSb3V0ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChlcnIuc3RhdHVzID09PSA0MDEpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdJbnRlcmNlcHQgRVJST1IgQ29kZSA0MDEnKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRva2VuUHJvdmlkZXIucmVmcmVzaFRva2VuT2JzZXJ2YWJsZSgpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoTWFwKChuZXdUb2tlbjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgaHR0cEhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2V0KCdBdXRob3JpemF0aW9uJywgYEJlYXJlciAke3RoaXMudG9rZW5Qcm92aWRlci5nZXRBY2Nlc3NUb2tlbigpfWApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zZXQoJ09yaWdpbicsIHRoaXMuYXBwU2V0dGluZ3NQcm92aWRlci5nZXRBcHBTZXR0aW5ncygpLk9yaWdpbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGF1dGhSZXEgPSByZXEuY2xvbmUoeyBoZWFkZXJzOiBodHRwSGVhZGVycyB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9rZW5TdGF0ZU9ic2VydmFibGUubmV4dCgnNDAxX1JFQ09WRVJFRCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUoYXV0aFJlcSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZmFjZWQgdW5rbm93biBlcnJvciwgYWJvcnRpbmcgYm9vdHN0cmFwaW5nIHByb2Nlc3MuLi4nKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KSwpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZW5ld0FjY2Vzc1Rva2VuKG5leHQsIHJlcSkge1xyXG4gICAgICAgIG5leHQuaGFuZGxlKHJlcSk7XHJcbiAgICB9XHJcbn1cclxuIl19