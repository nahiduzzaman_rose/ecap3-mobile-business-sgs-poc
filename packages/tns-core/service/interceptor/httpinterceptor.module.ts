import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {Injectable, NgModule} from '@angular/core';

import {HttpsRequestInterceptor} from './httpinterceptor';

@Injectable()
@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpsRequestInterceptor,
            multi: true
        }
    ]
})
export class InterceptorModule {
}
