"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var TokenState = /** @class */ (function () {
    function TokenState() {
        this.tokenStateObservable = new rxjs_1.BehaviorSubject('initial');
    }
    TokenState.prototype.getTokenStateObservable = function () {
        return this.tokenStateObservable;
    };
    TokenState.prototype.setTokenStateObservable = function (state) {
        this.tokenStateObservable.next(state);
    };
    TokenState = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], TokenState);
    return TokenState;
}());
exports.TokenState = TokenState;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9rZW4uc3RhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0b2tlbi5zdGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5QztBQUN6Qyw2QkFBaUQ7QUFHakQ7SUFHSTtRQUNJLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLHNCQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVNLDRDQUF1QixHQUE5QjtRQUNJLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDO0lBQ3JDLENBQUM7SUFFTSw0Q0FBdUIsR0FBOUIsVUFBK0IsS0FBSztRQUNoQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFiUSxVQUFVO1FBRHRCLGlCQUFVLEVBQUU7O09BQ0EsVUFBVSxDQWN0QjtJQUFELGlCQUFDO0NBQUEsQUFkRCxJQWNDO0FBZFksZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge09ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBUb2tlblN0YXRlIHtcclxuICAgIHRva2VuU3RhdGVPYnNlcnZhYmxlOiBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLnRva2VuU3RhdGVPYnNlcnZhYmxlID0gbmV3IEJlaGF2aW9yU3ViamVjdCgnaW5pdGlhbCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRUb2tlblN0YXRlT2JzZXJ2YWJsZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50b2tlblN0YXRlT2JzZXJ2YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0VG9rZW5TdGF0ZU9ic2VydmFibGUoc3RhdGUpIHtcclxuICAgICAgICB0aGlzLnRva2VuU3RhdGVPYnNlcnZhYmxlLm5leHQoc3RhdGUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==