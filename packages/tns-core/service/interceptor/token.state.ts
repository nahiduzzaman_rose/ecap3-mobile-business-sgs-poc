import {Injectable} from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';

@Injectable()
export class TokenState {
    tokenStateObservable: BehaviorSubject<string>;

    constructor() {
        this.tokenStateObservable = new BehaviorSubject('initial');
    }

    public getTokenStateObservable() {
        return this.tokenStateObservable;
    }

    public setTokenStateObservable(state) {
        this.tokenStateObservable.next(state);
    }
}
