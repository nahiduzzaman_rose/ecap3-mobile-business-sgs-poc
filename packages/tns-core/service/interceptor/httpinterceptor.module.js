"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var httpinterceptor_1 = require("./httpinterceptor");
var InterceptorModule = /** @class */ (function () {
    function InterceptorModule() {
    }
    InterceptorModule = __decorate([
        core_1.Injectable(),
        core_1.NgModule({
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: httpinterceptor_1.HttpsRequestInterceptor,
                    multi: true
                }
            ]
        })
    ], InterceptorModule);
    return InterceptorModule;
}());
exports.InterceptorModule = InterceptorModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cGludGVyY2VwdG9yLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImh0dHBpbnRlcmNlcHRvci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2Q0FBdUQ7QUFDdkQsc0NBQW1EO0FBRW5ELHFEQUEwRDtBQVkxRDtJQUFBO0lBQ0EsQ0FBQztJQURZLGlCQUFpQjtRQVY3QixpQkFBVSxFQUFFO1FBQ1osZUFBUSxDQUFDO1lBQ04sU0FBUyxFQUFFO2dCQUNQO29CQUNJLE9BQU8sRUFBRSx3QkFBaUI7b0JBQzFCLFFBQVEsRUFBRSx5Q0FBdUI7b0JBQ2pDLEtBQUssRUFBRSxJQUFJO2lCQUNkO2FBQ0o7U0FDSixDQUFDO09BQ1csaUJBQWlCLENBQzdCO0lBQUQsd0JBQUM7Q0FBQSxBQURELElBQ0M7QUFEWSw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0hUVFBfSU5URVJDRVBUT1JTfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7SW5qZWN0YWJsZSwgTmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHtIdHRwc1JlcXVlc3RJbnRlcmNlcHRvcn0gZnJvbSAnLi9odHRwaW50ZXJjZXB0b3InO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5ATmdNb2R1bGUoe1xyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcclxuICAgICAgICAgICAgdXNlQ2xhc3M6IEh0dHBzUmVxdWVzdEludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICBtdWx0aTogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEludGVyY2VwdG9yTW9kdWxlIHtcclxufVxyXG4iXX0=