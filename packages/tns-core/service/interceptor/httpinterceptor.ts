import {Observable, BehaviorSubject, throwError} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';

import {FeatureProvider} from '../provider/feature.provider';
import {TokenProvider} from '../provider/token.provider';
import {TokenState} from './token.state';
import {AppSettingsProvider} from '../provider/settings.provider';
import localize from 'nativescript-localize';
import { SpinnerService } from '../utility/spinner.service';
import { SnackBarService } from '../utility/snackbar.service';
//import { ComponentFactoryResolver } from '@angular/core/src/render3';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

    private http: any;
    private router: Router;
    private gettingAcessToken: boolean;
    private failedReqStack: any [];
    private tokenStateObservable: BehaviorSubject<string>;

    constructor(
        private tokenProvider: TokenProvider, 
        private injector: Injector, 
        private tokenState: TokenState, 
        private appSettingsProvider: AppSettingsProvider,
        private spinnerService: SpinnerService,
        private snackbarService: SnackBarService
        ) {
        console.log('=====HttpsRequestInterceptor INITIALIZED=====');
        this.gettingAcessToken = false;
        this.tokenStateObservable = this.tokenState.getTokenStateObservable();
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const featureProvider = this.injector.get(FeatureProvider);
        // console.log('intercepted for request');
        
        req = req.clone({
            setHeaders: {
                Authorization: `Bearer ${this.tokenProvider.getAccessToken()}`,
                'Origin': this.appSettingsProvider.getAppSettings().Origin
            }
        });

        return next.handle(req).pipe(catchError((err, caught: Observable<HttpEvent<any>>) => {
            console.error('Intercept got error', err);
            if (err.url && err.url.toLowerCase() === this.appSettingsProvider.getAppSettings().Token.toLowerCase() && err.status === 400) {
                switch (err.error.error) {
                    case "term_and_condition_acceptance_require":
                        return throwError(err);
                        break;
                    case "two_factor_code_require":
                        return throwError(err);
                        break;
                    case "incorrect_user_name_or_password":
                        return throwError(err);
                        break;
                    case "Session_Lock_Id":
                        return throwError(err);
                        break;
                    case "persona_require":
                        return throwError(err);
                        break;
                    case "verification_code_require":
                        return throwError(err);
                        break;
                    default:
                        break;
                }
                console.log('ITS TIME TO LOGOUT FROM CLIENT');
                this.spinnerService.hide();
                const that = this;
                this.tokenProvider.getAnnonymousToken().then((response: any) => {
                    that.tokenProvider.setAccessToken(response.access_token);
                    that.tokenProvider.setRefreshToken(response.refresh_token);
                    featureProvider.resetFeatures();
                    this.tokenStateObservable.next('400_RECOVERED');
                    const router = this.injector.get(Router);
                    router.navigate(['/']);
                });
            } else if (err.status === 401) {
                console.log('Intercept ERROR Code 401');
                return this.tokenProvider.refreshTokenObservable().pipe(
                    switchMap((newToken: any) => {
                        
                        let httpHeaders = new HttpHeaders()
                        .set('Authorization', `Bearer ${this.tokenProvider.getAccessToken()}`)
                        .set('Origin', this.appSettingsProvider.getAppSettings().Origin);
                        const authReq = req.clone({ headers: httpHeaders });

                        this.tokenStateObservable.next('401_RECOVERED');
                        return next.handle(authReq);
                    })
                );
            } else {
                console.log('faced unknown error, aborting bootstraping process...');
                this.snackbarService.show('Faced unknown error 502, aborting bootstraping process...', '#fff')
                this.spinnerService.hide();
                return throwError(err);
            }
        }),);
    }

    public renewAccessToken(next, req) {
        next.handle(req);
    }
}
