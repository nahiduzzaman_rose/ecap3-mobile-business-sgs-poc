import {Injectable} from "@angular/core";
import {Router, CanActivate, ActivatedRouteSnapshot} from "@angular/router";
import {FeatureProvider} from "../provider/feature.provider";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private featureProvider: FeatureProvider) {
    }

    canActivate(route: ActivatedRouteSnapshot): Promise<boolean> | boolean {
        return this.featureProvider.getFeatures().then(features => {
            let filter = require("lodash.filter");
            let canActivate = filter(features, (feature) => {
                return feature.Name === route.data.requiredFeature;
            });
            if (canActivate && canActivate.length) {
                return true;
            } else {
                this.router.navigate([route.data.authFailRedirection]);
                return false;
            }
        });
    }

    public resetFeatures() {
        // console.log('Resetting Features...');
        this.featureProvider.deleteFeatures();
    }
}
