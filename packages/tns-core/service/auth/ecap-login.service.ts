import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {TokenProvider} from "../provider/token.provider";
import {NavigationProvider} from "../provider/navigation.provider";
import {FeatureProvider} from "../provider/feature.provider";
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import {AuthGuard} from "./auth-guard.service";
import {Observable, throwError} from "rxjs";
import {map, catchError, finalize} from "rxjs/operators";
import {UserInfoService} from "./user-info.service";
import {connectionType, getConnectionType} from "tns-core-modules/connectivity";
import {RouterExtensions} from "nativescript-angular/router";
import { AppSettingsProvider } from "../provider/settings.provider";

@Injectable()
export class EcapLoginService {
    constructor(private tokenProvider: TokenProvider,
                private navigationProvider: NavigationProvider,
                private http: HttpClient,
                private featureActivator: AuthGuard,
                private routerExtensions: RouterExtensions,
                private featureProvider: FeatureProvider,
                private router: Router,
                private appSettingsProvider: AppSettingsProvider,
                private userInfoService: UserInfoService) {
    }

    header: any = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
        "Origin": this.appSettingsProvider.getAppSettings().Origin
    });

    notFoundStatus: number = 404;

    commonOptions: any = {headers: this.header, observe: "response"};

    public logoutFromPlatform() {
        // if (getConnectionType() === connectionType.none) {
        //     alert("Trackord requires an internet connection to logout.");
        //     return;
        // }
        // let url = this.appSettingsProvider.getAppSettings().Authentication + "logout?refreshtoken=" + this.tokenProvider.getRefreshToken();
        let url = this.appSettingsProvider.getAppSettings().Authentication + "logout";
        let body = {"RefreshToken": this.tokenProvider.getRefreshToken()};
        const header = new HttpHeaders({
            "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
            "Origin": this.appSettingsProvider.getAppSettings().Origin
        });
        return this.http.post(url, body, {headers: header}).pipe(map((response) => {
            if (response && response["StatusCode"] == 0) {
                return this.clearAllCacheData();
            } else {
                console.log("Can't logout Now!");
            }
        }));
        // Logout Finalize
    }

    public clearAllCacheData() {
        return this.tokenProvider.getAnnonymousToken().then((response: any) => {
            this.userInfoService.resetUserInfo();
            this.tokenProvider.setRefreshToken(response.refresh_token);
            this.tokenProvider.setAccessToken(response.access_token);
            this.featureProvider.resetFeatures();
            this.navigationProvider.assignNavigationsVisibility();
            this.routerExtensions.navigate([`/login`], {
                transition: {name: "fade"}, clearHistory: true
            });
        });
    }

    public signupUserFromPersonUsingBusinessSevice(personInfo, serviceApi): any {
        let personPayload = {
            Email: personInfo && personInfo.Email
        };
        return this.http.post(serviceApi, personPayload,
            {headers: this.header, observe: "response"})
            .pipe(map((response) => {
                return response;
            }));
    }

    public emailExists(email): any {
        let params = new HttpParams().append("Email", email);
        let options = this.commonOptions;
        options["params"] = params;
        return this.http.get(
            this.appSettingsProvider.getAppSettings().Security + "Authentication/IsEmailAvaiable",
            options
        ).pipe(map((response) => {
            return response;
        }));
    }

    public createNewUser(personInfo) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/CreateUser", personInfo,
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    public activateAccount(password, activationCode, currentCountryCode, currentPhoneNumber) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "Authentication/ActivateAccount", {
                "Password": password,
                "AccountActivationCode": activationCode,
                "CountryCode": currentCountryCode,
                "PhoneNumber": currentPhoneNumber
            },
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    public verifyAccount(activationCode) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/VerifyEmail", {"AccountId": activationCode},
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    public sendResetPasswordRequest(email, captcha) {
        let payload = {Email: email, CaptchaString: captcha};
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/RecoverAccount", payload,
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    public resetPassword(password, activationCode) {
        let payload = {NewPassword: password, RecoverAccountCode: activationCode};
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/ResetPassword", payload,
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    public checkActivationCodeValidation(activationCode) {
        let params = new HttpParams().append("ActivateAccountCode", activationCode);
        let options = this.commonOptions;
        options["params"] = params;
        return this.http.get(
            this.appSettingsProvider.getAppSettings().Security + "Authentication/ActivateAccountCodeCheck",
            options
        ).pipe(map((response) => {
            return response;
        }));
    }

    public verify2faCode(code, token): Observable<any> {
        let body = new HttpParams().set("grant_type", "authenticate_two_factor_code").set("two_factor_code", code).set("two_factor_token", token);

        const header = new HttpHeaders({
            "Content-Type": "application/x-www-form-urlencoded"
        });

        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), {headers: header}).pipe(map((response: any) => {
            this.tokenProvider.setRefreshToken(response.refresh_token);
            this.featureActivator.resetFeatures();
            return response;
        }, (error) => {
            console.log("2FA ERROR FOUND");
            return error;
        }));
    }

    public resend2FaCode(token, captcha): Observable<any> {
        const body = {
            "TwoFactorAuthToken": token,
            "CaptchaString": captcha
        };
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });

        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "Authentication/Refresh2FaCode", body, {headers: header});
    }

    public getCaptcha(): Observable<any> {
        const body = {};
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Captcha + "CreateCaptcha", body, {headers: header});
    }

    public submitCaptcha(id, value): Observable<any> {
        const body = {
            Id: id,
            Value: value
        };
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Captcha + "SubmitCaptcha", body, {headers: header});
    }

    public recoverAccount(email, verificationCode): Observable<any> {
        const body = {
            Email: email,
            VerificationCode: verificationCode
        };
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/RecoverAccount", body, {headers: header});
    }
}
