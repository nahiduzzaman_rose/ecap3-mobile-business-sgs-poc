"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var feature_provider_1 = require("../provider/feature.provider");
var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, featureProvider) {
        this.router = router;
        this.featureProvider = featureProvider;
    }
    AuthGuard.prototype.canActivate = function (route) {
        var _this = this;
        console.log("canActivate calling");
        return this.featureProvider.getFeatures().then(function (features) {
            var filter = require("lodash.filter");
            var canActivate = filter(features, function (feature) {
                return feature.Name === route.data.requiredFeature;
            });
            if (canActivate && canActivate.length) {
                return true;
            }
            else {
                _this.router.navigate([route.data.authFailRedirection]);
                return false;
            }
        });
    };
    AuthGuard.prototype.resetFeatures = function () {
        // console.log('Resetting Features...');
        this.featureProvider.deleteFeatures();
    };
    AuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router, feature_provider_1.FeatureProvider])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aC1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBQ3pDLDBDQUE0RTtBQUM1RSxpRUFBNkQ7QUFHN0Q7SUFFSSxtQkFBb0IsTUFBYyxFQUFVLGVBQWdDO1FBQXhELFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7SUFDNUUsQ0FBQztJQUVELCtCQUFXLEdBQVgsVUFBWSxLQUE2QjtRQUF6QyxpQkFjQztRQWJHLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtZQUNuRCxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDdEMsSUFBSSxXQUFXLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxVQUFDLE9BQU87Z0JBQ3ZDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUN2RCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUU7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztnQkFDdkQsT0FBTyxLQUFLLENBQUM7YUFDaEI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxpQ0FBYSxHQUFwQjtRQUNJLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUF4QlEsU0FBUztRQURyQixpQkFBVSxFQUFFO3lDQUdtQixlQUFNLEVBQTJCLGtDQUFlO09BRm5FLFNBQVMsQ0F5QnJCO0lBQUQsZ0JBQUM7Q0FBQSxBQXpCRCxJQXlCQztBQXpCWSw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtSb3V0ZXIsIENhbkFjdGl2YXRlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90fSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7RmVhdHVyZVByb3ZpZGVyfSBmcm9tIFwiLi4vcHJvdmlkZXIvZmVhdHVyZS5wcm92aWRlclwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXV0aEd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgZmVhdHVyZVByb3ZpZGVyOiBGZWF0dXJlUHJvdmlkZXIpIHtcclxuICAgIH1cclxuXHJcbiAgICBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk6IFByb21pc2U8Ym9vbGVhbj4gfCBib29sZWFuIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcImNhbkFjdGl2YXRlIGNhbGxpbmdcIik7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmVhdHVyZVByb3ZpZGVyLmdldEZlYXR1cmVzKCkudGhlbihmZWF0dXJlcyA9PiB7XHJcbiAgICAgICAgICAgIGxldCBmaWx0ZXIgPSByZXF1aXJlKFwibG9kYXNoLmZpbHRlclwiKTtcclxuICAgICAgICAgICAgbGV0IGNhbkFjdGl2YXRlID0gZmlsdGVyKGZlYXR1cmVzLCAoZmVhdHVyZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZlYXR1cmUuTmFtZSA9PT0gcm91dGUuZGF0YS5yZXF1aXJlZEZlYXR1cmU7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAoY2FuQWN0aXZhdGUgJiYgY2FuQWN0aXZhdGUubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtyb3V0ZS5kYXRhLmF1dGhGYWlsUmVkaXJlY3Rpb25dKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZXNldEZlYXR1cmVzKCkge1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdSZXNldHRpbmcgRmVhdHVyZXMuLi4nKTtcclxuICAgICAgICB0aGlzLmZlYXR1cmVQcm92aWRlci5kZWxldGVGZWF0dXJlcygpO1xyXG4gICAgfVxyXG59Il19