"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var localStorage = require("nativescript-localstorage");
var rxjs_1 = require("rxjs");
var rxjs_2 = require("rxjs");
var local_stroage_keys_1 = require("../../constraint/local.stroage.keys");
var angular_jwt_1 = require("@auth0/angular-jwt");
var UserInfoService = /** @class */ (function () {
    function UserInfoService() {
        this.userInfoEvent = new rxjs_2.Subject();
        this.subject = new rxjs_1.BehaviorSubject({});
    }
    UserInfoService.prototype.sendUserInfoEvent = function (data) {
        this.userInfoEvent.next(data);
    };
    UserInfoService.prototype.getUserInfoEvent = function () {
        return this.userInfoEvent;
    };
    UserInfoService.prototype.setUserInfo = function (data) {
        localStorage.setItem(local_stroage_keys_1.LocalStroageKeys.USER_INFO, JSON.stringify(data));
        // console.log("setUserInfo");
        // console.log(this.getUserInfo());
        this.subject.next({ userDetails: this.getUserInfo() });
    };
    UserInfoService.prototype.getUserInfo = function () {
        var userData = localStorage.getItem(local_stroage_keys_1.LocalStroageKeys.USER_INFO);
        // console.log('getUserInfo', userData);
        return JSON.parse(userData);
    };
    UserInfoService.prototype.resetUserInfo = function () {
        localStorage.removeItem(local_stroage_keys_1.LocalStroageKeys.USER_INFO);
        // console.log("resetUserInfo");
        // console.log(this.getUserInfo());
        this.subject.next({ userDetails: {} });
    };
    UserInfoService.prototype.updateUserDetailsObserved = function () {
        this.subject.next({ userDetails: this.getUserInfo() });
    };
    UserInfoService.prototype.getUserDetailsObserved = function () {
        return this.subject.asObservable();
    };
    UserInfoService.prototype.getUserDataByAccessToken = function (accessToken) {
        return this.getUserDataFromJwtDecoder(accessToken);
    };
    UserInfoService.prototype.getUserDataFromJwtDecoder = function (accessToken) {
        var user = {};
        var helper = new angular_jwt_1.JwtHelperService();
        var decodeUser = helper.decodeToken(accessToken);
        user['UserId'] = decodeUser.user_id;
        user['DisplayName'] = decodeUser.display_name;
        user['Roles'] = decodeUser.role;
        user['UserName'] = decodeUser.user_name;
        user['Email'] = decodeUser.email;
        user['PhoneNumber'] = decodeUser.phone_number;
        user['isLoggedIn'] = decodeUser.user_loggedin;
        return user;
    };
    UserInfoService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], UserInfoService);
    return UserInfoService;
}());
exports.UserInfoService = UserInfoService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1pbmZvLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1c2VyLWluZm8uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1RDtBQUN2RCx3REFBMEQ7QUFDMUQsNkJBQWlEO0FBQ2pELDZCQUE2QjtBQUM3QiwwRUFBcUU7QUFDckUsa0RBQXNEO0FBR3REO0lBSUk7UUFIUSxrQkFBYSxHQUFHLElBQUksY0FBTyxFQUFVLENBQUE7UUFDckMsWUFBTyxHQUFHLElBQUksc0JBQWUsQ0FBTSxFQUFFLENBQUMsQ0FBQztJQUcvQyxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCLFVBQWtCLElBQVM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELDBDQUFnQixHQUFoQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQscUNBQVcsR0FBWCxVQUFZLElBQVM7UUFDakIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxxQ0FBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLDhCQUE4QjtRQUM5QixtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUNJLElBQU0sUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEUsd0NBQXdDO1FBQ3hDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsdUNBQWEsR0FBYjtRQUNJLFlBQVksQ0FBQyxVQUFVLENBQUMscUNBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDcEQsZ0NBQWdDO1FBQ2hDLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFDLFdBQVcsRUFBRSxFQUFFLEVBQUMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxtREFBeUIsR0FBekI7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCxnREFBc0IsR0FBdEI7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELGtEQUF3QixHQUF4QixVQUF5QixXQUFXO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxtREFBeUIsR0FBekIsVUFBMEIsV0FBVztRQUNqQyxJQUFJLElBQUksR0FBUSxFQUFFLENBQUM7UUFDbkIsSUFBTSxNQUFNLEdBQUcsSUFBSSw4QkFBZ0IsRUFBRSxDQUFDO1FBQ3RDLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFakQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7UUFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUM7UUFDOUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFFOUMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQTdEUSxlQUFlO1FBRDNCLGlCQUFVLEVBQUU7O09BQ0EsZUFBZSxDQThEM0I7SUFBRCxzQkFBQztDQUFBLEFBOURELElBOERDO0FBOURZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBFdmVudEVtaXR0ZXJ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCAqIGFzIGxvY2FsU3RvcmFnZSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiO1xyXG5pbXBvcnQge09ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHtTdWJqZWN0fSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQge0xvY2FsU3Ryb2FnZUtleXN9IGZyb20gXCIuLi8uLi9jb25zdHJhaW50L2xvY2FsLnN0cm9hZ2Uua2V5c1wiO1xyXG5pbXBvcnQgeyBKd3RIZWxwZXJTZXJ2aWNlIH0gZnJvbSBcIkBhdXRoMC9hbmd1bGFyLWp3dFwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVXNlckluZm9TZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgdXNlckluZm9FdmVudCA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKVxyXG4gICAgcHJpdmF0ZSBzdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KHt9KTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIH1cclxuXHJcbiAgICBzZW5kVXNlckluZm9FdmVudChkYXRhOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnVzZXJJbmZvRXZlbnQubmV4dChkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VySW5mb0V2ZW50KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnVzZXJJbmZvRXZlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VXNlckluZm8oZGF0YTogYW55KSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5VU0VSX0lORk8sIEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcInNldFVzZXJJbmZvXCIpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuZ2V0VXNlckluZm8oKSk7XHJcbiAgICAgICAgdGhpcy5zdWJqZWN0Lm5leHQoe3VzZXJEZXRhaWxzOiB0aGlzLmdldFVzZXJJbmZvKCl9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VySW5mbygpIHtcclxuICAgICAgICBjb25zdCB1c2VyRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKExvY2FsU3Ryb2FnZUtleXMuVVNFUl9JTkZPKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnZ2V0VXNlckluZm8nLCB1c2VyRGF0YSk7XHJcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UodXNlckRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0VXNlckluZm8oKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5VU0VSX0lORk8pO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwicmVzZXRVc2VySW5mb1wiKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmdldFVzZXJJbmZvKCkpO1xyXG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KHt1c2VyRGV0YWlsczoge319KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVVc2VyRGV0YWlsc09ic2VydmVkKCkge1xyXG4gICAgICAgIHRoaXMuc3ViamVjdC5uZXh0KHt1c2VyRGV0YWlsczogdGhpcy5nZXRVc2VySW5mbygpfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlckRldGFpbHNPYnNlcnZlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlckRhdGFCeUFjY2Vzc1Rva2VuKGFjY2Vzc1Rva2VuKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VXNlckRhdGFGcm9tSnd0RGVjb2RlcihhY2Nlc3NUb2tlbik7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlckRhdGFGcm9tSnd0RGVjb2RlcihhY2Nlc3NUb2tlbikge1xyXG4gICAgICAgIGxldCB1c2VyOiBhbnkgPSB7fTtcclxuICAgICAgICBjb25zdCBoZWxwZXIgPSBuZXcgSnd0SGVscGVyU2VydmljZSgpO1xyXG4gICAgICAgIGxldCBkZWNvZGVVc2VyID0gaGVscGVyLmRlY29kZVRva2VuKGFjY2Vzc1Rva2VuKTtcclxuXHJcbiAgICAgICAgdXNlclsnVXNlcklkJ10gPSBkZWNvZGVVc2VyLnVzZXJfaWQ7XHJcbiAgICAgICAgdXNlclsnRGlzcGxheU5hbWUnXSA9IGRlY29kZVVzZXIuZGlzcGxheV9uYW1lO1xyXG4gICAgICAgIHVzZXJbJ1JvbGVzJ10gPSBkZWNvZGVVc2VyLnJvbGU7XHJcbiAgICAgICAgdXNlclsnVXNlck5hbWUnXSA9IGRlY29kZVVzZXIudXNlcl9uYW1lO1xyXG4gICAgICAgIHVzZXJbJ0VtYWlsJ10gPSBkZWNvZGVVc2VyLmVtYWlsO1xyXG4gICAgICAgIHVzZXJbJ1Bob25lTnVtYmVyJ10gPSBkZWNvZGVVc2VyLnBob25lX251bWJlcjtcclxuICAgICAgICB1c2VyWydpc0xvZ2dlZEluJ10gPSBkZWNvZGVVc2VyLnVzZXJfbG9nZ2VkaW47XHJcblxyXG4gICAgICAgIHJldHVybiB1c2VyO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==