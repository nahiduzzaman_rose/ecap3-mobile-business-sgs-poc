"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var token_provider_1 = require("../provider/token.provider");
var navigation_provider_1 = require("../provider/navigation.provider");
var feature_provider_1 = require("../provider/feature.provider");
var http_1 = require("@angular/common/http");
var auth_guard_service_1 = require("./auth-guard.service");
var operators_1 = require("rxjs/operators");
var user_info_service_1 = require("./user-info.service");
var router_2 = require("nativescript-angular/router");
var settings_provider_1 = require("../provider/settings.provider");
var EcapLoginService = /** @class */ (function () {
    function EcapLoginService(tokenProvider, navigationProvider, http, featureActivator, routerExtensions, featureProvider, router, appSettingsProvider, userInfoService) {
        this.tokenProvider = tokenProvider;
        this.navigationProvider = navigationProvider;
        this.http = http;
        this.featureActivator = featureActivator;
        this.routerExtensions = routerExtensions;
        this.featureProvider = featureProvider;
        this.router = router;
        this.appSettingsProvider = appSettingsProvider;
        this.userInfoService = userInfoService;
        this.header = new http_1.HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
            "Origin": this.appSettingsProvider.getAppSettings().Origin
        });
        this.notFoundStatus = 404;
        this.commonOptions = { headers: this.header, observe: "response" };
    }
    EcapLoginService.prototype.logoutFromPlatform = function () {
        var _this = this;
        // if (getConnectionType() === connectionType.none) {
        //     alert("Trackord requires an internet connection to logout.");
        //     return;
        // }
        // let url = this.appSettingsProvider.getAppSettings().Authentication + "logout?refreshtoken=" + this.tokenProvider.getRefreshToken();
        var url = this.appSettingsProvider.getAppSettings().Authentication + "logout";
        var body = { "RefreshToken": this.tokenProvider.getRefreshToken() };
        var header = new http_1.HttpHeaders({
            "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
            "Origin": this.appSettingsProvider.getAppSettings().Origin
        });
        return this.http.post(url, body, { headers: header }).pipe(operators_1.map(function (response) {
            if (response && response["StatusCode"] == 0) {
                return _this.clearAllCacheData();
            }
            else {
                console.log("Can't logout Now!");
            }
        }));
        // Logout Finalize
    };
    EcapLoginService.prototype.clearAllCacheData = function () {
        var _this = this;
        return this.tokenProvider.getAnnonymousToken().then(function (response) {
            _this.userInfoService.resetUserInfo();
            _this.tokenProvider.setRefreshToken(response.refresh_token);
            _this.tokenProvider.setAccessToken(response.access_token);
            _this.featureProvider.resetFeatures();
            _this.navigationProvider.assignNavigationsVisibility();
            _this.routerExtensions.navigate(["/login"], {
                transition: { name: "fade" }, clearHistory: true
            });
        });
    };
    EcapLoginService.prototype.signupUserFromPersonUsingBusinessSevice = function (personInfo, serviceApi) {
        var personPayload = {
            Email: personInfo && personInfo.Email
        };
        return this.http.post(serviceApi, personPayload, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) {
            return response;
        }));
    };
    EcapLoginService.prototype.emailExists = function (email) {
        var params = new http_1.HttpParams().append("Email", email);
        var options = this.commonOptions;
        options["params"] = params;
        return this.http.get(this.appSettingsProvider.getAppSettings().Security + "Authentication/IsEmailAvaiable", options).pipe(operators_1.map(function (response) {
            return response;
        }));
    };
    EcapLoginService.prototype.createNewUser = function (personInfo) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/CreateUser", personInfo, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) { return response; }));
    };
    EcapLoginService.prototype.activateAccount = function (password, activationCode, currentCountryCode, currentPhoneNumber) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "Authentication/ActivateAccount", {
            "Password": password,
            "AccountActivationCode": activationCode,
            "CountryCode": currentCountryCode,
            "PhoneNumber": currentPhoneNumber
        }, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) { return response; }));
    };
    EcapLoginService.prototype.verifyAccount = function (activationCode) {
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/VerifyEmail", { "AccountId": activationCode }, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) { return response; }));
    };
    EcapLoginService.prototype.sendResetPasswordRequest = function (email, captcha) {
        var payload = { Email: email, CaptchaString: captcha };
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/RecoverAccount", payload, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) { return response; }));
    };
    EcapLoginService.prototype.resetPassword = function (password, activationCode) {
        var payload = { NewPassword: password, RecoverAccountCode: activationCode };
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "SecurityCommand/ResetPassword", payload, { headers: this.header, observe: "response" })
            .pipe(operators_1.map(function (response) { return response; }));
    };
    EcapLoginService.prototype.checkActivationCodeValidation = function (activationCode) {
        var params = new http_1.HttpParams().append("ActivateAccountCode", activationCode);
        var options = this.commonOptions;
        options["params"] = params;
        return this.http.get(this.appSettingsProvider.getAppSettings().Security + "Authentication/ActivateAccountCodeCheck", options).pipe(operators_1.map(function (response) {
            return response;
        }));
    };
    EcapLoginService.prototype.verify2faCode = function (code, token) {
        var _this = this;
        var body = new http_1.HttpParams().set("grant_type", "authenticate_two_factor_code").set("two_factor_code", code).set("two_factor_token", token);
        var header = new http_1.HttpHeaders({
            "Content-Type": "application/x-www-form-urlencoded"
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), { headers: header }).pipe(operators_1.map(function (response) {
            _this.tokenProvider.setRefreshToken(response.refresh_token);
            _this.featureActivator.resetFeatures();
            return response;
        }, function (error) {
            console.log("2FA ERROR FOUND");
            return error;
        }));
    };
    EcapLoginService.prototype.resend2FaCode = function (token, captcha) {
        var body = {
            "TwoFactorAuthToken": token,
            "CaptchaString": captcha
        };
        var header = new http_1.HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Security + "Authentication/Refresh2FaCode", body, { headers: header });
    };
    EcapLoginService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [token_provider_1.TokenProvider,
            navigation_provider_1.NavigationProvider,
            http_1.HttpClient,
            auth_guard_service_1.AuthGuard,
            router_2.RouterExtensions,
            feature_provider_1.FeatureProvider,
            router_1.Router,
            settings_provider_1.AppSettingsProvider,
            user_info_service_1.UserInfoService])
    ], EcapLoginService);
    return EcapLoginService;
}());
exports.EcapLoginService = EcapLoginService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNhcC1sb2dpbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZWNhcC1sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBQ3pDLDBDQUF1QztBQUN2Qyw2REFBeUQ7QUFDekQsdUVBQW1FO0FBQ25FLGlFQUE2RDtBQUM3RCw2Q0FBeUU7QUFDekUsMkRBQStDO0FBRS9DLDRDQUF5RDtBQUN6RCx5REFBb0Q7QUFFcEQsc0RBQTZEO0FBQzdELG1FQUFvRTtBQUdwRTtJQUNJLDBCQUFvQixhQUE0QixFQUM1QixrQkFBc0MsRUFDdEMsSUFBZ0IsRUFDaEIsZ0JBQTJCLEVBQzNCLGdCQUFrQyxFQUNsQyxlQUFnQyxFQUNoQyxNQUFjLEVBQ2QsbUJBQXdDLEVBQ3hDLGVBQWdDO1FBUmhDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQVc7UUFDM0IscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBR3BELFdBQU0sR0FBUSxJQUFJLGtCQUFXLENBQUM7WUFDMUIsY0FBYyxFQUFFLGtCQUFrQjtZQUNsQyxlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxFQUFFO1lBQ2hFLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTTtTQUM3RCxDQUFDLENBQUM7UUFFSCxtQkFBYyxHQUFXLEdBQUcsQ0FBQztRQUU3QixrQkFBYSxHQUFRLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDO0lBVmpFLENBQUM7SUFZTSw2Q0FBa0IsR0FBekI7UUFBQSxpQkFvQkM7UUFuQkcscURBQXFEO1FBQ3JELG9FQUFvRTtRQUNwRSxjQUFjO1FBQ2QsSUFBSTtRQUNKLHNJQUFzSTtRQUN0SSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsY0FBYyxHQUFHLFFBQVEsQ0FBQztRQUM5RSxJQUFJLElBQUksR0FBRyxFQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRSxFQUFDLENBQUM7UUFDbEUsSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzNCLGVBQWUsRUFBRSxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUU7WUFDaEUsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNO1NBQzdELENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRO1lBQ2xFLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3pDLE9BQU8sS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0wsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNKLGtCQUFrQjtJQUN0QixDQUFDO0lBRU0sNENBQWlCLEdBQXhCO1FBQUEsaUJBV0M7UUFWRyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFhO1lBQzlELEtBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzNELEtBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN6RCxLQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1lBQ3RELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDdkMsVUFBVSxFQUFFLEVBQUMsSUFBSSxFQUFFLE1BQU0sRUFBQyxFQUFFLFlBQVksRUFBRSxJQUFJO2FBQ2pELENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLGtFQUF1QyxHQUE5QyxVQUErQyxVQUFVLEVBQUUsVUFBVTtRQUNqRSxJQUFJLGFBQWEsR0FBRztZQUNoQixLQUFLLEVBQUUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxLQUFLO1NBQ3hDLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxhQUFhLEVBQzNDLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDO2FBQzNDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRO1lBQ2YsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFTSxzQ0FBVyxHQUFsQixVQUFtQixLQUFLO1FBQ3BCLElBQUksTUFBTSxHQUFHLElBQUksaUJBQVUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNqQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzNCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQ2hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcsZ0NBQWdDLEVBQ3JGLE9BQU8sQ0FDVixDQUFDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRO1lBQ2hCLE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRU0sd0NBQWEsR0FBcEIsVUFBcUIsVUFBVTtRQUMzQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcsNEJBQTRCLEVBQUUsVUFBVSxFQUMvRyxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FBQzthQUMzQyxJQUFJLENBQUMsZUFBRyxDQUFDLFVBQUMsUUFBUSxJQUFLLE9BQUEsUUFBUSxFQUFSLENBQVEsQ0FBQyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVNLDBDQUFlLEdBQXRCLFVBQXVCLFFBQVEsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsa0JBQWtCO1FBQ25GLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLFFBQVEsR0FBRyxnQ0FBZ0MsRUFBRTtZQUNyRyxVQUFVLEVBQUUsUUFBUTtZQUNwQix1QkFBdUIsRUFBRSxjQUFjO1lBQ3ZDLGFBQWEsRUFBRSxrQkFBa0I7WUFDakMsYUFBYSxFQUFFLGtCQUFrQjtTQUNwQyxFQUNELEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDO2FBQzNDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRLElBQUssT0FBQSxRQUFRLEVBQVIsQ0FBUSxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sd0NBQWEsR0FBcEIsVUFBcUIsY0FBYztRQUMvQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcsNkJBQTZCLEVBQUUsRUFBQyxXQUFXLEVBQUUsY0FBYyxFQUFDLEVBQ25JLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDO2FBQzNDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRLElBQUssT0FBQSxRQUFRLEVBQVIsQ0FBUSxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sbURBQXdCLEdBQS9CLFVBQWdDLEtBQUssRUFBRSxPQUFPO1FBQzFDLElBQUksT0FBTyxHQUFHLEVBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsT0FBTyxFQUFDLENBQUM7UUFDckQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxHQUFHLGdDQUFnQyxFQUFFLE9BQU8sRUFDaEgsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUM7YUFDM0MsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLFFBQVEsRUFBUixDQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSx3Q0FBYSxHQUFwQixVQUFxQixRQUFRLEVBQUUsY0FBYztRQUN6QyxJQUFJLE9BQU8sR0FBRyxFQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxFQUFDLENBQUM7UUFDMUUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxHQUFHLCtCQUErQixFQUFFLE9BQU8sRUFDL0csRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUM7YUFDM0MsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLFFBQVEsRUFBUixDQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFTSx3REFBNkIsR0FBcEMsVUFBcUMsY0FBYztRQUMvQyxJQUFJLE1BQU0sR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDNUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNqQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzNCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQ2hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcseUNBQXlDLEVBQzlGLE9BQU8sQ0FDVixDQUFDLElBQUksQ0FBQyxlQUFHLENBQUMsVUFBQyxRQUFRO1lBQ2hCLE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRU0sd0NBQWEsR0FBcEIsVUFBcUIsSUFBSSxFQUFFLEtBQUs7UUFBaEMsaUJBZUM7UUFkRyxJQUFJLElBQUksR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLDhCQUE4QixDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUUxSSxJQUFNLE1BQU0sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDM0IsY0FBYyxFQUFFLG1DQUFtQztTQUN0RCxDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLEVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDOUgsS0FBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzNELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUN0QyxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUUsVUFBQyxLQUFLO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRU0sd0NBQWEsR0FBcEIsVUFBcUIsS0FBSyxFQUFFLE9BQU87UUFDL0IsSUFBTSxJQUFJLEdBQUc7WUFDVCxvQkFBb0IsRUFBRSxLQUFLO1lBQzNCLGVBQWUsRUFBRSxPQUFPO1NBQzNCLENBQUM7UUFDRixJQUFNLE1BQU0sR0FBRyxJQUFJLGtCQUFXLENBQUM7WUFDM0IsY0FBYyxFQUFFLGtCQUFrQjtTQUNyQyxDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEdBQUcsK0JBQStCLEVBQUUsSUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDekksQ0FBQztJQTVKUSxnQkFBZ0I7UUFENUIsaUJBQVUsRUFBRTt5Q0FFMEIsOEJBQWE7WUFDUix3Q0FBa0I7WUFDaEMsaUJBQVU7WUFDRSw4QkFBUztZQUNULHlCQUFnQjtZQUNqQixrQ0FBZTtZQUN4QixlQUFNO1lBQ08sdUNBQW1CO1lBQ3ZCLG1DQUFlO09BVDNDLGdCQUFnQixDQTZKNUI7SUFBRCx1QkFBQztDQUFBLEFBN0pELElBNkpDO0FBN0pZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtUb2tlblByb3ZpZGVyfSBmcm9tIFwiLi4vcHJvdmlkZXIvdG9rZW4ucHJvdmlkZXJcIjtcclxuaW1wb3J0IHtOYXZpZ2F0aW9uUHJvdmlkZXJ9IGZyb20gXCIuLi9wcm92aWRlci9uYXZpZ2F0aW9uLnByb3ZpZGVyXCI7XHJcbmltcG9ydCB7RmVhdHVyZVByb3ZpZGVyfSBmcm9tIFwiLi4vcHJvdmlkZXIvZmVhdHVyZS5wcm92aWRlclwiO1xyXG5pbXBvcnQge0h0dHBIZWFkZXJzLCBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcclxuaW1wb3J0IHtBdXRoR3VhcmR9IGZyb20gXCIuL2F1dGgtZ3VhcmQuc2VydmljZVwiO1xyXG5pbXBvcnQge09ic2VydmFibGUsIHRocm93RXJyb3J9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7bWFwLCBjYXRjaEVycm9yLCBmaW5hbGl6ZX0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcbmltcG9ydCB7VXNlckluZm9TZXJ2aWNlfSBmcm9tIFwiLi91c2VyLWluZm8uc2VydmljZVwiO1xyXG5pbXBvcnQge2Nvbm5lY3Rpb25UeXBlLCBnZXRDb25uZWN0aW9uVHlwZX0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvY29ubmVjdGl2aXR5XCI7XHJcbmltcG9ydCB7Um91dGVyRXh0ZW5zaW9uc30gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBBcHBTZXR0aW5nc1Byb3ZpZGVyIH0gZnJvbSBcIi4uL3Byb3ZpZGVyL3NldHRpbmdzLnByb3ZpZGVyXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBFY2FwTG9naW5TZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdG9rZW5Qcm92aWRlcjogVG9rZW5Qcm92aWRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbmF2aWdhdGlvblByb3ZpZGVyOiBOYXZpZ2F0aW9uUHJvdmlkZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGZlYXR1cmVBY3RpdmF0b3I6IEF1dGhHdWFyZCxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZmVhdHVyZVByb3ZpZGVyOiBGZWF0dXJlUHJvdmlkZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhcHBTZXR0aW5nc1Byb3ZpZGVyOiBBcHBTZXR0aW5nc1Byb3ZpZGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB1c2VySW5mb1NlcnZpY2U6IFVzZXJJbmZvU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIGhlYWRlcjogYW55ID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICBcIkF1dGhvcml6YXRpb25cIjogXCJiZWFyZXIgXCIgKyB0aGlzLnRva2VuUHJvdmlkZXIuZ2V0QWNjZXNzVG9rZW4oKSxcclxuICAgICAgICBcIk9yaWdpblwiOiB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5PcmlnaW5cclxuICAgIH0pO1xyXG5cclxuICAgIG5vdEZvdW5kU3RhdHVzOiBudW1iZXIgPSA0MDQ7XHJcblxyXG4gICAgY29tbW9uT3B0aW9uczogYW55ID0ge2hlYWRlcnM6IHRoaXMuaGVhZGVyLCBvYnNlcnZlOiBcInJlc3BvbnNlXCJ9O1xyXG5cclxuICAgIHB1YmxpYyBsb2dvdXRGcm9tUGxhdGZvcm0oKSB7XHJcbiAgICAgICAgLy8gaWYgKGdldENvbm5lY3Rpb25UeXBlKCkgPT09IGNvbm5lY3Rpb25UeXBlLm5vbmUpIHtcclxuICAgICAgICAvLyAgICAgYWxlcnQoXCJUcmFja29yZCByZXF1aXJlcyBhbiBpbnRlcm5ldCBjb25uZWN0aW9uIHRvIGxvZ291dC5cIik7XHJcbiAgICAgICAgLy8gICAgIHJldHVybjtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy8gbGV0IHVybCA9IHRoaXMuYXBwU2V0dGluZ3NQcm92aWRlci5nZXRBcHBTZXR0aW5ncygpLkF1dGhlbnRpY2F0aW9uICsgXCJsb2dvdXQ/cmVmcmVzaHRva2VuPVwiICsgdGhpcy50b2tlblByb3ZpZGVyLmdldFJlZnJlc2hUb2tlbigpO1xyXG4gICAgICAgIGxldCB1cmwgPSB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5BdXRoZW50aWNhdGlvbiArIFwibG9nb3V0XCI7XHJcbiAgICAgICAgbGV0IGJvZHkgPSB7XCJSZWZyZXNoVG9rZW5cIjogdGhpcy50b2tlblByb3ZpZGVyLmdldFJlZnJlc2hUb2tlbigpfTtcclxuICAgICAgICBjb25zdCBoZWFkZXIgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICBcIkF1dGhvcml6YXRpb25cIjogXCJiZWFyZXIgXCIgKyB0aGlzLnRva2VuUHJvdmlkZXIuZ2V0QWNjZXNzVG9rZW4oKSxcclxuICAgICAgICAgICAgXCJPcmlnaW5cIjogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHVybCwgYm9keSwge2hlYWRlcnM6IGhlYWRlcn0pLnBpcGUobWFwKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2VbXCJTdGF0dXNDb2RlXCJdID09IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNsZWFyQWxsQ2FjaGVEYXRhKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkNhbid0IGxvZ291dCBOb3chXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkpO1xyXG4gICAgICAgIC8vIExvZ291dCBGaW5hbGl6ZVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjbGVhckFsbENhY2hlRGF0YSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50b2tlblByb3ZpZGVyLmdldEFubm9ueW1vdXNUb2tlbigpLnRoZW4oKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy51c2VySW5mb1NlcnZpY2UucmVzZXRVc2VySW5mbygpO1xyXG4gICAgICAgICAgICB0aGlzLnRva2VuUHJvdmlkZXIuc2V0UmVmcmVzaFRva2VuKHJlc3BvbnNlLnJlZnJlc2hfdG9rZW4pO1xyXG4gICAgICAgICAgICB0aGlzLnRva2VuUHJvdmlkZXIuc2V0QWNjZXNzVG9rZW4ocmVzcG9uc2UuYWNjZXNzX3Rva2VuKTtcclxuICAgICAgICAgICAgdGhpcy5mZWF0dXJlUHJvdmlkZXIucmVzZXRGZWF0dXJlcygpO1xyXG4gICAgICAgICAgICB0aGlzLm5hdmlnYXRpb25Qcm92aWRlci5hc3NpZ25OYXZpZ2F0aW9uc1Zpc2liaWxpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtgL2xvZ2luYF0sIHtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IHtuYW1lOiBcImZhZGVcIn0sIGNsZWFySGlzdG9yeTogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2lnbnVwVXNlckZyb21QZXJzb25Vc2luZ0J1c2luZXNzU2V2aWNlKHBlcnNvbkluZm8sIHNlcnZpY2VBcGkpOiBhbnkge1xyXG4gICAgICAgIGxldCBwZXJzb25QYXlsb2FkID0ge1xyXG4gICAgICAgICAgICBFbWFpbDogcGVyc29uSW5mbyAmJiBwZXJzb25JbmZvLkVtYWlsXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Qoc2VydmljZUFwaSwgcGVyc29uUGF5bG9hZCxcclxuICAgICAgICAgICAge2hlYWRlcnM6IHRoaXMuaGVhZGVyLCBvYnNlcnZlOiBcInJlc3BvbnNlXCJ9KVxyXG4gICAgICAgICAgICAucGlwZShtYXAoKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZW1haWxFeGlzdHMoZW1haWwpOiBhbnkge1xyXG4gICAgICAgIGxldCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpLmFwcGVuZChcIkVtYWlsXCIsIGVtYWlsKTtcclxuICAgICAgICBsZXQgb3B0aW9ucyA9IHRoaXMuY29tbW9uT3B0aW9ucztcclxuICAgICAgICBvcHRpb25zW1wicGFyYW1zXCJdID0gcGFyYW1zO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KFxyXG4gICAgICAgICAgICB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5TZWN1cml0eSArIFwiQXV0aGVudGljYXRpb24vSXNFbWFpbEF2YWlhYmxlXCIsXHJcbiAgICAgICAgICAgIG9wdGlvbnNcclxuICAgICAgICApLnBpcGUobWFwKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjcmVhdGVOZXdVc2VyKHBlcnNvbkluZm8pIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuU2VjdXJpdHkgKyBcIlNlY3VyaXR5Q29tbWFuZC9DcmVhdGVVc2VyXCIsIHBlcnNvbkluZm8sXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiB0aGlzLmhlYWRlciwgb2JzZXJ2ZTogXCJyZXNwb25zZVwifSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKChyZXNwb25zZSkgPT4gcmVzcG9uc2UpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWN0aXZhdGVBY2NvdW50KHBhc3N3b3JkLCBhY3RpdmF0aW9uQ29kZSwgY3VycmVudENvdW50cnlDb2RlLCBjdXJyZW50UGhvbmVOdW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuU2VjdXJpdHkgKyBcIkF1dGhlbnRpY2F0aW9uL0FjdGl2YXRlQWNjb3VudFwiLCB7XHJcbiAgICAgICAgICAgICAgICBcIlBhc3N3b3JkXCI6IHBhc3N3b3JkLFxyXG4gICAgICAgICAgICAgICAgXCJBY2NvdW50QWN0aXZhdGlvbkNvZGVcIjogYWN0aXZhdGlvbkNvZGUsXHJcbiAgICAgICAgICAgICAgICBcIkNvdW50cnlDb2RlXCI6IGN1cnJlbnRDb3VudHJ5Q29kZSxcclxuICAgICAgICAgICAgICAgIFwiUGhvbmVOdW1iZXJcIjogY3VycmVudFBob25lTnVtYmVyXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiB0aGlzLmhlYWRlciwgb2JzZXJ2ZTogXCJyZXNwb25zZVwifSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKChyZXNwb25zZSkgPT4gcmVzcG9uc2UpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdmVyaWZ5QWNjb3VudChhY3RpdmF0aW9uQ29kZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5TZWN1cml0eSArIFwiU2VjdXJpdHlDb21tYW5kL1ZlcmlmeUVtYWlsXCIsIHtcIkFjY291bnRJZFwiOiBhY3RpdmF0aW9uQ29kZX0sXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiB0aGlzLmhlYWRlciwgb2JzZXJ2ZTogXCJyZXNwb25zZVwifSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKChyZXNwb25zZSkgPT4gcmVzcG9uc2UpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2VuZFJlc2V0UGFzc3dvcmRSZXF1ZXN0KGVtYWlsLCBjYXB0Y2hhKSB7XHJcbiAgICAgICAgbGV0IHBheWxvYWQgPSB7RW1haWw6IGVtYWlsLCBDYXB0Y2hhU3RyaW5nOiBjYXB0Y2hhfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuU2VjdXJpdHkgKyBcIlNlY3VyaXR5Q29tbWFuZC9SZWNvdmVyQWNjb3VudFwiLCBwYXlsb2FkLFxyXG4gICAgICAgICAgICB7aGVhZGVyczogdGhpcy5oZWFkZXIsIG9ic2VydmU6IFwicmVzcG9uc2VcIn0pXHJcbiAgICAgICAgICAgIC5waXBlKG1hcCgocmVzcG9uc2UpID0+IHJlc3BvbnNlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlc2V0UGFzc3dvcmQocGFzc3dvcmQsIGFjdGl2YXRpb25Db2RlKSB7XHJcbiAgICAgICAgbGV0IHBheWxvYWQgPSB7TmV3UGFzc3dvcmQ6IHBhc3N3b3JkLCBSZWNvdmVyQWNjb3VudENvZGU6IGFjdGl2YXRpb25Db2RlfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuU2VjdXJpdHkgKyBcIlNlY3VyaXR5Q29tbWFuZC9SZXNldFBhc3N3b3JkXCIsIHBheWxvYWQsXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiB0aGlzLmhlYWRlciwgb2JzZXJ2ZTogXCJyZXNwb25zZVwifSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKChyZXNwb25zZSkgPT4gcmVzcG9uc2UpKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY2hlY2tBY3RpdmF0aW9uQ29kZVZhbGlkYXRpb24oYWN0aXZhdGlvbkNvZGUpIHtcclxuICAgICAgICBsZXQgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKS5hcHBlbmQoXCJBY3RpdmF0ZUFjY291bnRDb2RlXCIsIGFjdGl2YXRpb25Db2RlKTtcclxuICAgICAgICBsZXQgb3B0aW9ucyA9IHRoaXMuY29tbW9uT3B0aW9ucztcclxuICAgICAgICBvcHRpb25zW1wicGFyYW1zXCJdID0gcGFyYW1zO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KFxyXG4gICAgICAgICAgICB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5TZWN1cml0eSArIFwiQXV0aGVudGljYXRpb24vQWN0aXZhdGVBY2NvdW50Q29kZUNoZWNrXCIsXHJcbiAgICAgICAgICAgIG9wdGlvbnNcclxuICAgICAgICApLnBpcGUobWFwKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB2ZXJpZnkyZmFDb2RlKGNvZGUsIHRva2VuKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBsZXQgYm9keSA9IG5ldyBIdHRwUGFyYW1zKCkuc2V0KFwiZ3JhbnRfdHlwZVwiLCBcImF1dGhlbnRpY2F0ZV90d29fZmFjdG9yX2NvZGVcIikuc2V0KFwidHdvX2ZhY3Rvcl9jb2RlXCIsIGNvZGUpLnNldChcInR3b19mYWN0b3JfdG9rZW5cIiwgdG9rZW4pO1xyXG5cclxuICAgICAgICBjb25zdCBoZWFkZXIgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFwiXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5Ub2tlbiwgYm9keS50b1N0cmluZygpLCB7aGVhZGVyczogaGVhZGVyfSkucGlwZShtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy50b2tlblByb3ZpZGVyLnNldFJlZnJlc2hUb2tlbihyZXNwb25zZS5yZWZyZXNoX3Rva2VuKTtcclxuICAgICAgICAgICAgdGhpcy5mZWF0dXJlQWN0aXZhdG9yLnJlc2V0RmVhdHVyZXMoKTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xyXG4gICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIjJGQSBFUlJPUiBGT1VORFwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIGVycm9yO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVzZW5kMkZhQ29kZSh0b2tlbiwgY2FwdGNoYSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICAgICAgXCJUd29GYWN0b3JBdXRoVG9rZW5cIjogdG9rZW4sXHJcbiAgICAgICAgICAgIFwiQ2FwdGNoYVN0cmluZ1wiOiBjYXB0Y2hhXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBoZWFkZXIgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuU2VjdXJpdHkgKyBcIkF1dGhlbnRpY2F0aW9uL1JlZnJlc2gyRmFDb2RlXCIsIGJvZHksIHtoZWFkZXJzOiBoZWFkZXJ9KTtcclxuICAgIH1cclxufVxyXG4iXX0=