import {Injectable, EventEmitter} from "@angular/core";
import * as localStorage from "nativescript-localstorage";
import {Observable, BehaviorSubject} from "rxjs";
import {Subject} from "rxjs";
import {LocalStroageKeys} from "../../constraint/local.stroage.keys";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable()
export class UserInfoService {
    private userInfoEvent = new Subject<string>()
    private subject = new BehaviorSubject<any>({});

    constructor() {
    }

    sendUserInfoEvent(data: any) {
        this.userInfoEvent.next(data);
    }

    getUserInfoEvent() {
        return this.userInfoEvent;
    }

    setUserInfo(data: any) {
        localStorage.setItem(LocalStroageKeys.USER_INFO, JSON.stringify(data));
        // console.log("setUserInfo");
        // console.log(this.getUserInfo());
        this.subject.next({userDetails: this.getUserInfo()});
    }

    getUserInfo() {
        const userData = localStorage.getItem(LocalStroageKeys.USER_INFO);
        // console.log('getUserInfo', userData);
        return JSON.parse(userData);
    }

    resetUserInfo() {
        localStorage.removeItem(LocalStroageKeys.USER_INFO);
        // console.log("resetUserInfo");
        // console.log(this.getUserInfo());
        this.subject.next({userDetails: {}});
    }

    updateUserDetailsObserved() {
        this.subject.next({userDetails: this.getUserInfo()});
    }

    getUserDetailsObserved(): Observable<any> {
        return this.subject.asObservable();
    }

    getUserDataByAccessToken(accessToken) {
        return this.getUserDataFromJwtDecoder(accessToken);
    }

    getUserDataFromJwtDecoder(accessToken) {
        let user: any = {};
        const helper = new JwtHelperService();
        let decodeUser = helper.decodeToken(accessToken);

        user['UserId'] = decodeUser.user_id;
        user['DisplayName'] = decodeUser.display_name;
        user['Roles'] = decodeUser.role;
        user['UserName'] = decodeUser.user_name;
        user['Email'] = decodeUser.email;
        user['PhoneNumber'] = decodeUser.phone_number;
        user['isLoggedIn'] = decodeUser.user_loggedin;

        return user;
    }
}
