import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SqlQueryBuilderService } from '../query-builder/sql-query-builder.service';
import { AppSettingsProvider } from '../provider/settings.provider';
import { TokenProvider } from '../provider/token.provider';

const header: any = new HttpHeaders({
    'Content-Type': 'application/json'
});

@Injectable()
export class PlatformDataService {

    constructor(private http: HttpClient, 
        private tokenProvider: TokenProvider, 
        private _sqlBuilder: SqlQueryBuilderService, 
        private appSettingsProvider: AppSettingsProvider) { 
    }

    // header: any = new HttpHeaders({
    //     'Content-Type': 'application/json',
    //     "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
    //     "Origin": this.appSettingsProvider.getAppSettings().Origin
    // });

    preparePostData(entityName: string, data: any, eventData?: any) {
        return { EntityName: entityName, JsonString: JSON.stringify(data), EventData: eventData };
    }

    prepareFilterQuery(filterQuery: FilterQuery) {
        return JSON.stringify(filterQuery);
    }

    prepareSetFieldValue(entityName: string, fields: string[], value: any, itemId: string, operation: string) {
        let filterQuery = <FilterQuery>{
            EntityName: entityName
        }

        filterQuery.Fields = fields;
        filterQuery.Value = value;
        filterQuery.ItemId = itemId;
        filterQuery.Operation = operation;

        return JSON.stringify(filterQuery);
    }

    prepareFilterFordDleteFilteredComplex(entityName: string, filters: DataFilter[]) {
        const filterQuery = <FilterQuery>{
            EntityName: entityName
        }

        filterQuery.Filters = filters;

        return JSON.stringify(filterQuery);
    }

    prepareDeleteFilterQuery(entityName, filters) {
        const filterQuery = <FilterQuery>{
            EntityName: entityName,
            Filters: filters
        };
        return JSON.stringify(filterQuery);
    }

    getBySqlFilterCount(entityName, query, timeout?, isExcludeCount?): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetBySQLFilter', {
            "EntityName": entityName,
            "Text": query,
            "ExcludeCount": false
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    getFullTextSearchCount(entityName, query, timeout, searchText, isExcludeCount?): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/FullTextSearch', {
            "EntityName": entityName,
            "SearchText": searchText,
            "Text": query,
            "ExcludeCount": false
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    getBySqlFilter(entityName, query, timeout?, isExcludeCount?): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetBySQLFilter', {
            "EntityName": entityName,
            "Text": query,
            "ExcludeCount": isExcludeCount
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    getFullTextSearch(entityName, query, timeout, searchText, isExcludeCount?): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/FullTextSearch', {
            "EntityName": entityName,
            "SearchText": searchText,
            "Text": query,
            "ExcludeCount": isExcludeCount
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    insert(entityName, data, eventData?: any): Observable<any> {
        data.Language = data.Language || localStorage.getItem('en-US');
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/Insert',
            this.preparePostData(entityName, data, eventData),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    update(entityName, data, eventData? : any): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/Update',
            this.preparePostData(entityName, data, eventData),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    get(entityName, fields): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetFilteredComplex',
            this.prepareFilterQuery({
                EntityName: entityName,
                DataFilters: [],
                Fields: fields
            }),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    getById(entityName, id, fields): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetFilteredComplex',
            this.prepareFilterQuery({
                EntityName: entityName,
                DataFilters: [
                    {
                        property: "ItemId",
                        operator: "=",
                        value: id
                    }
                ],
                Fields: fields
            }),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    destroy(entityName, data): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/Delete',
            this.preparePostData(entityName, data),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    getConnections(filterQuery: FilterQuery): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetConnections',
            filterQuery,
            { headers: header, withCredentials: true, observe: 'response' });
    }

    connect(connectorData): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/Connect', connectorData,
            { headers: header, withCredentials: true, observe: 'response' });
    }

    disconnect(itemId): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/Disconnect', { "ItemId": itemId },
            { headers: header, withCredentials: true, observe: 'response' });
    }

    getDistinct(entityName, distinctPropertyName, timeout): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetDistinct', {
            "EntityName": entityName,
            "DistinctPropertyName": distinctPropertyName,
            "PageNumber": 0,
            "PageLimit": 100
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    getDistinctCount(entityName, groupByFieldName, filterQuery): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetDistinctCount', {
            "EntityName": entityName,
            "GroupByFieldName": groupByFieldName,
            "PageLimit": 100,
            "PageNumber": 0,
            "FilterQuery": filterQuery
        }, { headers: header, withCredentials: true, observe: 'response' });
    }

    preparePayload(filters: Filter) {
        let payload = {
            EntityName: filters.EntityName,
            GroupByFieldName: filters.GroupByFieldName,
            PageNumber: filters.PageNumber,
            PageLimit: filters.PageLimit,
            FilterQuery: filters && filters.FilterQuery ? filters.FilterQuery : ""
        }
        if (filters && filters.AggregateFieldName) {
            payload['AggregateFieldName'] = filters.AggregateFieldName;
        }
        return payload;
    }

    getAggregateSum(filterQuery: FilterQuery): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetAggregateSum',
            this.preparePayload(filterQuery),
            { headers: header, withCredentials: true, observe: 'response' });
    }

    changeSecurityPermissionWithRowLevelSecurity(data): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/ChangeSecurityPermissionWithRowLevelSecurity',
            data,
            { headers: header, withCredentials: true, observe: 'response' });
    }

    getRowLevelSecurity(entityName, itemId): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetRowLevelSecurity',
            { EntityName: entityName, ItemId: itemId },
            { headers: header, withCredentials: true, observe: 'response' });
    }

    changeSecurityPermissionBySqlQuery(payload: any): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationCommand/ChangeSecurityPermissionBySqlQuery',
            payload,
            { headers: header, withCredentials: true, observe: 'response' });
    }

    getTenantInfo(): Observable<any> {

        const language = localStorage.getItem('en-US');

        return this.http.get(this.appSettingsProvider.getAppSettings().Identity + "?Language=" + language + "&cacheBuster=" + new Date().getTime(),
            { headers: header, withCredentials: true, observe: 'response' }).pipe(map((response) => {
                return response;
            }));
    }

    downloadSecureStorageFile(url) {
        return this.http.get(
            url,
            { headers: header, withCredentials: true, observe: 'response' }
        );
    }

    getDataBySqlFilter(entityName, fields, property, filterId, operator) {
        let query = this._sqlBuilder.prepareSqlFilterModel(property, operator,
            filterId, entityName, fields,
            0, 100);

        return this.getBySqlFilter(entityName, query, undefined).pipe(map(response => {
            const dataResponse = response.body;

            if (dataResponse['StatusCode'] != 0)
                return of([]);

            return dataResponse.Results;
        }));
    }

    getConnectionsBySqlFilter(requestPayload): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetConnectionsBySqlFilter', {
            "Text": requestPayload && requestPayload.Text,
            "EntityName": requestPayload && requestPayload.EntityName,
            "ExpandChild": requestPayload && requestPayload.ExpandChild ? requestPayload.ExpandChild : false,
            "ExpandParent": requestPayload && requestPayload.ExpandParent ? requestPayload.ExpandParent : false,
            "IncludeConnection": requestPayload && requestPayload.IncludeConnection ? requestPayload.IncludeConnection : false,
            "PageLimit": requestPayload && requestPayload.PageLimit ? requestPayload.PageLimit : 100,
            "PageNumber": requestPayload && requestPayload.PageNumber ? requestPayload.PageNumber : 0
        }, {
                headers: header,
                withCredentials: true,
                observe: 'response'
            });
    }

    public getResource(entityName, itemId, tag): Observable<any> {
        return this.http.get(this.appSettingsProvider.getAppSettings().DataCore + 'DataManipulationQuery/GetResource?EntityName=' + entityName + '&ItemId=' + itemId + '&Tag=' + tag,
            { headers: header, withCredentials: true, observe: 'response' });
    }
}


export interface FilterQuery {
    Value?: any,
    ItemId?: string,
    Operation?: any,
    EntityName?: string,
    Fields?: string[],
    DataFilters?: DataFilter[],
    OrderBy?: string,
    PageNumber?: number,
    PageLimit?: number,
    SortOrder?: string,
    Skip?: number,
    ExpandParent?: boolean,
    ExpandChild?: boolean,
    OnlyCount?: boolean,
    IncludeConnection?: boolean,
    Filters?: any,
    ItemsPerPage?: number,
    Descending?: boolean
}

export interface DataFilter {
    property?: string,
    operator?: string,
    value?: any,
    PropertyName?: string;
    Value?: string;
}

export interface Filter {
    EntityName?: string,
    GroupByFieldName?: string,
    PageNumber?: number,
    PageLimit?: number,
    FilterQuery?: string,
    AggregateFieldName?: string
}
