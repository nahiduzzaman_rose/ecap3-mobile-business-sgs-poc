import {Injectable, OnDestroy} from '@angular/core';
import { SnackBar, SnackBarOptions } from "@nstudio/nativescript-snackbar";

@Injectable()
export class SnackBarService implements OnDestroy{

    public snackBar: SnackBar;

    constructor() {
        this.snackBar = new SnackBar();
    }

    show(message: string, textColor = 'white', background = '#000') {
        if(message){
            this.snackBar.simple(message, textColor, background, 3, false);
        }
    }

    ngOnDestroy() {
        
    }
}

export interface SnackBarOptionInterface {
    actionText?: string
    actionTextColor?: string
    snackText?: string
    hideDelay?: number
    textColor?: string
    backgroundColor?: string
    maxLines?: number
    isRTL?: boolean
}
