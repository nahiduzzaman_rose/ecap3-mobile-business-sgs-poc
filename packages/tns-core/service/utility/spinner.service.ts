import {Injectable, OnDestroy} from '@angular/core';
import {LoadingIndicator, OptionsCommon} from "@nstudio/nativescript-loading-indicator";

@Injectable()
export class SpinnerService implements OnDestroy{
    private indicator: LoadingIndicator;
    private isBusy: boolean;

    constructor() {
        // this.indicator = new LoadingIndicator();
    }

    show(inputOptions?: OptionsCommon) {
        if (!this.isBusy) {
            if (!this.indicator) {
                this.indicator = new LoadingIndicator();
            }

            let loadingMessage = 'Loading....';


            let options: OptionsCommon  = {
                message: loadingMessage,
                margin: 10,
                dimBackground: false,
                hideBezel: false,
                userInteractionEnabled: false,
                android: {
                    cancelable: true,
                    cancelListener: function(dialog) { console.log("Loading cancelled"); },
                },
                ios: {
                    square: false
                }
            };

            if (inputOptions && typeof inputOptions === "object") {
                debugger
                Object.assign(options, inputOptions);
            }
            this.hide();
            if(options){
                this.indicator.show(options);
            }

            this.isBusy = true;
        }
    }

    hide() {
        if (this.indicator) {
            this.indicator.hide();
        }

        this.isBusy = false;
    }

    ngOnDestroy() {
        this.hide();
        this.indicator = null;
    }
}

export interface SpinnerOptionInterface {
    message?: string;
    progress?: number;
    android?: any;
    ios?: any;
}
