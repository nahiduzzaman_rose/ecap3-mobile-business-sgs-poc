"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SqlQueryBuilderService = /** @class */ (function () {
    function SqlQueryBuilderService() {
        this.sampleFilters = [
            {
                property: "",
                operator: "|",
                value: []
            }, {
                property: "",
                operator: "&",
                value: []
            }, {
                property: "",
                operator: "=",
                value: ""
            }, {
                property: "",
                operator: ">",
                value: ""
            }, {
                property: "",
                operator: ">=",
                value: ""
            }, {
                property: "",
                operator: "<",
                value: ""
            }, {
                property: "",
                operator: "<=",
                value: ""
            }, {
                property: "",
                operator: "!=",
                value: ""
            }, {
                property: "",
                operator: "search",
                value: ""
            }, {
                property: "",
                operator: "range",
                value: []
            }, {
                property: "",
                operator: "contains",
                value: []
            }, {
                property: "",
                operator: "!contains",
                value: []
            }
        ];
    }
    SqlQueryBuilderService_1 = SqlQueryBuilderService;
    SqlQueryBuilderService.prototype.prepareExpression = function (expressionObject) {
        var preparedExpression = "";
        var operator = expressionObject.operator;
        var value = expressionObject.value;
        var convertedOperator = {
            "|": "__or",
            "=": "__eql",
            ">": "__gt",
            ">=": "__gte",
            "&": "__and",
            "<": "__lt",
            "<=": "__lte",
            "!=": "__ne",
            "range": "__inc",
            "contains": "__in",
            "!contains": "__nin",
            "search": "__reg"
        };
        preparedExpression += expressionObject.property;
        preparedExpression += "=";
        preparedExpression += convertedOperator[operator];
        preparedExpression += "(";
        if (operator == "search") {
            if (value.charAt(0) != '/')
                preparedExpression += '/';
        }
        if (Array.isArray(value)) {
            for (var i = 0; i < value.length; i++) {
                preparedExpression += value[i];
                if (i < (value.length - 1))
                    preparedExpression += ",";
            }
        }
        else {
            preparedExpression += value;
        }
        if (operator == "search") {
            if (value.charAt(value.length - 2) != '/') {
                preparedExpression += '/i';
            }
        }
        preparedExpression += ")";
        return preparedExpression;
    };
    SqlQueryBuilderService.prototype.prepareProperties = function (properties) {
        var preparedProperties = "";
        for (var i = 0; i < properties.length; i++) {
            preparedProperties += properties[i];
            if (i < (properties.length - 1))
                preparedProperties += ",";
        }
        return preparedProperties;
    };
    SqlQueryBuilderService.prototype.prepareFilters = function (filters, expressionOperator) {
        if (expressionOperator === void 0) { expressionOperator = null; }
        var preparedFilters = "";
        if (!expressionOperator) {
            expressionOperator = " & ";
        }
        for (var i = 0, j = 0; i < filters.length; i++) {
            if (!this.isInvalid(filters[i].value)) {
                if (Array.isArray(filters[i].value)) {
                    if (filters[i].value.length < 1) {
                        continue;
                    }
                }
                j++;
                if (j > 1) {
                    preparedFilters += expressionOperator;
                }
                var expression = this.prepareExpression(filters[i]);
                preparedFilters += expression;
            }
        }
        return preparedFilters;
    };
    SqlQueryBuilderService.prototype.isInvalid = function (value) {
        return value === undefined || value === null || value === "";
    };
    SqlQueryBuilderService.prototype.prepareQuery = function (entityName, properties, filters, orderby, pagenumber, pagesize) {
        if (Array.isArray(properties)) {
            properties = this.prepareProperties(properties);
        }
        else {
            properties = "*";
        }
        return this.generateQuery(entityName, properties, filters, orderby, pagenumber, pagesize);
    };
    SqlQueryBuilderService.prototype.prepareCountQuery = function (entityName, properties, filters, orderby, pagenumber, pagesize) {
        properties = "__count";
        return this.generateQuery(entityName, properties, filters, orderby, pagenumber, pagesize);
    };
    SqlQueryBuilderService.prototype.generateQuery = function (entityName, properties, filters, orderby, pagenumber, pagesize) {
        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            var otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            var groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                var otherFilterQuery = this.prepareFilters(otherFilter);
                var groupedFilterQuery = this.prepareFilters(groupedFilter, " __or ");
                if (groupedFilterQuery && otherFilterQuery) {
                    filters = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
                }
                else if (otherFilterQuery) {
                    filters = otherFilterQuery;
                }
                else {
                    filters = groupedFilterQuery;
                }
            }
            else {
                filters = this.prepareFilters(filters);
            }
        }
        else {
            filters = null;
        }
        var query = "Select <" + properties + ">from<" + entityName + ">";
        if (filters) {
            query += "where<" + filters + ">";
        }
        if (orderby) {
            query += "Orderby<" + orderby + ">";
        }
        query += "pageNumber=<" + pagenumber + ">pageSize= <" + pagesize + ">";
        return query;
    };
    SqlQueryBuilderService.prototype.generateQueryForCSV = function (entityName, properties, filters, orderby) {
        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            var otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            var groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                var otherFilterQuery = this.prepareFilters(otherFilter);
                var groupedFilterQuery = this.prepareFilters(groupedFilter, " __or ");
                if (groupedFilterQuery && otherFilterQuery) {
                    filters = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
                }
                else if (otherFilterQuery) {
                    filters = otherFilterQuery;
                }
                else {
                    filters = groupedFilterQuery;
                }
            }
            else {
                filters = this.prepareFilters(filters);
            }
        }
        else {
            filters = null;
        }
        var query = "Select <" + properties + ">from<" + entityName + ">";
        if (filters) {
            query += "where<" + filters + ">";
        }
        if (orderby) {
            query += "Orderby<" + orderby + ">";
        }
        return query;
    };
    SqlQueryBuilderService.prototype.prepareConnectModel = function (itemId, parentName, parentId, childName, childId, tag, embededInfo) {
        var connectionModel = {
            ItemId: itemId,
            ParentEntityID: parentId,
            ParentEntityName: parentName,
            ChildEntityID: childId,
            ChildEntityName: childName,
            Tags: [tag || 'Debug'],
            EmbededInfo: embededInfo ? [JSON.stringify(embededInfo)] : null
        };
        return connectionModel;
    };
    SqlQueryBuilderService.prototype.prepareConnectModelForDenormalizedView = function (itemId, parentName, parentId, childName, childId, tag, viewId) {
        var connectionModel = {
            ItemId: itemId,
            ParentEntityID: parentId,
            ParentEntityName: parentName,
            ChildEntityID: childId,
            ChildEntityName: childName,
            Tags: [tag || 'Debug'],
            DenormalizedViewId: viewId
        };
        return connectionModel;
    };
    SqlQueryBuilderService.prototype.prepareGetOnlyConnectionModel = function (parentName, parentId, tag, pagenumber, pagesize) {
        var connectionFilter = [
            { "PropertyName": "ParentEntityName", "Value": parentName },
            { "PropertyName": "ParentEntityID", "Value": parentId },
            { "PropertyName": "Tags", "Value": tag }
        ];
        var connectionModel = {
            PageNumber: pagenumber,
            PageLimit: pagesize,
            EntityName: 'Connection',
            ExpandParent: false,
            ExpandChild: false,
            IncludeConnection: true,
            DataFilters: connectionFilter
        };
        return connectionModel;
    };
    SqlQueryBuilderService.prototype.prepareGetConnectionQuery = function (parentEntityName, parentEntityValue, childEntityName, childEntityValue, includeConnection, tags, expandParent, expandChild, pageNumber, pageLimit, dataFilterQueries) {
        var filterQuery = {
            EntityName: 'Connection',
            DataFilters: dataFilterQueries || [],
            ExpandParent: expandParent,
            ExpandChild: expandChild,
            Fields: null,
            IncludeConnection: includeConnection || false,
            PageNumber: pageNumber ? pageNumber : 0,
            PageLimit: pageLimit ? pageLimit : 100
        };
        if (parentEntityName)
            filterQuery.DataFilters.push({ "PropertyName": "ParentEntityName", "Value": parentEntityName });
        if (parentEntityValue)
            filterQuery.DataFilters.push({ "PropertyName": "ParentEntityID", "Value": parentEntityValue });
        if (childEntityName)
            filterQuery.DataFilters.push({ "PropertyName": "ChildEntityName", "Value": childEntityName });
        if (childEntityValue)
            filterQuery.DataFilters.push({ "PropertyName": "ChildEntityID", "Value": childEntityValue });
        tags.forEach(function (tag) {
            filterQuery.DataFilters.push({ "PropertyName": "Tags", "Value": tag });
        });
        return filterQuery;
    };
    SqlQueryBuilderService.prototype.prepareSqlFilterModel = function (property, operator, itemId, entityName, fields, pageNumber, pageSize) {
        var pn = pageNumber || 0;
        var ps = pageSize || 100;
        var filter = [{
                property: property,
                operator: operator,
                value: itemId && itemId.constructor === Array ? [itemId.join(',')] : itemId
            }];
        var query = this.prepareQuery(entityName, fields, filter, null, pn, ps);
        return query;
    };
    SqlQueryBuilderService.prototype.prepareDeleteModel = function (entityName, payload) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        };
    };
    SqlQueryBuilderService.prototype.prepareUpdateModel = function (entityName, payload) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        };
    };
    SqlQueryBuilderService.prototype.prepareInsertModel = function (entityName, payload) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        };
    };
    SqlQueryBuilderService.prototype.preparePermissionUpdateModel = function (permissionModel, entityName, id) {
        var permissionUpdateModel = {
            EntityName: entityName,
            ItemId: id,
            IdsAllowedToRead: permissionModel.IdsAllowedToRead,
            IdsAllowedToUpdate: permissionModel.IdsAllowedToUpdate,
            IdsAllowedToDelete: permissionModel.IdsAllowedToDelete,
            IdsAllowedToWrite: permissionModel.IdsAllowedToWrite,
            RolesAllowedToWrite: permissionModel.RolesAllowedToWrite,
            RolesAllowedToDelete: permissionModel.RolesAllowedToDelete,
            RolesAllowedToRead: permissionModel.RolesAllowedToRead,
            RolesAllowedToUpdate: permissionModel.RolesAllowedToUpdate
        };
        return permissionUpdateModel;
    };
    SqlQueryBuilderService.clone = function (obj) {
        //in case of premitives
        if (obj === null || typeof obj !== "object") {
            return obj;
        }
        //date objects should be 
        if (obj instanceof Date) {
            return new Date(obj.getTime());
        }
        //handle Array
        if (Array.isArray(obj)) {
            var clonedArr_1 = [];
            obj.forEach(function (element) {
                clonedArr_1.push(SqlQueryBuilderService_1.clone(element));
            });
            return clonedArr_1;
        }
        //lastly, handle objects
        var clonedObj = new obj.constructor();
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                clonedObj[prop] = SqlQueryBuilderService_1.clone(obj[prop]);
            }
        }
        return clonedObj;
    };
    var SqlQueryBuilderService_1;
    SqlQueryBuilderService = SqlQueryBuilderService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], SqlQueryBuilderService);
    return SqlQueryBuilderService;
}());
exports.SqlQueryBuilderService = SqlQueryBuilderService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3FsLXF1ZXJ5LWJ1aWxkZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNxbC1xdWVyeS1idWlsZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFHM0M7SUFFSTtRQUdBLGtCQUFhLEdBQUc7WUFDWjtnQkFDSSxRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsR0FBRztnQkFDYixLQUFLLEVBQUUsRUFBRTthQUNaLEVBQUU7Z0JBQ0MsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLEVBQUU7YUFDWixFQUFFO2dCQUNDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxHQUFHO2dCQUNiLEtBQUssRUFBRSxFQUFFO2FBQ1osRUFBRTtnQkFDQyxRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsR0FBRztnQkFDYixLQUFLLEVBQUUsRUFBRTthQUNaLEVBQUU7Z0JBQ0MsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLEVBQUU7YUFDWixFQUFFO2dCQUNDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxHQUFHO2dCQUNiLEtBQUssRUFBRSxFQUFFO2FBQ1osRUFBRTtnQkFDQyxRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsSUFBSTtnQkFDZCxLQUFLLEVBQUUsRUFBRTthQUNaLEVBQUU7Z0JBQ0MsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLElBQUk7Z0JBQ2QsS0FBSyxFQUFFLEVBQUU7YUFDWixFQUFFO2dCQUNDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixLQUFLLEVBQUUsRUFBRTthQUNaLEVBQUU7Z0JBQ0MsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLEtBQUssRUFBRSxFQUFFO2FBQ1osRUFBRTtnQkFDQyxRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsVUFBVTtnQkFDcEIsS0FBSyxFQUFFLEVBQUU7YUFDWixFQUFFO2dCQUNDLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixLQUFLLEVBQUUsRUFBRTthQUNaO1NBQ0osQ0FBQztJQXBERixDQUFDOytCQUhRLHNCQUFzQjtJQXlEL0Isa0RBQWlCLEdBQWpCLFVBQWtCLGdCQUFxQjtRQUNuQyxJQUFJLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7UUFDekMsSUFBSSxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBRW5DLElBQU0saUJBQWlCLEdBQUc7WUFDdEIsR0FBRyxFQUFFLE1BQU07WUFDWCxHQUFHLEVBQUUsT0FBTztZQUNaLEdBQUcsRUFBRSxNQUFNO1lBQ1gsSUFBSSxFQUFFLE9BQU87WUFDYixHQUFHLEVBQUUsT0FBTztZQUNaLEdBQUcsRUFBRSxNQUFNO1lBQ1gsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsTUFBTTtZQUNaLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLFdBQVcsRUFBRSxPQUFPO1lBQ3BCLFFBQVEsRUFBRSxPQUFPO1NBQ3BCLENBQUE7UUFDRCxrQkFBa0IsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7UUFDaEQsa0JBQWtCLElBQUksR0FBRyxDQUFDO1FBQzFCLGtCQUFrQixJQUFJLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELGtCQUFrQixJQUFJLEdBQUcsQ0FBQztRQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLEVBQUU7WUFDdEIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUc7Z0JBQ3RCLGtCQUFrQixJQUFJLEdBQUcsQ0FBQztTQUNqQztRQUNELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN0QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsa0JBQWtCLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO29CQUN0QixrQkFBa0IsSUFBSSxHQUFHLENBQUM7YUFDakM7U0FDSjthQUFNO1lBQ0gsa0JBQWtCLElBQUksS0FBSyxDQUFDO1NBQy9CO1FBRUQsSUFBSSxRQUFRLElBQUksUUFBUSxFQUFFO1lBQ3RCLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRTtnQkFDdkMsa0JBQWtCLElBQUksSUFBSSxDQUFDO2FBQzlCO1NBQ0o7UUFFRCxrQkFBa0IsSUFBSSxHQUFHLENBQUM7UUFFMUIsT0FBTyxrQkFBa0IsQ0FBQztJQUU5QixDQUFDO0lBRUQsa0RBQWlCLEdBQWpCLFVBQWtCLFVBQWU7UUFDN0IsSUFBSSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsa0JBQWtCLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQzNCLGtCQUFrQixJQUFJLEdBQUcsQ0FBQztTQUNqQztRQUNELE9BQU8sa0JBQWtCLENBQUM7SUFDOUIsQ0FBQztJQUVELCtDQUFjLEdBQWQsVUFBZSxPQUFZLEVBQUUsa0JBQXlCO1FBQXpCLG1DQUFBLEVBQUEseUJBQXlCO1FBQ2xELElBQUksZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDckIsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1NBQzlCO1FBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ2pDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUM3QixTQUFTO3FCQUNaO2lCQUNKO2dCQUNELENBQUMsRUFBRSxDQUFDO2dCQUNKLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDUCxlQUFlLElBQUksa0JBQWtCLENBQUM7aUJBQ3pDO2dCQUNELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEQsZUFBZSxJQUFJLFVBQVUsQ0FBQzthQUVqQztTQUVKO1FBQ0QsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVELDBDQUFTLEdBQVQsVUFBVSxLQUFVO1FBQ2hCLE9BQU8sS0FBSyxLQUFLLFNBQVMsSUFBSSxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssS0FBSyxFQUFFLENBQUM7SUFDakUsQ0FBQztJQUVELDZDQUFZLEdBQVosVUFBYSxVQUFlLEVBQUUsVUFBZSxFQUFFLE9BQVksRUFBRSxPQUFZLEVBQUUsVUFBZSxFQUFFLFFBQWE7UUFDckcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQzNCLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbkQ7YUFBTTtZQUNILFVBQVUsR0FBRyxHQUFHLENBQUM7U0FDcEI7UUFDRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsa0RBQWlCLEdBQWpCLFVBQWtCLFVBQWUsRUFBRSxVQUFlLEVBQUUsT0FBWSxFQUFFLE9BQVksRUFBRSxVQUFlLEVBQUUsUUFBYTtRQUMxRyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFRCw4Q0FBYSxHQUFiLFVBQWMsVUFBZSxFQUFFLFVBQWUsRUFBRSxPQUFZLEVBQUUsT0FBWSxFQUFFLFVBQWUsRUFBRSxRQUFhO1FBRXRHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN4QixxQ0FBcUM7WUFDckMsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE1BQU07Z0JBQzdDLE9BQU8sTUFBTSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGFBQWEsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsTUFBTTtnQkFDL0MsT0FBTyxNQUFNLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDMUQsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLGtCQUFrQixJQUFJLGdCQUFnQixFQUFFO29CQUN4QyxPQUFPLEdBQUcsZ0JBQWdCLEdBQUcsT0FBTyxHQUFHLGtCQUFrQixHQUFHLEtBQUssQ0FBQztpQkFDckU7cUJBQU0sSUFBSSxnQkFBZ0IsRUFBRTtvQkFDekIsT0FBTyxHQUFHLGdCQUFnQixDQUFDO2lCQUM5QjtxQkFBTTtvQkFDSCxPQUFPLEdBQUcsa0JBQWtCLENBQUM7aUJBQ2hDO2FBQ0o7aUJBQU07Z0JBQ0gsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDMUM7U0FDSjthQUFNO1lBQ0gsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNsQjtRQUNELElBQUksS0FBSyxHQUFHLFVBQVUsR0FBRyxVQUFVLEdBQUcsUUFBUSxHQUFHLFVBQVUsR0FBRyxHQUFHLENBQUM7UUFFbEUsSUFBSSxPQUFPLEVBQUU7WUFDVCxLQUFLLElBQUksUUFBUSxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUM7U0FDckM7UUFFRCxJQUFJLE9BQU8sRUFBRTtZQUNULEtBQUssSUFBSSxVQUFVLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQztTQUN2QztRQUVELEtBQUssSUFBSSxjQUFjLEdBQUcsVUFBVSxHQUFHLGNBQWMsR0FBRyxRQUFRLEdBQUcsR0FBRyxDQUFDO1FBQ3ZFLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxvREFBbUIsR0FBbkIsVUFBb0IsVUFBZSxFQUFFLFVBQWUsRUFBRSxPQUFZLEVBQUUsT0FBWTtRQUM1RSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDeEIscUNBQXFDO1lBQ3JDLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxNQUFNO2dCQUM3QyxPQUFPLE1BQU0sQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDO1lBQ3hDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE1BQU07Z0JBQy9DLE9BQU8sTUFBTSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzFELElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxrQkFBa0IsSUFBSSxnQkFBZ0IsRUFBRTtvQkFDeEMsT0FBTyxHQUFHLGdCQUFnQixHQUFHLE9BQU8sR0FBRyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7aUJBQ3JFO3FCQUFNLElBQUksZ0JBQWdCLEVBQUU7b0JBQ3pCLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQztpQkFDOUI7cUJBQU07b0JBQ0gsT0FBTyxHQUFHLGtCQUFrQixDQUFDO2lCQUNoQzthQUNKO2lCQUFNO2dCQUNILE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzFDO1NBQ0o7YUFBTTtZQUNILE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDbEI7UUFDRCxJQUFJLEtBQUssR0FBRyxVQUFVLEdBQUcsVUFBVSxHQUFHLFFBQVEsR0FBRyxVQUFVLEdBQUcsR0FBRyxDQUFDO1FBRWxFLElBQUksT0FBTyxFQUFFO1lBQ1QsS0FBSyxJQUFJLFFBQVEsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxPQUFPLEVBQUU7WUFDVCxLQUFLLElBQUksVUFBVSxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUM7U0FDdkM7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsb0RBQW1CLEdBQW5CLFVBQW9CLE1BQWMsRUFBRSxVQUFrQixFQUFFLFFBQWdCLEVBQUUsU0FBaUIsRUFBRSxPQUFlLEVBQUUsR0FBUyxFQUFFLFdBQWlCO1FBQ3RJLElBQUksZUFBZSxHQUFHO1lBQ2xCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsY0FBYyxFQUFFLFFBQVE7WUFDeEIsZ0JBQWdCLEVBQUUsVUFBVTtZQUM1QixhQUFhLEVBQUUsT0FBTztZQUN0QixlQUFlLEVBQUUsU0FBUztZQUMxQixJQUFJLEVBQUUsQ0FBQyxHQUFHLElBQUksT0FBTyxDQUFDO1lBQ3RCLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO1NBQ2xFLENBQUE7UUFFRCxPQUFPLGVBQWUsQ0FBQztJQUMzQixDQUFDO0lBRUQsdUVBQXNDLEdBQXRDLFVBQXVDLE1BQWMsRUFBRSxVQUFrQixFQUFFLFFBQWdCLEVBQUUsU0FBaUIsRUFBRSxPQUFlLEVBQUUsR0FBUyxFQUFFLE1BQVk7UUFDcEosSUFBSSxlQUFlLEdBQUc7WUFDbEIsTUFBTSxFQUFFLE1BQU07WUFDZCxjQUFjLEVBQUUsUUFBUTtZQUN4QixnQkFBZ0IsRUFBRSxVQUFVO1lBQzVCLGFBQWEsRUFBRSxPQUFPO1lBQ3RCLGVBQWUsRUFBRSxTQUFTO1lBQzFCLElBQUksRUFBRSxDQUFDLEdBQUcsSUFBSSxPQUFPLENBQUM7WUFDdEIsa0JBQWtCLEVBQUUsTUFBTTtTQUM3QixDQUFBO1FBRUQsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVELDhEQUE2QixHQUE3QixVQUE4QixVQUFrQixFQUFFLFFBQWdCLEVBQUUsR0FBVyxFQUFFLFVBQWtCLEVBQUUsUUFBZ0I7UUFDakgsSUFBSSxnQkFBZ0IsR0FBRztZQUNuQixFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFO1lBQzNELEVBQUUsY0FBYyxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUU7WUFDdkQsRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUU7U0FDM0MsQ0FBQztRQUVGLElBQUksZUFBZSxHQUFHO1lBQ2xCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFVBQVUsRUFBRSxZQUFZO1lBQ3hCLFlBQVksRUFBRSxLQUFLO1lBQ25CLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLGlCQUFpQixFQUFFLElBQUk7WUFDdkIsV0FBVyxFQUFFLGdCQUFnQjtTQUNoQyxDQUFDO1FBRUYsT0FBTyxlQUFlLENBQUE7SUFDMUIsQ0FBQztJQUVELDBEQUF5QixHQUF6QixVQUEwQixnQkFBeUIsRUFBRSxpQkFBdUIsRUFBRSxlQUF3QixFQUFFLGdCQUFzQixFQUFFLGlCQUEyQixFQUFFLElBQVksRUFBRSxZQUFzQixFQUFFLFdBQXFCLEVBQUUsVUFBbUIsRUFBRSxTQUFrQixFQUFFLGlCQUF5QjtRQUN4UixJQUFJLFdBQVcsR0FBRztZQUNkLFVBQVUsRUFBRSxZQUFZO1lBQ3hCLFdBQVcsRUFBRSxpQkFBaUIsSUFBSSxFQUFFO1lBQ3BDLFlBQVksRUFBRSxZQUFZO1lBQzFCLFdBQVcsRUFBRSxXQUFXO1lBQ3hCLE1BQU0sRUFBRSxJQUFJO1lBQ1osaUJBQWlCLEVBQUUsaUJBQWlCLElBQUksS0FBSztZQUM3QyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHO1NBQ3pDLENBQUM7UUFFRixJQUFJLGdCQUFnQjtZQUNoQixXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1FBRXBHLElBQUksaUJBQWlCO1lBQ2pCLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsY0FBYyxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUM7UUFFbkcsSUFBSSxlQUFlO1lBQ2YsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUM7UUFFbEcsSUFBSSxnQkFBZ0I7WUFDaEIsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7UUFHakcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7WUFDWixXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDM0UsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBRUQsc0RBQXFCLEdBQXJCLFVBQXNCLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsVUFBVyxFQUFFLFFBQVM7UUFDeEYsSUFBTSxFQUFFLEdBQUcsVUFBVSxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFNLEVBQUUsR0FBRyxRQUFRLElBQUksR0FBRyxDQUFDO1FBRTNCLElBQUksTUFBTSxHQUFHLENBQUM7Z0JBQ1YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixLQUFLLEVBQUUsTUFBTSxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTTthQUM5RSxDQUFDLENBQUM7UUFFSCxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFDdEMsTUFBTSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUN4QixFQUFFLENBQUMsQ0FBQztRQUVSLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxtREFBa0IsR0FBbEIsVUFBbUIsVUFBa0IsRUFBRSxPQUFZO1FBQy9DLE9BQU87WUFDSCxVQUFVLEVBQUUsVUFBVTtZQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUM7U0FDdEMsQ0FBQTtJQUNMLENBQUM7SUFFRCxtREFBa0IsR0FBbEIsVUFBbUIsVUFBa0IsRUFBRSxPQUFZO1FBQy9DLE9BQU87WUFDSCxVQUFVLEVBQUUsVUFBVTtZQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUM7U0FDdEMsQ0FBQztJQUNOLENBQUM7SUFFRCxtREFBa0IsR0FBbEIsVUFBbUIsVUFBa0IsRUFBRSxPQUFZO1FBQy9DLE9BQU87WUFDSCxVQUFVLEVBQUUsVUFBVTtZQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUM7U0FDdEMsQ0FBQTtJQUVMLENBQUM7SUFFRCw2REFBNEIsR0FBNUIsVUFBNkIsZUFBZSxFQUFFLFVBQVUsRUFBRSxFQUFFO1FBQ3hELElBQUkscUJBQXFCLEdBQUc7WUFDeEIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsTUFBTSxFQUFFLEVBQUU7WUFDVixnQkFBZ0IsRUFBRSxlQUFlLENBQUMsZ0JBQWdCO1lBQ2xELGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxrQkFBa0I7WUFDdEQsa0JBQWtCLEVBQUUsZUFBZSxDQUFDLGtCQUFrQjtZQUN0RCxpQkFBaUIsRUFBRSxlQUFlLENBQUMsaUJBQWlCO1lBQ3BELG1CQUFtQixFQUFFLGVBQWUsQ0FBQyxtQkFBbUI7WUFDeEQsb0JBQW9CLEVBQUUsZUFBZSxDQUFDLG9CQUFvQjtZQUMxRCxrQkFBa0IsRUFBRSxlQUFlLENBQUMsa0JBQWtCO1lBQ3RELG9CQUFvQixFQUFFLGVBQWUsQ0FBQyxvQkFBb0I7U0FDN0QsQ0FBQztRQUNGLE9BQU8scUJBQXFCLENBQUM7SUFDakMsQ0FBQztJQUVhLDRCQUFLLEdBQW5CLFVBQW9CLEdBQUc7UUFDbkIsdUJBQXVCO1FBQ3ZCLElBQUksR0FBRyxLQUFLLElBQUksSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLEVBQUU7WUFDekMsT0FBTyxHQUFHLENBQUM7U0FDZDtRQUVELHlCQUF5QjtRQUN6QixJQUFJLEdBQUcsWUFBWSxJQUFJLEVBQUU7WUFDckIsT0FBTyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztTQUNsQztRQUVELGNBQWM7UUFDZCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDcEIsSUFBSSxXQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEdBQUcsQ0FBQyxPQUFPLENBQUMsVUFBVSxPQUFPO2dCQUN6QixXQUFTLENBQUMsSUFBSSxDQUFDLHdCQUFzQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBO1lBQ3pELENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxXQUFTLENBQUM7U0FDcEI7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxTQUFTLEdBQUcsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdEMsS0FBSyxJQUFJLElBQUksSUFBSSxHQUFHLEVBQUU7WUFDbEIsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQixTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsd0JBQXNCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzdEO1NBQ0o7UUFFRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOztJQWpaUSxzQkFBc0I7UUFEbEMsaUJBQVUsRUFBRTs7T0FDQSxzQkFBc0IsQ0FrWmxDO0lBQUQsNkJBQUM7Q0FBQSxBQWxaRCxJQWtaQztBQWxaWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTcWxRdWVyeUJ1aWxkZXJTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIH1cclxuXHJcbiAgICBzYW1wbGVGaWx0ZXJzID0gW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHJvcGVydHk6IFwiXCIsXHJcbiAgICAgICAgICAgIG9wZXJhdG9yOiBcInxcIixcclxuICAgICAgICAgICAgdmFsdWU6IFtdXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBwcm9wZXJ0eTogXCJcIixcclxuICAgICAgICAgICAgb3BlcmF0b3I6IFwiJlwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogW11cclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIHByb3BlcnR5OiBcIlwiLFxyXG4gICAgICAgICAgICBvcGVyYXRvcjogXCI9XCIsXHJcbiAgICAgICAgICAgIHZhbHVlOiBcIlwiXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBwcm9wZXJ0eTogXCJcIixcclxuICAgICAgICAgICAgb3BlcmF0b3I6IFwiPlwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogXCJcIlxyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAgcHJvcGVydHk6IFwiXCIsXHJcbiAgICAgICAgICAgIG9wZXJhdG9yOiBcIj49XCIsXHJcbiAgICAgICAgICAgIHZhbHVlOiBcIlwiXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBwcm9wZXJ0eTogXCJcIixcclxuICAgICAgICAgICAgb3BlcmF0b3I6IFwiPFwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogXCJcIlxyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAgcHJvcGVydHk6IFwiXCIsXHJcbiAgICAgICAgICAgIG9wZXJhdG9yOiBcIjw9XCIsXHJcbiAgICAgICAgICAgIHZhbHVlOiBcIlwiXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBwcm9wZXJ0eTogXCJcIixcclxuICAgICAgICAgICAgb3BlcmF0b3I6IFwiIT1cIixcclxuICAgICAgICAgICAgdmFsdWU6IFwiXCJcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIHByb3BlcnR5OiBcIlwiLFxyXG4gICAgICAgICAgICBvcGVyYXRvcjogXCJzZWFyY2hcIixcclxuICAgICAgICAgICAgdmFsdWU6IFwiXCJcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIHByb3BlcnR5OiBcIlwiLFxyXG4gICAgICAgICAgICBvcGVyYXRvcjogXCJyYW5nZVwiLFxyXG4gICAgICAgICAgICB2YWx1ZTogW11cclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIHByb3BlcnR5OiBcIlwiLFxyXG4gICAgICAgICAgICBvcGVyYXRvcjogXCJjb250YWluc1wiLFxyXG4gICAgICAgICAgICB2YWx1ZTogW11cclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIHByb3BlcnR5OiBcIlwiLFxyXG4gICAgICAgICAgICBvcGVyYXRvcjogXCIhY29udGFpbnNcIixcclxuICAgICAgICAgICAgdmFsdWU6IFtdXHJcbiAgICAgICAgfVxyXG4gICAgXTtcclxuXHJcbiAgICBwcmVwYXJlRXhwcmVzc2lvbihleHByZXNzaW9uT2JqZWN0OiBhbnkpIHtcclxuICAgICAgICBsZXQgcHJlcGFyZWRFeHByZXNzaW9uID0gXCJcIjtcclxuICAgICAgICBsZXQgb3BlcmF0b3IgPSBleHByZXNzaW9uT2JqZWN0Lm9wZXJhdG9yO1xyXG4gICAgICAgIGxldCB2YWx1ZSA9IGV4cHJlc3Npb25PYmplY3QudmFsdWU7XHJcblxyXG4gICAgICAgIGNvbnN0IGNvbnZlcnRlZE9wZXJhdG9yID0ge1xyXG4gICAgICAgICAgICBcInxcIjogXCJfX29yXCIsXHJcbiAgICAgICAgICAgIFwiPVwiOiBcIl9fZXFsXCIsXHJcbiAgICAgICAgICAgIFwiPlwiOiBcIl9fZ3RcIixcclxuICAgICAgICAgICAgXCI+PVwiOiBcIl9fZ3RlXCIsXHJcbiAgICAgICAgICAgIFwiJlwiOiBcIl9fYW5kXCIsXHJcbiAgICAgICAgICAgIFwiPFwiOiBcIl9fbHRcIixcclxuICAgICAgICAgICAgXCI8PVwiOiBcIl9fbHRlXCIsXHJcbiAgICAgICAgICAgIFwiIT1cIjogXCJfX25lXCIsXHJcbiAgICAgICAgICAgIFwicmFuZ2VcIjogXCJfX2luY1wiLFxyXG4gICAgICAgICAgICBcImNvbnRhaW5zXCI6IFwiX19pblwiLFxyXG4gICAgICAgICAgICBcIiFjb250YWluc1wiOiBcIl9fbmluXCIsXHJcbiAgICAgICAgICAgIFwic2VhcmNoXCI6IFwiX19yZWdcIlxyXG4gICAgICAgIH1cclxuICAgICAgICBwcmVwYXJlZEV4cHJlc3Npb24gKz0gZXhwcmVzc2lvbk9iamVjdC5wcm9wZXJ0eTtcclxuICAgICAgICBwcmVwYXJlZEV4cHJlc3Npb24gKz0gXCI9XCI7XHJcbiAgICAgICAgcHJlcGFyZWRFeHByZXNzaW9uICs9IGNvbnZlcnRlZE9wZXJhdG9yW29wZXJhdG9yXTtcclxuICAgICAgICBwcmVwYXJlZEV4cHJlc3Npb24gKz0gXCIoXCI7XHJcbiAgICAgICAgaWYgKG9wZXJhdG9yID09IFwic2VhcmNoXCIpIHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlLmNoYXJBdCgwKSAhPSAnLycpXHJcbiAgICAgICAgICAgICAgICBwcmVwYXJlZEV4cHJlc3Npb24gKz0gJy8nO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2YWx1ZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgcHJlcGFyZWRFeHByZXNzaW9uICs9IHZhbHVlW2ldO1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgPCAodmFsdWUubGVuZ3RoIC0gMSkpXHJcbiAgICAgICAgICAgICAgICAgICAgcHJlcGFyZWRFeHByZXNzaW9uICs9IFwiLFwiO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJlcGFyZWRFeHByZXNzaW9uICs9IHZhbHVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG9wZXJhdG9yID09IFwic2VhcmNoXCIpIHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlLmNoYXJBdCh2YWx1ZS5sZW5ndGggLSAyKSAhPSAnLycpIHtcclxuICAgICAgICAgICAgICAgIHByZXBhcmVkRXhwcmVzc2lvbiArPSAnL2knO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwcmVwYXJlZEV4cHJlc3Npb24gKz0gXCIpXCI7XHJcblxyXG4gICAgICAgIHJldHVybiBwcmVwYXJlZEV4cHJlc3Npb247XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHByZXBhcmVQcm9wZXJ0aWVzKHByb3BlcnRpZXM6IGFueSkge1xyXG4gICAgICAgIGxldCBwcmVwYXJlZFByb3BlcnRpZXMgPSBcIlwiO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvcGVydGllcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBwcmVwYXJlZFByb3BlcnRpZXMgKz0gcHJvcGVydGllc1tpXTtcclxuICAgICAgICAgICAgaWYgKGkgPCAocHJvcGVydGllcy5sZW5ndGggLSAxKSlcclxuICAgICAgICAgICAgICAgIHByZXBhcmVkUHJvcGVydGllcyArPSBcIixcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHByZXBhcmVkUHJvcGVydGllcztcclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlRmlsdGVycyhmaWx0ZXJzOiBhbnksIGV4cHJlc3Npb25PcGVyYXRvciA9IG51bGwpIHtcclxuICAgICAgICBsZXQgcHJlcGFyZWRGaWx0ZXJzID0gXCJcIjtcclxuICAgICAgICBpZiAoIWV4cHJlc3Npb25PcGVyYXRvcikge1xyXG4gICAgICAgICAgICBleHByZXNzaW9uT3BlcmF0b3IgPSBcIiAmIFwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKGxldCBpID0gMCwgaiA9IDA7IGkgPCBmaWx0ZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pc0ludmFsaWQoZmlsdGVyc1tpXS52YWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZpbHRlcnNbaV0udmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlcnNbaV0udmFsdWUubGVuZ3RoIDwgMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBqKys7XHJcbiAgICAgICAgICAgICAgICBpZiAoaiA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmVwYXJlZEZpbHRlcnMgKz0gZXhwcmVzc2lvbk9wZXJhdG9yO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbGV0IGV4cHJlc3Npb24gPSB0aGlzLnByZXBhcmVFeHByZXNzaW9uKGZpbHRlcnNbaV0pO1xyXG4gICAgICAgICAgICAgICAgcHJlcGFyZWRGaWx0ZXJzICs9IGV4cHJlc3Npb247XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcHJlcGFyZWRGaWx0ZXJzO1xyXG4gICAgfVxyXG5cclxuICAgIGlzSW52YWxpZCh2YWx1ZTogYW55KSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlID09PSB1bmRlZmluZWQgfHwgdmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IFwiXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJlcGFyZVF1ZXJ5KGVudGl0eU5hbWU6IGFueSwgcHJvcGVydGllczogYW55LCBmaWx0ZXJzOiBhbnksIG9yZGVyYnk6IGFueSwgcGFnZW51bWJlcjogYW55LCBwYWdlc2l6ZTogYW55KSB7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkocHJvcGVydGllcykpIHtcclxuICAgICAgICAgICAgcHJvcGVydGllcyA9IHRoaXMucHJlcGFyZVByb3BlcnRpZXMocHJvcGVydGllcyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJvcGVydGllcyA9IFwiKlwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5nZW5lcmF0ZVF1ZXJ5KGVudGl0eU5hbWUsIHByb3BlcnRpZXMsIGZpbHRlcnMsIG9yZGVyYnksIHBhZ2VudW1iZXIsIHBhZ2VzaXplKTtcclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlQ291bnRRdWVyeShlbnRpdHlOYW1lOiBhbnksIHByb3BlcnRpZXM6IGFueSwgZmlsdGVyczogYW55LCBvcmRlcmJ5OiBhbnksIHBhZ2VudW1iZXI6IGFueSwgcGFnZXNpemU6IGFueSkge1xyXG4gICAgICAgIHByb3BlcnRpZXMgPSBcIl9fY291bnRcIjtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZW5lcmF0ZVF1ZXJ5KGVudGl0eU5hbWUsIHByb3BlcnRpZXMsIGZpbHRlcnMsIG9yZGVyYnksIHBhZ2VudW1iZXIsIHBhZ2VzaXplKTtcclxuICAgIH1cclxuXHJcbiAgICBnZW5lcmF0ZVF1ZXJ5KGVudGl0eU5hbWU6IGFueSwgcHJvcGVydGllczogYW55LCBmaWx0ZXJzOiBhbnksIG9yZGVyYnk6IGFueSwgcGFnZW51bWJlcjogYW55LCBwYWdlc2l6ZTogYW55KSB7XHJcblxyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZpbHRlcnMpKSB7XHJcbiAgICAgICAgICAgIC8vIHNlYXJjaCB0ZXh0IGFrYSByZWcgc2VhcmNoIGdyb3VwZWRcclxuICAgICAgICAgICAgbGV0IG90aGVyRmlsdGVyID0gZmlsdGVycy5maWx0ZXIoZnVuY3Rpb24gKGZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbHRlci5vcGVyYXRvciAhPT0gJ3NlYXJjaCc7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBsZXQgZ3JvdXBlZEZpbHRlciA9IGZpbHRlcnMuZmlsdGVyKGZ1bmN0aW9uIChmaWx0ZXIpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWx0ZXIub3BlcmF0b3IgPT09ICdzZWFyY2gnO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZ3JvdXBlZEZpbHRlcikgJiYgZ3JvdXBlZEZpbHRlci5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgb3RoZXJGaWx0ZXJRdWVyeSA9IHRoaXMucHJlcGFyZUZpbHRlcnMob3RoZXJGaWx0ZXIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGdyb3VwZWRGaWx0ZXJRdWVyeSA9IHRoaXMucHJlcGFyZUZpbHRlcnMoZ3JvdXBlZEZpbHRlciwgXCIgX19vciBcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZ3JvdXBlZEZpbHRlclF1ZXJ5ICYmIG90aGVyRmlsdGVyUXVlcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzID0gb3RoZXJGaWx0ZXJRdWVyeSArICcgJiAoICcgKyBncm91cGVkRmlsdGVyUXVlcnkgKyAnICkgJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAob3RoZXJGaWx0ZXJRdWVyeSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnMgPSBvdGhlckZpbHRlclF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzID0gZ3JvdXBlZEZpbHRlclF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZmlsdGVycyA9IHRoaXMucHJlcGFyZUZpbHRlcnMoZmlsdGVycyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaWx0ZXJzID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0gXCJTZWxlY3QgPFwiICsgcHJvcGVydGllcyArIFwiPmZyb208XCIgKyBlbnRpdHlOYW1lICsgXCI+XCI7XHJcblxyXG4gICAgICAgIGlmIChmaWx0ZXJzKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5ICs9IFwid2hlcmU8XCIgKyBmaWx0ZXJzICsgXCI+XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAob3JkZXJieSkge1xyXG4gICAgICAgICAgICBxdWVyeSArPSBcIk9yZGVyYnk8XCIgKyBvcmRlcmJ5ICsgXCI+XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBxdWVyeSArPSBcInBhZ2VOdW1iZXI9PFwiICsgcGFnZW51bWJlciArIFwiPnBhZ2VTaXplPSA8XCIgKyBwYWdlc2l6ZSArIFwiPlwiO1xyXG4gICAgICAgIHJldHVybiBxdWVyeTtcclxuICAgIH1cclxuXHJcbiAgICBnZW5lcmF0ZVF1ZXJ5Rm9yQ1NWKGVudGl0eU5hbWU6IGFueSwgcHJvcGVydGllczogYW55LCBmaWx0ZXJzOiBhbnksIG9yZGVyYnk6IGFueSwpe1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZpbHRlcnMpKSB7XHJcbiAgICAgICAgICAgIC8vIHNlYXJjaCB0ZXh0IGFrYSByZWcgc2VhcmNoIGdyb3VwZWRcclxuICAgICAgICAgICAgbGV0IG90aGVyRmlsdGVyID0gZmlsdGVycy5maWx0ZXIoZnVuY3Rpb24gKGZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbHRlci5vcGVyYXRvciAhPT0gJ3NlYXJjaCc7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBsZXQgZ3JvdXBlZEZpbHRlciA9IGZpbHRlcnMuZmlsdGVyKGZ1bmN0aW9uIChmaWx0ZXIpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWx0ZXIub3BlcmF0b3IgPT09ICdzZWFyY2gnO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZ3JvdXBlZEZpbHRlcikgJiYgZ3JvdXBlZEZpbHRlci5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgb3RoZXJGaWx0ZXJRdWVyeSA9IHRoaXMucHJlcGFyZUZpbHRlcnMob3RoZXJGaWx0ZXIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGdyb3VwZWRGaWx0ZXJRdWVyeSA9IHRoaXMucHJlcGFyZUZpbHRlcnMoZ3JvdXBlZEZpbHRlciwgXCIgX19vciBcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZ3JvdXBlZEZpbHRlclF1ZXJ5ICYmIG90aGVyRmlsdGVyUXVlcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzID0gb3RoZXJGaWx0ZXJRdWVyeSArICcgJiAoICcgKyBncm91cGVkRmlsdGVyUXVlcnkgKyAnICkgJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAob3RoZXJGaWx0ZXJRdWVyeSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnMgPSBvdGhlckZpbHRlclF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJzID0gZ3JvdXBlZEZpbHRlclF1ZXJ5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZmlsdGVycyA9IHRoaXMucHJlcGFyZUZpbHRlcnMoZmlsdGVycyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaWx0ZXJzID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0gXCJTZWxlY3QgPFwiICsgcHJvcGVydGllcyArIFwiPmZyb208XCIgKyBlbnRpdHlOYW1lICsgXCI+XCI7XHJcblxyXG4gICAgICAgIGlmIChmaWx0ZXJzKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5ICs9IFwid2hlcmU8XCIgKyBmaWx0ZXJzICsgXCI+XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAob3JkZXJieSkge1xyXG4gICAgICAgICAgICBxdWVyeSArPSBcIk9yZGVyYnk8XCIgKyBvcmRlcmJ5ICsgXCI+XCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcXVlcnk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJlcGFyZUNvbm5lY3RNb2RlbChpdGVtSWQ6IHN0cmluZywgcGFyZW50TmFtZTogc3RyaW5nLCBwYXJlbnRJZDogc3RyaW5nLCBjaGlsZE5hbWU6IHN0cmluZywgY2hpbGRJZDogc3RyaW5nLCB0YWc/OiBhbnksIGVtYmVkZWRJbmZvPzogYW55KSB7XHJcbiAgICAgICAgbGV0IGNvbm5lY3Rpb25Nb2RlbCA9IHtcclxuICAgICAgICAgICAgSXRlbUlkOiBpdGVtSWQsXHJcbiAgICAgICAgICAgIFBhcmVudEVudGl0eUlEOiBwYXJlbnRJZCxcclxuICAgICAgICAgICAgUGFyZW50RW50aXR5TmFtZTogcGFyZW50TmFtZSxcclxuICAgICAgICAgICAgQ2hpbGRFbnRpdHlJRDogY2hpbGRJZCxcclxuICAgICAgICAgICAgQ2hpbGRFbnRpdHlOYW1lOiBjaGlsZE5hbWUsXHJcbiAgICAgICAgICAgIFRhZ3M6IFt0YWcgfHwgJ0RlYnVnJ10sXHJcbiAgICAgICAgICAgIEVtYmVkZWRJbmZvOiBlbWJlZGVkSW5mbyA/IFtKU09OLnN0cmluZ2lmeShlbWJlZGVkSW5mbyldIDogbnVsbFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGNvbm5lY3Rpb25Nb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlQ29ubmVjdE1vZGVsRm9yRGVub3JtYWxpemVkVmlldyhpdGVtSWQ6IHN0cmluZywgcGFyZW50TmFtZTogc3RyaW5nLCBwYXJlbnRJZDogc3RyaW5nLCBjaGlsZE5hbWU6IHN0cmluZywgY2hpbGRJZDogc3RyaW5nLCB0YWc/OiBhbnksIHZpZXdJZD86IGFueSkge1xyXG4gICAgICAgIGxldCBjb25uZWN0aW9uTW9kZWwgPSB7XHJcbiAgICAgICAgICAgIEl0ZW1JZDogaXRlbUlkLFxyXG4gICAgICAgICAgICBQYXJlbnRFbnRpdHlJRDogcGFyZW50SWQsXHJcbiAgICAgICAgICAgIFBhcmVudEVudGl0eU5hbWU6IHBhcmVudE5hbWUsXHJcbiAgICAgICAgICAgIENoaWxkRW50aXR5SUQ6IGNoaWxkSWQsXHJcbiAgICAgICAgICAgIENoaWxkRW50aXR5TmFtZTogY2hpbGROYW1lLFxyXG4gICAgICAgICAgICBUYWdzOiBbdGFnIHx8ICdEZWJ1ZyddLFxyXG4gICAgICAgICAgICBEZW5vcm1hbGl6ZWRWaWV3SWQ6IHZpZXdJZFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGNvbm5lY3Rpb25Nb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlR2V0T25seUNvbm5lY3Rpb25Nb2RlbChwYXJlbnROYW1lOiBzdHJpbmcsIHBhcmVudElkOiBzdHJpbmcsIHRhZzogc3RyaW5nLCBwYWdlbnVtYmVyOiBudW1iZXIsIHBhZ2VzaXplOiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgY29ubmVjdGlvbkZpbHRlciA9IFtcclxuICAgICAgICAgICAgeyBcIlByb3BlcnR5TmFtZVwiOiBcIlBhcmVudEVudGl0eU5hbWVcIiwgXCJWYWx1ZVwiOiBwYXJlbnROYW1lIH0sXHJcbiAgICAgICAgICAgIHsgXCJQcm9wZXJ0eU5hbWVcIjogXCJQYXJlbnRFbnRpdHlJRFwiLCBcIlZhbHVlXCI6IHBhcmVudElkIH0sXHJcbiAgICAgICAgICAgIHsgXCJQcm9wZXJ0eU5hbWVcIjogXCJUYWdzXCIsIFwiVmFsdWVcIjogdGFnIH1cclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICBsZXQgY29ubmVjdGlvbk1vZGVsID0ge1xyXG4gICAgICAgICAgICBQYWdlTnVtYmVyOiBwYWdlbnVtYmVyLFxyXG4gICAgICAgICAgICBQYWdlTGltaXQ6IHBhZ2VzaXplLFxyXG4gICAgICAgICAgICBFbnRpdHlOYW1lOiAnQ29ubmVjdGlvbicsXHJcbiAgICAgICAgICAgIEV4cGFuZFBhcmVudDogZmFsc2UsXHJcbiAgICAgICAgICAgIEV4cGFuZENoaWxkOiBmYWxzZSxcclxuICAgICAgICAgICAgSW5jbHVkZUNvbm5lY3Rpb246IHRydWUsXHJcbiAgICAgICAgICAgIERhdGFGaWx0ZXJzOiBjb25uZWN0aW9uRmlsdGVyXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGNvbm5lY3Rpb25Nb2RlbFxyXG4gICAgfVxyXG5cclxuICAgIHByZXBhcmVHZXRDb25uZWN0aW9uUXVlcnkocGFyZW50RW50aXR5TmFtZT86IHN0cmluZywgcGFyZW50RW50aXR5VmFsdWU/OiBhbnksIGNoaWxkRW50aXR5TmFtZT86IHN0cmluZywgY2hpbGRFbnRpdHlWYWx1ZT86IGFueSwgaW5jbHVkZUNvbm5lY3Rpb24/OiBib29sZWFuLCB0YWdzPzogYW55W10sIGV4cGFuZFBhcmVudD86IGJvb2xlYW4sIGV4cGFuZENoaWxkPzogYm9vbGVhbiwgcGFnZU51bWJlcj86IG51bWJlciwgcGFnZUxpbWl0PzogbnVtYmVyLCBkYXRhRmlsdGVyUXVlcmllcz86IGFueVtdKSB7XHJcbiAgICAgICAgbGV0IGZpbHRlclF1ZXJ5ID0ge1xyXG4gICAgICAgICAgICBFbnRpdHlOYW1lOiAnQ29ubmVjdGlvbicsXHJcbiAgICAgICAgICAgIERhdGFGaWx0ZXJzOiBkYXRhRmlsdGVyUXVlcmllcyB8fCBbXSxcclxuICAgICAgICAgICAgRXhwYW5kUGFyZW50OiBleHBhbmRQYXJlbnQsXHJcbiAgICAgICAgICAgIEV4cGFuZENoaWxkOiBleHBhbmRDaGlsZCxcclxuICAgICAgICAgICAgRmllbGRzOiBudWxsLFxyXG4gICAgICAgICAgICBJbmNsdWRlQ29ubmVjdGlvbjogaW5jbHVkZUNvbm5lY3Rpb24gfHwgZmFsc2UsXHJcbiAgICAgICAgICAgIFBhZ2VOdW1iZXI6IHBhZ2VOdW1iZXIgPyBwYWdlTnVtYmVyIDogMCxcclxuICAgICAgICAgICAgUGFnZUxpbWl0OiBwYWdlTGltaXQgPyBwYWdlTGltaXQgOiAxMDBcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAocGFyZW50RW50aXR5TmFtZSlcclxuICAgICAgICAgICAgZmlsdGVyUXVlcnkuRGF0YUZpbHRlcnMucHVzaCh7IFwiUHJvcGVydHlOYW1lXCI6IFwiUGFyZW50RW50aXR5TmFtZVwiLCBcIlZhbHVlXCI6IHBhcmVudEVudGl0eU5hbWUgfSk7XHJcblxyXG4gICAgICAgIGlmIChwYXJlbnRFbnRpdHlWYWx1ZSlcclxuICAgICAgICAgICAgZmlsdGVyUXVlcnkuRGF0YUZpbHRlcnMucHVzaCh7IFwiUHJvcGVydHlOYW1lXCI6IFwiUGFyZW50RW50aXR5SURcIiwgXCJWYWx1ZVwiOiBwYXJlbnRFbnRpdHlWYWx1ZSB9KTtcclxuXHJcbiAgICAgICAgaWYgKGNoaWxkRW50aXR5TmFtZSlcclxuICAgICAgICAgICAgZmlsdGVyUXVlcnkuRGF0YUZpbHRlcnMucHVzaCh7IFwiUHJvcGVydHlOYW1lXCI6IFwiQ2hpbGRFbnRpdHlOYW1lXCIsIFwiVmFsdWVcIjogY2hpbGRFbnRpdHlOYW1lIH0pO1xyXG5cclxuICAgICAgICBpZiAoY2hpbGRFbnRpdHlWYWx1ZSlcclxuICAgICAgICAgICAgZmlsdGVyUXVlcnkuRGF0YUZpbHRlcnMucHVzaCh7IFwiUHJvcGVydHlOYW1lXCI6IFwiQ2hpbGRFbnRpdHlJRFwiLCBcIlZhbHVlXCI6IGNoaWxkRW50aXR5VmFsdWUgfSk7XHJcblxyXG5cclxuICAgICAgICB0YWdzLmZvckVhY2godGFnID0+IHtcclxuICAgICAgICAgICAgZmlsdGVyUXVlcnkuRGF0YUZpbHRlcnMucHVzaCh7IFwiUHJvcGVydHlOYW1lXCI6IFwiVGFnc1wiLCBcIlZhbHVlXCI6IHRhZyB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZpbHRlclF1ZXJ5O1xyXG4gICAgfVxyXG5cclxuICAgIHByZXBhcmVTcWxGaWx0ZXJNb2RlbChwcm9wZXJ0eSwgb3BlcmF0b3IsIGl0ZW1JZCwgZW50aXR5TmFtZSwgZmllbGRzLCBwYWdlTnVtYmVyPywgcGFnZVNpemU/KSB7XHJcbiAgICAgICAgY29uc3QgcG4gPSBwYWdlTnVtYmVyIHx8IDA7XHJcbiAgICAgICAgY29uc3QgcHMgPSBwYWdlU2l6ZSB8fCAxMDA7XHJcblxyXG4gICAgICAgIGxldCBmaWx0ZXIgPSBbe1xyXG4gICAgICAgICAgICBwcm9wZXJ0eTogcHJvcGVydHksXHJcbiAgICAgICAgICAgIG9wZXJhdG9yOiBvcGVyYXRvcixcclxuICAgICAgICAgICAgdmFsdWU6IGl0ZW1JZCAmJiBpdGVtSWQuY29uc3RydWN0b3IgPT09IEFycmF5ID8gW2l0ZW1JZC5qb2luKCcsJyldIDogaXRlbUlkXHJcbiAgICAgICAgfV07XHJcblxyXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gdGhpcy5wcmVwYXJlUXVlcnkoZW50aXR5TmFtZSxcclxuICAgICAgICAgICAgZmllbGRzLCBmaWx0ZXIsIG51bGwsIHBuLFxyXG4gICAgICAgICAgICBwcyk7XHJcblxyXG4gICAgICAgIHJldHVybiBxdWVyeTtcclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlRGVsZXRlTW9kZWwoZW50aXR5TmFtZTogc3RyaW5nLCBwYXlsb2FkOiBhbnkpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBFbnRpdHlOYW1lOiBlbnRpdHlOYW1lLFxyXG4gICAgICAgICAgICBKc29uU3RyaW5nOiBKU09OLnN0cmluZ2lmeShwYXlsb2FkKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcmVwYXJlVXBkYXRlTW9kZWwoZW50aXR5TmFtZTogc3RyaW5nLCBwYXlsb2FkOiBhbnkpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBFbnRpdHlOYW1lOiBlbnRpdHlOYW1lLFxyXG4gICAgICAgICAgICBKc29uU3RyaW5nOiBKU09OLnN0cmluZ2lmeShwYXlsb2FkKVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgcHJlcGFyZUluc2VydE1vZGVsKGVudGl0eU5hbWU6IHN0cmluZywgcGF5bG9hZDogYW55KSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgRW50aXR5TmFtZTogZW50aXR5TmFtZSxcclxuICAgICAgICAgICAgSnNvblN0cmluZzogSlNPTi5zdHJpbmdpZnkocGF5bG9hZClcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHByZXBhcmVQZXJtaXNzaW9uVXBkYXRlTW9kZWwocGVybWlzc2lvbk1vZGVsLCBlbnRpdHlOYW1lLCBpZCkge1xyXG4gICAgICAgIGxldCBwZXJtaXNzaW9uVXBkYXRlTW9kZWwgPSB7XHJcbiAgICAgICAgICAgIEVudGl0eU5hbWU6IGVudGl0eU5hbWUsXHJcbiAgICAgICAgICAgIEl0ZW1JZDogaWQsXHJcbiAgICAgICAgICAgIElkc0FsbG93ZWRUb1JlYWQ6IHBlcm1pc3Npb25Nb2RlbC5JZHNBbGxvd2VkVG9SZWFkLFxyXG4gICAgICAgICAgICBJZHNBbGxvd2VkVG9VcGRhdGU6IHBlcm1pc3Npb25Nb2RlbC5JZHNBbGxvd2VkVG9VcGRhdGUsXHJcbiAgICAgICAgICAgIElkc0FsbG93ZWRUb0RlbGV0ZTogcGVybWlzc2lvbk1vZGVsLklkc0FsbG93ZWRUb0RlbGV0ZSxcclxuICAgICAgICAgICAgSWRzQWxsb3dlZFRvV3JpdGU6IHBlcm1pc3Npb25Nb2RlbC5JZHNBbGxvd2VkVG9Xcml0ZSxcclxuICAgICAgICAgICAgUm9sZXNBbGxvd2VkVG9Xcml0ZTogcGVybWlzc2lvbk1vZGVsLlJvbGVzQWxsb3dlZFRvV3JpdGUsXHJcbiAgICAgICAgICAgIFJvbGVzQWxsb3dlZFRvRGVsZXRlOiBwZXJtaXNzaW9uTW9kZWwuUm9sZXNBbGxvd2VkVG9EZWxldGUsXHJcbiAgICAgICAgICAgIFJvbGVzQWxsb3dlZFRvUmVhZDogcGVybWlzc2lvbk1vZGVsLlJvbGVzQWxsb3dlZFRvUmVhZCxcclxuICAgICAgICAgICAgUm9sZXNBbGxvd2VkVG9VcGRhdGU6IHBlcm1pc3Npb25Nb2RlbC5Sb2xlc0FsbG93ZWRUb1VwZGF0ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHBlcm1pc3Npb25VcGRhdGVNb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGNsb25lKG9iaikge1xyXG4gICAgICAgIC8vaW4gY2FzZSBvZiBwcmVtaXRpdmVzXHJcbiAgICAgICAgaWYgKG9iaiA9PT0gbnVsbCB8fCB0eXBlb2Ygb2JqICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvYmo7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL2RhdGUgb2JqZWN0cyBzaG91bGQgYmUgXHJcbiAgICAgICAgaWYgKG9iaiBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBEYXRlKG9iai5nZXRUaW1lKCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9oYW5kbGUgQXJyYXlcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShvYmopKSB7XHJcbiAgICAgICAgICAgIGxldCBjbG9uZWRBcnIgPSBbXTtcclxuICAgICAgICAgICAgb2JqLmZvckVhY2goZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgICAgIGNsb25lZEFyci5wdXNoKFNxbFF1ZXJ5QnVpbGRlclNlcnZpY2UuY2xvbmUoZWxlbWVudCkpXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gY2xvbmVkQXJyO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9sYXN0bHksIGhhbmRsZSBvYmplY3RzXHJcbiAgICAgICAgbGV0IGNsb25lZE9iaiA9IG5ldyBvYmouY29uc3RydWN0b3IoKTtcclxuICAgICAgICBmb3IgKGxldCBwcm9wIGluIG9iaikge1xyXG4gICAgICAgICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KHByb3ApKSB7XHJcbiAgICAgICAgICAgICAgICBjbG9uZWRPYmpbcHJvcF0gPSBTcWxRdWVyeUJ1aWxkZXJTZXJ2aWNlLmNsb25lKG9ialtwcm9wXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBjbG9uZWRPYmo7XHJcbiAgICB9XHJcbn1cclxuIl19