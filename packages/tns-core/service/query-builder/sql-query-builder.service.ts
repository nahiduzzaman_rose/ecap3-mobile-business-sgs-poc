import { Injectable } from '@angular/core';

@Injectable()
export class SqlQueryBuilderService {

    constructor() {
    }

    sampleFilters = [
        {
            property: "",
            operator: "|",
            value: []
        }, {
            property: "",
            operator: "&",
            value: []
        }, {
            property: "",
            operator: "=",
            value: ""
        }, {
            property: "",
            operator: ">",
            value: ""
        }, {
            property: "",
            operator: ">=",
            value: ""
        }, {
            property: "",
            operator: "<",
            value: ""
        }, {
            property: "",
            operator: "<=",
            value: ""
        }, {
            property: "",
            operator: "!=",
            value: ""
        }, {
            property: "",
            operator: "search",
            value: ""
        }, {
            property: "",
            operator: "range",
            value: []
        }, {
            property: "",
            operator: "contains",
            value: []
        }, {
            property: "",
            operator: "!contains",
            value: []
        }
    ];

    prepareExpression(expressionObject: any) {
        let preparedExpression = "";
        let operator = expressionObject.operator;
        let value = expressionObject.value;

        const convertedOperator = {
            "|": "__or",
            "=": "__eql",
            ">": "__gt",
            ">=": "__gte",
            "&": "__and",
            "<": "__lt",
            "<=": "__lte",
            "!=": "__ne",
            "range": "__inc",
            "contains": "__in",
            "!contains": "__nin",
            "search": "__reg"
        }
        preparedExpression += expressionObject.property;
        preparedExpression += "=";
        preparedExpression += convertedOperator[operator];
        preparedExpression += "(";
        if (operator == "search") {
            if (value.charAt(0) != '/')
                preparedExpression += '/';
        }
        if (Array.isArray(value)) {
            for (let i = 0; i < value.length; i++) {
                preparedExpression += value[i];
                if (i < (value.length - 1))
                    preparedExpression += ",";
            }
        } else {
            preparedExpression += value;
        }

        if (operator == "search") {
            if (value.charAt(value.length - 2) != '/') {
                preparedExpression += '/i';
            }
        }

        preparedExpression += ")";

        return preparedExpression;

    }

    prepareProperties(properties: any) {
        let preparedProperties = "";
        for (let i = 0; i < properties.length; i++) {
            preparedProperties += properties[i];
            if (i < (properties.length - 1))
                preparedProperties += ",";
        }
        return preparedProperties;
    }

    prepareFilters(filters: any, expressionOperator = null) {
        let preparedFilters = "";
        if (!expressionOperator) {
            expressionOperator = " & ";
        }
        for (let i = 0, j = 0; i < filters.length; i++) {
            if (!this.isInvalid(filters[i].value)) {
                if (Array.isArray(filters[i].value)) {
                    if (filters[i].value.length < 1) {
                        continue;
                    }
                }
                j++;
                if (j > 1) {
                    preparedFilters += expressionOperator;
                }
                let expression = this.prepareExpression(filters[i]);
                preparedFilters += expression;

            }

        }
        return preparedFilters;
    }

    isInvalid(value: any) {
        return value === undefined || value === null || value === "";
    }

    prepareQuery(entityName: any, properties: any, filters: any, orderby: any, pagenumber: any, pagesize: any) {
        if (Array.isArray(properties)) {
            properties = this.prepareProperties(properties);
        } else {
            properties = "*";
        }
        return this.generateQuery(entityName, properties, filters, orderby, pagenumber, pagesize);
    }

    prepareCountQuery(entityName: any, properties: any, filters: any, orderby: any, pagenumber: any, pagesize: any) {
        properties = "__count";
        return this.generateQuery(entityName, properties, filters, orderby, pagenumber, pagesize);
    }

    generateQuery(entityName: any, properties: any, filters: any, orderby: any, pagenumber: any, pagesize: any) {

        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            let otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            let groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                let otherFilterQuery = this.prepareFilters(otherFilter);
                let groupedFilterQuery = this.prepareFilters(groupedFilter, " __or ");
                if (groupedFilterQuery && otherFilterQuery) {
                    filters = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
                } else if (otherFilterQuery) {
                    filters = otherFilterQuery;
                } else {
                    filters = groupedFilterQuery;
                }
            } else {
                filters = this.prepareFilters(filters);
            }
        } else {
            filters = null;
        }
        let query = "Select <" + properties + ">from<" + entityName + ">";

        if (filters) {
            query += "where<" + filters + ">";
        }

        if (orderby) {
            query += "Orderby<" + orderby + ">";
        }

        query += "pageNumber=<" + pagenumber + ">pageSize= <" + pagesize + ">";
        return query;
    }

    generateQueryForCSV(entityName: any, properties: any, filters: any, orderby: any,){
        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            let otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            let groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                let otherFilterQuery = this.prepareFilters(otherFilter);
                let groupedFilterQuery = this.prepareFilters(groupedFilter, " __or ");
                if (groupedFilterQuery && otherFilterQuery) {
                    filters = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
                } else if (otherFilterQuery) {
                    filters = otherFilterQuery;
                } else {
                    filters = groupedFilterQuery;
                }
            } else {
                filters = this.prepareFilters(filters);
            }
        } else {
            filters = null;
        }
        let query = "Select <" + properties + ">from<" + entityName + ">";

        if (filters) {
            query += "where<" + filters + ">";
        }

        if (orderby) {
            query += "Orderby<" + orderby + ">";
        }

        return query;
    }

    prepareConnectModel(itemId: string, parentName: string, parentId: string, childName: string, childId: string, tag?: any, embededInfo?: any) {
        let connectionModel = {
            ItemId: itemId,
            ParentEntityID: parentId,
            ParentEntityName: parentName,
            ChildEntityID: childId,
            ChildEntityName: childName,
            Tags: [tag || 'Debug'],
            EmbededInfo: embededInfo ? [JSON.stringify(embededInfo)] : null
        }

        return connectionModel;
    }

    prepareConnectModelForDenormalizedView(itemId: string, parentName: string, parentId: string, childName: string, childId: string, tag?: any, viewId?: any) {
        let connectionModel = {
            ItemId: itemId,
            ParentEntityID: parentId,
            ParentEntityName: parentName,
            ChildEntityID: childId,
            ChildEntityName: childName,
            Tags: [tag || 'Debug'],
            DenormalizedViewId: viewId
        }

        return connectionModel;
    }

    prepareGetOnlyConnectionModel(parentName: string, parentId: string, tag: string, pagenumber: number, pagesize: number) {
        let connectionFilter = [
            { "PropertyName": "ParentEntityName", "Value": parentName },
            { "PropertyName": "ParentEntityID", "Value": parentId },
            { "PropertyName": "Tags", "Value": tag }
        ];

        let connectionModel = {
            PageNumber: pagenumber,
            PageLimit: pagesize,
            EntityName: 'Connection',
            ExpandParent: false,
            ExpandChild: false,
            IncludeConnection: true,
            DataFilters: connectionFilter
        };

        return connectionModel
    }

    prepareGetConnectionQuery(parentEntityName?: string, parentEntityValue?: any, childEntityName?: string, childEntityValue?: any, includeConnection?: boolean, tags?: any[], expandParent?: boolean, expandChild?: boolean, pageNumber?: number, pageLimit?: number, dataFilterQueries?: any[]) {
        let filterQuery = {
            EntityName: 'Connection',
            DataFilters: dataFilterQueries || [],
            ExpandParent: expandParent,
            ExpandChild: expandChild,
            Fields: null,
            IncludeConnection: includeConnection || false,
            PageNumber: pageNumber ? pageNumber : 0,
            PageLimit: pageLimit ? pageLimit : 100
        };

        if (parentEntityName)
            filterQuery.DataFilters.push({ "PropertyName": "ParentEntityName", "Value": parentEntityName });

        if (parentEntityValue)
            filterQuery.DataFilters.push({ "PropertyName": "ParentEntityID", "Value": parentEntityValue });

        if (childEntityName)
            filterQuery.DataFilters.push({ "PropertyName": "ChildEntityName", "Value": childEntityName });

        if (childEntityValue)
            filterQuery.DataFilters.push({ "PropertyName": "ChildEntityID", "Value": childEntityValue });


        tags.forEach(tag => {
            filterQuery.DataFilters.push({ "PropertyName": "Tags", "Value": tag });
        });

        return filterQuery;
    }

    prepareSqlFilterModel(property, operator, itemId, entityName, fields, pageNumber?, pageSize?) {
        const pn = pageNumber || 0;
        const ps = pageSize || 100;

        let filter = [{
            property: property,
            operator: operator,
            value: itemId && itemId.constructor === Array ? [itemId.join(',')] : itemId
        }];

        const query = this.prepareQuery(entityName,
            fields, filter, null, pn,
            ps);

        return query;
    }

    prepareDeleteModel(entityName: string, payload: any) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        }
    }

    prepareUpdateModel(entityName: string, payload: any) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        };
    }

    prepareInsertModel(entityName: string, payload: any) {
        return {
            EntityName: entityName,
            JsonString: JSON.stringify(payload)
        }

    }

    preparePermissionUpdateModel(permissionModel, entityName, id) {
        let permissionUpdateModel = {
            EntityName: entityName,
            ItemId: id,
            IdsAllowedToRead: permissionModel.IdsAllowedToRead,
            IdsAllowedToUpdate: permissionModel.IdsAllowedToUpdate,
            IdsAllowedToDelete: permissionModel.IdsAllowedToDelete,
            IdsAllowedToWrite: permissionModel.IdsAllowedToWrite,
            RolesAllowedToWrite: permissionModel.RolesAllowedToWrite,
            RolesAllowedToDelete: permissionModel.RolesAllowedToDelete,
            RolesAllowedToRead: permissionModel.RolesAllowedToRead,
            RolesAllowedToUpdate: permissionModel.RolesAllowedToUpdate
        };
        return permissionUpdateModel;
    }

    public static clone(obj) {
        //in case of premitives
        if (obj === null || typeof obj !== "object") {
            return obj;
        }

        //date objects should be 
        if (obj instanceof Date) {
            return new Date(obj.getTime());
        }

        //handle Array
        if (Array.isArray(obj)) {
            let clonedArr = [];
            obj.forEach(function (element) {
                clonedArr.push(SqlQueryBuilderService.clone(element))
            });
            return clonedArr;
        }

        //lastly, handle objects
        let clonedObj = new obj.constructor();
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                clonedObj[prop] = SqlQueryBuilderService.clone(obj[prop]);
            }
        }

        return clonedObj;
    }
}
