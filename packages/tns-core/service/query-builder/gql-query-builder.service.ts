import { Injectable } from '@angular/core';
import gql from 'graphql-tag';
import { AppSettingsProvider } from '../provider/settings.provider';

@Injectable({
    providedIn: 'root',
})
export class GqlQueryBuilderService {
    constructor(
        private appSettingsProvider: AppSettingsProvider
    ) { }
    // prepareQuery(entityName: string, fields: any[], filters: any[], orderBy = '_id: -1', pageNumber = 0, pageSize = 10) {
    //     let filtersString = '';
    //     if (Array.isArray(filters)) {
    //         // search text aka reg search grouped
    //         const otherFilter = filters.filter(function (filter) {
    //             return filter.operator !== 'search';
    //         });
    //         const groupedFilter = filters.filter(function (filter) {
    //             return filter.operator === 'search';
    //         });
    //         if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
    //             const otherFilterQuery = this.prepareFilters(otherFilter);
    //             const groupedFilterQuery = this.prepareFilters(groupedFilter, ' __or ');
    //             if (groupedFilterQuery && otherFilterQuery) {
    //                 filtersString = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
    //             } else if (otherFilterQuery) {
    //                 filtersString = otherFilterQuery;
    //             } else {
    //                 filtersString = groupedFilterQuery;
    //             }
    //         } else {
    //             filtersString = this.prepareFilters(filters);
    //         }
    //     }
    //             // change findCheckList to AuditScheduleVisit
    //     return gql`
    //             query findData{
    //              ${entityName}s(
    //                Model:{
    //                  PageNumber: ${pageNumber + 1},
    //                  Filter: "{ ${filtersString} }",
    //                  Sort: "{${orderBy}}"
    //                }){
    //                  Data{
    //                    ${fields.join(',')}
    //                  },
    //                  ErrorMessage,
    //                  Success,
    //                  ValidationResult,
    //                  TotalCount
    //                }
    //             }
    //           `;
    // }

    prepareFilterString(filters: any): string {
        let filtersString = '';
        if (Array.isArray(filters)) {
          // search text aka reg search grouped
          const otherFilter = filters.filter(function (filter) {
            return filter.operator !== 'search';
          });
          const groupedFilter = filters.filter(function (filter) {
            return filter.operator === 'search';
          });
          if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
            const otherFilterQuery = this.prepareFilters(otherFilter);
            const groupedFilterQuery = this.prepareFilters(groupedFilter, ' __or ');
            if (groupedFilterQuery && otherFilterQuery) {
              filtersString = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
            } else if (otherFilterQuery) {
              filtersString = otherFilterQuery;
            } else {
              filtersString = groupedFilterQuery;
            }
          } else {
            filtersString = this.prepareFilters(filters);
          }
        } else {
          filtersString = filters ? filters : filtersString;
        }
        return filtersString;
    }

    prepareQuery(entityName: string, fields: any, filters: any, orderBy = '_id: -1', pageNumber = 0, pageSize = 15) {
        let filtersString = '';
        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            const otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            const groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                const otherFilterQuery = this.prepareFilters(otherFilter);
                const groupedFilterQuery = this.prepareFilters(groupedFilter, ' __or ');
                if (groupedFilterQuery && otherFilterQuery) {
                    filtersString = otherFilterQuery + ' & ( ' + groupedFilterQuery + ' ) ';
                } else if (otherFilterQuery) {
                    filtersString = otherFilterQuery;
                } else {
                    filtersString = groupedFilterQuery;
                }
            } else {
                filtersString = this.prepareFilters(filters);
            }
        } else {
            filtersString = filters ? filters : filtersString;
        }

        return gql`
            query findData{
                ${entityName}s(
                    Model:{
                        PageNumber: ${pageNumber + 1},
                        Filter: "{ ${filtersString} }",
                        Sort: "{${orderBy}}",
                        PageSize: ${pageSize}
                    }){
                    Data{
                        ${this.queryFieldsMap(fields)}
                    },
                    ErrorMessage,
                    Success,
                    ValidationResult,
                    TotalCount
                }
            }
        `;
    }

    prepareQueryWithOperator(entityName: string, fields: any, filters: any, orderBy = '_id: -1', pageNumber = 0, pageSize = 15) {
        const filtersString: string = this.prepareFilterString(filters);
        return gql`
                    query findData{
                     ${entityName}s(
                       Model:{
                         PageNumber: ${pageNumber + 1},
                         Filter: "{ ${filtersString} }",
                         Sort: "{${orderBy}}",
                         PageSize: ${pageSize}
                       }){
                         Data{
                           ${this.queryFieldsMap(fields)}
                         },
                         ErrorMessage,
                         Success,
                         ValidationResult,
                         TotalCount
                       }
                    }
                  `;
    }

    queryFieldsMap(fields?: Array<string | object>): string {
        return fields
            ? fields
                .map(field =>
                    typeof field === "object"
                        ? `${Object.keys(field)[0]} { ${this.queryFieldsMap(
                            Object.values(field)[0]
                        )} }`
                        : `${field}`
                )
                .join(", ")
            : "";
    }

    prepareInsertModel(entityName: string, model: any) {
        const modelString = this.convertObjectToGQLString(model);

        return gql`
            mutation create{
             ${entityName}(
                Model: ${modelString}, MutationType: INSERT
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;
    }

    prepareUpdateModel(entityName: string, model: any, entityId: string) {
        if (model['ItemId']) {
            delete model['ItemId'];
        }
        const modelString = this.convertObjectToGQLString(model);

        return gql`
            mutation update{
             ${entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: "{_id:'${entityId}'}"
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;
    }

    prepareDeleteModel(entityName: string, entityId: string) {
        return gql`
            mutation delete{
             ${entityName}(
                MutationType: DELETE,
                Filter: "{_id:'${entityId}'}"
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;
    }

    aggregateMutationCreateModel(entityName: string, model: any) {
        const modelString = this.convertObjectToGQLString(model);
        const mutationQuery = `mutation insert{
             ${entityName}(
                Model: ${modelString},
                MutationType: INSERT
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;

        return {
            'Uri': this.appSettingsProvider.getAppSettings().GQLMutationUri,
            'Verb': 'Post',
            'Payload': JSON.stringify({
                'query': mutationQuery,
                'operationName': 'insert',
                'variables': {}
            }),
            'SuccessIf': '{\"data.' + entityName + '.Success\":\"True\"}'
        };
    }

    aggregateMutationUpdateModel(entityName: string, model: any, filter?) {
        if (model.ItemId) {
            const itemId = model.ItemId;
            delete model.ItemId; // remove `ItemId` field from model for update call

            const modelString = this.convertObjectToGQLString(model);
            const mutationQuery = `mutation update{
             ${entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: "{_id:'${itemId}'}"
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;

            return {
                'Uri': this.appSettingsProvider.getAppSettings().GQLMutationUri,
                'Verb': 'Post',
                'Payload': JSON.stringify({
                    'operationName': 'update',
                    'variables': {},
                    'query': mutationQuery,
                }),
                'SuccessIf': '{"data.' + entityName + '.Success":"True"}'
            };
        }
    }

    aggregateMutationUpdateByFilterModel(entityName: string, model: any, filter) {

        delete model.ItemId; // remove `ItemId` field from model for update call
        const modelString = this.convertObjectToGQLString(model);
        const mutationQuery = `mutation update{
             ${entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: ${filter}
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;

        return {
            'Uri': this.appSettingsProvider.getAppSettings().GQLMutationUri,
            'Verb': 'Post',
            'Payload': JSON.stringify({
                'operationName': 'update',
                'variables': {},
                'query': mutationQuery,
            }),
            'SuccessIf': '{"data.' + entityName + '.Success":"True"}'
        };

    }
    prepareFilters(filters: any, expressionOperator = null) {
        let preparedFilters = '';
        if (!expressionOperator) {
            expressionOperator = ' , ';  // Change & to coma (,)
        }
        for (let i = 0, j = 0; i < filters.length; i++) {
            if (!this.isInvalid(filters[i].value)) {
                if (Array.isArray(filters[i].value)) {
                    if (filters[i].value.length < 1) {
                        continue;
                    }
                }
                j++;
                if (j > 1) {
                    preparedFilters += expressionOperator;
                }
                const expression = this.prepareExpression(filters[i]);
                preparedFilters += expression;

            }

        }

        return preparedFilters;
    }

    prepareExpression(expressionObject: any) {
        let preparedExpression = '';
        const operator = expressionObject.operator;
        const property = expressionObject.property;
        const value = expressionObject.value;

        const convertedOperator = {
            '=': `'${value}'`,
            'search': `/${expressionObject.value}/i`,
            'raw': `'${value}'`, // Support for raw mongo filter
            // '|': '__or',
            // '>': '__gt',
            // '>=': '__gte',
            // '&': '__and',
            // '<': '__lt',
            // '<=': '__lte',
            // '!=': '__ne',
            // 'range': '__inc',
            // 'contains': '__in',
            // '!contains': '__nin',
        };

        if (operator !== 'raw') {
            preparedExpression += property;
            preparedExpression += ':';
            preparedExpression += convertedOperator[operator];
        } else {
            preparedExpression = value;
        }

        return preparedExpression;
    }

    prepareFilterFromOperator(expressionArray: any[]) {
        const filters = [];
        expressionArray.forEach((expression) => {
            let stringReturn =true ;
            if(expression.hasOwnProperty('stringReturn')) {
                stringReturn = expression.stringReturn;
            }
            filters.push(this[expression.operator](expression.value, stringReturn));
        });
        return filters;
    }

    __equal(expression: any, stringReturn?: boolean) {
        debugger
        if (stringReturn) {
            debugger
            return `${expression.propertyName}: '${expression.value}'`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: ${expression.value}`
            };
        }
    }

    __greaterThan(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $gt: ${expression.value}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $gt: ${expression.value}}`
            };
        }
    }
    __lessThan(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $lt: ${expression.value}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $lt: ${expression.value}}`
            };
        }
    }
    __and(values: any, stringReturn?: boolean) {
        const filterObj: any = {
            operator: 'raw',
            value: ''
        }
        let queryString = '';
        values.forEach((value, index) => {
            const paramsValue = typeof value.value === 'string' ? value : value.value;
            if (values.length - 1 !== index) {
                queryString += `{${this[value.operator](paramsValue, true)}},`;
            } else {
                queryString += `{${this[value.operator](paramsValue, true)}}`;
            }
        });
        if (stringReturn) {
            return `$and:[ ${queryString} ]`;
        }
        filterObj.value += `$and:[ ${queryString} ]`;
        return filterObj;
    }
    __greaterThanOrEqual(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $gte: ${expression.value}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $gte: ${expression.value}}`
            };
        }
    }
    __lessThanOrEqual(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $lte: ${expression.value}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $lte: ${expression.value}}`
            };
        }
    }

    __search(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: /${expression.value}/i`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: /${expression.value}/i`
            };
        }
    }
    __or(values: any, stringReturn?: boolean) {
        const filterObj: any = {
            operator: 'raw',
            value: ''
        }
        let queryString = '';
        values.forEach((value, index) => {
            const paramsValue = typeof value.value === 'string' ? value : value.value;
            if (values.length - 1 !== index) {
                queryString += `{${this[value.operator](paramsValue, true)}},`;
            } else {
                queryString += `{${this[value.operator](paramsValue, true)}}`;
            }
        });
        if (stringReturn) {
            return `$or:[ ${queryString} ]`;
        }
        filterObj.value += `$or:[ ${queryString} ]`;
        return filterObj;
    }
    __contains(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $in: ${expression.value} }`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $in: ${expression.value}}`
            };
        }
    }
    __notContains(expression: any, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $nin: ${expression.value}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $nin: ${expression.value}}`
            };
        }
    }

    __range(expression, stringReturn?: boolean) {
        if (stringReturn) {
            return `${expression.propertyName}: { $gte: ${expression.value.startValue}, $lte: ${expression.value.endValue}}`;
        } else {
            return {
                operator: 'raw',
                value: `${expression.propertyName}: { $gte: ${expression.value.startValue}, $lte: ${expression.value.endValue}}`
            };
        }
    }


    isInvalid(value: any) {
        return value === undefined || value === null || value === '';
    }

    /**
     * @desc Converts nested Object to GraphQL string (Work In Progress)
     * @author Shaiful Bappy
     * @param fields
     * @param isRecursive
     */
    convertObjectToGQLString(fields, isRecursive = false) {
        let gqlString = '';
        const tempArray = [];
        if (fields) {
            Object.keys(fields).map(key => {
                if (Array.isArray(fields[key])) {
                    // If value type is `Array`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: [${this.convertObjectToGQLString(fields[key], true)}]` : `[${this.convertObjectToGQLString(fields[key], true)}]`
                    );
                } else if ((typeof fields[key] === 'object' && fields[key] !== null)) {
                    // If value type is `Object`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: {${this.convertObjectToGQLString(fields[key], true)}}` : `{${this.convertObjectToGQLString(fields[key], true)}}`
                    );

                } else if ((typeof fields[key] === 'boolean') || (typeof fields[key] === 'number')) {
                    // If value type is `Boolean`, do not enclose the value with quote
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: ${fields[key]}` : `${fields[key]}`
                    );
                } else {
                    // If value type is `String` or `Other`, enclose the value with quote
                    if (fields[key] === null) {
                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `${key}: ${fields[key]}` : `${fields[key]}`
                        );
                    } else {
                        // Encode double and single quote characters
                        let stringValue = fields[key];
                        stringValue = stringValue.replace(/\\/g, '\\\\');
                        stringValue = stringValue.replace(/"/g, '\\"');
                        stringValue = stringValue.replace(/'/g, "\'");

                        // encode `newline`
                        stringValue = stringValue.replace(/(?:\r\n|\r|\n)/g, '<br>');

                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `${key}: "${stringValue}"` : `"${stringValue}"`
                        );
                    }
                }
            });

            gqlString = `{${tempArray.join(',')}}`;
        }

        return isRecursive ? tempArray : gqlString;

        /* Alternative solution */
        // const gqlString = JSON.stringify(fields);
        // const gqlString = gqlString.replace(/"([^(")"]+)":/g,"$1:");
        // return gqlString;
    }


    convertObjectToGQLStringForUpdate(fields, propName: string, isRecursive = false) {
        let gqlString = '';
        const tempArray = [];
        if (fields) {
            Object.keys(fields).map(key => {
                if (Array.isArray(fields[key])) {
                    // If value type is `Array`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': [${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}]` : `[${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}]`
                    );
                } else if ((typeof fields[key] === 'object' && fields[key] !== null)) {
                    // If value type is `Object`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': {${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}}` : `{${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}}`
                    );

                } else if ((typeof fields[key] === 'boolean') || (typeof fields[key] === 'number')) {
                    // If value type is `Boolean`, do not enclose the value with quote
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': ${fields[key]}` : `${fields[key]}`
                    );
                } else {
                    // If value type is `String` or `Other`, enclose the value with quote
                    if (fields[key] === null) {
                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `'${propName}${key}': ${fields[key]}` : `${fields[key]}`
                        );
                    } else {
                        // Encode double and single quote characters
                        let stringValue = JSON.stringify(fields[key]);
                        stringValue = stringValue.replace(/\\/g, '\\\\');
                        stringValue = stringValue.replace(/"/g, '\\"');
                        stringValue = stringValue.replace(/'/g, "\'");

                        // encode `newline`
                        stringValue = stringValue.replace(/(?:\r\n|\r|\n)/g, '<br>');

                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `'${propName}${key}': ${stringValue}` : `${stringValue}`
                        );
                    }
                }
            });

            gqlString = `{${tempArray.join(',')}}`;
        }

        return isRecursive ? tempArray : gqlString;

    }
    /**
      * Update Item in array in multilevel
      * @param entityName 
      * @param model // add tags only 
      * @param entityId 
      * @param filter // ParentProperty.ChildItemId 
      * @param pushModal   // update objet which want to update 
      * @param setProp // ParentProperty.$ // add $
      * @param ArrayFilter  // optional 
      */

    prepareNestedArrayUpdateModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter: string) {

        // pushModal = 
        const modelString = this.convertObjectToGQLString(model);
        const updateQuery = this.convertObjectToGQLStringForUpdate(pushModal, `${setProp}`);
        const arrayFilter = ArrayFilter ? `, ArrayFilter: "{arrayFilter:[${ArrayFilter}]}"` : '';
        return gql`
            mutation updateNested {
             ${entityName}(
                Model: ${modelString},
                MutationType: PushUpdate,
                Filter: "{_id:'${entityId}' ${filter ? `${', ' + filter}` : ''}}",
                UpdateQuery: "${updateQuery}"
                ${arrayFilter}
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;
    }

    // prepareNestedObjectUpdateModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string) {
    //     return gql`
    //         mutation updateNested{
    //             ${this.buildNestedObjectUpdate({
    //                 entityName, model, entityId, filter, pushModal, setProp
    //             })}
    //         }
    //     `;
    // }

    buildNestedObjectUpdate(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter: string) {
        const config = {
            entityName, model, entityId, filter, pushModal, setProp, ArrayFilter
        };
        const modelString = this.convertObjectToGQLString(config.model);
        const updateQuery = this.convertObjectToGQLStringForUpdate(config.pushModal, `${config.setProp}`);
        const arrayFilter = config.ArrayFilter ? `, ArrayFilter: "{arrayFilter:[${config.ArrayFilter}]}"` : '';
        return `
                ${config.entityName}(
                    Model: ${modelString},
                    MutationType: PushUpdate,
                    Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
                    UpdateQuery: "${updateQuery}"
                    ${arrayFilter}
                ) {
                ErrorMessage
                Result
                Success
                ValidationResult
                }
            `;
    }

    buildUpdateQuery(entityName, model, entityId) {
        const config = { entityName, model, entityId };
        const modelString = this.convertObjectToGQLString(config.model);
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: "{_id:'${config.entityId}'}"
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }



    //    /**
    //  * Update Item in array in multilevel
    //  * @param entityName 
    //  * @param model // add tags only 
    //  * @param entityId 
    //  * @param filter // ParentProperty.ChildItemId 
    //  * @param pushModal   // update objet which want to update 
    //  * @param setProp // ParentProperty.$ // add $
    //  * @param ArrayFilter  // optional 
    //  */

    // aggregateMutationUpdateNestedArrayModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter: string) {

    //           // pushModal = 
    //           const modelString = this.convertObjectToGQLString(model);
    //           const updateQuery = this.convertObjectToGQLStringForUpdate(pushModal, `${setProp}`);
    //           const arrayFilter = ArrayFilter ? `, ArrayFilter: "{arrayFilter:[${ArrayFilter}]}"` : '';
    //           const mutationQuery =  `mutation updateNested {
    //                ${entityName}(
    //                   Model: ${modelString},
    //                   MutationType: PushUpdate,
    //                   Filter: "{_id:'${entityId}' ${filter ? `${', ' + filter}` : '' }}",
    //                   UpdateQuery: "${updateQuery}"
    //                   ${arrayFilter}
    //                ) {
    //                  ErrorMessage
    //                  Result
    //                  Success
    //                  ValidationResult
    //                }
    //               }
    //           `;
    //           return {
    //             'Uri': ShellDomainProvider['api'].GQLMutationUri,
    //             'Verb': 'Post',
    //             'Payload': JSON.stringify({
    //                 'operationName': 'update',
    //                 'variables': {},
    //                 'query': mutationQuery,
    //             }),
    //             'SuccessIf': '{"data.' + entityName + '.Success":"True"}'
    //         };


    // }

    buildNestedInsertQuery(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter: string) {
        const config = { entityName, model, entityId, filter, pushModal, setProp, ArrayFilter };
        const modelString = this.convertObjectToGQLString(config.model);
        const pushQuery = this.convertObjectToGQLStringForUpdate(config.pushModal, '');
        const arrayFilter = config.ArrayFilter ? `, ArrayFilter: "{${config.ArrayFilter}}"` : '';
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: PushUpdate,
                Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
                PushQuery: "{${config.setProp}: ${pushQuery}}",
                ${arrayFilter}
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }
}
