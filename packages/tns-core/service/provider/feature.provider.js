"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var LocalStroage = require("nativescript-localstorage");
var token_provider_1 = require("./token.provider");
var settings_provider_1 = require("./settings.provider");
var local_stroage_keys_1 = require("../../constraint/local.stroage.keys");
var FeatureProvider = /** @class */ (function () {
    function FeatureProvider(http, tokenProvider, appSettingsProvider) {
        this.http = http;
        this.tokenProvider = tokenProvider;
        this.appSettingsProvider = appSettingsProvider;
        this.getFeatureList = function () {
            var appFeatureList = LocalStroage.getItem(local_stroage_keys_1.LocalStroageKeys.APP_FEATURE_LIST);
            return appFeatureList && JSON.parse(appFeatureList);
        };
        this.setFeatures = function (appFeatureList) {
            LocalStroage.setItem(local_stroage_keys_1.LocalStroageKeys.APP_FEATURE_LIST, JSON.stringify(appFeatureList));
        };
        this.deleteFeatures = function () {
            LocalStroage.removeItem(local_stroage_keys_1.LocalStroageKeys.APP_FEATURE_LIST);
        };
    }
    FeatureProvider.prototype.getFeatures = function () {
        var _this = this;
        var features = this.getFeatureList();
        if (features && features.length > 0) {
            return Promise.resolve(this.getFeatureList());
        }
        else {
            var header = new http_1.HttpHeaders({
                "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
                "Origin": this.appSettingsProvider.getAppSettings().Origin
            });
            return this.http.get(this.appSettingsProvider.getAppSettings().Apps, { headers: header }).toPromise().then(function (response) {
                _this.setFeatures(response);
                return response;
            }, function (err) {
                return new Array([]);
            });
        }
    };
    FeatureProvider.prototype.resetFeatures = function () {
        this.deleteFeatures();
    };
    FeatureProvider = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, token_provider_1.TokenProvider, settings_provider_1.AppSettingsProvider])
    ], FeatureProvider);
    return FeatureProvider;
}());
exports.FeatureProvider = FeatureProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVhdHVyZS5wcm92aWRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZlYXR1cmUucHJvdmlkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQStEO0FBQy9ELHdEQUEwRDtBQUMxRCxtREFBaUQ7QUFDakQseURBQXdEO0FBQ3hELDBFQUFxRTtBQUdyRTtJQUVJLHlCQUFvQixJQUFnQixFQUFVLGFBQTRCLEVBQVUsbUJBQXdDO1FBQXhHLFNBQUksR0FBSixJQUFJLENBQVk7UUFBVSxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUFVLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUEyQnBILG1CQUFjLEdBQUc7WUFDckIsSUFBSSxjQUFjLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxxQ0FBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzdFLE9BQU8sY0FBYyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDO1FBRUssZ0JBQVcsR0FBRyxVQUFDLGNBQW1CO1lBQ3JDLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQzVGLENBQUMsQ0FBQztRQUVLLG1CQUFjLEdBQUc7WUFDcEIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxxQ0FBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQy9ELENBQUMsQ0FBQztJQXJDRixDQUFDO0lBRU0scUNBQVcsR0FBbEI7UUFBQSxpQkFrQkM7UUFqQkcsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXJDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0gsSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO2dCQUMzQixlQUFlLEVBQUUsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxFQUFFO2dCQUNoRSxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU07YUFDN0QsQ0FBQyxDQUFDO1lBRUgsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtnQkFDN0csS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0IsT0FBTyxRQUFRLENBQUM7WUFDcEIsQ0FBQyxFQUFFLFVBQUEsR0FBRztnQkFDRixPQUFPLElBQUksS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRU0sdUNBQWEsR0FBcEI7UUFDSSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQTNCUSxlQUFlO1FBRDNCLGlCQUFVLEVBQUU7eUNBR2lCLGlCQUFVLEVBQXlCLDhCQUFhLEVBQStCLHVDQUFtQjtPQUZuSCxlQUFlLENBeUMzQjtJQUFELHNCQUFDO0NBQUEsQUF6Q0QsSUF5Q0M7QUF6Q1ksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcclxuaW1wb3J0ICogYXMgTG9jYWxTdHJvYWdlIGZyb20gJ25hdGl2ZXNjcmlwdC1sb2NhbHN0b3JhZ2UnO1xyXG5pbXBvcnQgeyBUb2tlblByb3ZpZGVyIH0gZnJvbSBcIi4vdG9rZW4ucHJvdmlkZXJcIjtcclxuaW1wb3J0IHtBcHBTZXR0aW5nc1Byb3ZpZGVyfSBmcm9tIFwiLi9zZXR0aW5ncy5wcm92aWRlclwiO1xyXG5pbXBvcnQge0xvY2FsU3Ryb2FnZUtleXN9IGZyb20gXCIuLi8uLi9jb25zdHJhaW50L2xvY2FsLnN0cm9hZ2Uua2V5c1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRmVhdHVyZVByb3ZpZGVyIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsIHByaXZhdGUgdG9rZW5Qcm92aWRlcjogVG9rZW5Qcm92aWRlciwgcHJpdmF0ZSBhcHBTZXR0aW5nc1Byb3ZpZGVyOiBBcHBTZXR0aW5nc1Byb3ZpZGVyKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEZlYXR1cmVzICgpIHtcclxuICAgICAgICBsZXQgZmVhdHVyZXMgPSB0aGlzLmdldEZlYXR1cmVMaXN0KCk7XHJcblxyXG4gICAgICAgIGlmIChmZWF0dXJlcyAmJiBmZWF0dXJlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodGhpcy5nZXRGZWF0dXJlTGlzdCgpKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBoZWFkZXIgPSBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6IFwiYmVhcmVyIFwiICsgdGhpcy50b2tlblByb3ZpZGVyLmdldEFjY2Vzc1Rva2VuKCksXHJcbiAgICAgICAgICAgICAgICBcIk9yaWdpblwiOiB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5PcmlnaW5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuZ2V0QXBwU2V0dGluZ3MoKS5BcHBzLCB7aGVhZGVyczogaGVhZGVyfSkudG9Qcm9taXNlKCkudGhlbihyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldEZlYXR1cmVzKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuICAgICAgICAgICAgfSwgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQXJyYXkoW10pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlc2V0RmVhdHVyZXMoKSB7XHJcbiAgICAgICAgdGhpcy5kZWxldGVGZWF0dXJlcygpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RmVhdHVyZUxpc3QgPSAoKTogYW55IFtdID0+IHtcclxuICAgICAgICBsZXQgYXBwRmVhdHVyZUxpc3QgPSBMb2NhbFN0cm9hZ2UuZ2V0SXRlbShMb2NhbFN0cm9hZ2VLZXlzLkFQUF9GRUFUVVJFX0xJU1QpO1xyXG4gICAgICAgIHJldHVybiBhcHBGZWF0dXJlTGlzdCAmJiBKU09OLnBhcnNlKGFwcEZlYXR1cmVMaXN0KTtcclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIHNldEZlYXR1cmVzID0gKGFwcEZlYXR1cmVMaXN0OiBhbnkpID0+IHtcclxuICAgICAgICBMb2NhbFN0cm9hZ2Uuc2V0SXRlbShMb2NhbFN0cm9hZ2VLZXlzLkFQUF9GRUFUVVJFX0xJU1QsIEpTT04uc3RyaW5naWZ5KGFwcEZlYXR1cmVMaXN0KSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHB1YmxpYyBkZWxldGVGZWF0dXJlcyA9ICgpID0+IHtcclxuICAgICAgICBMb2NhbFN0cm9hZ2UucmVtb3ZlSXRlbShMb2NhbFN0cm9hZ2VLZXlzLkFQUF9GRUFUVVJFX0xJU1QpO1xyXG4gICAgfTtcclxufVxyXG4iXX0=