import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Injectable, Injector} from '@angular/core';
import {NgModule} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import * as LocalStroage from 'nativescript-localstorage';
import {AppSettingsProvider} from "./settings.provider";
import {LocalStroageKeys} from "../../constraint/local.stroage.keys";

@NgModule()
@Injectable()
export class TokenProvider {
    gettingAccessToken: boolean;

    constructor(private injector: Injector, private appSettingsProvider: AppSettingsProvider) {
    }

    public getAccessToken() {
        return LocalStroage.getItem(LocalStroageKeys.ACCESS_TOKEN);
    }

    public getRefreshToken() {
        return LocalStroage.getItem(LocalStroageKeys.REFRESH_TOKEN);
    }

    public setAccessToken(token) {
        LocalStroage.setItem(LocalStroageKeys.ACCESS_TOKEN, token);
    }

    public setRefreshToken(token) {
        LocalStroage.setItem(LocalStroageKeys.REFRESH_TOKEN, token);
    }

    public requestHeader() {
        new HttpHeaders({
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + this.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
    }

    public refreshTokenObservable(): Observable<any> {
        const http = this.injector.get(HttpClient);
        const body = new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', this.appSettingsProvider.getAppSettings().TenantId)
            .set('refresh_token', this.getRefreshToken());

        const header = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });

        return http.post(
            this.appSettingsProvider.getAppSettings().Token,
            body.toString(),
            {headers: header}
        ).pipe(tap((response: any) => {
            console.log('refreshTokenObservable');
            if (response && response.access_token) {
                this.setAccessToken(response.access_token);
            }
            this.gettingAccessToken = false;
        }));
    }

    public getAnnonymousToken(): Promise<any> {
        const http = this.injector.get(HttpClient);
        const body = new HttpParams().set('grant_type', 'authenticate_site'); // authenticate_site/client_credentials
        const header = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });

        return http.post(
            this.appSettingsProvider.getAppSettings().Token,
            body.toString(),
            {headers: header}
        ).toPromise();
    }
}
