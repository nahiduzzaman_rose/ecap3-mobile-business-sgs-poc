"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LocalStroage = require("nativescript-localstorage");
var local_stroage_keys_1 = require("../../constraint/local.stroage.keys");
var AppSettingsProvider = /** @class */ (function () {
    function AppSettingsProvider() {
        var _this = this;
        this.getAppSettingsData = function (appSettings) {
            if (appSettings == null || appSettings == '') {
                var appSettingsData = {
                    Apps: "",
                    Token: "",
                    Security: "",
                    Identity: "",
                    Navigation: "",
                    TenantId: "",
                    Origin: "",
                    DomainName: "",
                    SubDomainName: "",
                    Authentication: "",
                    DataCore: "",
                    IsMock: false
                };
                console.warn("AppSettings Data Not Found ", appSettings);
                return appSettingsData;
            }
            return (appSettings && JSON.parse(appSettings));
        };
        this.getAppSettings = function () {
            var appSettings = LocalStroage.getItem(local_stroage_keys_1.LocalStroageKeys.APP_SETTINGS);
            return _this.getAppSettingsData(appSettings);
        };
        this.setAppSettings = function (appSetting) {
            LocalStroage.setItem(local_stroage_keys_1.LocalStroageKeys.APP_SETTINGS, JSON.stringify(appSetting));
        };
        this.removeAppSettings = function () {
            LocalStroage.removeItem(local_stroage_keys_1.LocalStroageKeys.APP_SETTINGS);
        };
    }
    AppSettingsProvider = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], AppSettingsProvider);
    return AppSettingsProvider;
}());
exports.AppSettingsProvider = AppSettingsProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MucHJvdmlkZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXR0aW5ncy5wcm92aWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyx3REFBMEQ7QUFFMUQsMEVBQXFFO0FBR3JFO0lBRUk7UUFBQSxpQkFFQztRQUVPLHVCQUFrQixHQUFHLFVBQUMsV0FBZ0I7WUFDMUMsSUFBRyxXQUFXLElBQUksSUFBSSxJQUFJLFdBQVcsSUFBSSxFQUFFLEVBQUU7Z0JBQ3pDLElBQUksZUFBZSxHQUFrQjtvQkFDakMsSUFBSSxFQUFFLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsUUFBUSxFQUFFLEVBQUU7b0JBQ1osUUFBUSxFQUFFLEVBQUU7b0JBQ1osVUFBVSxFQUFFLEVBQUU7b0JBQ2QsUUFBUSxFQUFFLEVBQUU7b0JBQ1osTUFBTSxFQUFFLEVBQUU7b0JBQ1YsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsYUFBYSxFQUFFLEVBQUU7b0JBQ2pCLGNBQWMsRUFBRSxFQUFFO29CQUNsQixRQUFRLEVBQUUsRUFBRTtvQkFDWixNQUFNLEVBQUUsS0FBSztpQkFDaEIsQ0FBQTtnQkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUV6RCxPQUFPLGVBQWUsQ0FBQzthQUMxQjtZQUVELE9BQU8sQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBaUIsQ0FBQztRQUNwRSxDQUFDLENBQUE7UUFFTSxtQkFBYyxHQUFHO1lBQ3BCLElBQUksV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEUsT0FBTyxLQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDO1FBRUssbUJBQWMsR0FBRyxVQUFDLFVBQXdCO1lBQzdDLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNwRixDQUFDLENBQUM7UUFFSyxzQkFBaUIsR0FBRztZQUN2QixZQUFZLENBQUMsVUFBVSxDQUFDLHFDQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzNELENBQUMsQ0FBQztJQXJDRixDQUFDO0lBSlEsbUJBQW1CO1FBRC9CLGlCQUFVLEVBQUU7O09BQ0EsbUJBQW1CLENBMEMvQjtJQUFELDBCQUFDO0NBQUEsQUExQ0QsSUEwQ0M7QUExQ1ksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCAqIGFzIExvY2FsU3Ryb2FnZSBmcm9tICduYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlJztcclxuaW1wb3J0IHsgSUFwcFNldHRpbmdzIH0gZnJvbSAnLi4vLi4vbW9kZWwvYXBwLnNldHRpbmdzJztcclxuaW1wb3J0IHtMb2NhbFN0cm9hZ2VLZXlzfSBmcm9tIFwiLi4vLi4vY29uc3RyYWludC9sb2NhbC5zdHJvYWdlLmtleXNcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcFNldHRpbmdzUHJvdmlkZXIge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEFwcFNldHRpbmdzRGF0YSA9IChhcHBTZXR0aW5nczogYW55KTogSUFwcFNldHRpbmdzID0+IHtcclxuICAgICAgICBpZihhcHBTZXR0aW5ncyA9PSBudWxsIHx8IGFwcFNldHRpbmdzID09ICcnKSB7XHJcbiAgICAgICAgICAgIGxldCBhcHBTZXR0aW5nc0RhdGEgOiBJQXBwU2V0dGluZ3MgPSB7XHJcbiAgICAgICAgICAgICAgICBBcHBzOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgVG9rZW46IFwiXCIsXHJcbiAgICAgICAgICAgICAgICBTZWN1cml0eTogXCJcIixcclxuICAgICAgICAgICAgICAgIElkZW50aXR5OiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgTmF2aWdhdGlvbjogXCJcIixcclxuICAgICAgICAgICAgICAgIFRlbmFudElkOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgT3JpZ2luOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgRG9tYWluTmFtZTogXCJcIixcclxuICAgICAgICAgICAgICAgIFN1YkRvbWFpbk5hbWU6IFwiXCIsXHJcbiAgICAgICAgICAgICAgICBBdXRoZW50aWNhdGlvbjogXCJcIixcclxuICAgICAgICAgICAgICAgIERhdGFDb3JlOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgSXNNb2NrOiBmYWxzZVxyXG4gICAgICAgICAgICB9ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihcIkFwcFNldHRpbmdzIERhdGEgTm90IEZvdW5kIFwiLCBhcHBTZXR0aW5ncyk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYXBwU2V0dGluZ3NEYXRhO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIChhcHBTZXR0aW5ncyAmJiBKU09OLnBhcnNlKGFwcFNldHRpbmdzKSkgYXMgSUFwcFNldHRpbmdzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBcHBTZXR0aW5ncyA9ICgpOiBJQXBwU2V0dGluZ3MgPT4ge1xyXG4gICAgICAgIGxldCBhcHBTZXR0aW5ncyA9IExvY2FsU3Ryb2FnZS5nZXRJdGVtKExvY2FsU3Ryb2FnZUtleXMuQVBQX1NFVFRJTkdTKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRBcHBTZXR0aW5nc0RhdGEoYXBwU2V0dGluZ3MpO1xyXG4gICAgfTtcclxuXHJcbiAgICBwdWJsaWMgc2V0QXBwU2V0dGluZ3MgPSAoYXBwU2V0dGluZzogSUFwcFNldHRpbmdzKSA9PiB7XHJcbiAgICAgICAgTG9jYWxTdHJvYWdlLnNldEl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5BUFBfU0VUVElOR1MsIEpTT04uc3RyaW5naWZ5KGFwcFNldHRpbmcpKTtcclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIHJlbW92ZUFwcFNldHRpbmdzID0gKCkgPT4ge1xyXG4gICAgICAgIExvY2FsU3Ryb2FnZS5yZW1vdmVJdGVtKExvY2FsU3Ryb2FnZUtleXMuQVBQX1NFVFRJTkdTKTtcclxuICAgIH07XHJcbn1cclxuIl19