import { BehaviorSubject } from 'rxjs';
import { Injectable, NgModule} from '@angular/core';
import { FeatureProvider } from './feature.provider';

@Injectable()
@NgModule()
export class AppProvider {
    private apps: any[];
    private appsObservable: BehaviorSubject<any[]>;

    constructor(private featureProvider: FeatureProvider) {
        this.appsObservable = new BehaviorSubject([]);
    }

    public getCurrentAppObserable() {
        return this.appsObservable;
    }

    public setCurrentApps(apps) {
        this.apps = apps;
    }

    public assignAppsVisibility() {
        this.featureProvider.getFeatures().then((response: any) => {
            const featuresFlatMap = response.map((element: any) => element.Name);
            for (let index = 0; index < this.apps.length; index++) {
                const app = this.apps[index];
                if (app.path === '' || (app.data && featuresFlatMap.indexOf(app.data.requiredFeature) === -1)) {
                    app.data.isVisible = false;
                } else {
                    app.data.isVisible = true;
                }
            }
            this.appsObservable.next(this.apps);
        });
    }

    public getApps() {
        return this.apps;
    }
}
