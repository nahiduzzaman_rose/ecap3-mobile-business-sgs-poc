import { BehaviorSubject } from 'rxjs';
import { Injectable, NgModule } from '@angular/core';
import { Router,  NavigationCancel } from '@angular/router';
import { FeatureProvider } from './feature.provider';

@Injectable()
@NgModule()
export class NavigationProvider {
    private navigations: any[];
    private navigationsObservable: BehaviorSubject<any[]>;
    private lastCancelledNavigationUrl: string;
    private lastCancelledNavigationUrlCustom:string;

    constructor(private featureProvider: FeatureProvider, private router: Router) {
        // console.log('=====NavigationProvider INITIALIZED=====');
        this.lastCancelledNavigationUrl = '';
        this.navigationsObservable = new BehaviorSubject([]);

        this.router.events.subscribe(event => {
            if (event instanceof NavigationCancel) {
                // console.log('Navigation cancelation detected', event);
                this.lastCancelledNavigationUrl = event.url;
            }
        });
    }

    public setCustomLastCancelledNavigation(redirecUrl:string) {
        this.lastCancelledNavigationUrlCustom = redirecUrl;
    }
    public gettCustomLastCancelledNavigation():string {
        const tempUrlForCustomRedirectNavigation = this.lastCancelledNavigationUrlCustom;
        this.lastCancelledNavigationUrlCustom = '';
        return tempUrlForCustomRedirectNavigation;
    }

    public getCurrentNavigationObservable() {
        return this.navigationsObservable;
        //return this.navigations;
    }

    public setCurrentNavigations(navigations) {
        this.navigations = navigations;
        this.assignNavigationsVisibility();
    }

    public assignNavigationsVisibility() {
        // tslint:disable-next-line:no-debugger
        this.featureProvider.getFeatures().then((response: any) => {
            const featuresFlatMap = response.map((element: any) => element.Name);
            this.filterNavigations(this.navigations, featuresFlatMap);
            this.navigationsObservable.next(this.navigations);
        });
    }

    public filterNavigations(navigations: any[], featuresFlatMap: any[]) {
        for (let index = 0; index < navigations.length; index++) {
            if (navigations[index].type === 'collapse' || navigations[index].type === 'group') {
                if (featuresFlatMap.indexOf(navigations[index].id) === -1) {
                    navigations[index].isVisible = false;
                } else {
                    this.filterNavigations(navigations[index].children, featuresFlatMap);
                    //need to be refactor
                    navigations[index].isVisible = true;
                }
            } else if (featuresFlatMap.indexOf(navigations[index].id) === -1) {
                // navigations.splice(index, 1);
                navigations[index].isVisible = false;
            } else {
                navigations[index].isVisible = true;
            }
        }
    }

    public getApps() {
        return this.navigations;
    }

    public getLastCancelledNavigation(): string {
        const tempUrl = this.lastCancelledNavigationUrl;
        this.lastCancelledNavigationUrl = '';
        return tempUrl;
    }

    public resetLastCancelledNavigation() {
        this.lastCancelledNavigationUrl = '';
    }
}
