import { Injectable } from "@angular/core";
import * as LocalStroage from 'nativescript-localstorage';
import { IAppSettings } from '../../model/app.settings';
import {LocalStroageKeys} from "../../constraint/local.stroage.keys";

@Injectable()
export class AppSettingsProvider {

    constructor() {

    }

    private getAppSettingsData = (appSettings: any): IAppSettings => {
        if(appSettings == null || appSettings == '') {
            let appSettingsData : IAppSettings = {
                Apps: "",
                Token: "",
                Security: "",
                Captcha: "",
                Identity: "",
                Navigation: "",
                TenantId: "",
                Origin: "",
                DomainName: "",
                SubDomainName: "",
                Authentication: "",
                DataCore: "",
                IsMock: false,
                LocalDBName: "",
                Storage: "",
                SyncUrl: "",
                GQLQueryUri: "",
                GQLMutationUri: "",
                SyncAdapterId: "",
                ClmApi: "",
                ScertBusinessService: ""
            };
            console.warn("AppSettings Data Not Found ", appSettings);

            return appSettingsData;
        }

        return (appSettings && JSON.parse(appSettings)) as IAppSettings;
    }

    public getAppSettings = (): IAppSettings => {
        let appSettings = LocalStroage.getItem(LocalStroageKeys.APP_SETTINGS);
        return this.getAppSettingsData(appSettings);
    };

    public setAppSettings = (appSetting: IAppSettings) => {
        console.log('App settings called');
        LocalStroage.setItem(LocalStroageKeys.APP_SETTINGS, JSON.stringify(appSetting));
    };

    public removeAppSettings = () => {
        LocalStroage.removeItem(LocalStroageKeys.APP_SETTINGS);
    };
}
