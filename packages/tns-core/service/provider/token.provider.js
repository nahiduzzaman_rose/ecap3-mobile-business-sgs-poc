"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var operators_1 = require("rxjs/operators");
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var http_1 = require("@angular/common/http");
var LocalStroage = require("nativescript-localstorage");
var settings_provider_1 = require("./settings.provider");
var local_stroage_keys_1 = require("../../constraint/local.stroage.keys");
var TokenProvider = /** @class */ (function () {
    function TokenProvider(injector, appSettingsProvider) {
        this.injector = injector;
        this.appSettingsProvider = appSettingsProvider;
    }
    TokenProvider.prototype.getAccessToken = function () {
        return LocalStroage.getItem(local_stroage_keys_1.LocalStroageKeys.ACCESS_TOKEN);
    };
    TokenProvider.prototype.getRefreshToken = function () {
        return LocalStroage.getItem(local_stroage_keys_1.LocalStroageKeys.REFRESH_TOKEN);
    };
    TokenProvider.prototype.setAccessToken = function (token) {
        LocalStroage.setItem(local_stroage_keys_1.LocalStroageKeys.ACCESS_TOKEN, token);
    };
    TokenProvider.prototype.setRefreshToken = function (token) {
        LocalStroage.setItem(local_stroage_keys_1.LocalStroageKeys.REFRESH_TOKEN, token);
    };
    TokenProvider.prototype.requestHeader = function () {
        new http_1.HttpHeaders({
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + this.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
    };
    TokenProvider.prototype.refreshTokenObservable = function () {
        var _this = this;
        var http = this.injector.get(http_1.HttpClient);
        var body = new http_1.HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', this.appSettingsProvider.getAppSettings().TenantId)
            .set('refresh_token', this.getRefreshToken());
        var header = new http_1.HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
        return http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), { headers: header }).pipe(operators_1.tap(function (response) {
            console.log('refreshTokenObservable');
            if (response && response.access_token) {
                _this.setAccessToken(response.access_token);
            }
            _this.gettingAccessToken = false;
        }));
    };
    TokenProvider.prototype.getAnnonymousToken = function () {
        var http = this.injector.get(http_1.HttpClient);
        var body = new http_1.HttpParams().set('grant_type', 'authenticate_site'); // authenticate_site/client_credentials
        var header = new http_1.HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
        return http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), { headers: header }).toPromise();
    };
    TokenProvider = __decorate([
        core_2.NgModule(),
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_1.Injector, settings_provider_1.AppSettingsProvider])
    ], TokenProvider);
    return TokenProvider;
}());
exports.TokenProvider = TokenProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9rZW4ucHJvdmlkZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0b2tlbi5wcm92aWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDRDQUFtQztBQUNuQyxzQ0FBbUQ7QUFDbkQsc0NBQXVDO0FBQ3ZDLDZDQUF1RjtBQUN2Rix3REFBMEQ7QUFDMUQseURBQXdEO0FBQ3hELDBFQUFxRTtBQUlyRTtJQUdJLHVCQUFvQixRQUFrQixFQUFVLG1CQUF3QztRQUFwRSxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQVUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUN4RixDQUFDO0lBRU0sc0NBQWMsR0FBckI7UUFDSSxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVNLHVDQUFlLEdBQXRCO1FBQ0ksT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLHFDQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFTSxzQ0FBYyxHQUFyQixVQUFzQixLQUFLO1FBQ3ZCLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFTSx1Q0FBZSxHQUF0QixVQUF1QixLQUFLO1FBQ3hCLFlBQVksQ0FBQyxPQUFPLENBQUMscUNBQWdCLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFTSxxQ0FBYSxHQUFwQjtRQUNJLElBQUksa0JBQVcsQ0FBQztZQUNaLGNBQWMsRUFBRSxrQkFBa0I7WUFDbEMsZUFBZSxFQUFFLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ2xELFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsTUFBTTtTQUM3RCxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sOENBQXNCLEdBQTdCO1FBQUEsaUJBdUJDO1FBdEJHLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGlCQUFVLENBQUMsQ0FBQztRQUMzQyxJQUFNLElBQUksR0FBRyxJQUFJLGlCQUFVLEVBQUU7YUFDeEIsR0FBRyxDQUFDLFlBQVksRUFBRSxlQUFlLENBQUM7YUFDbEMsR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxDQUFDO2FBQ3BFLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7UUFFbEQsSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzNCLGNBQWMsRUFBRSxtQ0FBbUM7WUFDbkQsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNO1NBQzdELENBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsS0FBSyxFQUMvQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQ2YsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFDLENBQ3BCLENBQUMsSUFBSSxDQUFDLGVBQUcsQ0FBQyxVQUFDLFFBQWE7WUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3RDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLEVBQUU7Z0JBQ25DLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVNLDBDQUFrQixHQUF6QjtRQUNJLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGlCQUFVLENBQUMsQ0FBQztRQUMzQyxJQUFNLElBQUksR0FBRyxJQUFJLGlCQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLG1CQUFtQixDQUFDLENBQUMsQ0FBQyx1Q0FBdUM7UUFDN0csSUFBTSxNQUFNLEdBQUcsSUFBSSxrQkFBVyxDQUFDO1lBQzNCLGNBQWMsRUFBRSxtQ0FBbUM7WUFDbkQsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNO1NBQzdELENBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLENBQUMsS0FBSyxFQUMvQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQ2YsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFDLENBQ3BCLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQXBFUSxhQUFhO1FBRnpCLGVBQVEsRUFBRTtRQUNWLGlCQUFVLEVBQUU7eUNBSXFCLGVBQVEsRUFBK0IsdUNBQW1CO09BSC9FLGFBQWEsQ0FxRXpCO0lBQUQsb0JBQUM7Q0FBQSxBQXJFRCxJQXFFQztBQXJFWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7dGFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7SW5qZWN0YWJsZSwgSW5qZWN0b3J9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFBhcmFtcywgSHR0cFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCAqIGFzIExvY2FsU3Ryb2FnZSBmcm9tICduYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlJztcclxuaW1wb3J0IHtBcHBTZXR0aW5nc1Byb3ZpZGVyfSBmcm9tIFwiLi9zZXR0aW5ncy5wcm92aWRlclwiO1xyXG5pbXBvcnQge0xvY2FsU3Ryb2FnZUtleXN9IGZyb20gXCIuLi8uLi9jb25zdHJhaW50L2xvY2FsLnN0cm9hZ2Uua2V5c1wiO1xyXG5cclxuQE5nTW9kdWxlKClcclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVG9rZW5Qcm92aWRlciB7XHJcbiAgICBnZXR0aW5nQWNjZXNzVG9rZW46IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsIHByaXZhdGUgYXBwU2V0dGluZ3NQcm92aWRlcjogQXBwU2V0dGluZ3NQcm92aWRlcikge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBY2Nlc3NUb2tlbigpIHtcclxuICAgICAgICByZXR1cm4gTG9jYWxTdHJvYWdlLmdldEl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5BQ0NFU1NfVE9LRU4pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRSZWZyZXNoVG9rZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIExvY2FsU3Ryb2FnZS5nZXRJdGVtKExvY2FsU3Ryb2FnZUtleXMuUkVGUkVTSF9UT0tFTik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldEFjY2Vzc1Rva2VuKHRva2VuKSB7XHJcbiAgICAgICAgTG9jYWxTdHJvYWdlLnNldEl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5BQ0NFU1NfVE9LRU4sIHRva2VuKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0UmVmcmVzaFRva2VuKHRva2VuKSB7XHJcbiAgICAgICAgTG9jYWxTdHJvYWdlLnNldEl0ZW0oTG9jYWxTdHJvYWdlS2V5cy5SRUZSRVNIX1RPS0VOLCB0b2tlbik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlcXVlc3RIZWFkZXIoKSB7XHJcbiAgICAgICAgbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgICdBdXRob3JpemF0aW9uJzogJ2JlYXJlciAnICsgdGhpcy5nZXRBY2Nlc3NUb2tlbigpLFxyXG4gICAgICAgICAgICAnT3JpZ2luJzogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlZnJlc2hUb2tlbk9ic2VydmFibGUoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBodHRwID0gdGhpcy5pbmplY3Rvci5nZXQoSHR0cENsaWVudCk7XHJcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBIdHRwUGFyYW1zKClcclxuICAgICAgICAgICAgLnNldCgnZ3JhbnRfdHlwZScsICdyZWZyZXNoX3Rva2VuJylcclxuICAgICAgICAgICAgLnNldCgnY2xpZW50X2lkJywgdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuVGVuYW50SWQpXHJcbiAgICAgICAgICAgIC5zZXQoJ3JlZnJlc2hfdG9rZW4nLCB0aGlzLmdldFJlZnJlc2hUb2tlbigpKTtcclxuXHJcbiAgICAgICAgY29uc3QgaGVhZGVyID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLFxyXG4gICAgICAgICAgICAnT3JpZ2luJzogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBodHRwLnBvc3QoXHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2V0dGluZ3NQcm92aWRlci5nZXRBcHBTZXR0aW5ncygpLlRva2VuLFxyXG4gICAgICAgICAgICBib2R5LnRvU3RyaW5nKCksXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiBoZWFkZXJ9XHJcbiAgICAgICAgKS5waXBlKHRhcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygncmVmcmVzaFRva2VuT2JzZXJ2YWJsZScpO1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuYWNjZXNzX3Rva2VuKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldEFjY2Vzc1Rva2VuKHJlc3BvbnNlLmFjY2Vzc190b2tlbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5nZXR0aW5nQWNjZXNzVG9rZW4gPSBmYWxzZTtcclxuICAgICAgICB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEFubm9ueW1vdXNUb2tlbigpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHAgPSB0aGlzLmluamVjdG9yLmdldChIdHRwQ2xpZW50KTtcclxuICAgICAgICBjb25zdCBib2R5ID0gbmV3IEh0dHBQYXJhbXMoKS5zZXQoJ2dyYW50X3R5cGUnLCAnYXV0aGVudGljYXRlX3NpdGUnKTsgLy8gYXV0aGVudGljYXRlX3NpdGUvY2xpZW50X2NyZWRlbnRpYWxzXHJcbiAgICAgICAgY29uc3QgaGVhZGVyID0gbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLFxyXG4gICAgICAgICAgICAnT3JpZ2luJzogdGhpcy5hcHBTZXR0aW5nc1Byb3ZpZGVyLmdldEFwcFNldHRpbmdzKCkuT3JpZ2luXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBodHRwLnBvc3QoXHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2V0dGluZ3NQcm92aWRlci5nZXRBcHBTZXR0aW5ncygpLlRva2VuLFxyXG4gICAgICAgICAgICBib2R5LnRvU3RyaW5nKCksXHJcbiAgICAgICAgICAgIHtoZWFkZXJzOiBoZWFkZXJ9XHJcbiAgICAgICAgKS50b1Byb21pc2UoKTtcclxuICAgIH1cclxufVxyXG4iXX0=