"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
var feature_provider_1 = require("./feature.provider");
var AppProvider = /** @class */ (function () {
    function AppProvider(featureProvider) {
        this.featureProvider = featureProvider;
        this.appsObservable = new rxjs_1.BehaviorSubject([]);
    }
    AppProvider.prototype.getCurrentAppObserable = function () {
        return this.appsObservable;
    };
    AppProvider.prototype.setCurrentApps = function (apps) {
        this.apps = apps;
    };
    AppProvider.prototype.assignAppsVisibility = function () {
        var _this = this;
        this.featureProvider.getFeatures().then(function (response) {
            var featuresFlatMap = response.map(function (element) { return element.Name; });
            for (var index = 0; index < _this.apps.length; index++) {
                var app = _this.apps[index];
                if (app.path === '' || (app.data && featuresFlatMap.indexOf(app.data.requiredFeature) === -1)) {
                    app.data.isVisible = false;
                }
                else {
                    app.data.isVisible = true;
                }
            }
            _this.appsObservable.next(_this.apps);
        });
    };
    AppProvider.prototype.getApps = function () {
        return this.apps;
    };
    AppProvider = __decorate([
        core_1.Injectable(),
        core_1.NgModule(),
        __metadata("design:paramtypes", [feature_provider_1.FeatureProvider])
    ], AppProvider);
    return AppProvider;
}());
exports.AppProvider = AppProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnByb3ZpZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnByb3ZpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNkJBQXVDO0FBQ3ZDLHNDQUFvRDtBQUNwRCx1REFBcUQ7QUFJckQ7SUFJSSxxQkFBb0IsZUFBZ0M7UUFBaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxzQkFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSw0Q0FBc0IsR0FBN0I7UUFDSSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVNLG9DQUFjLEdBQXJCLFVBQXNCLElBQUk7UUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVNLDBDQUFvQixHQUEzQjtRQUFBLGlCQWFDO1FBWkcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFhO1lBQ2xELElBQU0sZUFBZSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQyxPQUFZLElBQUssT0FBQSxPQUFPLENBQUMsSUFBSSxFQUFaLENBQVksQ0FBQyxDQUFDO1lBQ3JFLEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDbkQsSUFBTSxHQUFHLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQzNGLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztpQkFDOUI7cUJBQU07b0JBQ0gsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2lCQUM3QjthQUNKO1lBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDZCQUFPLEdBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQWpDUSxXQUFXO1FBRnZCLGlCQUFVLEVBQUU7UUFDWixlQUFRLEVBQUU7eUNBSzhCLGtDQUFlO09BSjNDLFdBQVcsQ0FrQ3ZCO0lBQUQsa0JBQUM7Q0FBQSxBQWxDRCxJQWtDQztBQWxDWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZlYXR1cmVQcm92aWRlciB9IGZyb20gJy4vZmVhdHVyZS5wcm92aWRlcic7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbkBOZ01vZHVsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBQcm92aWRlciB7XHJcbiAgICBwcml2YXRlIGFwcHM6IGFueVtdO1xyXG4gICAgcHJpdmF0ZSBhcHBzT2JzZXJ2YWJsZTogQmVoYXZpb3JTdWJqZWN0PGFueVtdPjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZlYXR1cmVQcm92aWRlcjogRmVhdHVyZVByb3ZpZGVyKSB7XHJcbiAgICAgICAgdGhpcy5hcHBzT2JzZXJ2YWJsZSA9IG5ldyBCZWhhdmlvclN1YmplY3QoW10pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRDdXJyZW50QXBwT2JzZXJhYmxlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcHNPYnNlcnZhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRDdXJyZW50QXBwcyhhcHBzKSB7XHJcbiAgICAgICAgdGhpcy5hcHBzID0gYXBwcztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXNzaWduQXBwc1Zpc2liaWxpdHkoKSB7XHJcbiAgICAgICAgdGhpcy5mZWF0dXJlUHJvdmlkZXIuZ2V0RmVhdHVyZXMoKS50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZlYXR1cmVzRmxhdE1hcCA9IHJlc3BvbnNlLm1hcCgoZWxlbWVudDogYW55KSA9PiBlbGVtZW50Lk5hbWUpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5hcHBzLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYXBwID0gdGhpcy5hcHBzW2luZGV4XTtcclxuICAgICAgICAgICAgICAgIGlmIChhcHAucGF0aCA9PT0gJycgfHwgKGFwcC5kYXRhICYmIGZlYXR1cmVzRmxhdE1hcC5pbmRleE9mKGFwcC5kYXRhLnJlcXVpcmVkRmVhdHVyZSkgPT09IC0xKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcC5kYXRhLmlzVmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBhcHAuZGF0YS5pc1Zpc2libGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuYXBwc09ic2VydmFibGUubmV4dCh0aGlzLmFwcHMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBcHBzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcHM7XHJcbiAgICB9XHJcbn1cclxuIl19