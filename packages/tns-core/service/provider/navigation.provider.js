"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var feature_provider_1 = require("./feature.provider");
var NavigationProvider = /** @class */ (function () {
    function NavigationProvider(featureProvider, router) {
        var _this = this;
        this.featureProvider = featureProvider;
        this.router = router;
        // console.log('=====NavigationProvider INITIALIZED=====');
        this.lastCancelledNavigationUrl = '';
        this.navigationsObservable = new rxjs_1.BehaviorSubject([]);
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationCancel) {
                // console.log('Navigation cancelation detected', event);
                _this.lastCancelledNavigationUrl = event.url;
            }
        });
    }
    NavigationProvider.prototype.setCustomLastCancelledNavigation = function (redirecUrl) {
        this.lastCancelledNavigationUrlCustom = redirecUrl;
    };
    NavigationProvider.prototype.gettCustomLastCancelledNavigation = function () {
        var tempUrlForCustomRedirectNavigation = this.lastCancelledNavigationUrlCustom;
        this.lastCancelledNavigationUrlCustom = '';
        return tempUrlForCustomRedirectNavigation;
    };
    NavigationProvider.prototype.getCurrentNavigationObserable = function () {
        return this.navigationsObservable;
        //return this.navigations;
    };
    NavigationProvider.prototype.setCurrentNavigations = function (navigations) {
        this.navigations = navigations;
        this.assignNavigationsVisibility();
    };
    NavigationProvider.prototype.assignNavigationsVisibility = function () {
        var _this = this;
        // tslint:disable-next-line:no-debugger
        this.featureProvider.getFeatures().then(function (response) {
            var featuresFlatMap = response.map(function (element) { return element.Name; });
            _this.filterNavigations(_this.navigations, featuresFlatMap);
            _this.navigationsObservable.next(_this.navigations);
        });
    };
    NavigationProvider.prototype.filterNavigations = function (navigations, featuresFlatMap) {
        for (var index = 0; index < navigations.length; index++) {
            if (navigations[index].type === 'collapse' || navigations[index].type === 'group') {
                if (featuresFlatMap.indexOf(navigations[index].id) === -1) {
                    navigations[index].isVisible = false;
                }
                else {
                    this.filterNavigations(navigations[index].children, featuresFlatMap);
                    //need to be refactor
                    navigations[index].isVisible = true;
                }
            }
            else if (featuresFlatMap.indexOf(navigations[index].id) === -1) {
                // navigations.splice(index, 1);
                navigations[index].isVisible = false;
            }
            else {
                navigations[index].isVisible = true;
            }
        }
    };
    NavigationProvider.prototype.getApps = function () {
        return this.navigations;
    };
    NavigationProvider.prototype.getLastCancelledNavigation = function () {
        var tempUrl = this.lastCancelledNavigationUrl;
        this.lastCancelledNavigationUrl = '';
        return tempUrl;
    };
    NavigationProvider.prototype.resetLastCancelledNavigation = function () {
        this.lastCancelledNavigationUrl = '';
    };
    NavigationProvider = __decorate([
        core_1.Injectable(),
        core_1.NgModule(),
        __metadata("design:paramtypes", [feature_provider_1.FeatureProvider, router_1.Router])
    ], NavigationProvider);
    return NavigationProvider;
}());
exports.NavigationProvider = NavigationProvider;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5wcm92aWRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5hdmlnYXRpb24ucHJvdmlkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2QkFBdUM7QUFDdkMsc0NBQXFEO0FBQ3JELDBDQUE0RDtBQUM1RCx1REFBcUQ7QUFJckQ7SUFNSSw0QkFBb0IsZUFBZ0MsRUFBVSxNQUFjO1FBQTVFLGlCQVdDO1FBWG1CLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDeEUsMkRBQTJEO1FBQzNELElBQUksQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksc0JBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVyRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLO1lBQzlCLElBQUksS0FBSyxZQUFZLHlCQUFnQixFQUFFO2dCQUNuQyx5REFBeUQ7Z0JBQ3pELEtBQUksQ0FBQywwQkFBMEIsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO2FBQy9DO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sNkRBQWdDLEdBQXZDLFVBQXdDLFVBQWlCO1FBQ3JELElBQUksQ0FBQyxnQ0FBZ0MsR0FBRyxVQUFVLENBQUM7SUFDdkQsQ0FBQztJQUNNLDhEQUFpQyxHQUF4QztRQUNJLElBQU0sa0NBQWtDLEdBQUcsSUFBSSxDQUFDLGdDQUFnQyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxnQ0FBZ0MsR0FBRyxFQUFFLENBQUM7UUFDM0MsT0FBTyxrQ0FBa0MsQ0FBQztJQUM5QyxDQUFDO0lBRU0sMERBQTZCLEdBQXBDO1FBQ0ksT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7UUFDbEMsMEJBQTBCO0lBQzlCLENBQUM7SUFFTSxrREFBcUIsR0FBNUIsVUFBNkIsV0FBVztRQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRU0sd0RBQTJCLEdBQWxDO1FBQUEsaUJBT0M7UUFORyx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFhO1lBQ2xELElBQU0sZUFBZSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQyxPQUFZLElBQUssT0FBQSxPQUFPLENBQUMsSUFBSSxFQUFaLENBQVksQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQzFELEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDhDQUFpQixHQUF4QixVQUF5QixXQUFrQixFQUFFLGVBQXNCO1FBQy9ELEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQ3JELElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxVQUFVLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxPQUFPLEVBQUU7Z0JBQy9FLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZELFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2lCQUN4QztxQkFBTTtvQkFDSCxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsQ0FBQztvQkFDckUscUJBQXFCO29CQUNyQixXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztpQkFDdkM7YUFDSjtpQkFBTSxJQUFJLGVBQWUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUM5RCxnQ0FBZ0M7Z0JBQ2hDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2FBQ3hDO2lCQUFNO2dCQUNILFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2FBQ3ZDO1NBQ0o7SUFDTCxDQUFDO0lBRU0sb0NBQU8sR0FBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRU0sdURBQTBCLEdBQWpDO1FBQ0ksSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDO1FBQ2hELElBQUksQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUM7UUFDckMsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVNLHlEQUE0QixHQUFuQztRQUNJLElBQUksQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUM7SUFDekMsQ0FBQztJQTlFUSxrQkFBa0I7UUFGOUIsaUJBQVUsRUFBRTtRQUNaLGVBQVEsRUFBRTt5Q0FPOEIsa0NBQWUsRUFBa0IsZUFBTTtPQU5uRSxrQkFBa0IsQ0ErRTlCO0lBQUQseUJBQUM7Q0FBQSxBQS9FRCxJQStFQztBQS9FWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyLCAgTmF2aWdhdGlvbkNhbmNlbCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEZlYXR1cmVQcm92aWRlciB9IGZyb20gJy4vZmVhdHVyZS5wcm92aWRlcic7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbkBOZ01vZHVsZSgpXHJcbmV4cG9ydCBjbGFzcyBOYXZpZ2F0aW9uUHJvdmlkZXIge1xyXG4gICAgcHJpdmF0ZSBuYXZpZ2F0aW9uczogYW55W107XHJcbiAgICBwcml2YXRlIG5hdmlnYXRpb25zT2JzZXJ2YWJsZTogQmVoYXZpb3JTdWJqZWN0PGFueVtdPjtcclxuICAgIHByaXZhdGUgbGFzdENhbmNlbGxlZE5hdmlnYXRpb25Vcmw6IHN0cmluZztcclxuICAgIHByaXZhdGUgbGFzdENhbmNlbGxlZE5hdmlnYXRpb25VcmxDdXN0b206c3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZmVhdHVyZVByb3ZpZGVyOiBGZWF0dXJlUHJvdmlkZXIsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnPT09PT1OYXZpZ2F0aW9uUHJvdmlkZXIgSU5JVElBTElaRUQ9PT09PScpO1xyXG4gICAgICAgIHRoaXMubGFzdENhbmNlbGxlZE5hdmlnYXRpb25VcmwgPSAnJztcclxuICAgICAgICB0aGlzLm5hdmlnYXRpb25zT2JzZXJ2YWJsZSA9IG5ldyBCZWhhdmlvclN1YmplY3QoW10pO1xyXG5cclxuICAgICAgICB0aGlzLnJvdXRlci5ldmVudHMuc3Vic2NyaWJlKGV2ZW50ID0+IHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkNhbmNlbCkge1xyXG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ05hdmlnYXRpb24gY2FuY2VsYXRpb24gZGV0ZWN0ZWQnLCBldmVudCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uVXJsID0gZXZlbnQudXJsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldEN1c3RvbUxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uKHJlZGlyZWNVcmw6c3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5sYXN0Q2FuY2VsbGVkTmF2aWdhdGlvblVybEN1c3RvbSA9IHJlZGlyZWNVcmw7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0dEN1c3RvbUxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uKCk6c3RyaW5nIHtcclxuICAgICAgICBjb25zdCB0ZW1wVXJsRm9yQ3VzdG9tUmVkaXJlY3ROYXZpZ2F0aW9uID0gdGhpcy5sYXN0Q2FuY2VsbGVkTmF2aWdhdGlvblVybEN1c3RvbTtcclxuICAgICAgICB0aGlzLmxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uVXJsQ3VzdG9tID0gJyc7XHJcbiAgICAgICAgcmV0dXJuIHRlbXBVcmxGb3JDdXN0b21SZWRpcmVjdE5hdmlnYXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEN1cnJlbnROYXZpZ2F0aW9uT2JzZXJhYmxlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5hdmlnYXRpb25zT2JzZXJ2YWJsZTtcclxuICAgICAgICAvL3JldHVybiB0aGlzLm5hdmlnYXRpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRDdXJyZW50TmF2aWdhdGlvbnMobmF2aWdhdGlvbnMpIHtcclxuICAgICAgICB0aGlzLm5hdmlnYXRpb25zID0gbmF2aWdhdGlvbnM7XHJcbiAgICAgICAgdGhpcy5hc3NpZ25OYXZpZ2F0aW9uc1Zpc2liaWxpdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXNzaWduTmF2aWdhdGlvbnNWaXNpYmlsaXR5KCkge1xyXG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1kZWJ1Z2dlclxyXG4gICAgICAgIHRoaXMuZmVhdHVyZVByb3ZpZGVyLmdldEZlYXR1cmVzKCkudGhlbigocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBmZWF0dXJlc0ZsYXRNYXAgPSByZXNwb25zZS5tYXAoKGVsZW1lbnQ6IGFueSkgPT4gZWxlbWVudC5OYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5maWx0ZXJOYXZpZ2F0aW9ucyh0aGlzLm5hdmlnYXRpb25zLCBmZWF0dXJlc0ZsYXRNYXApO1xyXG4gICAgICAgICAgICB0aGlzLm5hdmlnYXRpb25zT2JzZXJ2YWJsZS5uZXh0KHRoaXMubmF2aWdhdGlvbnMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBmaWx0ZXJOYXZpZ2F0aW9ucyhuYXZpZ2F0aW9uczogYW55W10sIGZlYXR1cmVzRmxhdE1hcDogYW55W10pIHtcclxuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgbmF2aWdhdGlvbnMubGVuZ3RoOyBpbmRleCsrKSB7XHJcbiAgICAgICAgICAgIGlmIChuYXZpZ2F0aW9uc1tpbmRleF0udHlwZSA9PT0gJ2NvbGxhcHNlJyB8fCBuYXZpZ2F0aW9uc1tpbmRleF0udHlwZSA9PT0gJ2dyb3VwJykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGZlYXR1cmVzRmxhdE1hcC5pbmRleE9mKG5hdmlnYXRpb25zW2luZGV4XS5pZCkgPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmF2aWdhdGlvbnNbaW5kZXhdLmlzVmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpbHRlck5hdmlnYXRpb25zKG5hdmlnYXRpb25zW2luZGV4XS5jaGlsZHJlbiwgZmVhdHVyZXNGbGF0TWFwKTtcclxuICAgICAgICAgICAgICAgICAgICAvL25lZWQgdG8gYmUgcmVmYWN0b3JcclxuICAgICAgICAgICAgICAgICAgICBuYXZpZ2F0aW9uc1tpbmRleF0uaXNWaXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmZWF0dXJlc0ZsYXRNYXAuaW5kZXhPZihuYXZpZ2F0aW9uc1tpbmRleF0uaWQpID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgLy8gbmF2aWdhdGlvbnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgICAgIG5hdmlnYXRpb25zW2luZGV4XS5pc1Zpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG5hdmlnYXRpb25zW2luZGV4XS5pc1Zpc2libGUgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRBcHBzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5hdmlnYXRpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRMYXN0Q2FuY2VsbGVkTmF2aWdhdGlvbigpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IHRlbXBVcmwgPSB0aGlzLmxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uVXJsO1xyXG4gICAgICAgIHRoaXMubGFzdENhbmNlbGxlZE5hdmlnYXRpb25VcmwgPSAnJztcclxuICAgICAgICByZXR1cm4gdGVtcFVybDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVzZXRMYXN0Q2FuY2VsbGVkTmF2aWdhdGlvbigpIHtcclxuICAgICAgICB0aGlzLmxhc3RDYW5jZWxsZWROYXZpZ2F0aW9uVXJsID0gJyc7XHJcbiAgICB9XHJcbn1cclxuIl19