import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as LocalStroage from 'nativescript-localstorage';
import { TokenProvider } from "./token.provider";
import {AppSettingsProvider} from "./settings.provider";
import {LocalStroageKeys} from "../../constraint/local.stroage.keys";

@Injectable()
export class FeatureProvider {

    constructor(private http: HttpClient, private tokenProvider: TokenProvider, private appSettingsProvider: AppSettingsProvider) {
    }

    public getFeatures () {
        let features = this.getFeatureList();

        if (features && features.length > 0) {
            return Promise.resolve(this.getFeatureList());
        } else {
            const header = new HttpHeaders({
                "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
                "Origin": this.appSettingsProvider.getAppSettings().Origin
            });
            
            return this.http.get(this.appSettingsProvider.getAppSettings().Apps, {headers: header}).toPromise().then(response => {
                this.setFeatures(response);
                return response;
            }, err => {
                return new Array([]);
            });
        }
    }

    public resetFeatures() {
        this.deleteFeatures();
    }

    private getFeatureList = (): any [] => {
        let appFeatureList = LocalStroage.getItem(LocalStroageKeys.APP_FEATURE_LIST);
        return appFeatureList && JSON.parse(appFeatureList);
    };

    public setFeatures = (appFeatureList: any) => {
        LocalStroage.setItem(LocalStroageKeys.APP_FEATURE_LIST, JSON.stringify(appFeatureList));
    };

    public deleteFeatures = () => {
        LocalStroage.removeItem(LocalStroageKeys.APP_FEATURE_LIST);
    };
}
