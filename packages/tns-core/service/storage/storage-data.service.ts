import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {AppSettingService} from "~/root/services/configs/app-setting.service";


export class Directory {
	public ItemId: string = "";
	public Name: string = "";
	public ParentDirectoryId: string = "";
	public Tags: string[] = ["temporary", "disposable"];

	constructor(directoryName, parentDirectoryName) {
		this.ItemId = directoryName;
		this.Name = directoryName;
		this.ParentDirectoryId = parentDirectoryName;
	}
}

@Injectable()
export class StorageDataService {
	header: any = new HttpHeaders({
		"Content-Type": "application/json"
	});

	notFoundStatus: number = 404;

	commonOptions: any = { headers: this.header, observe: 'response' };

	storageUrl: any;
	// uploadUrl: any = environment.UploadUrl;
	constructor(private http: HttpClient, appSettingService: AppSettingService) {
        this.storageUrl = appSettingService.getAppSettings().Storage;
    }

	getNewGuid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}

	uploadFileObjectGenerator(attachments, fileId, appId, tags, metaData) {
		let fileObject = {
			ItemId: fileId,
			Name: this.getNewGuid() + "_" + attachments.name,
			FileData: attachments,
			Tags: tags ? JSON.stringify(tags) : JSON.stringify(["File"]),
			MetaData: metaData ? JSON.stringify(metaData) : JSON.stringify({
				FileInfo: { Value: attachments.name }
			}),
			ParentDirectoryId: ""
		}
		return fileObject;
	}

	getFileInfo(fileId): Observable<any> {
		let params = new HttpParams().append("FileId", fileId);
		let options = this.commonOptions;
		options['params'] = params;
		return this.http.get(
			this.storageUrl + "StorageQuery/GetFile",
			options
		).pipe(
		    map((response) => {
			    return response;
		    }),
            catchError(err => {
                console.error('Storage Service error', err);
                return of(null);
            })
        );
	}


	createFormData(object: Object, form?: FormData, namespace?: string): FormData {
		const formData = form || new FormData();
		for (let property in object) {
			if (!object.hasOwnProperty(property) || !object[property]) {
				continue;
			}
			const formKey = namespace ? `${namespace}[${property}]` : property;
			if (object[property] instanceof Date) {
				formData.append(formKey, object[property].toISOString());
			} else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
				this.createFormData(object[property], formData, formKey);
			} else {
				formData.append(formKey, object[property]);
			}
		}
		return formData;
	}
	getFilesInfo(fileIds): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageQuery/GetFiles",
			{
				FileIds: fileIds
			},
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	updateFileInfo(fileData): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageCommand/UpdateFile",
			fileData,
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	uploadFile(attachments, fileId, appId?, tags?, metaData?): Observable<any> {

		return this.http.post(
			this.storageUrl + "StorageCommand/UploadFile",
			this.createFormData(this.uploadFileObjectGenerator(attachments, fileId, appId, tags, metaData))
		).pipe(map((response) => {
			return response
		}));
	}

	createParentDirectory(parentDirectory): Observable<any> {
		let newDirectory = new Directory(parentDirectory, null);
		return this.http.post(
			this.storageUrl + "StorageCommand/CreateDirectory",
			newDirectory,
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	createAudioDirectory(parentDirectory): Observable<any> {
		let newDirectory = new Directory(parentDirectory + "Audio", parentDirectory);
		return this.http.post(
			this.storageUrl + "StorageCommand/CreateDirectory",
			newDirectory,
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	createConvertedAudioDirectory(parentDirectory): Observable<any> {
		let newDirectory = new Directory(parentDirectory + 'AudioConverted', parentDirectory);
		return this.http.post(
			this.storageUrl + "StorageCommand/CreateDirectory",
			newDirectory,
			this.commonOptions
		).pipe(map((response) => {
			return response
		}));
	}

	createImageDirectory(parentDirectory): Observable<any> {
		let newDirectory = new Directory(parentDirectory + "Image", parentDirectory);
		return this.http.post(
			this.storageUrl + "StorageCommand/CreateDirectory",
			newDirectory,
			this.commonOptions
		).pipe(map((response) => {
			return response
		}));
	}

	allDirectoryCreate(directoryName, callback) {
		this.createParentDirectory(directoryName).subscribe(() => {
			this.createAudioDirectory(directoryName).subscribe(() => {
				this.createConvertedAudioDirectory(directoryName).subscribe(() => {
					this.createImageDirectory(directoryName).subscribe(() => {
						callback(true);
					});
				});
			});
		});
	}

	createDirectory(directoryName): Observable<any> {
		let params = new HttpParams().append("DirectoryId", directoryName);
		let options = this.commonOptions;
		options['params'] = params;
		return this.http.get(
			this.storageUrl + "StorageQuery/GetDirectroy",
			options
		).pipe(map((response) => {
			if (!response) {
				return this.allDirectoryCreate(directoryName, (result) => {
					return result;
				});
			} else {
				return "exists";
			}
		}, (errorResponse) => {
			if (errorResponse.status == this.notFoundStatus) {
				return this.allDirectoryCreate(directoryName, (result) => {
					return result
				});
			} else {
				return errorResponse;
			}
		}));
	}

	deleteFile(itemId): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageCommand/DeleteFile",
			{ ItemId: itemId },
			this.commonOptions
		).pipe(map((response) => {
			return response
		}));
	}

	deleteAllFiles(itemIds): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageCommand/DeleteAll",
			{ ItemIds: itemIds },
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	downloadFile(fileId): Observable<any> {
		let params = new HttpParams().append("FileId", fileId);
		let options = this.commonOptions;
		options['params'] = params;
		return this.http.get(
			this.storageUrl + "StorageQuery/GetDownloadUri",
			options
		).pipe(map((response) => {
			return response;
		}));
	}

	executePipeline(pipelineCommand): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageCommand/ExecuteFileConversionPipeline",
			pipelineCommand,
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	generatePSU(itemId, name, parentDirectory, tags, accesModifier?) {
		let metaData = {
			"Title": {
				"Type": "String",
				"Value": name
			},
			"OriginalName": {
				"Type": "String",
				"Value": name
			}
		};
		return {
			"ItemId": itemId,
			"MetaData": JSON.stringify(metaData),
			"Name": name,
			"ParentDirectoryId": parentDirectory,
			"Tags": tags ? JSON.stringify(tags) : JSON.stringify(["File"]),
			"AccessModifier": accesModifier
		}
	}




	uploadPresignedFile(url, file): Observable<any> {
		return this.http.put(
			url,
			file
		).pipe(map((response) => {
			return response;
		}));
	}

	uploadPublicPresignedFile(url, file): Observable<any> {
		return this.http.put(
			url,
			file,
			{ headers: {"x-amz-acl": "public-read"}}
		).pipe(map((response) => {
			return response;
		}));
	}

	getPreSignedUrl(itemId, name, parentDirectory, tags, accessModifier?): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageQuery/GetPreSignedUrlForUpload",
			this.generatePSU(itemId, name, parentDirectory, tags, accessModifier),
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	executeFileConversion(conversionPayload): Observable<any> {
		return this.http.post(
			this.storageUrl + "StorageCommand/ExecuteFileConversionPipeline",
			this.commonOptions
		).pipe(map((response) => {
			return response;
		}));
	}

	downloadFileAsBlob(data) {
        var decodedUri = decodeURI(data.Url);
        if (navigator.msSaveBlob) {
          this.http.get(decodedUri, {
            responseType: "arraybuffer"
          }).
            pipe(map((data, headers) => {
              var fileBlob = new Blob([data], { type: 'text/plain' });
              navigator.msSaveBlob(fileBlob, data['Name']);
            }));
        } else {
          var a = document.createElement('a');
          a.setAttribute('href', decodedUri);
          a.setAttribute('target', '_blank');
          a.setAttribute('download', data['Name']);
          a.style.display = 'none';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }

    }

}
