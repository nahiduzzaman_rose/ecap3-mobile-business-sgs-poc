export interface IAppSettings {
    Apps: string;
    Token: string;
    Security: string;
    Captcha: string;
    Identity: string;
    Navigation: string;
    TenantId: string;
    Origin: string;
    DomainName: string;
    SubDomainName: string;
    Authentication: string;
    DataCore: string;
    IsMock: boolean;
    LocalDBName: string;
    Storage: string;
    SyncUrl: string;
    GQLQueryUri: string;
    GQLMutationUri: string;
    SyncAdapterId: string;
    ClmApi: string;
    ScertBusinessService: string;
}
