export interface INavigationItem {
    id: string;
    title: string;
    translate: string;
    type: string;
    url: string;
    icon: string;
    iconType: string;
    isVisible: boolean;
    isHidden: boolean;
}

export enum NavigationType {
    ITEM = "item",
    LOGOUT = "logout"
}

