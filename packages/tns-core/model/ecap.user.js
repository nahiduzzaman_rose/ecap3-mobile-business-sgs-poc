"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EcapUser = /** @class */ (function () {
    function EcapUser() {
        this.ItemId = "";
        this.UserId = "";
        this.Language = "";
        this.TenantId = "";
        this.DisplayName = "";
        this.Email = "";
        this.PhoneNumber = "";
        this.UserName = "";
        this.FirstName = "";
        this.LastName = "";
        this.ProfileImageId = "";
        // this.ItemId = "";
        // this.UserId = "";
        // this.Roles = [];
        // this.Language = "";
        // this.TenantId = "";
        // this.Active = false;
        // this.DisplayName = "";
        // this.Email = "";
        // this.EmailVarified = false;
        // this.PhoneNumber = "";
        // this.UserName = "";
        // this.FirstName = "";
        // this.LastName = "";
        // this.UserSignup = false;
        // this.TwoFactorEnabled = false;
        // this.ProfileImageId = "";
        // this.PersonaEnabled = false;
        // this.AutoExpire = false;
        // this.Person = new EcapPerson();
    }
    return EcapUser;
}());
exports.EcapUser = EcapUser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNhcC51c2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZWNhcC51c2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFxQkU7UUFwQkEsV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUNaLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFFWixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUVkLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFFWCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFHZCxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQU1sQixvQkFBb0I7UUFDcEIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQixzQkFBc0I7UUFDdEIsc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2Qix5QkFBeUI7UUFDekIsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5Qix5QkFBeUI7UUFDekIsc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixzQkFBc0I7UUFDdEIsMkJBQTJCO1FBQzNCLGlDQUFpQztRQUNqQyw0QkFBNEI7UUFDNUIsK0JBQStCO1FBQy9CLDJCQUEyQjtRQUMzQixrQ0FBa0M7SUFDcEMsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDLEFBMUNELElBMENDO0FBMUNZLDRCQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRWNhcFBlcnNvbiB9IGZyb20gXCIuL2VjYXAucGVyc29uXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgRWNhcFVzZXIge1xyXG4gIEl0ZW1JZCA9IFwiXCI7XHJcbiAgVXNlcklkID0gXCJcIjtcclxuICBSb2xlczogc3RyaW5nW107XHJcbiAgTGFuZ3VhZ2UgPSBcIlwiO1xyXG4gIFRlbmFudElkID0gXCJcIjtcclxuICBBY3RpdmU6IGJvb2xlYW47XHJcbiAgRGlzcGxheU5hbWUgPSBcIlwiO1xyXG4gIEVtYWlsID0gXCJcIjtcclxuICBFbWFpbFZhcmlmaWVkOiBib29sZWFuO1xyXG4gIFBob25lTnVtYmVyID0gXCJcIjtcclxuICBVc2VyTmFtZSA9IFwiXCI7XHJcbiAgRmlyc3ROYW1lID0gXCJcIjtcclxuICBMYXN0TmFtZSA9IFwiXCI7XHJcbiAgVXNlclNpZ251cDogYm9vbGVhbjtcclxuICBUd29GYWN0b3JFbmFibGVkOiBib29sZWFuO1xyXG4gIFByb2ZpbGVJbWFnZUlkID0gXCJcIjtcclxuICBQZXJzb25hRW5hYmxlZDogYm9vbGVhbjtcclxuICBBdXRvRXhwaXJlOiBib29sZWFuO1xyXG4gIFBlcnNvbjogRWNhcFBlcnNvbjtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAvLyB0aGlzLkl0ZW1JZCA9IFwiXCI7XHJcbiAgICAvLyB0aGlzLlVzZXJJZCA9IFwiXCI7XHJcbiAgICAvLyB0aGlzLlJvbGVzID0gW107XHJcbiAgICAvLyB0aGlzLkxhbmd1YWdlID0gXCJcIjtcclxuICAgIC8vIHRoaXMuVGVuYW50SWQgPSBcIlwiO1xyXG4gICAgLy8gdGhpcy5BY3RpdmUgPSBmYWxzZTtcclxuICAgIC8vIHRoaXMuRGlzcGxheU5hbWUgPSBcIlwiO1xyXG4gICAgLy8gdGhpcy5FbWFpbCA9IFwiXCI7XHJcbiAgICAvLyB0aGlzLkVtYWlsVmFyaWZpZWQgPSBmYWxzZTtcclxuICAgIC8vIHRoaXMuUGhvbmVOdW1iZXIgPSBcIlwiO1xyXG4gICAgLy8gdGhpcy5Vc2VyTmFtZSA9IFwiXCI7XHJcbiAgICAvLyB0aGlzLkZpcnN0TmFtZSA9IFwiXCI7XHJcbiAgICAvLyB0aGlzLkxhc3ROYW1lID0gXCJcIjtcclxuICAgIC8vIHRoaXMuVXNlclNpZ251cCA9IGZhbHNlO1xyXG4gICAgLy8gdGhpcy5Ud29GYWN0b3JFbmFibGVkID0gZmFsc2U7XHJcbiAgICAvLyB0aGlzLlByb2ZpbGVJbWFnZUlkID0gXCJcIjtcclxuICAgIC8vIHRoaXMuUGVyc29uYUVuYWJsZWQgPSBmYWxzZTtcclxuICAgIC8vIHRoaXMuQXV0b0V4cGlyZSA9IGZhbHNlO1xyXG4gICAgLy8gdGhpcy5QZXJzb24gPSBuZXcgRWNhcFBlcnNvbigpO1xyXG4gIH1cclxufVxyXG4iXX0=