
export interface ActionBarConfiguration {
    actionItems: IActionItem[];
    menuIconSrc?: string;
    logo?: Ilogo;
}

export interface Ilogo {
    src: string;
    width: string;
}

export interface IActionItem {
    actionItemIcon: string;
    androidActionItemPosition?: string;
    iosActionItemPosition?: string;
    actionItemText: string;
    url: string;
    callParentMethod?: boolean;
    type?: string;
}

export interface BackbuttonConfig {
    path: string
}

export enum ActionItemType {
    ACTIONBAR = "actionBar",
    POPUP = "popup",
    IOS_LEFT = "left",
    IOS_RIGHT = "right"
}

export enum ActionType {
    MENU = "menu",
    LOGOUT = "logout"
}