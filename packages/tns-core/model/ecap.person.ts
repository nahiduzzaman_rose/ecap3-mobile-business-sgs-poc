export class EcapPerson {
  ItemId: string;
  PersonId: string;
  UserId: string;
  CreatedBy: string;
  Language: string;
  Tags: string[];
  TenantId: string;
  Roles: string[];

  DisplayName: string;
  Email: string;
  UserName: string;

  Salutation: string;
  FirstName: string;
  LastName: string;
  MiddleName: string;

  Sex: string;
  DateOfBirth: Date;
  NationalID: string;
  PhoneNumber: string;
  ProfileImageId: string;
  Currency: string;

  Active: boolean;
  IsVerified: boolean;
  EmailVarified: boolean;

  CompanyName: string;
  CompanyId: string;
  Organization: string;
  OrganizationId: string;
  Designation: string;

  Address: string;
  AddressLine1: string;
  AddressLine2: string;
  Street: string;
  PostalCode: string;
  City: string;
  Country: string;
  State: string;
  Nationality: string;
  ShortBio: string;

  constructor() {}
}
