import { EcapPerson } from "./ecap.person";

export class EcapUser {
  ItemId = "";
  UserId = "";
  Roles: string[];
  Language = "";
  TenantId = "";
  Active: boolean;
  DisplayName = "";
  Email = "";
  EmailVarified: boolean;
  PhoneNumber = "";
  UserName = "";
  FirstName = "";
  LastName = "";
  UserSignup: boolean;
  TwoFactorEnabled: boolean;
  ProfileImageId = "";
  PersonaEnabled: boolean;
  AutoExpire: boolean;
  Person: EcapPerson;

  constructor() {
    // this.ItemId = "";
    // this.UserId = "";
    // this.Roles = [];
    // this.Language = "";
    // this.TenantId = "";
    // this.Active = false;
    // this.DisplayName = "";
    // this.Email = "";
    // this.EmailVarified = false;
    // this.PhoneNumber = "";
    // this.UserName = "";
    // this.FirstName = "";
    // this.LastName = "";
    // this.UserSignup = false;
    // this.TwoFactorEnabled = false;
    // this.ProfileImageId = "";
    // this.PersonaEnabled = false;
    // this.AutoExpire = false;
    // this.Person = new EcapPerson();
  }
}
