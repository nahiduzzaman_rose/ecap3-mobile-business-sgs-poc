import { Component, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { screen } from "tns-core-modules/platform";
import { VideoRecorder, Options as VideoRecorderOptions } from 'nativescript-videorecorder-x';
import * as fs from "tns-core-modules/file-system";


@Component({
    selector: "tns-platform-video",
    moduleId: module.id,
    templateUrl: "./video-default.component.html",
    styleUrls: [
        "./video-default.common.scss",
        "./video-default.component.scss"
    ]
})

export class PlatformVideoDefaultComponent implements OnInit, OnDestroy {
    screenRationWidth: number;
    screenRationHeight: number;
    videorecorder: VideoRecorder;
    videoPath: string;
    fileName: string;
    @Output() video: EventEmitter<any> = new EventEmitter<any>();

    constructor() {

    }

    saveFile() {
        //let path = '/storage/emulated/0/Android/data/ch.selise.smartcity/files/VID_1563173710311.mp4';

        const videoFile = fs.File.fromPath(this.videoPath);
        this.fileName = this.getFileName(this.videoPath);

        const videoFolder = fs.knownFolders.currentApp().getFolder('tns-video');

        const recordedFile = videoFolder.getFile(this.fileName);

        const binarySource = videoFile.readSync((err) => {
            console.log(err);
        });

        recordedFile.writeSync(binarySource, (err) => {
            console.log(err);
        });


        videoFile.remove().then(value => {
            console.log("Remove Status ", value);
        });

        this.video.emit(recordedFile);

        console.log('recordedFile',recordedFile);
    }

    removeFile(){
        const videoFile = fs.File.fromPath(this.videoPath);

        videoFile.remove().then(value => {
            console.log("Remove Status ", value);
        });
    }

    startRecord() {
        const options: VideoRecorderOptions = {
            hd: true,
            saveToGallery: false
        }
        const videorecorder = new VideoRecorder(options)

        videorecorder.record().then((data) => {
            console.log('data', data);
            this.videoPath = data.file;
        }).catch((err) => {
            console.log(err)
        })
    }

    public getFileName(recordingPath) {
        const lastIndex = recordingPath.lastIndexOf("/");
        const fnalIndex = recordingPath.lastIndexOf("4");
        const video = recordingPath.slice(lastIndex + 1, fnalIndex + 1);
        this.fileName = video;
        console.log('fileName',this.fileName);
        return this.fileName;
    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
    }

    ngOnDestroy() {

    }
}
