import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {PlatformVideoService} from "./services/platform-video.service";
import { PlatformVideoDefaultComponent } from "./components/video-default/video-default.component";



@NgModule({
    providers: [
        PlatformVideoService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        PlatformVideoDefaultComponent
    ],
    entryComponents: [
    ],
    exports: [
        PlatformVideoDefaultComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformVideoModule {}
