import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    OnDestroy,
    ChangeDetectorRef,
    TemplateRef
} from '@angular/core';
import { screen, device } from "tns-core-modules/platform";
import { ScrollView, ScrollEventData } from "ui/scroll-view";
import { LoadOnDemandListViewEventData, RadListView } from 'nativescript-ui-listview';
import {OrientationChangedEventData} from "application"
import * as app from "application";
import {Router} from "@angular/router";
import {DeviceType} from "tns-core-modules/ui/enums";


@Component({
    selector: "tns-platform-card",
    moduleId: module.id,
    templateUrl: "./card-default.component.html",
    styleUrls: [
        "./card-default.common.scss",
        "./card-default.component.scss"
    ]
})

export class PlatformCardDefaultComponent implements OnInit, OnDestroy {
    public result: string;
    public screenRationWidth: any;
    public spanCount: any;
    public changeOrientation: boolean;

    @Input() cardItemTemplate: TemplateRef<any>;
    @Input() data: any[] = [];
    @Input() noResultMessage: string = 'No data found';
    @Output() loadDataEvent = new EventEmitter<any>();
    @Output() onCardItemClickEvent = new EventEmitter<any>();

    constructor() {

    }

    public onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
        //console.log('args',args);
        const that = new WeakRef(this);
        const listView: RadListView = args.object;
        console.log('More Data requested');
        this.loadDataEvent.emit({
            data: null,
            listView : listView,
            args: args
        });
    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        // console.log(screen.mainScreen.heightDIPs);
        this.spanCount = device.deviceType === DeviceType.Tablet ? 2 : 1;

    }

    onCardItemClick(cardItemValue) {
        this.onCardItemClickEvent.emit(cardItemValue);
    }

    ngOnDestroy() {

    }
}
