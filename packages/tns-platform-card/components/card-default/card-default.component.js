"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var modal_content_component_1 = require("../modal-content/modal-content.component");
var platform_gdpr_service_1 = require("./../../services/platform-gdpr.service");
var lodash_1 = require("lodash");
var PlatformGdprDefaultComponent = /** @class */ (function () {
    function PlatformGdprDefaultComponent(modalService, viewContainerRef, gdprService, IAppPlatfromGdpr, formBuilder) {
        var _this = this;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.gdprService = gdprService;
        this.IAppPlatfromGdpr = IAppPlatfromGdpr;
        this.formBuilder = formBuilder;
        this.OnGDPRacceptanceSuccessFull = new core_1.EventEmitter();
        this.allGdprArrived = new core_1.EventEmitter(); // eventemmiter for all gdpr content loader
        this.readFullDocument = true;
        //  console.log("gdpr component loaded");
        this.termAndCondition = {}; // reseting at component load
        // =========================================
        // waiting for all gdpr contents arrival
        // =========================================
        this.allGdprArrived.subscribe(function (response) {
            if (response) {
                _this.denormalizeGdprContents();
            }
        });
    }
    PlatformGdprDefaultComponent.prototype.onScroll = function (event) {
        if (!this.readFullDocument && (event.target.offsetHeight + event.target.scrollTop) >= event.target.scrollHeight) {
            this.readFullDocument = true;
            this.updateGdprForm();
        }
    };
    PlatformGdprDefaultComponent.prototype.loadGdprContent = function () {
        var _this = this;
        if (this.getGdprContentUsingInterface) {
            this.IAppPlatfromGdpr.getGdprContents(this.requiredConditions, this.userEmail).subscribe(function (response) {
                if (response) {
                    _this.allGdprContents = _this.getDenomGdprContent(response);
                    _this.allGdprArrived.emit(true); // emitting event after all gdpr content arrived
                }
            });
        }
        else {
            console.log('Without Interface !!');
            this.gdprService.getAllGdprContent().subscribe(function (response) {
                // =================================================
                // if gdpr content loads from database
                // =================================================
                if (response) {
                    var gdprContentList = _this.getDenomGdprContent(response);
                    var sortOrder_1 = _this.requiredConditions;
                    gdprContentList.sort(function (a, b) {
                        return sortOrder_1.indexOf(a.TermAndConditionType) - sortOrder_1.indexOf(b.TermAndConditionType);
                    });
                    _this.allGdprContents = gdprContentList;
                    console.log('allGdprContents', _this.allGdprContents);
                    _this.allGdprArrived.emit(true); // emitting event after all gdpr content arrived
                }
            });
        }
    };
    PlatformGdprDefaultComponent.prototype.ngAfterContentChecked = function () {
    };
    PlatformGdprDefaultComponent.prototype.updateGdprForm = function () {
        this.termsConditionForm.controls['termValue'].setValue(this.readFullDocument);
        this.termsConditionForm.controls['termValue'].updateValueAndValidity();
    };
    PlatformGdprDefaultComponent.prototype.getDenomGdprContent = function (gdprDataList) {
        return lodash_1.forEach(gdprDataList, function (data) { return new DTOGDPR(data); });
    };
    PlatformGdprDefaultComponent.prototype.denormalizeGdprContents = function () {
        //  console.log('gdpr information arrived from mongo');
        //   console.log(this.allGdprContents);
        this.setCurrentGpdrOptions(0); // setting initial gdpr option to first element of array
        this.openGdprConditionModal(); // openning GDPR conditional modal
    };
    PlatformGdprDefaultComponent.prototype.setCurrentGpdrOptions = function (index) {
        this.currentGdprIndex = index ? index : 0;
        this.currentGdprType = this.requiredConditions[this.currentGdprIndex];
        console.log('currentGdprType', this.currentGdprType);
        //this.readFullDocument = false;
        //this.updateGdprForm();
    };
    PlatformGdprDefaultComponent.prototype.storeInitialData = function () {
        this.requiredConditions = this.requiredConditionsDetails.RequiredTermsAndConditions; // storing required conditions array
        this.contextId = this.requiredConditionsDetails.ContextId; // storing contextID supplied by login app as INPUT
    };
    PlatformGdprDefaultComponent.prototype.openGdprConditionModal = function () {
        var _this = this;
        var confirm = function (callback) {
            console.log('call confirm !!');
            // console.log(this.termsConditionForm.value);
            //this.termAndCondition[this.currentGdprType] = this.termsConditionForm.value.termValue;
            _this.termAndCondition[_this.currentGdprType] = true;
            var totalConditionCount = _this.requiredConditions.length - 1;
            if (totalConditionCount > _this.currentGdprIndex) {
                _this.setCurrentGpdrOptions(_this.currentGdprIndex + 1);
                callback(false);
            }
            else {
                var GdprData_1 = {
                    "grant_type": "authenticate_terms_and_conditions",
                    "context_id": _this.contextId,
                    "accepted_terms_and_conditions": JSON.stringify(_this.termAndCondition),
                    "required_conditions": JSON.stringify(_this.requiredConditions)
                };
                _this.gdprService.StoreGDPRdata(GdprData_1).subscribe(function (response) {
                    _this.OnGDPRacceptanceSuccessFull && _this.OnGDPRacceptanceSuccessFull.emit({ GdprData: GdprData_1, Email: _this.userEmail, Status: true });
                    callback(response);
                }, function (error) {
                    _this.OnGDPRacceptanceSuccessFull && _this.OnGDPRacceptanceSuccessFull.emit({ GdprData: GdprData_1, Email: _this.userEmail, Error: error.error, Status: false });
                    callback(true);
                });
            }
        };
        var cancel = function (callback) {
            console.log('call cenceled');
            _this.termAndCondition[_this.currentGdprType] = false;
            var GdprData = {
                "grant_type": "authenticate_terms_and_conditions",
                "context_id": _this.contextId,
                "accepted_terms_and_conditions": JSON.stringify(_this.termAndCondition),
                "required_conditions": JSON.stringify(_this.requiredConditions)
            };
            _this.gdprService.StoreGDPRdata(GdprData).subscribe(function (response) {
                callback(true);
            }, function (error) {
                callback(true);
                _this.OnGDPRacceptanceSuccessFull && _this.OnGDPRacceptanceSuccessFull.emit({ GdprData: GdprData, Email: _this.userEmail, Error: error.error, Status: false });
            });
        };
        var options = {
            context: {
                promptMsg: "This is the prompt message!!!",
                template: this.gdprContainer,
                gdprConfiguration: this.gdprConfiguration,
                functions: {
                    confirm: confirm,
                    cancel: cancel
                }
            },
            fullscreen: false,
            viewContainerRef: this.viewContainerRef
        };
        this.modalService.showModal(modal_content_component_1.ModalContentComponent, options)
            .then(function (dialogResult) {
            _this.result = dialogResult;
        });
    };
    PlatformGdprDefaultComponent.prototype.ngAfterViewInit = function () {
        this.storeInitialData(); // storing initial value
        // ================================================
        // get all gdpr content at first call
        // ================================================
        this.loadGdprContent();
        /* this.termsConditionForm = this.formBuilder.group({
            "termValue": [this.readFullDocument, Validators.requiredTrue]
        });

        this.gdprService.gdprReloadControl.subscribe(termsConditionDetails=>{
            if(termsConditionDetails){
                this.termAndCondition = termsConditionDetails;
                this.loadGdprContent();
            }
        }); */
    };
    PlatformGdprDefaultComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PlatformGdprDefaultComponent.prototype, "requiredConditionsDetails", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], PlatformGdprDefaultComponent.prototype, "getGdprContentUsingInterface", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PlatformGdprDefaultComponent.prototype, "userEmail", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PlatformGdprDefaultComponent.prototype, "gdprConfiguration", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], PlatformGdprDefaultComponent.prototype, "OnGDPRacceptanceSuccessFull", void 0);
    __decorate([
        core_1.ViewChild('gdprContainer'),
        __metadata("design:type", core_1.TemplateRef)
    ], PlatformGdprDefaultComponent.prototype, "gdprContainer", void 0);
    __decorate([
        core_1.HostListener('scroll', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], PlatformGdprDefaultComponent.prototype, "onScroll", null);
    PlatformGdprDefaultComponent = __decorate([
        core_1.Component({
            selector: "tns-platform-gdpr",
            moduleId: module.id,
            templateUrl: "./gdpr-default.component.html",
            styleUrls: [
                "./gdpr-default.common.scss",
                "./gdpr-default.component.scss"
            ]
        }),
        __param(3, core_1.Optional()), __param(3, core_1.Inject("IAppPlatfromGdpr")),
        __metadata("design:paramtypes", [modal_dialog_1.ModalDialogService,
            core_1.ViewContainerRef,
            platform_gdpr_service_1.PlatformGdprService, Object, forms_1.FormBuilder])
    ], PlatformGdprDefaultComponent);
    return PlatformGdprDefaultComponent;
}());
exports.PlatformGdprDefaultComponent = PlatformGdprDefaultComponent;
var DTOGDPR = /** @class */ (function () {
    function DTOGDPR(data) {
        data = data || {};
        this.ItemId = data && data.ItemId || "";
        this.Language = data && data.Language || "en-US";
        this.TermAndConditionContent = data && data.TermAndConditionContent || "";
        this.TermAndConditionType = data && data.TermAndConditionType || "";
    }
    return DTOGDPR;
}());
exports.DTOGDPR = DTOGDPR;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2Rwci1kZWZhdWx0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdkcHItZGVmYXVsdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FHdUI7QUFDdkIsd0NBQW9FO0FBQ3BFLGtFQUEwRjtBQUMxRixvRkFBaUY7QUFDakYsZ0ZBQTZFO0FBRTdFLGlDQUErQjtBQVkvQjtJQStCSSxzQ0FBb0IsWUFBZ0MsRUFDeEMsZ0JBQWtDLEVBQ2xDLFdBQWdDLEVBQ08sZ0JBQWdCLEVBQ3ZELFdBQXdCO1FBSnBDLGlCQWlCQztRQWpCbUIsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQ3hDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ08scUJBQWdCLEdBQWhCLGdCQUFnQixDQUFBO1FBQ3ZELGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBN0IxQixnQ0FBMkIsR0FBRyxJQUFJLG1CQUFZLEVBQU8sQ0FBQztRQU9oRSxtQkFBYyxHQUEwQixJQUFJLG1CQUFZLEVBQUUsQ0FBQyxDQUFDLDJDQUEyQztRQVN2RyxxQkFBZ0IsR0FBWSxJQUFJLENBQUM7UUFlN0IseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUMsQ0FBQyw2QkFBNkI7UUFDekQsNENBQTRDO1FBQzVDLHdDQUF3QztRQUN4Qyw0Q0FBNEM7UUFFNUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2xDLElBQUksUUFBUSxFQUFFO2dCQUNWLEtBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO2FBQ2xDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBeEJELCtDQUFRLEdBQVIsVUFBUyxLQUFVO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUU7WUFDN0csSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDO0lBcUJELHNEQUFlLEdBQWY7UUFBQSxpQkEyQkM7UUExQkcsSUFBRyxJQUFJLENBQUMsNEJBQTRCLEVBQUM7WUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQVk7Z0JBQ2xHLElBQUksUUFBUSxFQUFFO29CQUNWLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFFO29CQUMzRCxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGdEQUFnRDtpQkFDbkY7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQUk7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQWE7Z0JBQ3pELG9EQUFvRDtnQkFDcEQsc0NBQXNDO2dCQUN0QyxvREFBb0Q7Z0JBQ3BELElBQUksUUFBUSxFQUFFO29CQUNWLElBQUksZUFBZSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBRTtvQkFFMUQsSUFBSSxXQUFTLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDO29CQUN4QyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVMsQ0FBSyxFQUFFLENBQUs7d0JBQ3RDLE9BQU8sV0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsR0FBRyxXQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUNqRyxDQUFDLENBQUMsQ0FBQztvQkFDSCxLQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBRTtvQkFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3BELEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsZ0RBQWdEO2lCQUNuRjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRUQsNERBQXFCLEdBQXJCO0lBRUEsQ0FBQztJQUVELHFEQUFjLEdBQWQ7UUFDSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDM0UsQ0FBQztJQUVELDBEQUFtQixHQUFuQixVQUFvQixZQUFZO1FBQzdCLE9BQU8sZ0JBQU8sQ0FBQyxZQUFZLEVBQUUsVUFBQyxJQUFJLElBQUksT0FBQSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBakIsQ0FBaUIsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFTyw4REFBdUIsR0FBL0I7UUFDSSx1REFBdUQ7UUFDeEQsdUNBQXVDO1FBQ3RDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLHdEQUF3RDtRQUN2RixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDLGtDQUFrQztJQUNyRSxDQUFDO0lBRU0sNERBQXFCLEdBQTVCLFVBQTZCLEtBQWM7UUFDdkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDcEQsZ0NBQWdDO1FBQ2hDLHdCQUF3QjtJQUM1QixDQUFDO0lBRU0sdURBQWdCLEdBQXZCO1FBQ0ksSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLG9DQUFvQztRQUN6SCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxtREFBbUQ7SUFDbEgsQ0FBQztJQUVNLDZEQUFzQixHQUE3QjtRQUFBLGlCQStEQztRQTdERyxJQUFJLE9BQU8sR0FBRyxVQUFDLFFBQVE7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQy9CLDhDQUE4QztZQUM5Qyx3RkFBd0Y7WUFDeEYsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDbkQsSUFBTSxtQkFBbUIsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUMvRCxJQUFJLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDN0MsS0FBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDdEQsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25CO2lCQUFNO2dCQUNILElBQU0sVUFBUSxHQUFHO29CQUNiLFlBQVksRUFBRSxtQ0FBbUM7b0JBQ2pELFlBQVksRUFBRSxLQUFJLENBQUMsU0FBUztvQkFDNUIsK0JBQStCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7b0JBQ3RFLHFCQUFxQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDO2lCQUNqRSxDQUFBO2dCQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ3ZELEtBQUksQ0FBQywyQkFBMkIsSUFBSSxLQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEVBQUMsUUFBUSxFQUFFLFVBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSSxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztvQkFDckksUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLEtBQUksQ0FBQywyQkFBMkIsSUFBSSxLQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEVBQUMsUUFBUSxFQUFFLFVBQVEsRUFBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FBQztvQkFDekosUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNuQixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxNQUFNLEdBQUcsVUFBQyxRQUFRO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDN0IsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDcEQsSUFBTSxRQUFRLEdBQUc7Z0JBQ2IsWUFBWSxFQUFFLG1DQUFtQztnQkFDakQsWUFBWSxFQUFFLEtBQUksQ0FBQyxTQUFTO2dCQUM1QiwrQkFBK0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDdEUscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUM7YUFDakUsQ0FBQTtZQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ3ZELFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixLQUFJLENBQUMsMkJBQTJCLElBQUksS0FBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxTQUFTLEVBQUcsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUM7WUFDOUosQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUFFRixJQUFJLE9BQU8sR0FBdUI7WUFDOUIsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRSwrQkFBK0I7Z0JBQzFDLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYTtnQkFDNUIsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtnQkFDekMsU0FBUyxFQUFFO29CQUNQLE9BQU8sRUFBRSxPQUFPO29CQUNoQixNQUFNLEVBQUUsTUFBTTtpQkFDakI7YUFDSjtZQUNELFVBQVUsRUFBRSxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDMUMsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLCtDQUFxQixFQUFFLE9BQU8sQ0FBQzthQUN0RCxJQUFJLENBQUMsVUFBQyxZQUFvQjtZQUN2QixLQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQTtRQUM5QixDQUFDLENBQUMsQ0FBQTtJQUNWLENBQUM7SUFFRCxzREFBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyx3QkFBd0I7UUFFakQsbURBQW1EO1FBQ25ELHFDQUFxQztRQUNyQyxtREFBbUQ7UUFFbkQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXZCOzs7Ozs7Ozs7Y0FTTTtJQUNWLENBQUM7SUFFRCwrQ0FBUSxHQUFSO0lBRUEsQ0FBQztJQXRNUTtRQUFSLFlBQUssRUFBRTs7bUZBQWdDO0lBQy9CO1FBQVIsWUFBSyxFQUFFOztzRkFBdUM7SUFDdEM7UUFBUixZQUFLLEVBQUU7O21FQUFvQjtJQUNuQjtRQUFSLFlBQUssRUFBRTs7MkVBQXlCO0lBQ3ZCO1FBQVQsYUFBTSxFQUFFOztxRkFBdUQ7SUFDcEM7UUFBM0IsZ0JBQVMsQ0FBQyxlQUFlLENBQUM7a0NBQWlCLGtCQUFXO3VFQUFNO0lBaUI3RDtRQURDLG1CQUFZLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7Ozs7Z0VBTWxDO0lBN0JRLDRCQUE0QjtRQVZ4QyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUU7Z0JBQ1AsNEJBQTRCO2dCQUM1QiwrQkFBK0I7YUFDbEM7U0FDSixDQUFDO1FBb0NNLFdBQUEsZUFBUSxFQUFFLENBQUEsRUFBRSxXQUFBLGFBQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO3lDQUhSLGlDQUFrQjtZQUN0Qix1QkFBZ0I7WUFDckIsMkNBQW1CLFVBRW5CLG1CQUFXO09BbkMzQiw0QkFBNEIsQ0F5TXhDO0lBQUQsbUNBQUM7Q0FBQSxBQXpNRCxJQXlNQztBQXpNWSxvRUFBNEI7QUEyTXpDO0lBS0ksaUJBQVksSUFBSztRQUNiLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDO1FBQ2pELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLEVBQUUsQ0FBQztRQUMxRSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxFQUFFLENBQUM7SUFDeEUsQ0FBQztJQUNMLGNBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQztBQVpZLDBCQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgSW5wdXQsIEV2ZW50RW1pdHRlciwgVmlld0NoaWxkLFxyXG4gICAgVGVtcGxhdGVSZWYsIE91dHB1dCwgSW5qZWN0LCBIb3N0TGlzdGVuZXIsIEVsZW1lbnRSZWYsIEFmdGVyQ29udGVudENoZWNrZWQsIE9wdGlvbmFsLCBWaWV3Q29udGFpbmVyUmVmLCBBZnRlclZpZXdJbml0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSwgTW9kYWxEaWFsb2dPcHRpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nJ1xyXG5pbXBvcnQgeyBNb2RhbENvbnRlbnRDb21wb25lbnQgfSBmcm9tICcuLi9tb2RhbC1jb250ZW50L21vZGFsLWNvbnRlbnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUGxhdGZvcm1HZHByU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2VydmljZXMvcGxhdGZvcm0tZ2Rwci5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5pbXBvcnQge2ZvckVhY2h9IGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcInRucy1wbGF0Zm9ybS1nZHByXCIsXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9nZHByLWRlZmF1bHQuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1xyXG4gICAgICAgIFwiLi9nZHByLWRlZmF1bHQuY29tbW9uLnNjc3NcIixcclxuICAgICAgICBcIi4vZ2Rwci1kZWZhdWx0LmNvbXBvbmVudC5zY3NzXCJcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQbGF0Zm9ybUdkcHJEZWZhdWx0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LEFmdGVyVmlld0luaXQge1xyXG4gICAgcHVibGljIHJlc3VsdDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgcmVxdWlyZWRDb25kaXRpb25zRGV0YWlsczogYW55O1xyXG4gICAgQElucHV0KCkgZ2V0R2RwckNvbnRlbnRVc2luZ0ludGVyZmFjZTogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIHVzZXJFbWFpbCA6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGdkcHJDb25maWd1cmF0aW9uIDogYW55O1xyXG4gICAgQE91dHB1dCgpIE9uR0RQUmFjY2VwdGFuY2VTdWNjZXNzRnVsbCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQFZpZXdDaGlsZCgnZ2RwckNvbnRhaW5lcicpIGdkcHJDb250YWluZXIgOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gICAgLy8gcGVyc29uSWRSZWNpZXZlZCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgICAvLyBnZXRQZXJzb25JZCA9IHRoaXMucGVyc29uSWRSZWNpZXZlZC5hc09ic2VydmFibGUoKTtcclxuICAgIGFsbEdkcHJDb250ZW50czogYW55W107IC8vIGFsbCBnZHByIGNvbnRlbnRzIHN0b3JlXHJcbiAgICBjdXJyZW50R2RwclR5cGU6IGFueTsgLy8gY3VycmVudCBnZHByIHR5cGVcclxuICAgIGN1cnJlbnRHZHBySW5kZXg6IGFueTsgLy8gY3VycmVudCBnZHByIGluZGV4XHJcbiAgICBhbGxHZHByQXJyaXZlZDogRXZlbnRFbWl0dGVyPGJvb2xlYW4+ID0gbmV3IEV2ZW50RW1pdHRlcigpOyAvLyBldmVudGVtbWl0ZXIgZm9yIGFsbCBnZHByIGNvbnRlbnQgbG9hZGVyXHJcbiAgICBnZHByTW9kYWxIZWFkZXJUaXRsZTogYW55OyAvLyBnZHByIG1vZGFsIGhlYWRlciB0aXRsZVxyXG4gICAgZ2Rwck1vZGFsQWdyZWVUZXh0OiBhbnk7IC8vIGdkcHIgbW9kYWwgYWdyZWUgdGV4dFxyXG4gICAgZ2Rwck1vZGFsRGlzQWdyZWVUZXh0OiBhbnk7IC8vIGdkcHIgbW9kYWwgZGlzYWdyZWUgdGV4dFxyXG4gICAgdGVybUFuZENvbmRpdGlvbjogYW55OyAvLyB0ZXJtIGFuZCBjb25kaXRpb24gc3RvcmVcclxuICAgIHJlcXVpcmVkQ29uZGl0aW9uczogYW55OyAvLyByZXF1aXJlZCBjb25kaXRpb25zIGFycmF5XHJcbiAgICBjb250ZXh0SWQ6IGFueTsgLy8gc3RvcmUgY29udGV4dCBJZCBvZiBHRFBSXHJcbiAgICBwdWJsaWMgdGVybXNDb25kaXRpb25Gb3JtOiBGb3JtR3JvdXA7IC8vIERlY2xhcmF0aW9uIG9mIFQmQyBmb3JtXHJcblxyXG4gICAgcmVhZEZ1bGxEb2N1bWVudDogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBASG9zdExpc3RlbmVyKCdzY3JvbGwnLCBbJyRldmVudCddKVxyXG4gICAgb25TY3JvbGwoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGlmICghdGhpcy5yZWFkRnVsbERvY3VtZW50ICYmIChldmVudC50YXJnZXQub2Zmc2V0SGVpZ2h0ICsgZXZlbnQudGFyZ2V0LnNjcm9sbFRvcCkgPj0gZXZlbnQudGFyZ2V0LnNjcm9sbEhlaWdodCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlYWRGdWxsRG9jdW1lbnQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUdkcHJGb3JtKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UsIFxyXG4gICAgICAgIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcclxuICAgICAgICBwcml2YXRlIGdkcHJTZXJ2aWNlOiBQbGF0Zm9ybUdkcHJTZXJ2aWNlLFxyXG4gICAgICAgQE9wdGlvbmFsKCkgQEluamVjdChcIklBcHBQbGF0ZnJvbUdkcHJcIikgcHJpdmF0ZSBJQXBwUGxhdGZyb21HZHByLFxyXG4gICAgICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyKSB7XHJcblxyXG4gICAgICAgIC8vICBjb25zb2xlLmxvZyhcImdkcHIgY29tcG9uZW50IGxvYWRlZFwiKTtcclxuICAgICAgICB0aGlzLnRlcm1BbmRDb25kaXRpb24gPSB7fTsgLy8gcmVzZXRpbmcgYXQgY29tcG9uZW50IGxvYWRcclxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgICAgIC8vIHdhaXRpbmcgZm9yIGFsbCBnZHByIGNvbnRlbnRzIGFycml2YWxcclxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuYWxsR2RwckFycml2ZWQuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbm9ybWFsaXplR2RwckNvbnRlbnRzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2FkR2RwckNvbnRlbnQoKXtcclxuICAgICAgICBpZih0aGlzLmdldEdkcHJDb250ZW50VXNpbmdJbnRlcmZhY2Upe1xyXG4gICAgICAgICAgICB0aGlzLklBcHBQbGF0ZnJvbUdkcHIuZ2V0R2RwckNvbnRlbnRzKHRoaXMucmVxdWlyZWRDb25kaXRpb25zLCB0aGlzLnVzZXJFbWFpbCkuc3Vic2NyaWJlKChyZXNwb25zZTphbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsR2RwckNvbnRlbnRzID0gdGhpcy5nZXREZW5vbUdkcHJDb250ZW50KHJlc3BvbnNlKSA7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxHZHByQXJyaXZlZC5lbWl0KHRydWUpOyAvLyBlbWl0dGluZyBldmVudCBhZnRlciBhbGwgZ2RwciBjb250ZW50IGFycml2ZWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdXaXRob3V0IEludGVyZmFjZSAhIScpO1xyXG4gICAgICAgICAgICB0aGlzLmdkcHJTZXJ2aWNlLmdldEFsbEdkcHJDb250ZW50KCkuc3Vic2NyaWJlKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgICAgICAgICAgICAgICAvLyBpZiBnZHByIGNvbnRlbnQgbG9hZHMgZnJvbSBkYXRhYmFzZVxyXG4gICAgICAgICAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGdkcHJDb250ZW50TGlzdCA9IHRoaXMuZ2V0RGVub21HZHByQ29udGVudChyZXNwb25zZSkgO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBzb3J0T3JkZXIgPSB0aGlzLnJlcXVpcmVkQ29uZGl0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICBnZHByQ29udGVudExpc3Quc29ydChmdW5jdGlvbihhOmFueSwgYjphbnkpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc29ydE9yZGVyLmluZGV4T2YoYS5UZXJtQW5kQ29uZGl0aW9uVHlwZSkgLSBzb3J0T3JkZXIuaW5kZXhPZihiLlRlcm1BbmRDb25kaXRpb25UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbEdkcHJDb250ZW50cyA9IGdkcHJDb250ZW50TGlzdCA7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2FsbEdkcHJDb250ZW50cycsdGhpcy5hbGxHZHByQ29udGVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsR2RwckFycml2ZWQuZW1pdCh0cnVlKTsgLy8gZW1pdHRpbmcgZXZlbnQgYWZ0ZXIgYWxsIGdkcHIgY29udGVudCBhcnJpdmVkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyQ29udGVudENoZWNrZWQoKSB7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlR2RwckZvcm0oKSB7XHJcbiAgICAgICAgdGhpcy50ZXJtc0NvbmRpdGlvbkZvcm0uY29udHJvbHNbJ3Rlcm1WYWx1ZSddLnNldFZhbHVlKHRoaXMucmVhZEZ1bGxEb2N1bWVudCk7XHJcbiAgICAgICAgdGhpcy50ZXJtc0NvbmRpdGlvbkZvcm0uY29udHJvbHNbJ3Rlcm1WYWx1ZSddLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZW5vbUdkcHJDb250ZW50KGdkcHJEYXRhTGlzdCl7XHJcbiAgICAgICByZXR1cm4gZm9yRWFjaChnZHByRGF0YUxpc3QsIChkYXRhKT0+IG5ldyBEVE9HRFBSKGRhdGEpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRlbm9ybWFsaXplR2RwckNvbnRlbnRzKCk6IHZvaWQge1xyXG4gICAgICAgIC8vICBjb25zb2xlLmxvZygnZ2RwciBpbmZvcm1hdGlvbiBhcnJpdmVkIGZyb20gbW9uZ28nKTtcclxuICAgICAgIC8vICAgY29uc29sZS5sb2codGhpcy5hbGxHZHByQ29udGVudHMpO1xyXG4gICAgICAgIHRoaXMuc2V0Q3VycmVudEdwZHJPcHRpb25zKDApOyAvLyBzZXR0aW5nIGluaXRpYWwgZ2RwciBvcHRpb24gdG8gZmlyc3QgZWxlbWVudCBvZiBhcnJheVxyXG4gICAgICAgIHRoaXMub3BlbkdkcHJDb25kaXRpb25Nb2RhbCgpOyAvLyBvcGVubmluZyBHRFBSIGNvbmRpdGlvbmFsIG1vZGFsXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldEN1cnJlbnRHcGRyT3B0aW9ucyhpbmRleD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY3VycmVudEdkcHJJbmRleCA9IGluZGV4ID8gaW5kZXggOiAwO1xyXG4gICAgICAgIHRoaXMuY3VycmVudEdkcHJUeXBlID0gdGhpcy5yZXF1aXJlZENvbmRpdGlvbnNbdGhpcy5jdXJyZW50R2RwckluZGV4XTtcclxuICAgICAgICBjb25zb2xlLmxvZygnY3VycmVudEdkcHJUeXBlJyx0aGlzLmN1cnJlbnRHZHByVHlwZSk7XHJcbiAgICAgICAgLy90aGlzLnJlYWRGdWxsRG9jdW1lbnQgPSBmYWxzZTtcclxuICAgICAgICAvL3RoaXMudXBkYXRlR2RwckZvcm0oKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RvcmVJbml0aWFsRGF0YSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnJlcXVpcmVkQ29uZGl0aW9ucyA9IHRoaXMucmVxdWlyZWRDb25kaXRpb25zRGV0YWlscy5SZXF1aXJlZFRlcm1zQW5kQ29uZGl0aW9uczsgLy8gc3RvcmluZyByZXF1aXJlZCBjb25kaXRpb25zIGFycmF5XHJcbiAgICAgICAgdGhpcy5jb250ZXh0SWQgPSB0aGlzLnJlcXVpcmVkQ29uZGl0aW9uc0RldGFpbHMuQ29udGV4dElkOyAvLyBzdG9yaW5nIGNvbnRleHRJRCBzdXBwbGllZCBieSBsb2dpbiBhcHAgYXMgSU5QVVRcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb3BlbkdkcHJDb25kaXRpb25Nb2RhbCgpIHtcclxuXHJcbiAgICAgICAgbGV0IGNvbmZpcm0gPSAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2NhbGwgY29uZmlybSAhIScpO1xyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnRlcm1zQ29uZGl0aW9uRm9ybS52YWx1ZSk7XHJcbiAgICAgICAgICAgIC8vdGhpcy50ZXJtQW5kQ29uZGl0aW9uW3RoaXMuY3VycmVudEdkcHJUeXBlXSA9IHRoaXMudGVybXNDb25kaXRpb25Gb3JtLnZhbHVlLnRlcm1WYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy50ZXJtQW5kQ29uZGl0aW9uW3RoaXMuY3VycmVudEdkcHJUeXBlXSA9IHRydWU7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvdGFsQ29uZGl0aW9uQ291bnQgPSB0aGlzLnJlcXVpcmVkQ29uZGl0aW9ucy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICBpZiAodG90YWxDb25kaXRpb25Db3VudCA+IHRoaXMuY3VycmVudEdkcHJJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRDdXJyZW50R3Bkck9wdGlvbnModGhpcy5jdXJyZW50R2RwckluZGV4ICsgMSk7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBHZHByRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBcImdyYW50X3R5cGVcIjogXCJhdXRoZW50aWNhdGVfdGVybXNfYW5kX2NvbmRpdGlvbnNcIixcclxuICAgICAgICAgICAgICAgICAgICBcImNvbnRleHRfaWRcIjogdGhpcy5jb250ZXh0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJhY2NlcHRlZF90ZXJtc19hbmRfY29uZGl0aW9uc1wiOiBKU09OLnN0cmluZ2lmeSh0aGlzLnRlcm1BbmRDb25kaXRpb24pLFxyXG4gICAgICAgICAgICAgICAgICAgIFwicmVxdWlyZWRfY29uZGl0aW9uc1wiOiBKU09OLnN0cmluZ2lmeSh0aGlzLnJlcXVpcmVkQ29uZGl0aW9ucylcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuZ2RwclNlcnZpY2UuU3RvcmVHRFBSZGF0YShHZHByRGF0YSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLk9uR0RQUmFjY2VwdGFuY2VTdWNjZXNzRnVsbCAmJiB0aGlzLk9uR0RQUmFjY2VwdGFuY2VTdWNjZXNzRnVsbC5lbWl0KHtHZHByRGF0YTogR2RwckRhdGEsIEVtYWlsOiB0aGlzLnVzZXJFbWFpbCwgU3RhdHVzOiB0cnVlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2socmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuT25HRFBSYWNjZXB0YW5jZVN1Y2Nlc3NGdWxsICYmIHRoaXMuT25HRFBSYWNjZXB0YW5jZVN1Y2Nlc3NGdWxsLmVtaXQoe0dkcHJEYXRhOiBHZHByRGF0YSxFbWFpbDogdGhpcy51c2VyRW1haWwsIEVycm9yOiBlcnJvci5lcnJvciwgU3RhdHVzOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBsZXQgY2FuY2VsID0gKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjYWxsIGNlbmNlbGVkJyk7XHJcbiAgICAgICAgICAgIHRoaXMudGVybUFuZENvbmRpdGlvblt0aGlzLmN1cnJlbnRHZHByVHlwZV0gPSBmYWxzZTtcclxuICAgICAgICAgICAgY29uc3QgR2RwckRhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBcImdyYW50X3R5cGVcIjogXCJhdXRoZW50aWNhdGVfdGVybXNfYW5kX2NvbmRpdGlvbnNcIixcclxuICAgICAgICAgICAgICAgIFwiY29udGV4dF9pZFwiOiB0aGlzLmNvbnRleHRJZCxcclxuICAgICAgICAgICAgICAgIFwiYWNjZXB0ZWRfdGVybXNfYW5kX2NvbmRpdGlvbnNcIjogSlNPTi5zdHJpbmdpZnkodGhpcy50ZXJtQW5kQ29uZGl0aW9uKSxcclxuICAgICAgICAgICAgICAgIFwicmVxdWlyZWRfY29uZGl0aW9uc1wiOiBKU09OLnN0cmluZ2lmeSh0aGlzLnJlcXVpcmVkQ29uZGl0aW9ucylcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdkcHJTZXJ2aWNlLlN0b3JlR0RQUmRhdGEoR2RwckRhdGEpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLk9uR0RQUmFjY2VwdGFuY2VTdWNjZXNzRnVsbCAmJiB0aGlzLk9uR0RQUmFjY2VwdGFuY2VTdWNjZXNzRnVsbC5lbWl0KHtHZHByRGF0YTogR2RwckRhdGEsRW1haWw6IHRoaXMudXNlckVtYWlsLCAgRXJyb3I6IGVycm9yLmVycm9yLCBTdGF0dXM6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGxldCBvcHRpb25zOiBNb2RhbERpYWxvZ09wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHQ6IHsgXHJcbiAgICAgICAgICAgICAgICBwcm9tcHRNc2c6IFwiVGhpcyBpcyB0aGUgcHJvbXB0IG1lc3NhZ2UhISFcIiwgIFxyXG4gICAgICAgICAgICAgICAgdGVtcGxhdGU6IHRoaXMuZ2RwckNvbnRhaW5lcixcclxuICAgICAgICAgICAgICAgIGdkcHJDb25maWd1cmF0aW9uOiB0aGlzLmdkcHJDb25maWd1cmF0aW9uLFxyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb25zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uZmlybTogY29uZmlybSxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWw6IGNhbmNlbFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52aWV3Q29udGFpbmVyUmVmXHJcbiAgICAgICAgfTtcclxuICAgIFxyXG4gICAgICAgIHRoaXMubW9kYWxTZXJ2aWNlLnNob3dNb2RhbChNb2RhbENvbnRlbnRDb21wb25lbnQsIG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKChkaWFsb2dSZXN1bHQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXN1bHQgPSBkaWFsb2dSZXN1bHRcclxuICAgICAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zdG9yZUluaXRpYWxEYXRhKCk7IC8vIHN0b3JpbmcgaW5pdGlhbCB2YWx1ZVxyXG5cclxuICAgICAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuICAgICAgICAvLyBnZXQgYWxsIGdkcHIgY29udGVudCBhdCBmaXJzdCBjYWxsXHJcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4gICAgICAgIHRoaXMubG9hZEdkcHJDb250ZW50KCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLyogdGhpcy50ZXJtc0NvbmRpdGlvbkZvcm0gPSB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgICAgICAgXCJ0ZXJtVmFsdWVcIjogW3RoaXMucmVhZEZ1bGxEb2N1bWVudCwgVmFsaWRhdG9ycy5yZXF1aXJlZFRydWVdXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZ2RwclNlcnZpY2UuZ2RwclJlbG9hZENvbnRyb2wuc3Vic2NyaWJlKHRlcm1zQ29uZGl0aW9uRGV0YWlscz0+e1xyXG4gICAgICAgICAgICBpZih0ZXJtc0NvbmRpdGlvbkRldGFpbHMpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy50ZXJtQW5kQ29uZGl0aW9uID0gdGVybXNDb25kaXRpb25EZXRhaWxzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkR2RwckNvbnRlbnQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pOyAqL1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRFRPR0RQUntcclxuICAgIEl0ZW1JZDogc3RyaW5nO1xyXG4gICAgTGFuZ3VhZ2U6IHN0cmluZztcclxuICAgIFRlcm1BbmRDb25kaXRpb25Db250ZW50OiBzdHJpbmc7XHJcbiAgICBUZXJtQW5kQ29uZGl0aW9uVHlwZTogc3RyaW5nO1xyXG4gICAgY29uc3RydWN0b3IoZGF0YT8pe1xyXG4gICAgICAgIGRhdGEgPSBkYXRhIHx8IHt9O1xyXG4gICAgICAgIHRoaXMuSXRlbUlkID0gZGF0YSAmJiBkYXRhLkl0ZW1JZCB8fCBcIlwiO1xyXG4gICAgICAgIHRoaXMuTGFuZ3VhZ2UgPSBkYXRhICYmIGRhdGEuTGFuZ3VhZ2UgfHwgXCJlbi1VU1wiO1xyXG4gICAgICAgIHRoaXMuVGVybUFuZENvbmRpdGlvbkNvbnRlbnQgPSBkYXRhICYmIGRhdGEuVGVybUFuZENvbmRpdGlvbkNvbnRlbnQgfHwgXCJcIjtcclxuICAgICAgICB0aGlzLlRlcm1BbmRDb25kaXRpb25UeXBlID0gZGF0YSAmJiBkYXRhLlRlcm1BbmRDb25kaXRpb25UeXBlIHx8IFwiXCI7XHJcbiAgICB9XHJcbn0iXX0=