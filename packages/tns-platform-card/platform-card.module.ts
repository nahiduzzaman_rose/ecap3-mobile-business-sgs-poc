import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {PlatformCardService} from "./services/platform-card.service";
import { PlatformCardDefaultComponent } from "./components/card-default/card-default.component";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";



@NgModule({
    providers: [
        PlatformCardService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule,
        InfiniteScrollModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        PlatformCardDefaultComponent
    ],
    entryComponents: [
    ],
    exports: [
        PlatformCardDefaultComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformCardModule {}
