import { Injectable } from '@angular/core';
import { QueryRef, Apollo } from 'apollo-angular';
import { WatchQueryOptions, MutationOptions } from 'apollo-client';
import { R } from 'apollo-angular/types';
import { Observable } from 'rxjs';
import { FetchResult } from 'apollo-link';

@Injectable({
    providedIn: 'root',
})
export class GraphqlDataService {

    constructor(private apollo: Apollo) {}

    watchQuery<T, V = R>(options: WatchQueryOptions<V>): QueryRef<T, V> {
        return this.apollo.watchQuery(options);
    }

    mutate<T, V = R>(options: MutationOptions<T, V>): Observable<FetchResult<T>> {
        return this.apollo.use('Mutations').mutate(options);
    }

    watchQueryCache<T, V = R>(options: WatchQueryOptions<V>): QueryRef<T, V> {
        return this.apollo.use('QueryCache').watchQuery(options);
    }

    mutateCache<T, V = R>(options: MutationOptions<T, V>): Observable<FetchResult<T>> {
        return this.apollo.use('MutationsCache').mutate(options);
    }
}