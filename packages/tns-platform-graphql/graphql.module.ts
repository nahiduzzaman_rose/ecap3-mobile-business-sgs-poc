import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgModule, NO_ERRORS_SCHEMA, ModuleWithProviders, Inject } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule, BrowserTransferStateModule, TransferState, makeStateKey } from '@angular/platform-browser';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { AppSettingsProvider } from "../tns-core/service/provider/settings.provider";
import { configService } from "./services/config.service";


const STATE_KEY = makeStateKey<any>('apollo.state');

@NgModule({
    providers: [],
    imports: [
        NativeScriptCommonModule,
        HttpClientModule,
        ApolloModule,
        HttpLinkModule,
        NativeScriptHttpClientModule
    ],
    declarations: [],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})

export class GraphQLModule {
    private apolloClient: Apollo = null;
    private cache: InMemoryCache = new InMemoryCache({
        addTypename: false
    });

    static forRoot(config?: any): ModuleWithProviders {
        return {
            ngModule: GraphQLModule,
            providers: [
                {
                    provide: configService,
                    useValue: config
                }
            ]
        };
    }

    constructor(apollo: Apollo,
        private httpLink: HttpLink,
        private appSettingsProvider: AppSettingsProvider,
        @Inject(configService) public config
    ) {
        const queryUri = config.queryUri;
        const mutationUri = config.mutationUri;
        console.log(queryUri);
        console.log(mutationUri);

        const { queryHttp, mutationHttp } = this.getHttpLink(this.httpLink, queryUri, mutationUri);
        const authMiddleware = this.middleware()

        const cacheRemoveConfig = {
            watchQuery: {
                fetchPolicy: 'network-only',
                errorPolicy: 'ignore',
            },
            query: {
                fetchPolicy: 'network-only',
                errorPolicy: 'all',
            }
        }
        // create Apollo Default
        this.createClient(apollo, authMiddleware, queryHttp, mutationHttp, cacheRemoveConfig);
    }

    /* public initModule() {
        const queryUri = this.appSettingsProvider.getAppSettings().GQLQueryUri;
        const mutationUri = this.appSettingsProvider.getAppSettings().GQLMutationUri

        console.log('queryUrix', queryUri);
        const { queryHttp, mutationHttp } = this.getHttpLink(this.httpLink, queryUri, mutationUri);
        const authMiddleware = this.middleware()

        const cacheRemoveConfig = {
            watchQuery: {
                fetchPolicy: 'network-only',
                errorPolicy: 'ignore',
            },
            query: {
                fetchPolicy: 'network-only',
                errorPolicy: 'all',
            }
        }

        // create Apollo Default
        this.createClient(this.apolloClient, authMiddleware, queryHttp, mutationHttp, cacheRemoveConfig);
    } */

    private createClient(apollo: Apollo, authMiddleware: ApolloLink, queryHttp, mutationHttp, cacheRemoveConfig) {
        apollo.create({
            link: queryHttp,
            cache: this.cache,
            defaultOptions: cacheRemoveConfig,
        });
        // create Apollo Mutations
        apollo.create({
            link: mutationHttp,
            cache: this.cache,
            defaultOptions: cacheRemoveConfig,
        }, 'Mutations');

        apollo.create({
            link: queryHttp,
            cache: this.cache,
        }, 'QueryCache');

        // create Apollo Mutations
        apollo.create({
            link: mutationHttp,
            cache: this.cache,
        }, 'MutationsCache');
    }

    private middleware() {
        return new ApolloLink((operation, forward) => {
            const token = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNyc2Etc2hhMjU2IiwidHlwIjoiSldUIn0.eyJ0ZW5hbnRfaWQiOiJGQzAwNjI3OC0yRDUxLTQ3MDMtOTUyQi1DMzEwMjY3NDYwMjYiLCJzdWIiOiI4MjQ2Nzk1OC00NzY4LTRlNjQtOTI4ZC1hZTkyYTg4ZWQ2NWUiLCJzaXRlX2lkIjoiNEFERDY4QzUtMkIzRC00NDc4LUFGQzYtMTE1MTBGOTU1QjkxIiwib3JpZ2luIjoic3dpc3NwYXJ0bmVyc3BvcnRhbC5mYWtlZG9tYWluLmNvbSIsInNlc3Npb25faWQiOiJlZTVlNTg3MTk4MTU0NzY0YTNiYThmMzk3OGRmMjY1NyIsInVzZXJfaWQiOiI4MjQ2Nzk1OC00NzY4LTRlNjQtOTI4ZC1hZTkyYTg4ZWQ2NWUiLCJkaXNwbGF5X25hbWUiOiJrYW1ydXp6YW1hbiB6YW1hbiIsInNpdGVfbmFtZSI6IkVjYXAgVGVhbSIsInVzZXJfbmFtZSI6Im1kLmthbXJ1enphbWFuQHNlbGlzZS5jaCIsImVtYWlsIjoibWQua2FtcnV6emFtYW5Ac2VsaXNlLmNoIiwicGhvbmVfbnVtYmVyIjoiKzg4MDE3MjI3NTM0OTciLCJsYW5ndWFnZSI6ImVuLVVTIiwidXNlcl9sb2dnZWRpbiI6IlRydWUiLCJuYW1lIjoiODI0Njc5NTgtNDc2OC00ZTY0LTkyOGQtYWU5MmE4OGVkNjVlIiwicm9sZSI6WyJhZG1pbiIsImFwcHVzZXIiLCJhbm9ueW1vdXMiXSwibmJmIjoxNTQ5MjAwMTc4LCJleHAiOjE1OTEyMDAxNzgsImlzcyI6IkNOPUVudGVycHJpc2UgQ2xvdWQgQXBwbGljYXRpb24gUGxhdGZvcm0iLCJhdWQiOiIqIn0.YwBsw1tRl2FwZyw-0r25g_Jbj7XvZ0FSqvbIRKnDJOTr5fF55n4mltDP0bm7c1SnSYjOsklgsoQ71NLHD1Mq3n4geJb_Vdf--elAgUZYJxQOxQgzNiQWf3DEOPiv_liUN9kcl_bRGoqtiWqZ9Ft4ZHEmTLcOMQim_tISXSc9F3NzXp2HL5nq6HFySpl21EOKoLpV2_KHRKhfVD9ePmWi5nfK9WX1WLJJKyWHYfJHHJoa_GRqEkLsunqmq7P4yADK_SEbtbKGVbhH7JixUjUf0gZjAvODN8SP1jmAPRNW5RWgxvG_JnxT0aF_BLbitCNQw94qgWtSdjg0qrgafegKug";
            operation.setContext(({ headers }) => ({
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }));
            return forward(operation);
        });
    }

    private getHttpLink(httpLink: HttpLink, queryUri: any, mutationUri: any) {
        const queryHttp = httpLink.create({
            uri: queryUri,
            withCredentials: true,
            method: 'POST'
        });
        const mutationHttp = httpLink.create({
            uri: mutationUri,
            withCredentials: true,
            method: 'POST'
        });
        return { queryHttp, mutationHttp };
    }
}
