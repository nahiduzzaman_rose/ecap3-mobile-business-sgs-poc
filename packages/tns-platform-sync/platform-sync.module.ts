import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {PlatformSyncService} from "./services/platform-sync.service";
import {PlatformSyncComponent} from "./component/platform-sync.component";
// import {SyncStatusCancellationComponent} from "./component/sync-status-cancellation/sync-status-cancellation.component";
import {TNSFontIconModule} from "nativescript-ngx-fonticon";
import { SyncStatusCancellationComponent } from "./component/sync-status-cancellation/sync-status-cancellation.component";
// import {DataDownLoadService} from "~/packages/tns-platform-sync/services/download-service";
// import {InterceptorModule} from "~/packages/shared";

@NgModule({
    providers: [
        PlatformSyncService,
        // DataDownLoadService
    ],
    imports: [
        NativeScriptCommonModule,
        TNSFontIconModule,
        // InterceptorModule
    ],
    declarations: [
        PlatformSyncComponent,
        SyncStatusCancellationComponent
    ],
    exports: [PlatformSyncComponent],
    entryComponents: [
        // SyncStatusCancellationComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformSyncModule {
}

// import {NativeScriptCommonModule} from "nativescript-angular/common";
// import {NativeScriptFormsModule} from "nativescript-angular/forms";
// import {ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
// import {PlatformSyncService} from "./services/platform-sync.service";
// import {PlatformSyncComponent} from "./component/platform-sync.component";
// import {DataDownLoadService} from "~/packages/tns-platform-sync/services/download-service";
// import {
//     AggregateService, AppProvider,
//     AuthGuard, EcapLoginService,
//     FeatureProvider,
//     InterceptorModule, NavigationProvider,
//     PlatformDataService, SqlQueryBuilderService, StorageDataService,
//     TokenProvider, UserInfoService
// } from "~/packages/shared";
// import {CouchbaseAdapterService} from "~/packages/shared/service/local-pds/couchbase-adapter.service";
// import {TokenState} from "~/packages/shared/service/auth/token.state";
// // import {InterceptorModule} from "~/packages/shared";
//
// @NgModule({
//     providers: [
//         PlatformSyncService,
//         DataDownLoadService
//     ],
//     imports: [
//         NativeScriptCommonModule,
//         // InterceptorModule
//     ],
//     declarations: [
//         PlatformSyncComponent
//     ],
//     exports: [PlatformSyncComponent],
//     schemas: [NO_ERRORS_SCHEMA]
// })
// export class PlatformSyncModule {
//     public static forRoot(): ModuleWithProviders {
//         return {
//             ngModule: PlatformSyncModule,
//             providers: [PlatformSyncService, DataDownLoadService]
//         };
//     }
// }
