export class SyncStepModel {
    StepCode: string;
    StepName: string;
    StatusCode: string;
    Description?: string;
}
