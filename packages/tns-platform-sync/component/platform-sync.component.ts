import {RouterExtensions,} from "nativescript-angular/router";
import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
    Injectable,
    Inject,
    ChangeDetectionStrategy, ViewContainerRef, ChangeDetectorRef, HostListener
} from "@angular/core";
import {Router} from "@angular/router";
import {Page} from "ui/page";

import {PlatformSyncService} from "../services/platform-sync.service";
import {File, Folder, path} from "tns-core-modules/file-system";
import localize from "nativescript-localize";

var appModule = require("application");
import {Zip} from "nativescript-zip";
import * as fs from "file-system";
import {LocalPlatformDataService, StorageDataService, TokenProvider} from "@ecap3/tns-core";
import * as bgHttp from "nativescript-background-http";
import {DataDownLoadService} from "../services/download-service";
import * as _ from "lodash";
import {from, Observable, of, Subscription} from "rxjs";
import {android as androidApp, ios as iosApp} from "tns-core-modules/application";
import {Downloader, ProgressEventData, DownloadEventData} from "nativescript-downloader";
import {DownloadProgress} from "nativescript-download-progress";
import {DownloadManager} from "nativescript-downloadmanager";
import {getFile} from "tns-core-modules/http";
import {ModalDialogOptions, ModalDialogService} from "nativescript-angular/directives/dialogs";
import {connectionType, getConnectionType} from "tns-core-modules/connectivity";
// import {SyncStatusCancellationComponent} from "~/packages/tns-platform-sync/component/sync-status-cancellation/sync-status-cancellation.component";

import * as firebase from "nativescript-plugin-firebase"
import {catchError, map} from "rxjs/operators";

@Component({
    selector: "app-platform-sync",
    moduleId: module.id,
    templateUrl: "./platform-sync.component.html",
    styleUrls: ["./platform-sync-common.css", "./platform-sync.component.android.css", "./platform-sync.component.ios.css"],
    providers: [DataDownLoadService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlatformSyncComponent implements OnInit, OnDestroy {
    public folderName: string;
    public fileName: string;
    public currentSyncId: string = "";
    public sourceFileId: string = "";
    public isSyncMode: boolean = false;
    public zipPreSignedUploadUrl: string = "";
    public syncZipFilePath: string = "";
    private session: any;
    public tasks: bgHttp.Task[] = [];
    public events: { eventTitle: string, eventData: any }[] = [];
    public allFilesURLs = [];
    public allFilesLocalPaths = [];
    private allPresignedUrls = [];

    private successFileUploadIds = [];
    private failedFileUploadIds = [];

    private successFileDownloadIds = [];
    private failedFileDownloadIds = [];

    private fileToUploadsCount = 0;
    private isAllFileUploaded = false;

    public unzipFilePath: any;
    public syncProgressStepArray: any;

    private isInternetFailedGen: boolean = false;
    private isSyncCancelledGen: boolean = false;
    private isAllFileUploadedGen: boolean = false;
    private isFileDownloadSkippedGen: boolean = false;

    subscription: Subscription[] = [];

    @Input()
    isReadOnlySyncInput: boolean = false;

    @Input()
    syncPrepDataInput: any = {};

    @Input()
    isFilesNeedToDownloadInput: boolean = false;

    @Input()
    filesToDownloadInput: any = [];

    @Input()
    isSyncFilesReadyForZipInput: boolean = false;

    @Input()
    syncFilesFolderInput: Folder;

    @Input()
    filesToUploadInput: any = [];

    @Input()
    outputSyncBulkArchiveFileIdInput: any = "";

    @Output()
    initFilesSyncFinish: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    startLocalMediaUpload: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    filesDownloadFinish: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    uploadFinish: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    updateSyncLog: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    processFileUploadUpdate: EventEmitter<any> = new EventEmitter<any>();

    constructor(private router: Router,
                private routerExtensions: RouterExtensions,
                private page: Page,
                private modal: ModalDialogService,
                private vcRef: ViewContainerRef,
                private localDataService: LocalPlatformDataService,
                private platformSyncService: PlatformSyncService,
                private dataDownLoadService: DataDownLoadService,
                private storageDataService: StorageDataService,
                private tokenProvider: TokenProvider,
                private cdrf: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        if (this.isSyncFilesReadyForZipInput) {
            this.session = bgHttp.session("image-upload");
            this.subscription.push(
                from(this.zipLocalSyncFolder()).subscribe(resZip => {
                    if (resZip) {
                        this.subscription.push(
                            this.getMultiplePreSignedUrls().subscribe(resMPU => {
                                if (resMPU) {
                                    this.subscription.push(
                                        this.getLocalSyncFolderZipPreSignedURL().subscribe(resZpU => {
                                            this.uploadSyncZipFile();
                                        }, error1 => {
                                            console.error('Unable to get preSignedUrl');
                                            this.platformSyncService.setSyncCancelledFlag(true);
                                        })
                                    );
                                } else {
                                    console.error('Sync Error: Unable to get PreSigned URLs');
                                }
                            })
                        )
                    } else {
                        console.error('Sync Error: Unable to Zip source folder');
                    }
                })
            );
        }

        // Subscribe to Sync Step Events
        this._subscribeToSyncEvents();
    }

    public _subscribeToSyncEvents() {
        this.subscription.push(
            // Refresh accessToken before sync request
            this.tokenProvider.refreshTokenObservable()
                .subscribe(_ => _),
            // Subscribe to Sync Step progress event
            this.platformSyncService.getSyncStepStatusArray().subscribe((retArr: any) => {
                this.syncProgressStepArray = retArr;
            }),
            // Check for Internet connection
            this.platformSyncService.getInternetFailureFlag().subscribe(res => {
                this.isInternetFailedGen = res;
                console.log("isInternetFailed", this.isInternetFailedGen);
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
            }),
            // Check if Sync canceled
            this.platformSyncService.getSyncCancelledFlag().subscribe(res => {
                this.isSyncCancelledGen = res;
                console.log("this.isInternetFailedGen", this.isSyncCancelledGen);
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
            }),
            // Check if all source input file uploaded
            this.platformSyncService.getIsAllFileUploadedFlag().subscribe(res => {
                this.isAllFileUploadedGen = res;
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
                if (this.isAllFileUploadedGen) {
                    /*this.syncProgressStepArray[1].StatusCode = "Finished";
                    this.syncProgressStepArray[2].StatusCode = "Progress";
                    this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/
                    this.platformSyncService.finishCurrentStep('Uploading_Media');

                    this.uploadFinish.emit(this.sourceFileId);
                    this.processFileUploadUpdate.emit({
                        "FailedUploadFiles": this.failedFileUploadIds,
                        "SuccessUploadFiles": this.successFileUploadIds
                    });
                }
            }),
            // Check if download skipped
            this.platformSyncService.getIsFileDownloadSkipFlag().subscribe(res => {
                this.isFileDownloadSkippedGen = res;
                console.log("isFileDownloadSkipped::", res);
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
                if (this.isFileDownloadSkippedGen) {
                    /*this.syncProgressStepArray[4].StatusCode = "Finished";
                    this.syncProgressStepArray[5].StatusCode = "Finished";
                    this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/

                    this.platformSyncService.setSkipDownloadStep();
                }
            })
        );

        const that = this;
        firebase.addOnMessageReceivedCallback(function (message) {
            console.log("::NotificationReceived message.data.OutputSyncBulkArchiveFileId::", message.data.OutputSyncBulkArchiveFileId);
            if (!that.isSyncCancelledGen && !that.isInternetFailedGen) {
                that.getPushedFileInfo(message.data.OutputSyncBulkArchiveFileId);
            }
        });
    }

    private zipLocalSyncFolder(): Promise<any> {
        let srcpath = fs.path.join(fs.knownFolders.currentApp().path, this.syncFilesFolderInput.name);
        let dest = fs.path.join(fs.knownFolders.currentApp().path, "/" + this.syncFilesFolderInput.name + ".zip");
        return Zip.zipWithProgress(srcpath, dest, this.zipOnProgress, true, false)
            .then(() => {
                console.log("source upload zip successfully created: ", dest);
                this.syncZipFilePath = dest;
                return this.syncFilesFolderInput.remove()
                    .then(fres => {
                        console.log('Success removing the source temp folder', fres);
                        return true;
                    }).catch(err => {
                        console.log(err.stack);
                        return false;
                    });
            })
            .catch(err => {
                console.log(`zip error: ${JSON.stringify(err)}`);
            });
    }

    public getPushedFileInfo(bulkArchiveFileId) {
        if (this.outputSyncBulkArchiveFileIdInput === bulkArchiveFileId) {
            console.log("::Its my device notification::");
            if (!this.isReadOnlySyncInput) {
                /*this.syncProgressStepArray[2].StatusCode = "Finished";
                this.syncProgressStepArray[3].StatusCode = "Progress";
                this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/

                this.platformSyncService.finishCurrentStep('Downloading_Data');
            }
            return this.storageDataService.getFileInfo(bulkArchiveFileId).subscribe(response => {
                this.downloadZipFile(response.body.Url);
            });
        } else {
            console.log("::Its not my device notification::");
        }
    }

    public downloadZipFile(downURL) {
        console.log("Download Server Output ZipFile: ", downURL);
        const that = this;
        let folder = fs.knownFolders.documents();
        let file = fs.path.join(folder.path, this.outputSyncBulkArchiveFileIdInput + ".zip");

        getFile(downURL, file).then(function (r) {
            console.log("path::", r.path);
            that.unzipFilePath = r.path;
            that.unzipFile();
        }, function (e) {
            console.log("err::", e);
            //// Argument (e) is Error!
        });
    }

    private unzipFile() {
        console.log("unzipFilePath::", this.unzipFilePath);
        let syncFilesFolder = fs.knownFolders.currentApp().path + "/sync_files";
        console.log("DestinationFolderAddress::", syncFilesFolder);
        Zip.unzipWithProgress(this.unzipFilePath, syncFilesFolder, this.unzipOnProgress, true)
            .then(() => {
                console.log(`unzip successfully completed`);
                const zipOriginalFilePath = File.fromPath(this.unzipFilePath);
                zipOriginalFilePath.remove().then(removed => {
                        console.log("DownLoaded Zip File Removed", removed);
                        // this.setSyncFiles.emit(syncFilesFolder);
                        this.platformSyncService.setFileUnzipCompleted(syncFilesFolder);
                    }
                );
                // this.setSyncFiles.emit(syncFilesFolder);
            })
            .catch(err => {
                console.log(`unzip error: ${err}`);
            });
    }

    private unzipOnProgress(percent: number) {
        console.log(`unzip progress: ${percent}`);
    }

    // Zip Process //
    private zipOnProgress(percent: number) {
        console.log(`zip progress: ${percent}`);
    }

    private getMultiplePreSignedUrls(): Observable<any> {
        if (getConnectionType() === connectionType.none) {
            this.platformSyncService.setInternetFailureFlag(true);
            return of(false);
        }
        if (this.filesToUploadInput.length > 0) {
            return this.platformSyncService.getMultiplePreSignedUrl(this.filesToUploadInput).pipe(map(res => {
                console.log("Multiple Pre Signed Response for file", this.filesToUploadInput, res);
                this.allPresignedUrls = res;
                return true;
            }));
        }

        return of(true);
    }

    private getLocalSyncFolderZipPreSignedURL(): Observable<any> {
        if (getConnectionType() === connectionType.none) {
            this.platformSyncService.setInternetFailureFlag(true);
            return of(null)
        }
        this.sourceFileId = this.platformSyncService.getNewGuid();
        return this.platformSyncService.getPreSignedUrl(this.sourceFileId, this.syncFilesFolderInput.name + ".zip", null, ["File"], null)
            .pipe(
                map(res => {
                    if (res.FileId === this.sourceFileId) {
                        this.zipPreSignedUploadUrl = res.UploadUrl;
                        console.log('entityrecord Zip Upload Url',this.zipPreSignedUploadUrl);
                        return true;
                    }

                    return false;
                }),
                catchError(err => {
                    console.error('getPreSignedUrl error', err);
                    this.platformSyncService.setSyncCancelledFlag(true);
                    return of(false);
                })
            );

    }

    private uploadSyncZipFile() {
        const name = this.syncZipFilePath.substr(this.syncZipFilePath.lastIndexOf("/") + 1);
        const syncZipFile = fs.path.normalize(fs.knownFolders.currentApp().path + "/" + name);
        const description = `${name} (Sync Upload)`;
        const request = {
            url: this.zipPreSignedUploadUrl,
            method: "PUT",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: description,
            androidAutoDeleteAfterUpload: true,
            androidNotificationTitle: "Sync Zip Upload",
        };
        let task: bgHttp.Task;
        let lastEvent = "";
        if (getConnectionType() === connectionType.none) {
            this.platformSyncService.setInternetFailureFlag(true);
        } else {
            task = this.session.uploadFile(syncZipFile, request);
        }

        function onEvent(e) {
            if (lastEvent !== e.eventName) {
                // suppress all repeating progress events and only show the first one
                lastEvent = e.eventName;
            } else {
                return;
            }

            if (e.eventName === "complete") {
                console.log(`Successfully uploaded sourceFile ${this.sourceFileId}, path: ${syncZipFile}`);
                if (!this.isReadOnlySyncInput) {
                    /*this.syncProgressStepArray[0].StatusCode = "Finished";
                    this.syncProgressStepArray[1].StatusCode = "Progress";
                    this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/

                    this.platformSyncService.finishCurrentStep('Uploading_Data');
                }
                this.uploadSyncMediaFiles();
            } else if (e.eventName === "error") {
                console.error(`Error uploading sourceFile ${this.sourceFileId}, path: ${syncZipFile}`);
                this.platformSyncService.setSyncCancelledFlag(true);
            }
        }

        task.on("progress", onEvent.bind(this));
        task.on("error", onEvent.bind(this));
        task.on("responded", onEvent.bind(this));
        task.on("complete", onEvent.bind(this));
        lastEvent = "";
        this.tasks.push(task);
    }

    private uploadSyncMediaFiles() {
        if (this.filesToUploadInput.length > 0) {
            for (let i = 0; i < this.filesToUploadInput.length; i++) {
                const fileObj = this.filesToUploadInput[i];
                for (let j = 0; j < this.allPresignedUrls.length; j++) {
                    const preSignedObj = this.allPresignedUrls[j];
                    if (fileObj.ItemId === preSignedObj.FileId) {
                        const filePathtoUpload = fileObj.LocalPath;
                        console.log("UploadFilePath::", filePathtoUpload);
                        // this.fileToUploadsCount++;
                        this.uploadFile(preSignedObj.FileId, preSignedObj.UploadUrl, filePathtoUpload, false);
                    }
                }
            }
        } else {
            this.platformSyncService.setIsAllFileUploadedFlag(true);
        }
    }

    private uploadFile(fileId, upUrl, filePathForUpload, needToDelete?) {
        const name = filePathForUpload.substr(filePathForUpload.lastIndexOf("/") + 1);
        console.log(`Starting uploadFile Media file: ${filePathForUpload}, MediaFileName: ${name}, Url: ${upUrl} `);
        const request = {
            url: upUrl,
            method: "PUT",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: this.fileToUploadsCount,
            androidAutoDeleteAfterUpload: needToDelete,
            androidNotificationTitle: "Upload local MediaFile Upload HTTP background",
        };
        let task: bgHttp.Task;
        let lastEvent = "";
        task = this.session.uploadFile(filePathForUpload, request);

        const that = this;
        task.on("error", function (e) {
            console.error('Error uploading FileId::', fileId, e);
            that.fileToUploadsCount++;
            that.failedFileUploadIds.push(fileId);
            if (that.fileToUploadsCount === that.filesToUploadInput.length) {
                that.platformSyncService.setIsAllFileUploadedFlag(true);
            }
        });
        task.on("cancelled", function (e) {
            console.log('Cancelled FileId::', fileId);
            that.fileToUploadsCount++;
            that.failedFileUploadIds.push(fileId);
            if (that.fileToUploadsCount === that.filesToUploadInput.length) {
                that.platformSyncService.setIsAllFileUploadedFlag(true);
            }
        });
        task.on("complete", function (e) {
            // console.log('Complete FileId::', fileId);
            that.fileToUploadsCount++;
            that.successFileUploadIds.push(fileId);
            if (that.fileToUploadsCount === that.filesToUploadInput.length) {
                that.platformSyncService.setIsAllFileUploadedFlag(true);
            }
        });
        lastEvent = "";
        this.tasks.push(task);
    }

    // public uploadCompleted() {
    //     console.log("::Upload Completed::");
    //     console.log("::this.sourceFileId::", this.sourceFileId);
    //     return this.uploadFinish.emit(this.sourceFileId);
    // }

    @HostListener('unloaded')
    ngOnDestroy() {
        this.subscription.forEach(item => item.unsubscribe());
        this.cdrf.detach();
    }
}
