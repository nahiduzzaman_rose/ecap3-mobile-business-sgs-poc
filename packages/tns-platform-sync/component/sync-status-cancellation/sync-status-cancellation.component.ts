import {AfterViewInit, Component, OnDestroy, OnInit, ViewChildren, QueryList} from '@angular/core';
import * as application from "tns-core-modules/application";
import {ModalDialogParams} from "nativescript-angular";
import {isAndroid, isIOS, device, screen} from "tns-core-modules/platform";
import {PlatformSyncService} from "../../services/platform-sync.service";
import {GridLayout} from "tns-core-modules/ui/layouts/grid-layout";
import {Label} from "tns-core-modules/ui/label";
import {AnimationCurve} from "tns-core-modules/ui/enums";
import {Subscription} from "rxjs";

@Component({
    selector: 'gr-sync-status-cancellation',
    templateUrl: './sync-status-cancellation.component.html',
    styleUrls: ['./sync-status-cancellation.component.css'],
    moduleId: module.id,
})
export class SyncStatusCancellationComponent implements OnInit, OnDestroy {
    @ViewChildren('cmp') components: QueryList<Label>;
    screenRationWidth: any;
    screenRationHeight: any;
    isTabWidthIsNineSixty: any;
    public syncProgressStepArray: any;
    public windowHeight = 0;
    public sub1: Subscription;
    public sub2: Subscription;
    public sub3: Subscription;
    constructor(private params: ModalDialogParams, private platformSyncService: PlatformSyncService) {
    }

    ngOnInit() {
        this.sub1 = this.platformSyncService.getSyncStepStatusArray().subscribe((retArr: any) => {
            console.log("getSyncStepStatusArray", retArr);
            this.syncProgressStepArray = retArr;
        });
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
        application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
            args.cancel = true;
            console.log("::SyncStatusModal::");
            this.modalClose(false);
        });
        this.windowHeight = screen.mainScreen.heightDIPs;
    }

    ngOnDestroy(): void {
        console.log(":ngOnDestroyed:");
        if (this.sub1 != undefined) {
            this.sub1.unsubscribe();
        }
        if (this.sub2 != undefined) {
            this.sub2.unsubscribe();
        }
        this.modalClose(false);
    }

    onModalLoaded(e: any): void {
        if (isAndroid) {
            e.object._dialogFragment.getDialog().setCanceledOnTouchOutside(false);
        }
    }

    public modalClose(param: boolean) {
        console.log("SyncStatusModalClose::", param);
        this.params.closeCallback(param);
    }

    startProgressAnimation(pId) {
        pId.animate({
            rotate: 360,
            delay: 1,
            // iterations: 5,
            // curve: enums.AnimationCurve.easeIn
            // curve: AnimationCurve.easeInOut,
            duration: 10000,
            iterations: Number.POSITIVE_INFINITY,
        });
    }
}
