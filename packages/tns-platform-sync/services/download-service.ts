import {Injectable, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {Downloader, ProgressEventData, DownloadEventData} from "nativescript-downloader";

@Injectable({
    providedIn: 'root',
})

export class DataDownLoadService implements OnInit {
    downloader: Downloader;
    private previousButtonSubject: Subject<void>;

    constructor() {
        this.downloader = new Downloader();
    }

    /**
     * @ngdoc method
     * @name downloadDb
     * @description  download generic method for nay file
     * @memberof DataDownLoadService
     * @param apiUrl : - https://myserver.com/mypath
     * @param filename :- myfile.zip ...
     * @param downloadlocation : mobile local location
     */
    ngOnInit(): void {
        console.log("::DataDownLoadServiceInitiated::");
    }

    downloadFile(apiUrl, filename?, downloadlocation?) {
        const subject = new Subject<any>();
        // Request format for Downlaoder
        //     DownloadOptions {
        //     url: string;
        //     query?: Object | string;
        //     headers?: Object;
        //     path?: string;
        //     fileName?: string;
        //   }
        // Prepare the header with token work
        const headerHttp = {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + 'Token...'
        }
        const url = apiUrl;
        const zipDownloaderId = this.downloader.createDownload({
            url: url,
            path: downloadlocation,
            fileName: filename
        });

        console.log("DOWNALODER", zipDownloaderId);
        // console.log(this.downloader.getPath(zipDownloaderId));
        // console.log(this.downloader.getPath(zipDownloaderId));
        console.log(this.downloader.getStatus(zipDownloaderId));

        this.downloader
            .start(zipDownloaderId, (progressData: ProgressEventData) => {
                console.log(`Progress : ${progressData.value}%`);
                // console.log(`Current Size : ${progressData.currentSize}%`);
                // console.log(`Total Size : ${progressData.totalSize}%`);
                // console.log(`Download Speed in bytes : ${progressData.speed}%`);
            })
            .then((completed: DownloadEventData) => {
                console.log(`zip file saved at : ${completed.path}`);
                subject.next(completed.path);
            })
            .catch(error => {
                console.log('downloadDb', error.message);
                subject.error(error);
            });

        return subject.asObservable();
    }
}
