import {Injectable, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AuthGuard, TokenProvider, EcapLoginService, AppSettingsProvider} from "@ecap3/tns-core";
import {BehaviorSubject, forkJoin, Observable, of, Subject} from "rxjs";
import {delay, map, mergeMap} from "rxjs/operators";
import {capitalizationType} from "tns-core-modules/ui/dialogs";
import all = capitalizationType.all;
import * as bgHttp from "nativescript-background-http";
import * as fs from "tns-core-modules/file-system";
import {Folder} from "tns-core-modules/file-system";
import {SyncStepModel} from "../models/sync-step.model";
import localize from "nativescript-localize";

// import {knownFolders, File, Folder, path} from "tns-core-modules/file-system";

@Injectable()
export class PlatformSyncService implements OnInit {
    public fileUnzipCompleted = new Subject<string>();
    public filesNeedToDownload = new Subject<string>();
    public isInternetFails = new Subject<boolean>();
    public isSyncCancelled = new Subject<boolean>();
    public isAllFileUploadedSer = new Subject<boolean>();
    public isFileDownloadSkipped = new Subject<boolean>();
    public currentActiveStep = new BehaviorSubject(null);
    public syncStepsStatusArray: BehaviorSubject<Array<SyncStepModel>> = new BehaviorSubject([]);

    // SyncProgress Status. "Yettostart" | "Progress" | "Finished"
    public syncStatusTypes = {
        Yettostart: 'Yettostart',
        Progress: 'Progress',
        Finished: 'Finished',
        Canceled: 'Canceled',
        Error: 'Error',
    };

    constructor(private http: HttpClient,
                private featureActivator: AuthGuard,
                public ecapLoginService: EcapLoginService,
                private appSettingsProvider: AppSettingsProvider,
                private tokenProvider: TokenProvider) {
    }

    header: any = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
        "Origin": this.appSettingsProvider.getAppSettings().SubDomainName
    });

    notFoundStatus: number = 404;

    commonOptions: any = {headers: this.header, observe: "response"};

    public ngOnInit(): void {
    }

    public setInitialSyncStep() {
        this.syncStepsStatusArray.next(
            [
                {StepCode: "Uploading_Data", StepName: localize("tns.uploadingData"), StatusCode: this.syncStatusTypes.Progress, Description: ''},
                {StepCode: "Uploading_Media", StepName: localize("tns.uploadingMedia"), StatusCode: this.syncStatusTypes.Yettostart, Description: ''},
                {StepCode: "Downloading_Data", StepName: localize("tns.downloadingData"), StatusCode: this.syncStatusTypes.Yettostart, Description: ''},
                {StepCode: "Downloading_Media", StepName: localize("tns.downloadingMedia"), StatusCode: this.syncStatusTypes.Yettostart, Description: ''},
                {StepCode: "Sync_Finished", StepName: localize("tns.finalizingSync"), StatusCode: this.syncStatusTypes.Yettostart, Description: ''},
            ]
        );
    }

    public setCurrentActiveStep(stepNumber: number) {
        this.currentActiveStep.next(stepNumber);
    }

    public getCurrentActiveStep(stepNumber: number) {
        return this.currentActiveStep;
    }

    public setFileUnzipCompleted(data: any) {
        this.fileUnzipCompleted.next(data);
    }

    public getFileUnzipCompleted() {
        return this.fileUnzipCompleted;
    }

    public setFileIsFileNeedToDownload(data: any) {
        this.filesNeedToDownload.next(data);
    }

    public getFileIsFileNeedToDownload() {
        return this.filesNeedToDownload;
    }

    public setSyncStepStatusArray(data: any) {
        this.syncStepsStatusArray.next(data);
    }

    public getSyncStepStatusArray() {
        return this.syncStepsStatusArray;
    }

    public setInternetFailureFlag(flag: boolean) {
        this.isInternetFails.next(flag);
    }

    public getInternetFailureFlag(): Observable<boolean> {
        return this.isInternetFails;
    }

    public setSyncCancelledFlag(flag: boolean) {
        this.isSyncCancelled.next(flag);
    }

    public getSyncCancelledFlag(): Observable<boolean> {
        return this.isSyncCancelled;
    }
    public setIsAllFileUploadedFlag(flag: boolean) {
        this.isAllFileUploadedSer.next(flag);
    }

    public getIsAllFileUploadedFlag(): Observable<boolean> {
        return this.isAllFileUploadedSer;
    }

    public setIsFileDownloadSkipFlag(flag: boolean) {
        this.isFileDownloadSkipped.next(flag);
    }

    public getIsFileDownloadSkipFlag(): Observable<boolean> {
        return this.isFileDownloadSkipped;
    }

    public finishCurrentStep(stepName) {
        const stepIndex = this.getStepIndexByName(stepName);
        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;

        currentSyncStepsStatusArray[stepIndex].StatusCode = this.syncStatusTypes.Finished;
        if (currentSyncStepsStatusArray[stepIndex + 1]) {
            let nextStepIndex = stepIndex + 1;
            currentSyncStepsStatusArray[nextStepIndex].StatusCode = this.syncStatusTypes.Progress;
            this.setCurrentActiveStep(nextStepIndex);
        }

        this.setSyncStepStatusArray(currentSyncStepsStatusArray);
    }

    getStepIndexByName(stepName): number {
        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;;

        let indexes = currentSyncStepsStatusArray.reduce((arr, letter, index) => {
            if (letter.StepCode === stepName) arr.push(index);
            return arr;
        }, []);

        return <number>indexes.length > 0 ? indexes[0] : null;
    }

    public setSkipDownloadStep() {
        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;
        currentSyncStepsStatusArray[4].StatusCode = this.syncStatusTypes.Finished;
        currentSyncStepsStatusArray[5].StatusCode = this.syncStatusTypes.Finished;
        this.setSyncStepStatusArray(currentSyncStepsStatusArray);
    }

    public setStepDescription(stepNumber, desc) {
        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;
        if (currentSyncStepsStatusArray && currentSyncStepsStatusArray[stepNumber]) {
            currentSyncStepsStatusArray[stepNumber].Description = desc;
            this.setSyncStepStatusArray(currentSyncStepsStatusArray);
        }
    }

    public setCurrentStepDescription(desc) {
        const currentActiveStep = this.currentActiveStep.value;
        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;
        if (currentSyncStepsStatusArray && currentSyncStepsStatusArray[currentActiveStep]) {
            currentSyncStepsStatusArray[currentActiveStep].Description = desc;
            this.setSyncStepStatusArray(currentSyncStepsStatusArray);
        }
    }

    resetSync() {
        this.setInternetFailureFlag(false);
        this.setSyncCancelledFlag(false);
        this.setIsAllFileUploadedFlag(false);
        this.setIsFileDownloadSkipFlag(false);
        this.setCurrentActiveStep(0);

        this.setInitialSyncStep();
    }

    finishSync() {
        this.setInternetFailureFlag(false);
        this.setSyncCancelledFlag(false);
        this.setIsAllFileUploadedFlag(false);
        this.setIsFileDownloadSkipFlag(false);

        let currentSyncStepsStatusArray = this.syncStepsStatusArray.value;
        currentSyncStepsStatusArray[5].StatusCode = this.syncStatusTypes.Finished;
        this.setSyncStepStatusArray(currentSyncStepsStatusArray);
    }

    getSyncRootFolder(): Folder {
        return <Folder>fs.knownFolders.currentApp();
    }

    getSyncSourceUploadFolder(): Folder {
        let syncFilesFolder: Folder = this.getSyncRootFolder();
        return <Folder>syncFilesFolder.getFolder("entityrecord");
    }

    getSyncOutputFolder(): Folder {
        let syncFilesFolder: Folder = this.getSyncRootFolder();
        return <Folder>syncFilesFolder.getFolder("sync_files");
    }

    public createSyncFilesFolder(): Observable<any> {
        let folderPath = fs.path.join(this.getSyncRootFolder().path, "entityrecord");
        const syncFilesUpFolder = fs.Folder.fromPath(folderPath);

        return of(syncFilesUpFolder).pipe(delay(1000));
    }

    removeSyncDownFolderFiles() {
        let extractedSyncFilesFolder: Folder = this.getSyncOutputFolder();
        if (extractedSyncFilesFolder) {
            // >> fs-delete-folder-code
            // Remove a folder and recursively its content.
            extractedSyncFilesFolder.clear()
                .then(fres => {
                    // Success removing the folder.
                    console.log("resultMessage", "Extracted Sync Files Folder successfully cleared!");
                    return true;
                }).catch(err => {
                console.log(err.stack);
                return false;
            });
        }
    }



    public getPreSignedUrl(itemId, name, parentDirectory, tags, accessModifier?): Observable<any> {
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(
            this.appSettingsProvider.getAppSettings().Storage + "StorageQuery/GetPreSignedUrlForUpload",
            this.generatePSU(itemId, name, parentDirectory, tags, accessModifier),
            {headers: header}
        ).pipe(map((response) => {
            return response;
        }));
    }

    public getMultiplePreSignedUrl(allFiles): Observable<any> {
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        return this.http.post(
            this.appSettingsProvider.getAppSettings().Storage + "StorageQuery/GetPreSignedUrlsForUpload",
            allFiles,
            {headers: header}
        ).pipe(map((response) => {
            return response;
        }));
    }

    private generatePSU(itemId, name, parentDirectory, tags, accesModifier?) {
        let metaData = {
            "Title": {
                "Type": "String",
                "Value": name
            },
            "OriginalName": {
                "Type": "String",
                "Value": name
            }
        };
        return {
            "ItemId": itemId,
            "MetaData": "",
            "Name": name,
            "ParentDirectoryId": parentDirectory,
            "Tags": tags ? JSON.stringify(tags) : JSON.stringify(["File"]),
            "AccessModifier": accesModifier
        };
    }

    public getNewGuid() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0, v = c === "x" ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
