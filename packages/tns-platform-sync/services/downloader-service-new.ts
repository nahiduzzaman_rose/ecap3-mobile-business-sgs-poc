import {Injectable, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {Downloader, ProgressEventData, DownloadEventData} from "nativescript-downloader";

@Injectable({
    providedIn: 'root',
})

export class DownloaderServiceNew {
    public database: any;
    downloader: Downloader = new Downloader();

    constructor() {
        console.log("::NewFileDownloader::");
    }


    /**
     * @ngdoc method
     * @name downloadDb
     * @description  download generic method for nay file
     * @memberof DataDownLoadService
     * @param apiUrl : - https://myserver.com/mypath
     * @param filename :- myfile.zip ...
     * @param downloadlocation : mobile local location
     */
    downloadFile(apiUrl, filename, downloadlocation) {
        const subject = new Subject<any>();

        // Request format for Downlaoder
        //      DownloadOptions {
        //     url: string;
        //     query?: Object | string;
        //     headers?: Object;
        //     path?: string;
        //     fileName?: string;
        //   }

        // Prepare the header with token work

        const headerHttp = {
            "Content-Type": "application/json",
            "Authorization": 'Bearer ' + 'Token...'
        }

        const url = apiUrl;

        const zipDownloaderId = this.downloader.createDownload({
            url: url,
            headers: headerHttp,
            path: downloadlocation,
            fileName: filename
        });

        this.downloader
            .start(zipDownloaderId, (progressData: ProgressEventData) => {
                console.log(`Progress : ${progressData.value}%`);
                console.log(`Current Size : ${progressData.currentSize}%`);
                console.log(`Total Size : ${progressData.totalSize}%`);
                console.log(`Download Speed in bytes : ${progressData.speed}%`);
            })
            .then((completed: DownloadEventData) => {
                console.log(`zip file saved at : ${completed.path}`);
                subject.next(true);
            })
            .catch(error => {
                console.log('downloadDb', error.message);
                subject.error(error);
            });

        return subject.asObservable();
    }


}