import { EventEmitter } from "@angular/core";
import { ImagePreviewMode, ImageManager } from "../types/image-manager.type";

export interface IImageGallery {
    viewMode: ImageSelectionMode;
    imageList: Array<any>;
    selectedImage: EventEmitter<any>;
    selectedImagesList: EventEmitter<Array<any>>;
    sendSelectedImages(): Array<any>;
    sendSelectedImage(): any;
    clearSelectedImages(): Array<any>;
}

export interface ImageSelectionMode extends ImageManager {

}