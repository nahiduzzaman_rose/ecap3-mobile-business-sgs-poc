import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {PlatformImageService} from "./services/platform-image.service";
import { PlatformImageDefaultComponent } from "./components/image-default/image-default.component";
import { ImagePreviewerComponent } from "./components/image-previewer/image-previewer.component";
import { ImageManagerContainerComponent, ImageOptionContainerComponent } from "./components";
import { ImageGalleryComponent } from "./components/image-gallery/image-gallery.component";
import { ImageDetailUpdateComponent } from "./components/image-detail-update/image-detail-update.component";
import { ImageMultiPreviewerComponent } from "./components/image-multi-previewer/image-multi-previewer.component";
import { ImagePickerComponent } from "./components/image-picker/image-picker.component";



@NgModule({
    providers: [
        PlatformImageService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        PlatformImageDefaultComponent,
        ImagePreviewerComponent, 
        ImageManagerContainerComponent, 
        ImageOptionContainerComponent, 
        ImageGalleryComponent, 
        ImageDetailUpdateComponent, 
        ImageMultiPreviewerComponent, 
        ImagePickerComponent
    ],
    entryComponents: [
    ],
    exports: [
        PlatformImageDefaultComponent,
        ImagePreviewerComponent, 
        ImageManagerContainerComponent, 
        ImageOptionContainerComponent, 
        ImageGalleryComponent, 
        ImageDetailUpdateComponent, 
        ImageMultiPreviewerComponent, 
        ImagePickerComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformImageModule {}
