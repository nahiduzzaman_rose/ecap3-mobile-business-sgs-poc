import {Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from "@angular/core";
import {PlatformImageService} from "../../services/platform-image.service";
import {ImageControlPanel} from "../../types/image-container.type";
import {ImageSelectionMode} from "./../../contracts/image-gallery.interface";
import {ImagePreviewMode} from "../../types/image-manager.type";
import {knownFolders, File, Folder, path} from "tns-core-modules/file-system";
import * as utilsModule from "tns-core-modules/utils/utils";
import {PhotoEditor, PhotoEditorControl} from "nativescript-photo-editor";
import {fromFile, fromFileOrResource, ImageSource} from "tns-core-modules/image-source";
import {Image} from "tns-core-modules/ui/image";
import {ImageOptionContainerComponent} from "../image-option-container/image-option-container.component";
import {device, screen} from "tns-core-modules/platform";
import { ImagePickerPayload } from "@ecap3/tns-platform-image/types";
import {localize} from "nativescript-localize";

@Component({
    selector: "tns-platform-image-manager-container",
    templateUrl: "./image-manager-container.component.html",
    styleUrls: ["./image-manager-container.component.css"],
    moduleId: module.id,
})

export class ImageManagerContainerComponent implements OnInit {

    private _imageManagerConfig: ImageControlPanel;
    private isUploadFromDevice: boolean = false;
    public isImageEditorMode: boolean = false;
    public editableImgSource: string = "";
    screenRationWidth:any;
    screenRationHeight:any;
    isTabWidthIsNineSixty:any;
    public editorModeContext: any = {
        enableSaveButton: false,
        toggleSaveButton: (value: boolean) => {
            if (value !== null && value !== undefined) {
                this.editorModeContext.enableSaveButton = value;
            } else {
                this.editorModeContext.enableSaveButton = !this.editorModeContext.enableSaveButton;
            }
        }
    };

    @ViewChild(ImageOptionContainerComponent, {static: false}) imageOptionContainerComponent: ImageOptionContainerComponent;
    @ViewChild("camImage", {static: false}) camImageRef: ElementRef;

    @Input() galleryImageList: Array<any> = [];

    @Input() showImageType: boolean;

    @Output() closeDrawer: EventEmitter<any> = new EventEmitter();

    @Output()
    onImageCapture: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onImageUpload: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onSaveBackToCamera: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onSaveImage: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onImagePickerEventData: EventEmitter<ImagePickerPayload> = new EventEmitter<ImagePickerPayload>();

    @Output()
    selectedImage: EventEmitter<any> = new EventEmitter();

    @Output()
    selectedImageList: EventEmitter<any> = new EventEmitter();
    @Output()
    getTemplateData: EventEmitter<any> = new EventEmitter();
    radioImageOptions: any[];

    radioSelectedOption: any;

    @Input()
    set ImageManagerConfig(value: ImageControlPanel) {
        this._imageManagerConfig = value;

        if (value.galleryMode) {
            if (value.galleryMode.previewMode === ImagePreviewMode.multiple) {
                this.platformImageService.setGalleryMultiSelectMode(true);
            } else {
                this.platformImageService.setGalleryMultiSelectMode(false);
            }
        }
    }

    get ImageManagerConfig(): ImageControlPanel {
        return this._imageManagerConfig;
    }

    constructor(public platformImageService: PlatformImageService) {
    }

    ngOnInit() {
        console.log('showImageType', this.showImageType);
        this.screenRationWidth=(screen.mainScreen.widthDIPs/1280);
        this.screenRationHeight=(screen.mainScreen.heightDIPs/800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
        this.platformImageService.galleryMultiSelectMode$.subscribe(value => {
            if (value) {
                if (this.ImageManagerConfig.galleryMode) {
                    this.ImageManagerConfig.galleryMode.previewMode = ImagePreviewMode.multiple;
                } else {
                    this.ImageManagerConfig.galleryMode = {previewMode: ImagePreviewMode.multiple};
                }
            } else {
                if (this.ImageManagerConfig.galleryMode) {
                    this.ImageManagerConfig.galleryMode.previewMode = ImagePreviewMode.single;
                } else {
                    this.ImageManagerConfig.galleryMode = {previewMode: ImagePreviewMode.single};
                }
            }
        });

        this.radioImageOptions = [{
            text: localize("tns.internal"),
            value:"INTERNAL",
            selected: true
        },{
            text:localize("tns.external"),
            value:"EXTERNAL",
            selected: false
        }];

        this.radioSelectedOption =  this.radioImageOptions[0];
    }

    onChangeRadioOptions(radioOption, radioImageOptions){
        radioOption.selected = !radioOption.selected;

        if (!radioOption.selected) {
            return;
        }

        // uncheck all other options
        radioImageOptions.forEach(option => {
            if (option.text !== radioOption.text) {
                option.selected = false;
            }
        });

        this.radioSelectedOption = radioOption;
    }

    isMultiImagePreview(): boolean {
        return this.ImageManagerConfig.galleryMode && this.ImageManagerConfig.galleryMode.previewMode === ImagePreviewMode.multiple;
    }

    imageCaptured(data) {
        console.log("from image manager container Image data");
        console.log(data);
        if (data.path !== undefined) {
            this.editableImgSource = data.path;
            this.isImageEditorMode = true;
            this.onImageCapture.emit(data);
        }
    }

    imageUploaded(data) {
        console.log("Image1 imageUploaded");
        console.log(data);
        if (data.paths != undefined) {
            if (data.paths.length > 0) {
                this.editableImgSource = data.paths[0].path;
                this.isImageEditorMode = true;
                this.isUploadFromDevice = true;
                this.onImageUpload.emit(data);
            }
        }
    }

    imageData(data: ImagePickerPayload) {
        this.onImagePickerEventData.emit(data);
    }

    saveBackToCamera() {
        const passData = {SaveFilePath: this.editableImgSource, IsSelectFromDevice: this.isUploadFromDevice};
        this.onSaveBackToCamera.emit(passData);
        this.isImageEditorMode = false;
        this.isUploadFromDevice = false;
        this.platformImageService.selectedImage = null;
        setTimeout(() => {
            this.imageOptionContainerComponent.pickerComponent.captureImage();
        }, 0);
    }

     saveImage() {
        const passData = {
            SaveFilePath: this.editableImgSource, 
            IsSelectFromDevice: this.isUploadFromDevice,
            radioSelectedOption: this.radioSelectedOption
        };
        this.onSaveImage.emit(passData);
        this.isImageEditorMode = false;
        this.isUploadFromDevice = false;
        this.platformImageService.selectedImage = null;
    }

    removeUnusedFile() {
        const pathArray = this.editableImgSource.split("/");
        const fileName = pathArray[pathArray.length - 1];
        const folder = utilsModule.ad.getApplicationContext().getExternalFilesDir(null).getAbsolutePath();
        const imagePath = path.join(folder, fileName);
        console.log("ImagePath", imagePath);
        File.fromPath(imagePath).remove().then(value => {
            console.log("Remove Status ", value);
        });
        console.log("after removal::",imagePath);
    }

    closeCamDrawer() {
        if (this.isImageEditorMode) {
            if (!this.isUploadFromDevice) {
                console.log("CamDrawerCloseMImg");
                this.removeUnusedFile();
            }
            this.isImageEditorMode = false;
            this.isUploadFromDevice = false;
        }
        this.platformImageService.selectedImage = null;
        this.closeDrawer.emit();
    }

    cropImage() {
        const photoEditor = new PhotoEditor();
        //const imageSource = fromFile(this.editableImgSource);
        //console.log("ORIG IMAGE: ", imageSource.height, imageSource.width)
        photoEditor.editPhoto({
            imageSource:fromFile(this.editableImgSource), // originalImage.imageSource,
            hiddenControls: [
                // PhotoEditorControl.Save,
                // PhotoEditorControl.Clear,
                // PhotoEditorControl.Draw,
                PhotoEditorControl.Text,
            ],
        }).then((newImage: ImageSource) => {
            const lastIndex = this.editableImgSource.lastIndexOf("/");
            const imgSavePath = this.editableImgSource.slice(0, lastIndex);
            console.log("NewImage", newImage);
            
            if (!this.isUploadFromDevice) {
                // console.log("CRPRMImg");
                this.removeUnusedFile();
            console.log("file removed");
            }
            this.editableImgSource = "";
            this.camImageRef.nativeElement.src = "";
            const newImgSource = imgSavePath + "/" + "IMG" + "_" + new Date().getTime() + ".jpg";
            newImage.saveToFile(newImgSource, "jpg", 100);
            this.editableImgSource = newImgSource;
            this.camImageRef.nativeElement.src = this.editableImgSource;
        }).catch((e) => {
            // this.imgReload = false;
            console.error(e);
        });
    }

    previewImageTap(data) {
        this.selectedImage.emit(data);
    }

    previewImageListSelection(data) {
        this.selectedImageList.emit(data);
    }

    initiateEditorMode(event) {
        console.log("Initiate Edit Mode");
        console.log(event.MetaData.DirectionInfo);
        console.log(event.MetaData.DirectionInfo.Value);
        this.isImageEditorMode = true;
        this.isUploadFromDevice = true;
        this.editableImgSource = event.path.path;
        this.editorModeContext.enableSaveButton = true;
        this.getTemplateData.emit(event.MetaData.DirectionInfo.Value);
    }
}
