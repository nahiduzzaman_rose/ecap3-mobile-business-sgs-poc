import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from "@angular/core";
import {ImageControlPanel, ImagePickerPayload} from "../../types";
import { Switch } from "tns-core-modules/ui/switch";
import { ImagePickerComponent } from "../image-picker/image-picker.component";
import {device, screen} from "tns-core-modules/platform";
import { PlatformImageService } from "../../services/platform-image.service";


@Component({
    selector: "tns-platform-image-option-container",
    templateUrl: "./image-option-container.component.html",
    styleUrls: ["../image-manager-container/image-manager-container.component.css", "./image-option-container.component.css"],
    moduleId: module.id,
})
export class ImageOptionContainerComponent implements OnInit {
    public mediaSelMode: boolean = false;
    screenRationWidth:any;
    screenRationHeight:any;
    isTabWidthIsNineSixty:any;

    @ViewChild(ImagePickerComponent, { static: false }) pickerComponent: ImagePickerComponent;

    @Input()
    galleryImageList: Array<any> = [];

    @Input()
    imageOptionContainer: ImageControlPanel;

    @Output()
    closeCameraDrawer: EventEmitter<any> = new EventEmitter();

    @Output()
    onImageCapture: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onImageUpload: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    onImagePickerEventData: EventEmitter<ImagePickerPayload> = new EventEmitter<ImagePickerPayload>();

    @Output()
    selectedImage: EventEmitter<any> = new EventEmitter();

    @Output()
    selectedImageList: EventEmitter<any> = new EventEmitter();

    constructor( public platformImageService: PlatformImageService) {
    }

    ngOnInit() {
        this.screenRationWidth=(screen.mainScreen.widthDIPs/1280);
        this.screenRationHeight=(screen.mainScreen.heightDIPs/800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
    }

    public onMediaSelectionChange(args) {
        console.log('SwitchChange');
        let firstSwitch = <Switch>args.object;
        if (firstSwitch.checked) {
            this.platformImageService.setGalleryMultiSelectMode(true);
            this.mediaSelMode = true;
            console.log("ON");
        } else {
            this.platformImageService.setGalleryMultiSelectMode(false);
            this.mediaSelMode = false;
            console.log("OFF");
        }
    }

    public imageCaptured (data) {
        this.onImageCapture.emit(data);
    }

    public imageUploaded (data) {
        this.onImageUpload.emit(data);
    }

    public imageData (data: ImagePickerPayload) {
        this.onImagePickerEventData.emit(data);
    }

    tapFromCompo() {
        console.log("tap from compo");
    }
    closeDrawer() {
        // console.log("CD");
        this.closeCameraDrawer.emit();
    }

    previewImageTap( data ) {
        this.selectedImage.emit(data);
    }

    previewImageListSelection( data ) {
        this.selectedImageList.emit(data);
    }
}
