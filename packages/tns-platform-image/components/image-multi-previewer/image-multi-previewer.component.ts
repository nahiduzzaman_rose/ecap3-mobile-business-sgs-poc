import { Component, OnInit } from "@angular/core";
import { PlatformImageService } from "../../services/platform-image.service";

@Component({
  selector: "tns-platform-image-multi-previewer",
  templateUrl: "./image-multi-previewer.component.html",
  styleUrls: ["./image-multi-previewer.component.css"],
  moduleId: module.id,
})
export class ImageMultiPreviewerComponent implements OnInit {

  constructor(public platformImageService: PlatformImageService) { }

  ngOnInit( ) {
  }

}
