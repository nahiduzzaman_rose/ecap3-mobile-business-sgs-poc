import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { IImageGallery, ImageSelectionMode } from "../../contracts/image-gallery.interface";
import { ImagePreviewMode } from "../../types/image-manager.type";
import { PlatformImageService } from "../../services/platform-image.service";

@Component({
    selector: "tns-platform-image-gallery",
    templateUrl: "./image-gallery.component.html",
    styleUrls: ["./image-gallery.component.css"],
    moduleId: module.id,
})

export class ImageGalleryComponent implements OnInit, IImageGallery {

    private _selectedImage: any;
    private _selectedImages = [];

    @Input()
    viewMode: ImageSelectionMode;

    @Input()
    imageList: any[] = [];

    @Output()
    selectedImage: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    selectedImagesList: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

    constructor(public platformImageService: PlatformImageService) {
        this.viewMode = { previewMode: ImagePreviewMode.single };
    }

    ngOnInit() {
        console.log("ViewMode", this.viewMode);
    }

    isImageSelected(image, index) {
        if (!this.platformImageService.getGallerytMultiSelectMode()) {
            return false;
        } else {
            return this._selectedImages.findIndex((existing) => existing.ItemId === image.ItemId) > -1 ? true : false;
        }
    }

    onImageTap(image, index) {

        if (this.platformImageService.getGallerytMultiSelectMode()) {
            const selectedIndex = this._selectedImages.findIndex((existing) => existing.ItemId === image.ItemId);
            if (selectedIndex > -1) {
                this._selectedImages.splice(selectedIndex, 1);
            } else {
                this._selectedImages.push(image);
            }
            this.sendSelectedImages();
        } else {
            this._selectedImage = this.platformImageService.selectedImage;
            if (this._selectedImage && this._selectedImage.ItemId === image.ItemId) {
                this._selectedImage = null;
            } else {
                this._selectedImage = image;
            }
            this.sendSelectedImage();
        }
    }

    sendSelectedImages(): any[] {
        this.platformImageService.selectedImageList = [...this._selectedImages];
        this.selectedImagesList.emit(this.platformImageService.selectedImageList);

        return this.platformImageService.selectedImageList;
    }

    sendSelectedImage() {
        this.platformImageService.selectedImage = this._selectedImage ? { ...this._selectedImage } : null;
        this.selectedImage.emit(this.platformImageService.selectedImage);

        return this.platformImageService.selectedImage;
    }

    clearSelectedImages(): any[] {
        this._selectedImage = null;
        this._selectedImages.slice(0, this._selectedImages.length);

        this.sendSelectedImage();
        this.sendSelectedImages();

        return [...this._selectedImages];
    }
}
