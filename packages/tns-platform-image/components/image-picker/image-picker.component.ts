import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { PlatformImageService } from "../../services/platform-image.service";
import { ImagePickerPayload, ImagePickerAction, ImagePicker } from "../../types/image-picker.type";
import { PhotoEditor, PhotoEditorControl } from "nativescript-photo-editor";
import * as camera from "nativescript-camera";
import * as permissions from "nativescript-permissions";

import { Image } from "tns-core-modules/ui/image";
import * as frameModule from "tns-core-modules/ui/frame";
import * as imageSource from "tns-core-modules/image-source";
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";
import { device, screen } from "tns-core-modules/platform";

import * as imagepicker from "nativescript-imagepicker";

@Component({
  selector: "tns-platform-image-picker",
  templateUrl: "./image-picker.component.html",
  styleUrls: ["../image-manager-container/image-manager-container.component.css", "./image-picker.component.css"],
  moduleId: module.id,
})
export class ImagePickerComponent implements OnInit {

  private saveToGallery: boolean = false;
  private multipleImageSelection: boolean = false;
  private readonly keepAspectRatio: boolean = true;
  private readonly width: number = 300;
  private readonly height: number = 300;
  screenRationWidth: any;
  screenRationHeight: any;
  isTabWidthIsNineSixty: any;
  private imageSource: imageSource.ImageSource;

  private uploadContext: imagepicker.ImagePicker;

  @Input()
  pickerConfig: ImagePicker;

  @Output()
  onImageCapture: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onImageUpload: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onImagePickerEventData: EventEmitter<ImagePickerPayload> = new EventEmitter<ImagePickerPayload>();

  constructor(public platformImageService: PlatformImageService) {
    this.pickerConfig = {
      enableCapture: true,
      enableUpload: true,
      saveToGallery: false,
      multipleImageSelection: false
    };
  }

  ngOnInit() {

    this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
    this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
    if (screen.mainScreen.widthDIPs === 960) {
      this.isTabWidthIsNineSixty = true;
    }

    this.saveToGallery = this.pickerConfig.saveToGallery;
    this.multipleImageSelection = this.pickerConfig.multipleImageSelection;

    this.uploadContext = imagepicker.create({
      mode: this.multipleImageSelection ? "multiple" : "single" // use "multiple" for multiple selection
    });
  }

  captureImage() {
    let payload = {};
    const actionData = {
      action: ImagePickerAction.capture,
      data: payload
    };

    const options = {
      width: this.width,
      height: this.height,
      keepAspectRatio: this.keepAspectRatio,
      saveToGallery: this.saveToGallery
    };

    camera.requestPermissions().then(()=>{  
      camera.takePicture(options)
      .then((imageAsset: ImageAsset) => {
        console.log("got file from camera", imageAsset);
        Object.assign(payload, { ...imageAsset.options, path: imageAsset.android });

        this.platformImageService.updateImagePickerEvent(actionData);
        this.onImageCapture.emit(payload);
        this.onImagePickerEventData.emit(actionData);

      }).catch(err => {
        console.log(err.message);
        this.platformImageService.updateImagePickerEvent(actionData);
        this.onImageCapture.emit(payload);
        this.onImagePickerEventData.emit(actionData);
      });
    }).catch(err=> {
      console.log(err.message);
    })
  }

  uploadFromLibrary() {

    const payload = {};
    const actionData = {
      action: ImagePickerAction.upload,
      data: payload
    };

    this.uploadContext
      .authorize()
      .then(() => {
        return this.uploadContext.present();
      })
      .then((selection) => {
        const images = [];
        selection.forEach((selected) => {
          // process the selected image
          images.push({ ...selected.options, path: selected.android })
        });
        Object.assign(payload, { paths: images });

        this.platformImageService.updateImagePickerEvent(actionData);
        this.onImageUpload.emit(payload);
        this.onImagePickerEventData.emit(actionData);
      }).catch((e) => {
        // process error
        this.platformImageService.updateImagePickerEvent(actionData);
        this.onImageUpload.emit(payload);
        this.onImagePickerEventData.emit(actionData);
      });

  }
}
