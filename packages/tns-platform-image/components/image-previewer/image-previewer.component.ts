import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import { PlatformImageService } from "../../services/platform-image.service";
import {device, screen} from "tns-core-modules/platform";

@Component({
    selector: "tns-platform-image-previewer",
    templateUrl: "./image-previewer.component.html",
    styleUrls: ["./image-previewer.component.css"],
    moduleId: module.id,
})
export class ImagePreviewerComponent implements OnInit {

    screenRationWidth:any;
    screenRationHeight:any;
    isTabWidthIsNineSixty:any;

    @Output()
    backToEditorFromImagePicker: EventEmitter<any> = new EventEmitter();

    constructor(public platformImageService: PlatformImageService) { }

    ngOnInit() {
        this.screenRationWidth=(screen.mainScreen.widthDIPs/1280);
        this.screenRationHeight=(screen.mainScreen.heightDIPs/800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
    }

    gotoEditPhoto(imageData) {
        console.log("Go to edit photo");
        console.log(imageData);
        this.backToEditorFromImagePicker.emit(imageData);
        // this.platformImageService.updateImagePickerEvent();
    }
}
