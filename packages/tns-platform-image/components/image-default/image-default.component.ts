import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
    selector: "tns-platform-image",
    moduleId: module.id,
    templateUrl: "./image-default.component.html",
    styleUrls: [
        "./image-default.common.scss",
        "./image-default.component.scss"
    ]
})

export class PlatformImageDefaultComponent implements OnInit, OnDestroy {
    public result: string;
    public screenRationWidth: any;
    public spanCount: any;
    public changeOrientation: boolean;

    loadingData: boolean;

    constructor() {
        
    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }
}
