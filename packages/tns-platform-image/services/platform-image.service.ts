import { ImagePickerPayload } from "./../types/image-picker.type";
import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";

@Injectable()
export class PlatformImageService {
    private _selectedImageList: Array<any>;
    private _selectedPreviewImage: any;

    private imagePickerSubject = new Subject<ImagePickerPayload>();
    private multiSelectedSubject = new BehaviorSubject<boolean>(false);

    imagePickerEvent$ = this.imagePickerSubject.asObservable();
    galleryMultiSelectMode$ = this.multiSelectedSubject.asObservable();

    constructor() {
        this.selectedImageList = [];
    }

    set selectedImageList(value: Array<any>) {
        this._selectedImageList = value;
    }

    get selectedImageList(): Array<any> {
        return this._selectedImageList;
    }

    set selectedImage(value: any) {
        this._selectedPreviewImage = value;
    }

    get selectedImage(): any {
        return this._selectedPreviewImage;
    }

    setGalleryMultiSelectMode( value: boolean) {
        this.multiSelectedSubject.next(value);
    }

    toggleGalleryMultiSelectMode() {
        this.multiSelectedSubject.next(this.multiSelectedSubject.value);
    }

    getGallerytMultiSelectMode(): boolean {
        return this.multiSelectedSubject.value;
    }

    updateImagePickerEvent(value: ImagePickerPayload): void {
        this.imagePickerSubject.next(value);
    }
}