export interface ImageManager {
    previewMode: ImagePreviewMode;
}

export enum ImagePreviewMode {
    single = "SINGLE",
    multiple = "MULTIPLE"
}