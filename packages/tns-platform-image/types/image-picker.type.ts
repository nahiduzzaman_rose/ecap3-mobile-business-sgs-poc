export interface ImagePicker {
    enableCapture: boolean;
    enableUpload: boolean;
    saveToGallery: boolean;
    multipleImageSelection?: boolean;
}


export interface ImagePickerPayload {
    action: ImagePickerAction,
    data: any;
}

export enum ImagePickerAction {
    capture = "CAPTURE",
    upload = "UPLOAD"
}