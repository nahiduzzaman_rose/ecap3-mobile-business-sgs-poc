import { ImagePicker } from "./image-picker.type";
import { ImageSelectionMode } from "../contracts/image-gallery.interface";

export interface ImageControlPanel {
    title: string;
    imagePickerConfig?: ImagePicker;
    hideImagePicker?: boolean;
    hideGalleryFilter: boolean;
    hideGallery: boolean;
    hideMultiSelectToggle?: boolean;
    galleryFilterTemplate?: any;
    imageDetailsCaptureTemplate?: any;
    galleryComponent?: any;
    galleryMode?: ImageSelectionMode;
}