"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var action_bar_component_1 = require("./components/action-bar/action-bar.component");
var side_bar_component_1 = require("./components/side-bar/side-bar.component");
var side_user_bar_component_1 = require("./components/side-user-bar/side-user-bar.component");
var nav_bar_component_1 = require("./components/nav-bar/nav-bar.component");
var tns_core_1 = require("@ecap3/tns-core");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var CoreTemplateModule = /** @class */ (function () {
    function CoreTemplateModule() {
    }
    CoreTemplateModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                tns_core_1.EcapCoreModule,
                nativescript_ngx_fonticon_1.TNSFontIconModule
            ],
            declarations: [
                action_bar_component_1.EcapActionBarComponent,
                side_bar_component_1.EcapSideBarComponent,
                nav_bar_component_1.EcapNavBarComponent,
                side_user_bar_component_1.EcapSideUserBarComponent
            ],
            exports: [
                action_bar_component_1.EcapActionBarComponent,
                side_bar_component_1.EcapSideBarComponent,
                nav_bar_component_1.EcapNavBarComponent,
                side_user_bar_component_1.EcapSideUserBarComponent
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], CoreTemplateModule);
    return CoreTemplateModule;
}());
exports.CoreTemplateModule = CoreTemplateModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS10ZW1wbGF0ZS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjb3JlLXRlbXBsYXRlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RDtBQUN6RCxzREFBcUU7QUFFckUscUZBQXNGO0FBQ3RGLCtFQUFnRjtBQUNoRiw4RkFBOEY7QUFDOUYsNEVBQTZFO0FBQzdFLDRDQUFpRDtBQUNqRCx1RUFBOEQ7QUFzQjlEO0lBQUE7SUFDQSxDQUFDO0lBRFksa0JBQWtCO1FBcEI5QixlQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsaUNBQXdCO2dCQUN4Qix5QkFBYztnQkFDZCw2Q0FBaUI7YUFDcEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsNkNBQXNCO2dCQUN0Qix5Q0FBb0I7Z0JBQ3BCLHVDQUFtQjtnQkFDbkIsa0RBQXdCO2FBQzNCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLDZDQUFzQjtnQkFDdEIseUNBQW9CO2dCQUNwQix1Q0FBbUI7Z0JBQ25CLGtEQUF3QjthQUMzQjtZQUNELE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzlCLENBQUM7T0FDVyxrQkFBa0IsQ0FDOUI7SUFBRCx5QkFBQztDQUFBLEFBREQsSUFDQztBQURZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUF9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge05hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vblwiO1xyXG5cclxuaW1wb3J0IHsgRWNhcEFjdGlvbkJhckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9hY3Rpb24tYmFyL2FjdGlvbi1iYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRWNhcFNpZGVCYXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2lkZS1iYXIvc2lkZS1iYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRWNhcFNpZGVVc2VyQmFyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3NpZGUtdXNlci1iYXIvc2lkZS11c2VyLWJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBFY2FwTmF2QmFyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25hdi1iYXIvbmF2LWJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBFY2FwQ29yZU1vZHVsZSB9IGZyb20gXCJAZWNhcDMvdG5zLWNvcmVcIjtcclxuaW1wb3J0IHsgVE5TRm9udEljb25Nb2R1bGUgfSBmcm9tICduYXRpdmVzY3JpcHQtbmd4LWZvbnRpY29uJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgICAgIEVjYXBDb3JlTW9kdWxlLFxyXG4gICAgICAgIFROU0ZvbnRJY29uTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgRWNhcEFjdGlvbkJhckNvbXBvbmVudCxcclxuICAgICAgICBFY2FwU2lkZUJhckNvbXBvbmVudCxcclxuICAgICAgICBFY2FwTmF2QmFyQ29tcG9uZW50LFxyXG4gICAgICAgIEVjYXBTaWRlVXNlckJhckNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBFY2FwQWN0aW9uQmFyQ29tcG9uZW50LFxyXG4gICAgICAgIEVjYXBTaWRlQmFyQ29tcG9uZW50LFxyXG4gICAgICAgIEVjYXBOYXZCYXJDb21wb25lbnQsXHJcbiAgICAgICAgRWNhcFNpZGVVc2VyQmFyQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlVGVtcGxhdGVNb2R1bGUge1xyXG59XHJcbiJdfQ==