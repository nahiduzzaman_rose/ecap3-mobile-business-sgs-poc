export * from './core-template.module';
export * from './components/action-bar/action-bar.component';
export * from './components/side-bar/side-bar.component';
export * from './components/nav-bar/nav-bar.component';
