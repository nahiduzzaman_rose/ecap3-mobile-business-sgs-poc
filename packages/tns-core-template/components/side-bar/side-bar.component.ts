import {Component, OnInit, Input, Output, EventEmitter, TemplateRef} from "@angular/core";

@Component({
    selector: "EcapSideBar",
    moduleId: module.id,
    templateUrl: "./side-bar.component.html",
    styleUrls: [
        "./side-bar-common.scss",
        "./side-bar.component.scss"
    ]
})
export class EcapSideBarComponent implements OnInit {
    @Input() navigations: any;
    @Input() appversionName: string;
    @Input() customSideNavTemplate: TemplateRef<any>;
    @Output() onLogoutEvent :EventEmitter<any> = new EventEmitter();
    
    constructor() {

    }

    logoutEvent(event){
        this.onLogoutEvent.emit('logout');
    }

    ngOnInit() {
        console.log("Loaded EcapSideBarComponent!");
    }

}
