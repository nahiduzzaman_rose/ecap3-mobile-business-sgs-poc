"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var EcapSideBarComponent = /** @class */ (function () {
    function EcapSideBarComponent() {
    }
    EcapSideBarComponent.prototype.ngOnInit = function () {
        console.log("Loaded EcapSideBarComponent");
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EcapSideBarComponent.prototype, "navigations", void 0);
    EcapSideBarComponent = __decorate([
        core_1.Component({
            selector: "EcapSideBar",
            moduleId: module.id,
            templateUrl: "./side-bar.component.html",
            styleUrls: [
                "./side-bar-common.scss",
                "./side-bar.component.scss"
            ]
        }),
        __metadata("design:paramtypes", [])
    ], EcapSideBarComponent);
    return EcapSideBarComponent;
}());
exports.EcapSideBarComponent = EcapSideBarComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZS1iYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2lkZS1iYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXVEO0FBV3ZEO0lBRUk7SUFFQSxDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBUFE7UUFBUixZQUFLLEVBQUU7OzZEQUFrQjtJQURqQixvQkFBb0I7UUFUaEMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRTtnQkFDUCx3QkFBd0I7Z0JBQ3hCLDJCQUEyQjthQUM5QjtTQUNKLENBQUM7O09BQ1csb0JBQW9CLENBVWhDO0lBQUQsMkJBQUM7Q0FBQSxBQVZELElBVUM7QUFWWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBJbnB1dH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiRWNhcFNpZGVCYXJcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3NpZGUtYmFyLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgICBzdHlsZVVybHM6IFtcclxuICAgICAgICBcIi4vc2lkZS1iYXItY29tbW9uLnNjc3NcIixcclxuICAgICAgICBcIi4vc2lkZS1iYXIuY29tcG9uZW50LnNjc3NcIlxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWNhcFNpZGVCYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KCkgbmF2aWdhdGlvbnM6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxvYWRlZCBFY2FwU2lkZUJhckNvbXBvbmVudFwiKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19