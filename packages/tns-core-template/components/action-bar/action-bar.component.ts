import { Component, ElementRef, OnInit, ViewChild, Input, EventEmitter, AfterViewInit, ViewRef, ChangeDetectorRef, OnDestroy, Output } from "@angular/core";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application/application";
import {EcapLoginService, SpinnerService} from '@ecap3/tns-core';
import { RouterExtensions } from "nativescript-angular/router";
import { Router, NavigationEnd } from "@angular/router";
import {Location} from '@angular/common';
import { Subscription } from "rxjs";
import { ActionBarConfiguration, BackbuttonConfig } from "@ecap3/tns-core/model/actionBar.interface";


@Component({
    selector: "EcapActionBar",
    moduleId: module.id,
    templateUrl: "./action-bar.component.html",
    styleUrls: [
        "./action-bar-common.scss",
        "./action-bar.component.scss"
    ]
})
export class EcapActionBarComponent implements OnInit, OnDestroy {

    public showBackButton : boolean;
    public activeUrl: string;
    public subscription: Subscription;
    
    @Input() showMenuButton : boolean;
    @Input() isFlat : boolean;
    @Input() actionBarConfiguration: ActionBarConfiguration;
    @Input() title: string;
    @Input() description: string;
    @Input() backbuttonIconUrl: string;
    @Input() backbuttonConfig: BackbuttonConfig;
    @Output() actionItemClick = new EventEmitter<any>();


    constructor(private ecapLoginService: EcapLoginService, 
        private routerExtensions: RouterExtensions, 
        private router: Router,
        private spinnerService: SpinnerService,
        private location: Location) {
    }

    ngOnInit() {
       // console.log('showMenuButton = ', this.showMenuButton);        
       // console.log('actionBarConfiguration = ', this.actionBarConfiguration);        
    }

    goBack(){
        if(this.backbuttonConfig) {
            this.routerExtensions.navigateByUrl(this.backbuttonConfig.path,{
                transition: {
                    name: "slide"
                }
            });
        }
        else{
            this.location.back();
        }
    }

    onDrawerButtonTap(): void {
        // console.log("taped onDrawerButtonTap");
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavBtnTap(): void {
        // This code will be called only in Android.
        console.log("Navigation button tapped!");
    }

    onTap(actionBarItem: any): void {
        if (actionBarItem.url) {
            this.routerExtensions.navigate([actionBarItem.url], {
                transition: {
                    name: "slide"
                }
            });
        } else {
            if (!actionBarItem.type && actionBarItem.callParentMethod) {
                this.actionItemClick.emit(actionBarItem);
            } else {
                switch (actionBarItem.type) {
                    case 'logout':
                        this.logout();
                        break;
                    case 'menu':
                        this.onDrawerButtonTap();
                        break;
                    default: 
                        console.log('default: no event found');
                }
            }
        }
    }

    logout() {
        console.log("taped logout");
        this.ecapLoginService.logoutFromPlatform().subscribe((response) => {
            console.log("Successfully logout!!!");
        }, (error) => {
            if (error.status === 401) {
                this.ecapLoginService.clearAllCacheData();
            }
        });
    }

    ngOnDestroy() {
        //console.log("Destroyed !!!!!!");
        //this.subscription.unsubscribe();
    }
}
