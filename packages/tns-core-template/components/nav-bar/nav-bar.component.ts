import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import { NavigationProvider, AppSettingsProvider, EcapLoginService } from "@ecap3/tns-core";
import {RouterExtensions} from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Router, NavigationEnd } from "@angular/router";
import { filter } from "rxjs/operators";


@Component({
    selector: "EcapNavBar",
    moduleId: module.id,
    templateUrl: "./nav-bar.component.html",
    styleUrls: [
        "./nav-bar-common.scss",
        "./nav-bar.component.scss"
    ]
})
export class EcapNavBarComponent implements OnInit {
    private _activatedUrl: string;

    @Input() navigations: any;
    @Output() onLogoutEvent :EventEmitter<any> = new EventEmitter();

    constructor(private router: Router,
        private ecapLoginService: EcapLoginService, 
        private routerExtensions: RouterExtensions,
        private navigationProvider: NavigationProvider) {

    }

    onNavigationTap(item:any) {
        console.log('GetCurrentNavigation', item);
    }

    onNavItemTap(navItem: any): void {
        if(navItem.url) {            
            //console.log('navItemRoute',navItemRoute);
            this.routerExtensions.navigate([navItem.url], {
                transition: {
                    name: "slide"
                }
            });
        } else {
            switch (navItem.type) {
                case 'logout':
                    this.logout();
                    break;
                default: 
                    console.log('default: no event found');
            }
        }
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    logout() {
        this.onLogoutEvent.emit('logout');
        /* this.ecapLoginService.logoutFromPlatform().subscribe((response) => {
            console.log("Successfully logout!!!");
        }, (error) => {
            if (error.status === 401) {
                this.ecapLoginService.clearAllCacheData();
            }
        }); */
    }
    
    isComponentSelected(url: string): boolean {
        //console.log('url',url);
        return this._activatedUrl === url;
    }

    ngOnInit() {
        //this._activatedUrl = "/tasklist ";
        // console.log("Loaded EcapNavBarComponent", this.navigations);
        // console.log('GetApps :: ', this.navigationProvider.getApps());
        this.navigationProvider.getCurrentNavigationObservable().subscribe( response => {
            //console.log('GetCurrentNavigationObserable :: ', response);
        });

        this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);
    }

}
