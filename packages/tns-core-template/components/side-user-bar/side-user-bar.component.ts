import {Component, OnInit, OnDestroy, Input, TemplateRef} from "@angular/core";
import {Subscription} from "rxjs";
import { UserInfoService, EcapUser } from "@ecap3/tns-core";

@Component({
    selector: "EcapSideUserBar",
    moduleId: module.id,
    templateUrl: "./side-user-bar.component.html",
    styleUrls: [
        "./side-user-bar-common.scss",
        "./side-user-bar.component.scss"
    ]
})
export class EcapSideUserBarComponent implements OnInit, OnDestroy {
    @Input() customSideNavTemplate: TemplateRef<any>;
    userInfo: EcapUser = new EcapUser();
    subscription: Subscription;

    constructor(private userInfoService: UserInfoService) {

    }

    ngOnInit() {
        console.log("Loaded EcapSideUserBarComponent");
        
        // const userData = this.userInfoService.getUserInfo();
        // console.log('User Details', userData);
        // this.setUserData(userData);

        this.subscription = this.userInfoService.getUserDetailsObserved().subscribe(userDetails => {
            // console.log('User Details', userDetails);
            if(userDetails && userDetails.UserId !== null && userDetails.UserId !== '') {
                this.setUserData(userDetails.userDetails);
            }
        });
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    setUserData(userDetails) {
        // console.log('User setUserData', userDetails);
        if(userDetails && userDetails.UserId !== null && userDetails.UserId !== '') {
            this.userInfo.UserId = userDetails.UserId;
            this.userInfo.ItemId = userDetails.UserId;
            this.userInfo.Roles = userDetails.Roles;
            this.userInfo.UserName = userDetails.UserName;
            this.userInfo.Email = userDetails.Email;
            this.userInfo.PhoneNumber = userDetails.PhoneNumber;
            this.userInfo.DisplayName = userDetails.DisplayName;
        }
    }

}
