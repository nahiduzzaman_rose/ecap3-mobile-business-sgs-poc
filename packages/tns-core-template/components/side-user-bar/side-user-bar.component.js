"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var tns_core_1 = require("@ecap3/tns-core");
var EcapSideUserBarComponent = /** @class */ (function () {
    function EcapSideUserBarComponent(userInfoService) {
        this.userInfoService = userInfoService;
        this.userInfo = new tns_core_1.EcapUser();
    }
    EcapSideUserBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Loaded EcapSideUserBarComponent");
        // const userData = this.userInfoService.getUserInfo();
        // console.log('User Details', userData);
        // this.setUserData(userData);
        this.subscription = this.userInfoService.getUserDetailsObserved().subscribe(function (userDetails) {
            // console.log('User Details', userDetails);
            if (userDetails && userDetails.UserId !== null && userDetails.UserId !== '') {
                _this.setUserData(userDetails.userDetails);
            }
        });
    };
    EcapSideUserBarComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    };
    EcapSideUserBarComponent.prototype.setUserData = function (userDetails) {
        // console.log('User setUserData', userDetails);
        if (userDetails && userDetails.UserId !== null && userDetails.UserId !== '') {
            this.userInfo.UserId = userDetails.UserId;
            this.userInfo.ItemId = userDetails.UserId;
            this.userInfo.Roles = userDetails.Roles;
            this.userInfo.UserName = userDetails.UserName;
            this.userInfo.Email = userDetails.Email;
            this.userInfo.PhoneNumber = userDetails.PhoneNumber;
            this.userInfo.DisplayName = userDetails.DisplayName;
        }
    };
    EcapSideUserBarComponent = __decorate([
        core_1.Component({
            selector: "EcapSideUserBar",
            moduleId: module.id,
            templateUrl: "./side-user-bar.component.html",
            styleUrls: [
                "./side-user-bar-common.scss",
                "./side-user-bar.component.scss"
            ]
        }),
        __metadata("design:paramtypes", [tns_core_1.UserInfoService])
    ], EcapSideUserBarComponent);
    return EcapSideUserBarComponent;
}());
exports.EcapSideUserBarComponent = EcapSideUserBarComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZS11c2VyLWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaWRlLXVzZXItYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRDtBQUUzRCw0Q0FBNEQ7QUFXNUQ7SUFLSSxrQ0FBb0IsZUFBZ0M7UUFBaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBSHBELGFBQVEsR0FBYSxJQUFJLG1CQUFRLEVBQUUsQ0FBQztJQUtwQyxDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUFBLGlCQWFDO1FBWkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1FBRS9DLHVEQUF1RDtRQUN2RCx5Q0FBeUM7UUFDekMsOEJBQThCO1FBRTlCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFdBQVc7WUFDbkYsNENBQTRDO1lBQzVDLElBQUcsV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssSUFBSSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFO2dCQUN4RSxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUM3QztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhDQUFXLEdBQVg7UUFDSSx3Q0FBd0M7UUFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRUQsOENBQVcsR0FBWCxVQUFZLFdBQVc7UUFDbkIsZ0RBQWdEO1FBQ2hELElBQUcsV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssSUFBSSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFO1lBQ3hFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxRQUFRLENBQUM7WUFDOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7U0FDdkQ7SUFDTCxDQUFDO0lBeENRLHdCQUF3QjtRQVRwQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLGdDQUFnQztZQUM3QyxTQUFTLEVBQUU7Z0JBQ1AsNkJBQTZCO2dCQUM3QixnQ0FBZ0M7YUFDbkM7U0FDSixDQUFDO3lDQU11QywwQkFBZTtPQUwzQyx3QkFBd0IsQ0EwQ3BDO0lBQUQsK0JBQUM7Q0FBQSxBQTFDRCxJQTBDQztBQTFDWSw0REFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3l9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBVc2VySW5mb1NlcnZpY2UsIEVjYXBVc2VyIH0gZnJvbSBcIkBlY2FwMy90bnMtY29yZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJFY2FwU2lkZVVzZXJCYXJcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3NpZGUtdXNlci1iYXIuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1xyXG4gICAgICAgIFwiLi9zaWRlLXVzZXItYmFyLWNvbW1vbi5zY3NzXCIsXHJcbiAgICAgICAgXCIuL3NpZGUtdXNlci1iYXIuY29tcG9uZW50LnNjc3NcIlxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRWNhcFNpZGVVc2VyQmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIHVzZXJJbmZvOiBFY2FwVXNlciA9IG5ldyBFY2FwVXNlcigpO1xyXG4gICAgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB1c2VySW5mb1NlcnZpY2U6IFVzZXJJbmZvU2VydmljZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkxvYWRlZCBFY2FwU2lkZVVzZXJCYXJDb21wb25lbnRcIik7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gY29uc3QgdXNlckRhdGEgPSB0aGlzLnVzZXJJbmZvU2VydmljZS5nZXRVc2VySW5mbygpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdVc2VyIERldGFpbHMnLCB1c2VyRGF0YSk7XHJcbiAgICAgICAgLy8gdGhpcy5zZXRVc2VyRGF0YSh1c2VyRGF0YSk7XHJcblxyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9uID0gdGhpcy51c2VySW5mb1NlcnZpY2UuZ2V0VXNlckRldGFpbHNPYnNlcnZlZCgpLnN1YnNjcmliZSh1c2VyRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdVc2VyIERldGFpbHMnLCB1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgIGlmKHVzZXJEZXRhaWxzICYmIHVzZXJEZXRhaWxzLlVzZXJJZCAhPT0gbnVsbCAmJiB1c2VyRGV0YWlscy5Vc2VySWQgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFVzZXJEYXRhKHVzZXJEZXRhaWxzLnVzZXJEZXRhaWxzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIC8vIHVuc3Vic2NyaWJlIHRvIGVuc3VyZSBubyBtZW1vcnkgbGVha3NcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFVzZXJEYXRhKHVzZXJEZXRhaWxzKSB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ1VzZXIgc2V0VXNlckRhdGEnLCB1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgaWYodXNlckRldGFpbHMgJiYgdXNlckRldGFpbHMuVXNlcklkICE9PSBudWxsICYmIHVzZXJEZXRhaWxzLlVzZXJJZCAhPT0gJycpIHtcclxuICAgICAgICAgICAgdGhpcy51c2VySW5mby5Vc2VySWQgPSB1c2VyRGV0YWlscy5Vc2VySWQ7XHJcbiAgICAgICAgICAgIHRoaXMudXNlckluZm8uSXRlbUlkID0gdXNlckRldGFpbHMuVXNlcklkO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJJbmZvLlJvbGVzID0gdXNlckRldGFpbHMuUm9sZXM7XHJcbiAgICAgICAgICAgIHRoaXMudXNlckluZm8uVXNlck5hbWUgPSB1c2VyRGV0YWlscy5Vc2VyTmFtZTtcclxuICAgICAgICAgICAgdGhpcy51c2VySW5mby5FbWFpbCA9IHVzZXJEZXRhaWxzLkVtYWlsO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJJbmZvLlBob25lTnVtYmVyID0gdXNlckRldGFpbHMuUGhvbmVOdW1iZXI7XHJcbiAgICAgICAgICAgIHRoaXMudXNlckluZm8uRGlzcGxheU5hbWUgPSB1c2VyRGV0YWlscy5EaXNwbGF5TmFtZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==