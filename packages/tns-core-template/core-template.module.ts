import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptCommonModule} from "nativescript-angular/common";

import { EcapActionBarComponent } from './components/action-bar/action-bar.component';
import { EcapSideBarComponent } from './components/side-bar/side-bar.component';
import { EcapSideUserBarComponent } from './components/side-user-bar/side-user-bar.component';
import { EcapNavBarComponent } from './components/nav-bar/nav-bar.component';
import { EcapCoreModule } from "@ecap3/tns-core";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        EcapCoreModule,
        TNSFontIconModule
    ],
    declarations: [
        EcapActionBarComponent,
        EcapSideBarComponent,
        EcapNavBarComponent,
        EcapSideUserBarComponent
    ],
    exports: [
        EcapActionBarComponent,
        EcapSideBarComponent,
        EcapNavBarComponent,
        EcapSideUserBarComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class CoreTemplateModule {
}
