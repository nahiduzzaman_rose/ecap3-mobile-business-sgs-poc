import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular/forms";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ReactiveFormsModule} from '@angular/forms';
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import { WebViewExtModule } from "@nota/nativescript-webview-ext/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { PlatformCkeditorService } from "./services/platform-ckeditor.service";
import { PlatformCkeditorComponent } from "./components/ckeditor/ckeditor.component";

@NgModule({
    providers: [
        PlatformCkeditorService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule,
        NativeScriptUIListViewModule,
        WebViewExtModule
    ],
    declarations: [
        PlatformCkeditorComponent
    ],
    entryComponents: [
    ],
    exports: [
        PlatformCkeditorComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformCkeditorModule {}
