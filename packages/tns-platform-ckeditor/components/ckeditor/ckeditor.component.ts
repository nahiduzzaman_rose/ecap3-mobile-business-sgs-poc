import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    OnDestroy,
    ChangeDetectorRef,
    TemplateRef
} from '@angular/core';
import {isAndroid, screen} from "tns-core-modules/platform";
import {
    LoadEventData,
    LoadFinishedEventData,
    ShouldOverideUrlLoadEventData,
    WebViewExt
} from "@nota/nativescript-webview-ext";
import {LoadingIndicator} from "@nstudio/nativescript-loading-indicator";


@Component({
    selector: "tns-platform-ckeditor",
    moduleId: module.id,
    templateUrl: "./ckeditor.component.html",
    styleUrls: [
        "./ckeditor.component.scss"
    ]
})

export class PlatformCkeditorComponent implements OnInit, OnDestroy {
    public screenRationWidth: any;
    @Input() fieldValue: any;
    @Output() onValueChange = new EventEmitter<any>();

    webview : WebViewExt;
    private isWebViewLoading: boolean;
    private indicator: LoadingIndicator;

    constructor() {

    }

    ngOnInit() {
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        console.log(screen.mainScreen.heightDIPs);

    }

    private webviewLoaded(args: LoadEventData) {
        this.webview = args.object;
        this.webview.registerLocalResource("ckeditor", "~/webviews/ckef/index.html");

        if (isAndroid) {
            this.webview.src = "~/webviews/ckef/index.html";
        } else {
            this.webview.src = "~/webviews/ckef/index.html";
        }

        this.webview.on(WebViewExt.shouldOverrideUrlLoadingEvent, (args: ShouldOverideUrlLoadEventData) => {
            // console.log(args.url);
        });


        this.webview.on(WebViewExt.loadFinishedEvent, (args: LoadFinishedEventData) => {
            this.isWebViewLoading = false;
            this.webview.executeJavaScript("registerCKEditorData('"+ this.fieldValue +"')");
            this.sendCurrentFieldData(this.fieldValue);
            this.indicator.hide();
        });

        this.webview.on("gotMessage", (msg) => {
            this.onValueChange.emit(msg.data);
        });
    }

    sendCurrentFieldData(fieldValue) {
        this.webview.emitToWebView(fieldValue, fieldValue);
    }

    ngOnDestroy() {
        this.webview = null;
    }
}
