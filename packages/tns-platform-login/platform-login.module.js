"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("nativescript-angular/common");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var login_default_component_1 = require("./components/login-default/login-default.component");
var forms_2 = require("@angular/forms");
var angular_1 = require("nativescript-localize/angular");
var login_service_1 = require("./services/login.service");
var PlatformLoginModule = /** @class */ (function () {
    function PlatformLoginModule() {
    }
    PlatformLoginModule = __decorate([
        core_1.NgModule({
            providers: [
                login_service_1.LoginService
            ],
            imports: [
                forms_1.NativeScriptFormsModule,
                common_1.NativeScriptCommonModule,
                forms_2.ReactiveFormsModule,
                angular_1.NativeScriptLocalizeModule
            ],
            declarations: [
                login_default_component_1.PlatformLoginDefaultComponent
            ],
            exports: [
                login_default_component_1.PlatformLoginDefaultComponent
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], PlatformLoginModule);
    return PlatformLoginModule;
}());
exports.PlatformLoginModule = PlatformLoginModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGxhdGZvcm0tbG9naW4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGxhdGZvcm0tbG9naW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0RBQXFFO0FBQ3JFLG9EQUFtRTtBQUNuRSxzQ0FBeUQ7QUFDekQsOEZBQWlHO0FBQ2pHLHdDQUFtRDtBQUNuRCx5REFBeUU7QUFDekUsMERBQXNEO0FBb0J0RDtJQUFBO0lBQ0EsQ0FBQztJQURZLG1CQUFtQjtRQWxCL0IsZUFBUSxDQUFDO1lBQ04sU0FBUyxFQUFFO2dCQUNQLDRCQUFZO2FBQ2Y7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsK0JBQXVCO2dCQUN2QixpQ0FBd0I7Z0JBQ3hCLDJCQUFtQjtnQkFDbkIsb0NBQTBCO2FBQzdCO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLHVEQUE2QjthQUNoQztZQUNELE9BQU8sRUFBRTtnQkFDTCx1REFBNkI7YUFDaEM7WUFDRCxPQUFPLEVBQUUsQ0FBQyx1QkFBZ0IsQ0FBQztTQUM5QixDQUFDO09BQ1csbUJBQW1CLENBQy9CO0lBQUQsMEJBQUM7Q0FBQSxBQURELElBQ0M7QUFEWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQge05hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtQbGF0Zm9ybUxvZ2luRGVmYXVsdENvbXBvbmVudH0gZnJvbSBcIi4vY29tcG9uZW50cy9sb2dpbi1kZWZhdWx0L2xvZ2luLWRlZmF1bHQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7UmVhY3RpdmVGb3Jtc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQge05hdGl2ZVNjcmlwdExvY2FsaXplTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2FsaXplL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHtMb2dpblNlcnZpY2V9IGZyb20gXCIuL3NlcnZpY2VzL2xvZ2luLnNlcnZpY2VcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBMb2dpblNlcnZpY2VcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0TG9jYWxpemVNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBQbGF0Zm9ybUxvZ2luRGVmYXVsdENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBQbGF0Zm9ybUxvZ2luRGVmYXVsdENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtOT19FUlJPUlNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGxhdGZvcm1Mb2dpbk1vZHVsZSB7XHJcbn1cclxuIl19