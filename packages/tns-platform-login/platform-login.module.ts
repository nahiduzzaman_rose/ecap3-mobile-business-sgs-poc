import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";
import { PlatformLoginDefaultComponent } from "./components/login-default/login-default.component";
import { LoginService } from "./services/login.service";
import {PrivacyPdfModalComponent} from "./components/privacy-pdf-modal/privacy-pdf-modal.component";

@NgModule({
    providers: [
        LoginService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        ReactiveFormsModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        PlatformLoginDefaultComponent,
        PrivacyPdfModalComponent
    ],
    exports: [
        PlatformLoginDefaultComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PlatformLoginModule {
}
