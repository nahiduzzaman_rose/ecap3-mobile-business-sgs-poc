import {Observable} from "rxjs";
import {map} from 'rxjs/operators';

import {Injectable, OnInit, EventEmitter} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {TokenProvider, AuthGuard, EcapLoginService, AppSettingsProvider, UtilityService} from '@ecap3/tns-core';


@Injectable()
export class LoginService implements OnInit {
    header: any;

    personaConfig: any;
    notFoundStatus: number = 404;

    commonOptions: any = {headers: this.header, observe: 'response'};
    public gdprControl: EventEmitter<any>;
    OnCaptchaSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();


    constructor(private http: HttpClient,
                private featureActivator: AuthGuard,
                public ecapLoginService: EcapLoginService,
                private tokenProvider: TokenProvider,
                private utilityService: UtilityService,
                private appSettingsProvider: AppSettingsProvider) {

        this.header = new HttpHeaders({
            "Content-Type": "application/json",
            'Authorization': 'bearer ' + this.tokenProvider.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
        
        this.gdprControl = new EventEmitter<any>();
    }

    public ngOnInit(): void {
    }

    public getAuthenticatedAccessToken(username: string, password: string, captcha: string): Observable<any>  /*Promise<any>*/ {
        let body = new HttpParams().set('grant_type', 'password').set('username', username).set('password', password);
        if (captcha)
            body = body.append('captcha_value', captcha);
        const header = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer ' + this.tokenProvider.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });
        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), {headers: header}).pipe(map((response: any) => {
            // console.log('LoginResponseUserDetails::', response);

            this.tokenProvider.setRefreshToken(response.refresh_token);
            this.tokenProvider.setAccessToken(response.access_token);
            this.featureActivator.resetFeatures();
            return response;
        }, (error) => {
            return error;
        }));
    }
    
    public getAuthenticatedPersonaAccessCookie(challangeCode: string, captcha: string, persona: string): Observable<any>  /*Promise<any>*/ {
        let body = new HttpParams()
            .set('grant_type', 'authenticate_persona')
            .set('challange_code', challangeCode)
            .set('next_url', '')
            .set('username', '')
            .set('password', '')
            .set('persona', persona);
        if (captcha)
            body = body.append('captcha_value', captcha);
        const header = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer ' + this.tokenProvider.getAccessToken(),
            'Origin': this.appSettingsProvider.getAppSettings().Origin
        });

        return this.http.post(this.appSettingsProvider.getAppSettings().Token, body.toString(), {headers: header}).pipe(map((response: any) => {
            debugger;
            this.tokenProvider.setRefreshToken(response.refresh_token);
            this.featureActivator.resetFeatures();
            const jsonResponse = response;
            return jsonResponse;
        }, (error) => {
            return error;
        }));
    }

    public resend2FaCode(token, captcha): Observable<any> {
        return this.ecapLoginService.resend2FaCode(token, captcha).pipe(map(response => response));
    }

    public verify2faCode(code, token): Observable<any> {
        return this.ecapLoginService.verify2faCode(code, token).pipe(map(response => response));
    }

    public emailExists(email) {
        return this.ecapLoginService.emailExists(email).pipe(map(response => response));
    }

    public getPersonInfo(firstName, lastName, email, password, registeredBy, personDefaultInfo?) {
        let person = {
            Password: password,
            FirstName: firstName || '',
            LastName: lastName || '',
            DisplayName: (firstName || '') + ' ' + (lastName || ''),
            Email: email,
            UserName: email,
            ItemId: this.utilityService.getNewGuid(),
            SignUp: true,
            Tags: personDefaultInfo ? personDefaultInfo.Tags : ['Is-A-Visitor'],
            Roles: personDefaultInfo ? personDefaultInfo.Roles : ['anonymous', 'appuser', 'visitor'],
            Language: personDefaultInfo ? personDefaultInfo.Language : 'en-US',
            RegisteredBy: registeredBy,
            MailPurpose: personDefaultInfo ? personDefaultInfo.MailPurpose : '',
            PersonInfo: {
                FirstName: firstName || '',
                LastName: lastName || '',
                DisplayName: (firstName || '') + ' ' + (lastName || ''),
                Tags: personDefaultInfo ? personDefaultInfo.Tags : ['Is-A-Visitor'],
                MailChimpSubscriptionStatus: personDefaultInfo && personDefaultInfo.MailChimpSubscriptionStatus || 0
            }
        }

        return person;
    }

    public signupUserFromPerson(firstName, lastName, email, password, personDefaultInfo?) {
        if (personDefaultInfo && personDefaultInfo.signupUserFromPersonUsingBusinessService) {
            let personInfo = this.getPersonInfo(firstName, lastName, email, password, 6, personDefaultInfo);
            return this.ecapLoginService.signupUserFromPersonUsingBusinessSevice(personInfo, personDefaultInfo && personDefaultInfo.signupUserFromPersonUsingBusinessServiceApi).map(response => {
                if (response && response.body && response.body.Success == true) {
                    response.body.StatusCode = 0;
                } else {
                    response.body.StatusCode = 1;
                }
                return response;
            })
        } else {
            let personInfo = this.getPersonInfo(firstName, lastName, email, password, 6, personDefaultInfo);
            return this.ecapLoginService.createNewUser(personInfo).pipe(map(response => {
                return response;
            }));
        }
    }

    public createNewUser(firstName, lastName, email, password, personDefaultInfo) {
        let personInfo = this.getPersonInfo(firstName, lastName, email, password, 2, personDefaultInfo);
        return this.ecapLoginService.createNewUser(personInfo).pipe(map(response => {
            return response;
        }));
    }

    public checkActivationCodeValidation(activationCode) {
        return this.ecapLoginService.checkActivationCodeValidation(activationCode).pipe(map(response => response));
    }

    public activateAccount(password, activationCode, currentCountryCode, currentPhoneNumber) {
        return this.ecapLoginService.activateAccount(password, activationCode, currentCountryCode, currentPhoneNumber).pipe(map(response => response));
    }

    public verifyAccount(activationCode) {
        return this.ecapLoginService.verifyAccount(activationCode).pipe(map(response => response));
    }

    public sendResetPasswordRequest(email, captcha) {
        return this.ecapLoginService.sendResetPasswordRequest(email, captcha).pipe(map(response => response));
    }

    public resetPassword(password, activationCode) {
        return this.ecapLoginService.resetPassword(password, activationCode).pipe(map(response => response));
    }

    public setPersonaConfig(personaConfig) {
        this.personaConfig = personaConfig;
    }

    public getPersonaConfig() {
        return this.personaConfig;
    }

    public showGdpr(data) {
		this.gdprControl.emit(data);
    }
    
    
    getCaptcha(){
        return this.ecapLoginService.getCaptcha();
    }

    submitCaptcha(id, value){
        return this.ecapLoginService.submitCaptcha(id, value);
    }

    recoverAccount(email, verificationCode){
        return this.ecapLoginService.recoverAccount(email, verificationCode);
    }

    recreateCaptcha(){
        this.OnCaptchaSubmit.emit(true);
    }
}
