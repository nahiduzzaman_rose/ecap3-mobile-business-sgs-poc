import { localize } from 'nativescript-localize';
import {RouterExtensions,} from "nativescript-angular/router";
import {ModalDialogOptions, ModalDialogService} from "nativescript-angular/directives/dialogs";
import {Component, ElementRef, OnInit, ViewChild, Input, ViewContainerRef, NgZone, OnDestroy} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";
import {connectionType, getConnectionType} from "tns-core-modules/connectivity";
import {prompt} from "tns-core-modules/ui/dialogs";
import {Page} from "tns-core-modules/ui/page";
import {FormBuilder, FormGroup, ValidationErrors, Validators, FormControl} from "@angular/forms";
import {UserModel} from "../../contracts/user.model";
import {
    FeatureProvider,
    NavigationProvider,
    UserInfoService,
    AppSettingsProvider,
    LocalPlatformDataService,
    SpinnerService,
    SpinnerOptionInterface,
    SnackBarService
} from "@ecap3/tns-core";
import {LoginService} from "../../services/login.service";
import {LoadingIndicator, Mode} from "@nstudio/nativescript-loading-indicator";
import * as utils from "tns-core-modules/utils/utils";
import {screen, device} from "platform";
import * as orientation from "nativescript-orientation"
import {OrientationChangedEventData, android} from "application"
import * as app from "application";
import {Subscription, ReplaySubject} from "rxjs";
import {PrivacyPdfModalComponent} from "../privacy-pdf-modal/privacy-pdf-modal.component";
import { first, takeUntil, switchMap } from "rxjs/operators";
import {knownCollections} from "nativescript-ui-dataform/ui-dataform.common";
import validators = knownCollections.validators;

@Component({
    selector: "tns-platform-login-default",
    moduleId: module.id,
    templateUrl: "./login-default.component.html",
    styleUrls: [
        "./login-default.component.scss",
        "./login-default-common.scss",
        "./login-default.component.ios.scss"
    ]
})
export class PlatformLoginDefaultComponent implements OnInit, OnDestroy {
    // user: User;
    // isLoggingIn = true;
    // isAuthenticating = false;
    loginForm: FormGroup;
    emailRegEx = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    private indicator: LoadingIndicator;

    loadPdfFlag: boolean;
    termsConditons: any;
    termsConditonsNew: any = [];
    termsConditonsTemp: any = [];
    public termsConditionDetails: any;
    public loginScreenWidth: string;
    public loginScreenHeight: string;
    public device: string;
    public subscription: Subscription;
    public afterLoginRoute: string;
    public imgSrc: string;
    public captchaImageSrc: string = '';
    public captchaData: any;
    captchaRefreshSubscribe: Subscription;
    recreateCaptchaSubmission: Subscription;
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    isPasswordHidden: boolean = true;
    showPasswordIconColor: string = 'black';


    @Input() loginDefaultRoutes: any;
    @Input() loginPageData: LoginConfiguration;
    @ViewChild("mainContainer", {static: false}) mainContainer: ElementRef;
    @ViewChild("logoContainer", {static: false}) logoContainer: ElementRef;
    @ViewChild("formControls", {static: false}) formControls: ElementRef;
    @ViewChild("password", {static: false}) password: ElementRef;
    public savingData: boolean;
    isForgotPasswordEnabled: boolean;
    isResetSuccsessfull: boolean;
    showLogin: boolean = true;

    constructor(private router: Router,
                private routerExtensions: RouterExtensions,
                private page: Page, private formBuilder: FormBuilder,
                private loginService: LoginService,
                private featureporvider: FeatureProvider,
                private navigationProvider: NavigationProvider,
                private userInfoService: UserInfoService,
                private spinnerService: SpinnerService,
                private vcRef: ViewContainerRef,
                private appSettingsProvider: AppSettingsProvider,
                private localPlatformDataService: LocalPlatformDataService,
                private snackBarService: SnackBarService,
                private modal: ModalDialogService,
                private zone: NgZone) {
        console.log('PlatformLoginDefaultComponent Init!!!!!!!');
        this.indicator = new LoadingIndicator();

        app.on(app.orientationChangedEvent, (args: OrientationChangedEventData) => {
            console.log('currentOrientation ==>', args.newValue);

            this.zone.run(() => {
                if (this.device == 'Phone') {
                    if (args.newValue == 'portrait') {
                        this.loginScreenWidth = '100%';
                        this.loginScreenHeight = '100%';
                    } else {
                        this.loginScreenHeight = "auto";
                    }
                } else {
                    if (args.newValue == 'portrait') {
                        this.loginScreenWidth = '40%';
                        this.loginScreenHeight = this.loginPageData.loginScreenSize.height || '50%';
                    } else {
                        this.loginScreenWidth = '40%';
                        this.loginScreenHeight = "auto";
                    }
                }
            })
        })


    }

    initForm(data?: UserModel) {
        this.loginForm = this.formBuilder.group({
            email: [data && data.Email || '', [Validators.required, Validators.email]],
            password: [data && data.Password || '', !this.isForgotPasswordEnabled ? [Validators.required] : []],
            captchaValue: ['', this.isForgotPasswordEnabled ? [Validators.required] : []]
        });
        return this.loginForm;
    }

    focusPassword() {
        this.password.nativeElement.focus();
    }

    showPassword(){
        this.isPasswordHidden = !this.isPasswordHidden;
        if(this.isPasswordHidden) {
            this.showPasswordIconColor = 'black';
        }
        else {
            this.showPasswordIconColor = 'blue';
        }
    }

    startBackgroundAnimation(background) {
        if (this.loginPageData.showBackgroundAnimation) {
            background.animate({
                scale: {x: 1.1, y: 1.1},
                duration: 10000
            });
        }
    }

    submit() {
        this.login();
    }

    resetPassword(){
        const id = this.captchaData.Id;
        const value = this.loginForm.value.captchaValue;
        this.loginService.submitCaptcha(id,value).subscribe((responseCaptcha) => {
            const email = this.loginForm.value.email;
            const verificationCode = responseCaptcha.VerificationCode;
            if(responseCaptcha.StatusCode == 0){
                return this.loginService.recoverAccount(email, verificationCode).subscribe((response) => {
                    if(response.Errors.IsValid && response.StatusCode == 0){
                        this.isResetSuccsessfull = true;
                        this.isForgotPasswordEnabled = false;
                        this.showLogin = false;
                        this.snackBarService.show('An email has been sent to your account.', 'white');
                    }else{
                        this.snackBarService.show('Account recovery failed.', 'white');
                    }
                });
            }else{
                if(responseCaptcha.ErrorMessages[0] == 'Value did not match.'){
                    this.snackBarService.show('Captcha did not match', 'white');
                }
            }
        })
    }

    showLoginForm(){
        this.showLogin = true;
        this.isResetSuccsessfull = false;
    }

    public resetFeature(): void {
        this.featureporvider.resetFeatures(); // reset feature
    }

    ngOnInit() {
        this.imgSrc = this.loginPageData.backgroundImageSrc;
        this.page.actionBarHidden = true;
        this.device = device.deviceType;
        let currentOrientation = orientation.getOrientation();
        this.zone.run(() => {
            if (this.device == 'Phone') {
                this.loginScreenWidth = '100%';
                this.loginScreenHeight = '100%';
            } else {
                console.log('currentOrientation', currentOrientation);
                if (currentOrientation == 'portrait') {
                    this.loginScreenWidth = '70%';
                    this.loginScreenHeight = this.loginPageData.loginScreenSize.height || '50%';
                } else {
                    this.loginScreenWidth = '40%';
                    this.loginScreenHeight = "auto";
                }
            }
        });

        this.initForm({
            Email: this.loginPageData.defaultCredential.email,
            Password: this.loginPageData.defaultCredential.password,
            CaptchaValue: '',
        } as UserModel);

        // terms conditions
        this.termsConditons = this.loginPageData.termsConditons;
        for (let i = 0; i < this.termsConditons.length; i++) {
            this.termsConditonsTemp.push(this.termsConditons[i]);
            if ((i + 1) % 3 == 0) {
                this.termsConditonsNew.push(this.termsConditonsTemp);
                this.termsConditonsTemp = [];
            }
        }
        if (this.termsConditonsTemp.length > 0)
            this.termsConditonsNew.push(this.termsConditonsTemp);
    }

    login() {
        //this.routerExtensions.navigate([this.afterLoginRoute]);
        let spinnerOptions: SpinnerOptionInterface = {
            android: {
                color: "#0066b3"
            }
        }
        this.spinnerService.show(spinnerOptions);

        this.savingData = true;
        const formValue = this.loginForm.value;
        // console.log(formValue);
        console.log(JSON.stringify(this.loginForm.value));
        if (getConnectionType() === connectionType.none) {
            alert("This Application requires an internet connection to log in.");
            return;
        }
        if (formValue.password === null) {
            alert("Password missing.");
            return;
        }
        //this.routerExtensions.navigate(["/audit"]);
        this.loginService.getAuthenticatedAccessToken(formValue.email, formValue.password, null)
            .subscribe(response => {
                console.log('response', response);
                // this.userInfoService.changeCurrentUserData(response.user);
                this.setRouteSessionAfterLogin(response);
                this.savingData = false;
                //setTimeout(() => {
                    this.spinnerService.hide();
                //}, 1700);
            }, error => {
                console.log('error', error);
                setTimeout(() => {
                    this.spinnerService.hide();
                }, 1000);
               console.log("error", error.error.error)
                //this.termsConditionDetails = JSON.parse(error.error.error_description);
                switch (error.error.error) {
                    case "term_and_condition_acceptance_require":
                        if (this.loginPageData && this.loginPageData.gdprEnable)
                            this.loginService.showGdpr(this.termsConditionDetails);
                            this.spinnerService.hide();
                        break;
                    case "two_factor_code_require":
                        this.spinnerService.hide();
                        break;
                    case "incorrect_user_name_or_password":
                        this.snackBarService.show(localize("tns.unauthorizedUsernameOrPassword"));
                        this.spinnerService.hide();
                        break;
                    case "Session_Lock_Id":
                        this.snackBarService.show(localize("tns.sessionLocked"));
                        this.spinnerService.hide();
                        break;
                    case "persona_require":
                        this.spinnerService.hide();
                        break;
                    case "verification_code_require":
                        this.snackBarService.show(localize("tns.sessionLocked"));
                        this.spinnerService.hide();
                        break;
                    default:
                        this.spinnerService.hide();
                        break;
                }
            });
    }

    private validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    public setRouteSessionAfterLogin(response: any): void {
        const userDetails = this.userInfoService.getUserDataByAccessToken(response.access_token);
        const userRoles = userDetails.Roles;

        //this.localPlatformDataService.dropDataBase();
        console.log('here=====>',this.localPlatformDataService.getDatabaseInfo().dbName)
        //if (!this.localPlatformDataService.getDatabaseInfo().dbName) {
            let dbName = this.appSettingsProvider.getAppSettings().LocalDBName;
            this.localPlatformDataService.initDatabase(dbName + '_' + userDetails.UserId);
        //    console.log('Initialized Couchbase database: ', dbName + '_' + userDetails.UserId);
        //}
        // console.log('Database created', dbName + '_' + userDetails.UserId);
        for (let index = 0; index < this.loginDefaultRoutes.length; index++) {
            if (userRoles.indexOf(this.loginDefaultRoutes[index]["role"]) !== -1) {
                let navigateUrl = `${this.loginDefaultRoutes[index]["route"]}`;
                this.router.navigateByUrl(navigateUrl);
                break;
            }
            // else {
            //     this.snackBarService.show('You dont have enough permission!', 'white');
            // }
        }
        this.navigationProvider.assignNavigationsVisibility();
        this.navigationProvider.getCurrentNavigationObservable().subscribe((data) => {
            if (data && data.length > 0) {
                this.userInfoService.setUserInfo(userDetails);
                //this.userInfoService.sendUserInfoEvent(userDetails);
            }
        });
    }

    logout(unsubscribeUserId) {
        console.log("unsubscribeUserId::", unsubscribeUserId);
        if (getConnectionType() === connectionType.none) {
            alert("Please check internet connection");
            return;
        }
    }

    enableLogin(){
        this.isForgotPasswordEnabled = false;
        this.captchaImageSrc = null;
        this.captchaData = null;
        this.isResetSuccsessfull = false;
        this.showLogin = true;
        this.initForm({
            Email: this.loginForm.get('email').value,
            Password: this.loginForm.get('password').value,
            CaptchaValue: this.loginForm.patchValue({'captchaValue':''})
        } as UserModel);
    }

    forgotPassword() {
        /* prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for Trackord to reset your password.",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result) {
            }
        }); */
        this.loginForm.get('captchaValue').setValue('');
        this.isForgotPasswordEnabled = true;
        // const data :UserModel = {};
        this.initForm({
            Email: this.loginForm.get('email').value,
            Password: this.loginForm.get('password').value,
            CaptchaValue: this.loginForm.patchValue({'captchaValue':''})
        } as UserModel);
        this.getCaptcha();
    }

    getCaptcha() {
        this.loginService.getCaptcha().subscribe((response) => {
            this.captchaData = response;
            this.captchaImageSrc = 'data:image/png;base64,' + response.Captcha;
        });
    }

    captchHandler() {
        this.captchaImageSrc = null;
        this.loginForm.patchValue({'captchaValue':''});
        this.getCaptcha();
        this.subscribeRecreateCaptcha();
    }

    subscribeRecreateCaptcha(){
        this.recreateCaptchaSubmission && this.recreateCaptchaSubmission.unsubscribe();
        this.recreateCaptchaSubmission =  this.loginService.OnCaptchaSubmit.pipe(first(),takeUntil(this.destroyed$)).subscribe(response => {
            if (response) {
                this.getCaptcha();
                this.subscribeRecreateCaptcha();
            }
        });
    }

    public loadTermsAndConditon(termsConditon: any) {
        if (this.loadPdfFlag === true) {
            return;
        }

        if (termsConditon.type === "link") {
            utils.openUrl(termsConditon.url);
        }
        if (termsConditon.type === "pdf") {
            this.loadPdfFlag = true;

            const options: ModalDialogOptions = {
                context: {
                    termsConditon: termsConditon
                },
                stretched: false,
                fullscreen: true,
                viewContainerRef: this.vcRef,
            };
            this.modal.showModal(PrivacyPdfModalComponent, options).then(response => {
                this.loadPdfFlag = false;
            });
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}

export interface LoginConfiguration {
    welcomeText?: string;
    welcomeDescription?: string;
    loginHeaderText?: string;
    socialLogin?: boolean;
    createAccount?: boolean;
    rememberMe?: boolean;
    forgotPassword?: boolean;
    languageOption?: boolean;
    showBackgroundAnimation?: boolean;
    backgroundImageSrc: string;
    topLogo: any;
    bottomlogo?: any;
    centerText?: boolean;
    topText?: any;
    loginButtonRadius?: number;
    leftSideLogo?: any;
    termsConditons?: any;
    action?: any,
    landingUri?: any,
    gdprEnable?:boolean;
    termsAndConditionsHidden?: boolean;
    defaultCredential?: LoginFields;
    loginScreenSize? : LoginScreenSize;
}

export interface LoginFields {
    email: string;
    password: string;
}

export interface LoginScreenSize {
    height?: string;
    width?: string;
}
