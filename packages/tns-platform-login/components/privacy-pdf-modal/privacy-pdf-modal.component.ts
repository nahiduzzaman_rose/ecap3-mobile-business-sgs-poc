import {Component, OnDestroy, OnInit,} from "@angular/core";
import {ModalDialogParams} from "nativescript-angular/directives/dialogs";
import * as application from "tns-core-modules/application";
import {PDFView} from "nativescript-pdf-view";
import {registerElement} from 'nativescript-angular';
import {LoadingIndicator} from "@nstudio/nativescript-loading-indicator";
import * as fs from "tns-core-modules/file-system";

registerElement('PDFView', () => PDFView);

@Component({
    selector: "gr-privacy-pdf-modal",
    moduleId: module.id,
    templateUrl: "./privacy-pdf-modal.component.html",
    styleUrls: ["./privacy-pdf-modal-component.css"],
})

export class PrivacyPdfModalComponent implements OnInit, OnDestroy {
    private indicator: LoadingIndicator;
    public passData: any = {};
    public pdfSrc = "";
    public termsConditon: any;

    constructor(private params: ModalDialogParams) {
        this.indicator = new LoadingIndicator();
    }


    ngOnInit(): void {
        application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
            args.cancel = true;
            console.log("::Event Detail Modal Go Back::");
            this.modalClose(false);
        });
        this.indicator.show();
        // this.indicator.show({android: {indeterminate: true}});
        var appFolder = fs.knownFolders.currentApp();

        this.termsConditon = this.params.context.termsConditon;

        console.log("this.termsConditon", this.termsConditon);
        if (this.termsConditon.type === "pdf") {
            // this.pdfSrc=this.termsConditon.url;
            this.pdfSrc = fs.path.join(appFolder.path, 'gr_files', this.termsConditon.url);
            console.log("this.pdfSrc", this.pdfSrc);
        }

        // if (this.params.context.flag == 1) {
        //     this.pdfSrc = fs.path.join(appFolder.path, 'gr_files', 'Impressum.pdf');
        // } else {
        //     this.pdfSrc = fs.path.join(appFolder.path, 'gr_files', 'Privacy.pdf');
        // }
    }

    public modalClose(param: boolean) {
        console.log("Close", param);
        this.params.closeCallback(param);
    }

    public onPDFLoad() {
        console.log('Loaded PDF!');
        this.indicator.hide();
    }

    ngOnDestroy() {
        console.log("::Modal View Destroyed::");
    }

    // Event handler for Page 'loaded' event attached in main-page.xml
    // public pageLoaded(args: observable.EventData) {
    //     // Get the event sender
    //     let page = <pages.Page>args.object;
    //     let view: PDFViewNg = page.getViewById('pdfview');
    //     if (view) {
    //         view.on("load", () => {
    //             let items = view.getBookmarksByLabel("PAGE 4")
    //             console.error("found:", items);
    //             view.goToBookmark(items[0]);
    //             console.log("Author:", view.getAuthor());
    //             console.log("Pages:", view.getPageCount());
    //         });
    //     }
    // }
}
