"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page");
var ProjectDefaultComponent = /** @class */ (function () {
    function ProjectDefaultComponent(page) {
        this.page = page;
        this.actionBarConfiguration = [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 1',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 2',
                visible: true,
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemType: 'popup',
                actionItemText: 'Log Out',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemType: 'popup',
                actionItemText: 'Go to Project',
                visible: true,
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 5',
                visible: true,
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 6',
                visible: true,
                url: null,
                callParentMethod: false
            }
        ];
    }
    ProjectDefaultComponent.prototype.actionItemClicked = function (event) {
        console.log('project event', event);
    };
    ProjectDefaultComponent.prototype.ngOnInit = function () {
        // this.page.actionBarHidden = true;
    };
    ProjectDefaultComponent = __decorate([
        core_1.Component({
            selector: 'ns-project-default',
            templateUrl: './project-default.component.html',
            styleUrls: ['./project-default.component.css'],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [page_1.Page])
    ], ProjectDefaultComponent);
    return ProjectDefaultComponent;
}());
exports.ProjectDefaultComponent = ProjectDefaultComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1kZWZhdWx0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInByb2plY3QtZGVmYXVsdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsaURBQThDO0FBUTlDO0lBc0RJLGlDQUFvQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQXJEOUIsMkJBQXNCLEdBQVM7WUFDM0I7Z0JBQ0ksY0FBYyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksR0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsY0FBYyxFQUFFLFdBQVc7Z0JBQzNCLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixPQUFPLEVBQUUsSUFBSTtnQkFDYixHQUFHLEVBQUUsSUFBSTtnQkFDVCxnQkFBZ0IsRUFBRSxLQUFLO2dCQUN2QixJQUFJLEVBQUUsTUFBTTthQUNmO1lBQ0Q7Z0JBQ0ksY0FBYyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksR0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsY0FBYyxFQUFFLFdBQVc7Z0JBQzNCLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixPQUFPLEVBQUUsSUFBSTtnQkFDYixHQUFHLEVBQUUsSUFBSTtnQkFDVCxnQkFBZ0IsRUFBRSxLQUFLO2FBQzFCO1lBQ0Q7Z0JBQ0ksY0FBYyxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksR0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUQsY0FBYyxFQUFFLE9BQU87Z0JBQ3ZCLGNBQWMsRUFBRSxTQUFTO2dCQUN6QixPQUFPLEVBQUUsSUFBSTtnQkFDYixHQUFHLEVBQUUsSUFBSTtnQkFDVCxnQkFBZ0IsRUFBRSxLQUFLO2dCQUN2QixJQUFJLEVBQUUsUUFBUTthQUNqQjtZQUNEO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxPQUFPO2dCQUN2QixjQUFjLEVBQUUsZUFBZTtnQkFDL0IsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFDLFVBQVU7Z0JBQ2QsZ0JBQWdCLEVBQUUsS0FBSzthQUMxQjtZQUNEO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxXQUFXO2dCQUMzQixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsZ0JBQWdCLEVBQUUsSUFBSTthQUN6QjtZQUNEO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxXQUFXO2dCQUMzQixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsZ0JBQWdCLEVBQUUsS0FBSzthQUMxQjtTQUNKLENBQUM7SUFFZ0MsQ0FBQztJQUVuQyxtREFBaUIsR0FBakIsVUFBa0IsS0FBVTtRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUNJLG9DQUFvQztJQUV4QyxDQUFDO0lBL0RRLHVCQUF1QjtRQU5uQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsa0NBQWtDO1lBQy9DLFNBQVMsRUFBRSxDQUFDLGlDQUFpQyxDQUFDO1lBQzlDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUN0QixDQUFDO3lDQXVENEIsV0FBSTtPQXREckIsdUJBQXVCLENBaUVuQztJQUFELDhCQUFDO0NBQUEsQUFqRUQsSUFpRUM7QUFqRVksMERBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtQYWdlfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbnMtcHJvamVjdC1kZWZhdWx0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wcm9qZWN0LWRlZmF1bHQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcHJvamVjdC1kZWZhdWx0LmNvbXBvbmVudC5jc3MnXSxcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWRcclxufSlcclxuZXhwb3J0IGNsYXNzIFByb2plY3REZWZhdWx0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGFjdGlvbkJhckNvbmZpZ3VyYXRpb24gOiBhbnkgPSBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhY3Rpb25JdGVtSWNvbjogU3RyaW5nLmZyb21DaGFyQ29kZShwYXJzZUludCgnMHgnKydmMDM5JykpLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVHlwZTogJ2FjdGlvbkJhcicsXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UZXh0OiAnTWVudSAxJyxcclxuICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgdXJsOiBudWxsLFxyXG4gICAgICAgICAgICBjYWxsUGFyZW50TWV0aG9kOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogJ21lbnUnXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YxZTAnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAnYWN0aW9uQmFyJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdNZW51IDInLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YwNjQnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAncG9wdXAnLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVGV4dDogJ0xvZyBPdXQnLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiAnbG9nb3V0J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhY3Rpb25JdGVtSWNvbjogU3RyaW5nLmZyb21DaGFyQ29kZShwYXJzZUludCgnMHgnKydmMDY0JykpLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVHlwZTogJ3BvcHVwJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdHbyB0byBQcm9qZWN0JyxcclxuICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgdXJsOicvcHJvamVjdCcsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YyNjQnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAnYWN0aW9uQmFyJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdNZW51IDUnLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYWN0aW9uSXRlbUljb246IFN0cmluZy5mcm9tQ2hhckNvZGUocGFyc2VJbnQoJzB4JysnZjI2MycpKSxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVR5cGU6ICdhY3Rpb25CYXInLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVGV4dDogJ01lbnUgNicsXHJcbiAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgIHVybDogbnVsbCxcclxuICAgICAgICAgICAgY2FsbFBhcmVudE1ldGhvZDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICBdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcGFnZTogUGFnZSkgeyB9XHJcblxyXG4gICAgYWN0aW9uSXRlbUNsaWNrZWQoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdwcm9qZWN0IGV2ZW50JyxldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcblxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=