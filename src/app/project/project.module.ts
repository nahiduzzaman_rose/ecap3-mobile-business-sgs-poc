import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { PlatformCardModule } from '@ecap3/tns-platform-card';
import { PlatformImageModule, PlatformImageService } from '@ecap3/tns-platform-image';
import { PlatformVideoModule } from '@ecap3/tns-platform-video';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular';
import { ImageAppComponent } from './components/image-app/image-app.component';
import { ProjectDefaultComponent } from './components/project-default/project-default.component';
import { ProjectRoutingModule } from './project-routing.module';

@NgModule({
  providers:[PlatformImageService],
  declarations: [ProjectDefaultComponent, ImageAppComponent],
  imports: [
    NativeScriptCommonModule,
    ProjectRoutingModule,
    CoreTemplateModule,
    PlatformCardModule,
    PlatformVideoModule,
    NativeScriptUISideDrawerModule,
    PlatformImageModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ProjectModule { }
