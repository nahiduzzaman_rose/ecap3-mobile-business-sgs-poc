"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var project_default_component_1 = require("./components/project-default/project-default.component");
var project_routing_module_1 = require("./project-routing.module");
var tns_core_template_1 = require("@ecap3/tns-core-template");
var ProjectModule = /** @class */ (function () {
    function ProjectModule() {
    }
    ProjectModule = __decorate([
        core_1.NgModule({
            declarations: [project_default_component_1.ProjectDefaultComponent],
            imports: [
                common_1.NativeScriptCommonModule,
                project_routing_module_1.ProjectRoutingModule,
                tns_core_template_1.CoreTemplateModule
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], ProjectModule);
    return ProjectModule;
}());
exports.ProjectModule = ProjectModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9qZWN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRDtBQUMzRCxzREFBdUU7QUFDdkUsb0dBQWlHO0FBQ2pHLG1FQUFnRTtBQUNoRSw4REFBOEQ7QUFXOUQ7SUFBQTtJQUE2QixDQUFDO0lBQWpCLGFBQWE7UUFUekIsZUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFLENBQUMsbURBQXVCLENBQUM7WUFDdkMsT0FBTyxFQUFFO2dCQUNQLGlDQUF3QjtnQkFDeEIsNkNBQW9CO2dCQUNwQixzQ0FBa0I7YUFDbkI7WUFDRCxPQUFPLEVBQUUsQ0FBQyx1QkFBZ0IsQ0FBQztTQUM1QixDQUFDO09BQ1csYUFBYSxDQUFJO0lBQUQsb0JBQUM7Q0FBQSxBQUE5QixJQUE4QjtBQUFqQixzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1hbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFByb2plY3REZWZhdWx0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3Byb2plY3QtZGVmYXVsdC9wcm9qZWN0LWRlZmF1bHQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUHJvamVjdFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL3Byb2plY3Qtcm91dGluZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBDb3JlVGVtcGxhdGVNb2R1bGUgfSBmcm9tICdAZWNhcDMvdG5zLWNvcmUtdGVtcGxhdGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtQcm9qZWN0RGVmYXVsdENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgUHJvamVjdFJvdXRpbmdNb2R1bGUsXHJcbiAgICBDb3JlVGVtcGxhdGVNb2R1bGVcclxuICBdLFxyXG4gIHNjaGVtYXM6IFtOT19FUlJPUlNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUHJvamVjdE1vZHVsZSB7IH1cclxuIl19