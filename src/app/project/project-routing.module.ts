import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { ProjectDefaultComponent } from './components/project-default/project-default.component';
import { ImageAppComponent } from './components/image-app/image-app.component';

const routes: Routes = [
    { path: '', component: ProjectDefaultComponent },
    { path: 'image', component: ImageAppComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ProjectRoutingModule { }
