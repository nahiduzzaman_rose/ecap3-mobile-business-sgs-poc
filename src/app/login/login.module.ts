import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { LoginRoutingModule } from "./login.routing";
import { LoginDefaultComponent } from "./components/login-default/login-default.component";
import { PlatformLoginModule } from "@ecap3/tns-platform-login";
import { PlatformGdprModule } from "@ecap3/tns-platform-gdpr";

@NgModule({
  providers: [
  ],
  imports: [
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    LoginRoutingModule,
    PlatformLoginModule,
    PlatformGdprModule
  ],
  declarations: [
      LoginDefaultComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule { }
