import { localize } from 'nativescript-localize';
import {Component, OnInit, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {LoginService} from '@ecap3/tns-platform-login';
import {PlatformGdprService} from "@ecap3/tns-platform-gdpr";
import {NavigationProvider, UserInfoService} from "@ecap3/tns-core";
import {LoginConfiguration} from "@ecap3/tns-platform-login/components/login-default/login-default.component";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login-default.component.html",
    styleUrls: [
        "./login-default-common.scss",
        "./login-default.component.scss"
    ]
})
export class LoginDefaultComponent implements OnInit, OnDestroy {
    showGDPR = false;
    requiredConditionsDetails: any;
    getGdprContentUsingInterface: boolean = false;

    subscription: Subscription[] = [];

    loginDefaultRoutes: any[] = [
        {
            role: 'admin',
            route: '/shipments'
        },
        {
            role: 'site manager',
            route: '/shipments'
        }
    ];
    loginPageData: LoginConfiguration =
        {
            welcomeText: localize("tns.welcome"),
            welcomeDescription: null,
            loginHeaderText: localize("tns.loginHeader"),
            socialLogin: false,
            createAccount: false,
            rememberMe: false,
            forgotPassword: true,
            languageOption: true,
            showBackgroundAnimation: true,
            backgroundImageSrc: "res://login_bg",
            topLogo: {
                src: "res://logo"
            },
            bottomlogo: {
                src: "",
                url: "",
            },
            centerText: false,
            topText: {
                welcomeText: '',
                welcomeDescription: "",
            },
            loginButtonRadius: 80,
            leftSideLogo: null,
            action: "",
            landingUri: "",
            defaultCredential: {
                email: 'aardo@yopmail.com',
                password: '1qazZAQ!'
            },
            gdprEnable: true,
            termsAndConditionsHidden: true,
            termsConditons: [
                // { url: "", label: "tns-login.IMRESSUM", type: 'link'}, // type or pdf
                // { url: "", label: "tns-login.DATA_PRIVACY", type: 'link'}, // type or pdf
                // { url: "", label: "tns-login.LEGAL", type: 'link'} // type or pdf

                // { url: "https://fb.ch", label: "Imressum", type: 'link'}, // type or pdf
                // { url: "https://selise.ch", label: "Data Privacy", type: 'link'}, // type or pdf
                // { url: "https://google.com", label: "Legal", type: 'link'} // type or pdf
            ],
            loginScreenSize: {
                height: '60%'
            }
        }

    gdprConfiguration: any = {
        logo: {
            src: "res://login_logo"
        }
    }

    constructor(private loginService: LoginService, private gdprService: PlatformGdprService,
                private navigationProvider: NavigationProvider,
                private userInfoService: UserInfoService) {
        console.log('LoginComponent Biz App!!');

    }

    ngOnInit() {
        console.log('Login Page Loaded!');

        this.subscription.push(
            this.loginService.gdprControl.subscribe((errorData) => {
                if (this.showGDPR) {
                    this.showGDPR = false;
                }

                setTimeout(() => {
                    this.showGDPR = true;
                }, 1);

                this.requiredConditionsDetails = errorData;
            })
        );

        this.subscription.push(
            this.gdprService.gdprEvent.subscribe((response) => {
                console.log('data response', response);
                if (response && response.access_token) {
                    this.setRouteSessionAfterLogin(response);
                }
                this.showGDPR = false;
            })
        );
    }

    public setRouteSessionAfterLogin(response: any): void {
        const userDetails = this.userInfoService.getUserDataByAccessToken(response.access_token);
        this.navigationProvider.assignNavigationsVisibility();
        this.navigationProvider.getCurrentNavigationObservable().subscribe((data) => {
            if (data && data.length > 0) {
                this.userInfoService.setUserInfo(userDetails);
                this.userInfoService.sendUserInfoEvent(userDetails);
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription.length > 0) {
            this.subscription.forEach(item => item.unsubscribe());
        }
    }
}
