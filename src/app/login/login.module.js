"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("nativescript-angular/common");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var login_routing_1 = require("./login.routing");
var login_default_component_1 = require("./components/login-default/login-default.component");
var tns_platform_login_1 = require("@ecap3/tns-platform-login");
var tns_platform_gdpr_1 = require("@ecap3/tns-platform-gdpr");
exports.routerConfig = [{
        path: "",
        component: login_default_component_1.LoginDefaultComponent
    }];
var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            providers: [],
            imports: [
                forms_1.NativeScriptFormsModule,
                common_1.NativeScriptCommonModule,
                login_routing_1.LoginRoutingModule,
                tns_platform_login_1.PlatformLoginModule,
                tns_platform_gdpr_1.PlatformGdprModule
            ],
            declarations: [
                login_default_component_1.LoginDefaultComponent
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0RBQXVFO0FBQ3ZFLG9EQUFxRTtBQUNyRSxzQ0FBMkQ7QUFDM0QsaURBQXFEO0FBQ3JELDhGQUEyRjtBQUMzRixnRUFBZ0U7QUFDaEUsOERBQThEO0FBR2pELFFBQUEsWUFBWSxHQUFHLENBQUM7UUFDM0IsSUFBSSxFQUFFLEVBQUU7UUFDUixTQUFTLEVBQUUsK0NBQXFCO0tBQ2pDLENBQUMsQ0FBQztBQWlCSDtJQUFBO0lBQTJCLENBQUM7SUFBZixXQUFXO1FBZnZCLGVBQVEsQ0FBQztZQUNSLFNBQVMsRUFBRSxFQUNWO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLCtCQUF1QjtnQkFDdkIsaUNBQXdCO2dCQUN4QixrQ0FBa0I7Z0JBQ2xCLHdDQUFtQjtnQkFDbkIsc0NBQWtCO2FBQ25CO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLCtDQUFxQjthQUN4QjtZQUNELE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzVCLENBQUM7T0FDVyxXQUFXLENBQUk7SUFBRCxrQkFBQztDQUFBLEFBQTVCLElBQTRCO0FBQWYsa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTG9naW5Sb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vbG9naW4ucm91dGluZ1wiO1xyXG5pbXBvcnQgeyBMb2dpbkRlZmF1bHRDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL2xvZ2luLWRlZmF1bHQvbG9naW4tZGVmYXVsdC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgUGxhdGZvcm1Mb2dpbk1vZHVsZSB9IGZyb20gXCJAZWNhcDMvdG5zLXBsYXRmb3JtLWxvZ2luXCI7XHJcbmltcG9ydCB7IFBsYXRmb3JtR2Rwck1vZHVsZSB9IGZyb20gXCJAZWNhcDMvdG5zLXBsYXRmb3JtLWdkcHJcIjtcclxuXHJcblxyXG5leHBvcnQgY29uc3Qgcm91dGVyQ29uZmlnID0gW3tcclxuICBwYXRoOiBcIlwiLFxyXG4gIGNvbXBvbmVudDogTG9naW5EZWZhdWx0Q29tcG9uZW50XHJcbn1dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBwcm92aWRlcnM6IFtcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxyXG4gICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgTG9naW5Sb3V0aW5nTW9kdWxlLFxyXG4gICAgUGxhdGZvcm1Mb2dpbk1vZHVsZSxcclxuICAgIFBsYXRmb3JtR2Rwck1vZHVsZVxyXG4gIF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgIExvZ2luRGVmYXVsdENvbXBvbmVudFxyXG4gIF0sXHJcbiAgc2NoZW1hczogW05PX0VSUk9SU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbk1vZHVsZSB7IH1cclxuIl19