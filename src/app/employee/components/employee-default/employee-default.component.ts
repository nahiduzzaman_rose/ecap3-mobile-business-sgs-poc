import { Component, OnInit } from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';

@Component({
  selector: 'ns-employee-default',
  templateUrl: './employee-default.component.html',
  styleUrls: ['./employee-default.component.scss'],
  moduleId: module.id,
})
export class EmployeeDefaultComponent implements OnInit {

  actionBarConfiguration : any = SCConfig.ActionBarConfiguration;
  constructor() { }

  ngOnInit() {
  }

}
