import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { EmployeeDefaultComponent } from './components/employee-default/employee-default.component';
import { EmployeeRoutingModule } from './employee-routing.module';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { PlatformCardModule } from '@ecap3/tns-platform-card';


@NgModule({
  declarations: [EmployeeDefaultComponent],
  imports: [
    NativeScriptCommonModule,
    EmployeeRoutingModule,
    CoreTemplateModule,
    PlatformCardModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EmployeeModule { }
