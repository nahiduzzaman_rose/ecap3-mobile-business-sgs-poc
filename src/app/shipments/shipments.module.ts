import { ReactiveFormsModule } from '@angular/forms';
import { ModalDialogService } from 'nativescript-angular/modal-dialog';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { ShipmentsDefaultComponent } from './components/shipments-default/shipments-default.component';
import { ShipmentsRoutingModule } from './shipments-routing.module';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { PlatformCardModule } from '@ecap3/tns-platform-card';
import { ShipmentForwardComponent } from './components/shipment-forward/shipment-forward.component';
import { DropDownModule } from "nativescript-drop-down/angular";
import { ShipmentModalViewComponent } from './components/shipment-modal-view/shipment-modal-view.component';
import { NativeScriptFormsModule } from 'nativescript-angular';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptLocalizeModule } from 'nativescript-localize/angular';
import { PlatformDialogModule } from '@ecap3/tns-platform-dialog/platform-dialog.module';
import { SharedDataModule } from "~/app/shared-data/shared-data.module";
import { GraphQLModule } from '@ecap3/tns-platform-graphql';

@NgModule({
  declarations: [ShipmentsDefaultComponent, ShipmentForwardComponent, ShipmentModalViewComponent],
  entryComponents: [ShipmentModalViewComponent],
    imports: [
        NativeScriptCommonModule,
        ShipmentsRoutingModule,
        CoreTemplateModule,
        PlatformCardModule,
        PlatformDialogModule,
        DropDownModule,
        ReactiveFormsModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NativeScriptUIListViewModule,
        NativeScriptLocalizeModule,
        SharedDataModule
    ],
  providers: [ModalDialogService],
  schemas: [NO_ERRORS_SCHEMA]
})

export class ShipmentsModule { }
