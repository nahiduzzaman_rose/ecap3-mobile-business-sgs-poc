import { Component, OnInit, OnDestroy} from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { ItemEventData } from 'tns-core-modules/ui/list-view/list-view';
import { RouterExtensions } from 'nativescript-angular';
import { SeedDataConstant } from '~/app/sc-config/constant/seed-data.constant';

@Component({
    selector: 'ns-shipments-default',
    templateUrl: './shipments-default.component.html',
    styleUrls: ['./shipments-default.component.scss'],
    moduleId: module.id,
})

export class ShipmentsDefaultComponent implements OnInit, OnDestroy {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    shipments: any[] = [];

    constructor(
        private routerExtensions: RouterExtensions,
    ) {}

    onItemTap(args: ItemEventData) {
        const selectedTask = this.shipments[args.index];
        this.routerExtensions.navigate(['shipment-details', selectedTask.ItemId], {
            transition: {
                name: "slide"
            }
        });
    }

    ngOnInit() {
        this.shipments = SeedDataConstant.Shipments;
    }
    ngOnDestroy(): void {

    }


}
