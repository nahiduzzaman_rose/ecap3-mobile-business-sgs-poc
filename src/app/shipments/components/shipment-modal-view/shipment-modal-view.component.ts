import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ListPicker } from 'tns-core-modules/ui/list-picker';
import { EventData } from 'tns-core-modules/data/observable/observable';
import { ShipmentsService } from '../../services/shipments.service';
import { ActivatedRoute } from '@angular/router';
import {ItemEventData} from "ui/list-view";

@Component({
    selector: 'ns-shipment-modal-view',
    templateUrl: './shipment-modal-view.component.html',
    styleUrls: ['./shipment-modal-view.component.scss'],
    moduleId: module.id,
})

export class ShipmentModalViewComponent implements OnInit {
    zones: any = [];
    currentShipmentId: any;
    currentShipmentNumber: any;
    constructor(
        private activatedRoute: ActivatedRoute,
        private params: ModalDialogParams
    ) {
        // this.zones = [{
        //     "Id": "a190e0c2-3875-4f6a-a3ae-0bfe78c670d9",
        //     "Name": "Nasa UL 01",
        //     "Siteid": "57a8184a-9016-4c62-86aa-654da9e910be",
        //     "LocationType": 2,
        //     "LocationTypeName": "Unloading Zones",
        //     "WKTType": 1,
        //     "LocationPoint": "38.88417983441229,-77.01702961852413",
        //     "LocationPolygon": null,
        //     "IsDefault": false,
        //     "DirectionWayPoints": null,
        //     "LocationPointLat": "38.88417983441229",
        //     "LocationPointLng": "-77.01702961852413"
        // }, {
        //     "Id": "8f6d60c9-693d-4d07-8402-026c9c632548",
        //     "Name": "Nasa UL - 2",
        //     "Siteid": "57a8184a-9016-4c62-86aa-654da9e910be",
        //     "LocationType": 1,
        //     "LocationTypeName": "Unloading Zones",
        //     "WKTType": 1,
        //     "LocationPoint": "38.88438027020404,-77.01691160132748",
        //     "LocationPolygon": null,
        //     "IsDefault": false,
        //     "DirectionWayPoints": null,
        //     "LocationPointLat": "38.88438027020404",
        //     "LocationPointLng": "-77.01691160132748"
        // }];
        this.zones = params.context.data.zones;
        this.currentShipmentNumber = params.context.data.shipmentNumber;
        console.log("Zones for modal",this.zones);
    }

    public onTap(zone: any) {
        debugger;
        const selectedZone = zone;
        this.params.closeCallback(selectedZone);
    }

    public onClose() {
        this.params.closeCallback();
    }

    // getSiteLocations(siteId) {
    //     this.shipmentService.getSiteLocations(siteId).subscribe( response => {
    //         this.unloadingZones = response;
    //         console.log("All Zones", this.unloadingZones);
    //         this.getOtherZones();
    //     });
    // }
    //
    // getOtherZones() {
    //     this.shipmentService.getUnloadingZoneName(this.currentShipmentId).subscribe(response  => {
    //         this.currentUnloadingZone = response;
    //         console.log("response",this.currentUnloadingZone);
    //         for (let i of this.unloadingZones) {
    //
    //             this.zones.push(i.Name);
    //         }
    //         console.log("Zones",this.zones);
    //     });
    // }

    ngOnInit() {
        this.currentShipmentId = this.activatedRoute.snapshot.params['ItemId'];
       // this.getSiteLocations(this.siteId);
    }
}
