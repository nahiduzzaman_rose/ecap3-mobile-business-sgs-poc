
import { ModalDialogService, ModalDialogOptions } from 'nativescript-angular/modal-dialog';
import {Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { ShipmentModalViewComponent } from '~/app/shipments/components/shipment-modal-view/shipment-modal-view.component';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import {ShipmentsService} from "~/app/shipments/services/shipments.service";
import {ActivatedRoute} from "@angular/router";
import {
    ModalContentComponent,
    modalHeaderData
} from '@ecap3/tns-platform-dialog/components/modal-content/modal-content.component';
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';
import {Observable} from "rxjs";
import {map} from "rxjs/internal/operators";
import {PlatformDataService, SnackBarService} from "@ecap3/tns-core";
import {RouterExtensions} from "nativescript-angular";

@Component({
    selector: 'ns-shipment-forward',
    providers: [ModalDialogService],
    templateUrl: './shipment-forward.component.html',
    styleUrls: ['./shipment-forward.component.scss'],
    moduleId: module.id,
})
export class ShipmentForwardComponent implements OnInit {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    @ViewChild('modalSyncConfirmation', { static: false }) modalSyncConfirmation: TemplateRef<any>;
    modalHeaderData : modalHeaderData;

    zone: any;

    shipmentForward: FormGroup;
    screenRationWidth: number;
    screenRationHeight: number;
    isTabWidthIsNineSixty: boolean;
    ScreenHeight: number;
    currentShipmentNumber: any;
    zones: any = [];
    unloadingZones: any;
    currentUnloadingZone: any;
    currentShipmentId: any;
    backbuttonConfig: BackbuttonConfig;
    siteId: string;
    currentShipment: any = [];
    textLength: number;
    shipmentForwardData: any = {};

    public constructor(
        private modal: ModalDialogService, 
        private vcRef: ViewContainerRef, 
        private fb: FormBuilder,
        private shipmentService: ShipmentsService,
        private activatedRoute: ActivatedRoute,
        private modalService: ModalDialogService,
        private platformDataService: PlatformDataService,
        private viewContainerRef: ViewContainerRef,
        private routerExtensions: RouterExtensions,
        private snackBarService: SnackBarService
    ) {}

    public onopen() {
        console.log("Drop Down opened.");
    }

    public onclose() {
        console.log("Drop Down closed.");
    }

    public showModal() {
        let option: ModalDialogOptions = {
            context: {
                data: {
                    zones: this.zones,
                    shipmentNumber: this.currentShipmentNumber
                }
            },
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(ShipmentModalViewComponent, option).then(response => {
            this.currentUnloadingZone = response;
            this.zone = response.Name;

            this.shipmentForward.get('zone').patchValue(this.zone);
        });
    }

    forwardShipment() {
        this.shipmentForwardData = {
            ItemId: this.currentShipmentId,
            SiteId: this.siteId,
            Status: 3,
            RoutingStatus: this.currentShipment.RoutingStatus,
            DestinationId: this.currentUnloadingZone.Id,
            DestinationName: this.currentUnloadingZone.Name,
            DestinationType: this.currentUnloadingZone.LocationType,
            DestinationLat: this.currentUnloadingZone.LocationPointLat,
            DestinationLng: this.currentUnloadingZone.LocationPointLng
        };

        console.log("Shipment Forward values",this.shipmentForwardData);
        this.shipmentService.forwardShipment(this.shipmentForwardData).subscribe(response => {
            this.snackBarService.show('Successfully forwarded.', 'white');
            this.routerExtensions.navigate(['shipment-details', this.currentShipmentId, this.siteId], {
                transition: {
                    name: "slide"
                }
            });
        });

    }

    showConfirmation() {
        let confirm = (callback) => {
            debugger;
            callback('ok');
            //console.log('call confirm !!', this.currentUnloadingZone);
            this.forwardShipment();

        };
        let cancel = (callback) => {
            callback('cancel');
            console.log('call canceled');
        };
        let chooseTemplate = (templateType?: string) => {
            return this.modalSyncConfirmation;
        }
        let options: ModalDialogOptions = {
            context: {
                promptMsg: 'This is the prompt message!!!',
                template: chooseTemplate(),
                width: "300",
                okButtonText: 'Ok',
                cancelButtonText: 'Cancel',
                functions: {
                    confirm: confirm,
                    cancel: cancel
                }
            },
            fullscreen: false,
            viewContainerRef: this.viewContainerRef
        };
        debugger;
        this.modal.showModal(ModalContentComponent, options)
            .then((dialogResult: string) => {
                console.log(dialogResult);
            })
    }

    onTap() {
        console.log("Return Value",this.shipmentForward.value);
    }

    getSiteLocations(siteId) {
        this.shipmentService.getSiteLocations(siteId).subscribe( response => {
            this.unloadingZones = response;
            this.getShipment();
        });
    }

    getOtherZones() {
       for (let zone of this.unloadingZones) {
           if(zone.Id !== this.currentShipment.DestinationId) {
               this.zones.push(zone);
           }
       }
    }

    goBack() {
        this.backbuttonConfig = {
            path : '/shipment-details/' + this.currentShipmentId + '/' + this.siteId
        };
    }

    getShipment() {
        this.shipmentService.getShipmentById(this.currentShipmentId).subscribe(response  => {
            this.currentShipment = response['Data'][0];
            this.currentShipmentNumber = this.currentShipment.ShipmentNumber;
            this.getOtherZones();
        });
    }
    onTextChange(event) {
        this.textLength = event.value.length;
    }

    ngOnInit() {
        this.textLength = 0;
        this.ScreenHeight = screen.mainScreen.heightDIPs;
        this.screenRationWidth = (screen.mainScreen.widthDIPs / 1280);
        this.screenRationHeight = (screen.mainScreen.heightDIPs / 800);
        this.currentShipmentId = this.activatedRoute.snapshot.params['ItemId'];
        this.siteId = this.activatedRoute.snapshot.params['SiteId'];
        this.getSiteLocations(this.siteId);
        //this.getShipment();
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }
        this.shipmentForward = this.fb.group({
            zone: [''],
            message: ['']
        });
        this.goBack();
    }
}
