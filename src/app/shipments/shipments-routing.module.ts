import { ShipmentsDefaultComponent } from './components/shipments-default/shipments-default.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';
import { ShipmentForwardComponent } from './components/shipment-forward/shipment-forward.component';

const routes: Routes = [
  { 
      path: '', 
      component: ShipmentsDefaultComponent,
  },
  { 
    path: 'shipment-forward/:SiteId',
    component: ShipmentForwardComponent,
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})

export class ShipmentsRoutingModule { }
