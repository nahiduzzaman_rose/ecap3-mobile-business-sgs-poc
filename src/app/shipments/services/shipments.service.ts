import { SqlQueryBuilderService } from '@ecap3/tns-core/service/query-builder/sql-query-builder.service';
import { PlatformDataService } from '@ecap3/tns-core/service/pds/platform-data.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {BusinessService} from "~/app/shared-data/service/business.service";
import { BehaviorSubject, Observable } from 'rxjs';
import { GqlQueryBuilderService } from '@ecap3/tns-core/service/query-builder/gql-query-builder.service';
import { GraphqlDataService } from '@ecap3/tns-platform-graphql';

@Injectable({
    providedIn: 'root'
})
export class ShipmentsService {
    private url = ' https://my-json-server.typicode.com/Rashid46/demo/shipments';
    private siteId = new BehaviorSubject('');
    currentSiteId = this.siteId.asObservable();
    constructor(
        private http: HttpClient,
        private platformDataService: PlatformDataService,
        private sqlQueryBuilderService: SqlQueryBuilderService,
        private businessService : BusinessService,
        private graphqlDataService : GraphqlDataService,
        private gqlQueryBuilderService : GqlQueryBuilderService
    ) { }

    changeSiteId(siteId: string) {
        this.siteId.next(siteId);
    }

    getShipments(siteId: string, unloadingZoneId: string , pageNumber?: number): Observable<any> {
        const filters = [];
        const  orderBy = 'ScheduleStartDateTime: -1';
        if(unloadingZoneId){ 
            filters.push({
                operator: "raw",
                value: `'SiteId':'${siteId}','DestinationId':'${unloadingZoneId}','Tags': { $in: [ 'Is-Valid-Shipment' ] },$or:[{ Status: 0 }, { Status: 1 }, { Status: 2 }, { Status: 3 }, { Status: 4 }, { Status: 8 }]` 
            });
        }else{
            filters.push({
                operator: "raw",
                value: `'SiteId':'${siteId}','Tags': { $in: [ 'Is-Valid-Shipment' ] },$or:[{ Status: 0 }, { Status: 1 }, { Status: 2 }, { Status: 3 }, { Status: 4 }, { Status: 8 }]`
            });
        }

        const fields = ['ItemId', 'ShipmentNumber', 'Slot', 'UnloadingZoneName', 'SiteName', 'OrganizationName', 'Status','VehicleType',
        'SiteId','UnloadingZoneId','ScheduleStartDateTime','ScheduleEndDateTime', {ShipmentEquipments: ['ItemId', 'NameEn']}];
      
        return this.graphqlDataService.watchQuery<any>({
            query: this.gqlQueryBuilderService.prepareQuery('Shipment', fields, filters, orderBy , pageNumber, 100),
        })
            .valueChanges
            .pipe(
                map(result => {
                    if (result.data && result.data.Shipments && result.data.Shipments.Data) {
                        return result.data.Shipments;
                    }
                    return null;
                })
            );
    }

    /* getShipmentById(shipmentId: string, filterQuery?: any) {
        console.log("Running!!!!!!!!!");
        const filters = [];

        filters.push({
            property: 'Tags',
            operator: 'contains',
            value: "Is-Valid-Shipment"
        });

        filters.push({
            property: 'ItemId',
            operator: '=',
            value: shipmentId
        });

        // const fields = ['ItemId','SiteId','ShipmentNumber','SiteAddress', 'Slot', 'OrganizationName',
        //     'SiteName', 'OrgResponsiblePersonName', 'OrgResponsiblePersonPhone', 'DriverName', 'DriverPhone', 'Status',
        //     'ShipmentEquipments', 'VehicleType', 'ScheduleEndDateTime', 'ScheduleStartDateTime', 'ShipmentMaterials', 'Notes',
        //     'RoutingStatus','DestinationLat','DestinationLng','DestinationId','DestinationName','DestinationType',
        //     'UnloadingZoneName'];

        const fields = ['ItemId','SiteId','ShipmentNumber','SiteAddress', 'Slot', 'OrganizationName',
            'SiteName', 'OrgResponsiblePersonName', 'OrgResponsiblePersonPhone', 'DriverName', 'DriverPhone', 'Status', 
            'VehicleType', 'ScheduleEndDateTime', 'ScheduleStartDateTime', 'Notes', 
            'RoutingStatus','DestinationLat','DestinationLng','DestinationId','DestinationName','DestinationType',
            'UnloadingZoneName'];
        const query = this.sqlQueryBuilderService.prepareQuery("Shipment", fields, filters,
            'CreateDate' + ' __' + 'asc', 0, 0);

        return this.platformDataService.getBySqlFilter('Shipment', query).pipe(map((response: Response) => {
            return response;
        }));
    } */

    //GQL call uncomment when ready
    getShipmentById(shipmentId: string, filterQuery?: any) {
        const  orderBy = 'CreateDate: 1';
        const  pageNumber = 0;
        const filters = [];

        filters.push({
            operator: "raw",
            value: `'_id':'${shipmentId}','Tags': { $in: [ 'Is-Valid-Shipment' ] }` 
        });
        console.log('a!');
        debugger

        //'ShipmentMaterials' must be included in future
        const fields = ['ItemId', 'SiteId', 'ShipmentNumber', 'SiteAddress', 'Slot', 'OrganizationName',
            'SiteName', 'OrgResponsiblePersonName', 'OrgResponsiblePersonPhone', 'DriverName', 'DriverPhone', 'Status',
            {ShipmentEquipments: ['ItemId', 'NameEn']}, {ShipmentMaterials:['ShipmentId','MaterialName','MaterialId','Quantity','Unit']},
            {ShipmentFiles:['FileId','FileName','FileSize', 'FileType', {Thumbnails:['FileId','Height','Width','Tag','Dimension']}]}, 
            'VehicleType', 'ScheduleEndDateTime', 'ScheduleStartDateTime', 'Notes',
            'RoutingStatus','DestinationLat','DestinationLng','DestinationId','DestinationName','DestinationType',
            'UnloadingZoneName'];
            
        return this.graphqlDataService.watchQuery<any>({
            query: this.gqlQueryBuilderService.prepareQuery('Shipment', fields, filters, orderBy , pageNumber, 100),
        })
            .valueChanges
            .pipe(
                map(result => {
                    debugger
                    if (result.data && result.data.Shipments && result.data.Shipments.Data.length) {
                        return result.data.Shipments;
                    }
                    return null;
                })
            );
    }

    forwardShipment(forwardShipmentData): Observable<Response> {
        const eventData = {
            EventType: 'Shipment.Forwarded'
        }
        return this.platformDataService.update('Shipment', forwardShipmentData, eventData).pipe(map((response: Response) => {
            return response;
        }));
    }

    getSiteLocations(siteId) {
        return this.businessService.getSiteLocations(siteId).pipe(map((response) => {
            return response["body"];
        }));
    }
}
