import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TaskAnswerComponent } from './components/task-answer/task-answer.component';
import { TaskUpdateAnswerComponent } from './components/task-update-answer/task-update-answer.component';


const routes: Routes = [
    { 
        path: '', 
        redirectTo: 'task-answer', 
        pathMatch: 'full' 
    },
    { 
        path: 'task-answer', 
        component: TaskAnswerComponent
    },
    { 
        path: 'task-update-answer', 
        component: TaskUpdateAnswerComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class TaskAnswerRoutingModule { }
