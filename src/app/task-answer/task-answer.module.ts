import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskAnswerComponent } from './components/task-answer/task-answer.component';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { PlatformCardModule } from '@ecap3/tns-platform-card';
import { TaskAnswerRoutingModule } from './task-answer-routing.module';
import { TNSCheckBoxModule } from '@nstudio/nativescript-checkbox/angular';
import { NativeScriptUIAutoCompleteTextViewModule } from 'nativescript-ui-autocomplete/angular/autocomplete-directives';
import { PlatformCkeditorModule } from '@ecap3/tns-platform-ckeditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NativeScriptFormsModule } from 'nativescript-angular';
import { TaskUpdateAnswerComponent } from './components/task-update-answer/task-update-answer.component';

@NgModule({
    declarations: [TaskAnswerComponent, TaskUpdateAnswerComponent],
    imports: [
        NativeScriptCommonModule,
        TaskAnswerRoutingModule,
        CoreTemplateModule,
        PlatformCardModule,
        TNSCheckBoxModule,
        NativeScriptUIAutoCompleteTextViewModule,
        PlatformCkeditorModule,
        FormsModule,
        ReactiveFormsModule,
        NativeScriptFormsModule 
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class TaskAnswerModule { }
