import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TaskAnswerService {

    constructor(
        private localPlatformDataService : LocalPlatformDataService
    ) { }
    range(start, stop, step) {
        if (typeof stop == 'undefined') {
            stop = start;
            start = 0;
        };
        if (typeof step == 'undefined') {
            step = 1;
        };
        let result = [];
        for (let i = start; step > 0 ? i <= stop : i > stop; i += step) {
            result.push(i);
        };
        return result;
    };

    saveUpdatedAnswer(id, model): Observable<any> {
        return this.localPlatformDataService.update(id, model).pipe(map(res => {
            return res;
        }));
    }
}
