import { Component, OnInit, ViewChild } from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { RouterExtensions } from 'nativescript-angular/router';
import { ItemEventData } from 'tns-core-modules/ui/list-view/list-view';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { RadAutoCompleteTextViewComponent } from "nativescript-ui-autocomplete/angular";
import { AutoCompleteSuggestMode, AutoCompleteCompletionMode, AutoCompleteDisplayMode, TokenModel } from 'nativescript-ui-autocomplete';
import { TaskAnswerService } from '../../services/task-answer.service';
import {OrientationChangedEventData} from "application"
import * as app from "application";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskDetailsService } from '~/app/task-details/services/task-details.service';
import { ActivatedRoute } from '@angular/router';
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';


@Component({
    selector: 'ns-task-answer',
    templateUrl: './task-answer.component.html',
    styleUrls: ['./task-answer.component.scss'],
    moduleId: module.id
})

export class TaskAnswerComponent implements OnInit {
    public device: string;
    public currentOrientation: string;
    filterType = null;
    filterTypes = {
        Planned: SCConfig.TaskStatus.PENDING.Key,
        CarriedOut: SCConfig.TaskStatus.COMPLETED.Key,
    };
    tasks = ['1','2'];    
    actionBarConfiguration : any = '';
    radioAnswerOptions: any[];
    radioSelectedOption: any;

    answerForm: FormGroup

    private _items: ObservableArray<TokenModel>

    private countries = [{
        name: "Australia",
        image: "res://icon"
    },
    {
        name: "Australia",
        image: "res://icon"
    }];
    bottomActionBarHeight: string;
    currentTaskId: string;
    currentTask: any;
    linearScaleQuestion: any;
    LinearAnswer: any;
    circles: any[];
    backbuttonConfig: BackbuttonConfig;

    constructor(
        private routerExtensions: RouterExtensions,
        private taskAnswerService: TaskAnswerService,
        private fb: FormBuilder,
        private taskDetailsService: TaskDetailsService,
        private activatedRoute: ActivatedRoute
    ) {
        this.initDataItems();
        app.on(app.orientationChangedEvent, (args: OrientationChangedEventData) => {
            console.log('currentOrientation ==>', args.newValue);
            if (args.newValue == 'portrait') {
                this.bottomActionBarHeight = '350';
                this.currentOrientation = 'portrait';
            } else {
                this.bottomActionBarHeight = '350';
                this.currentOrientation = 'landscape';
            }
        })
    }

    @ViewChild("autocomplete", { static: false }) autocomplete: RadAutoCompleteTextViewComponent;
    

    actionItemClicked(event: any) {
        if (event.actionItemType === 'sync') {
            this.routerExtensions.navigate(["/sync", "audit-list", false], {transition: {name: "fade"}, clearHistory: true});
        }
    }

    toggleFilter(filterType: string) {
        console.log(filterType);
        this.filterType = filterType;
    }

    onItemTap(args: ItemEventData) {
        console.log(`Index: ${args.index}; View: ${args.view}`);
        this.routerExtensions.navigate(['tasklist','1']);
    }

    onChangeRadioOptions(radioOption, radioAnswerOptions, formItem){
        formItem.setValue(radioOption.value);
        console.log('Selected radioOption !',radioOption);
        radioOption.selected = !radioOption.selected;

        if (!radioOption.selected) {
            return;
        }

        // uncheck all other options
        radioAnswerOptions.forEach(option => {
            if (option.text !== radioOption.text) {
                option.selected = false;
            }
        });

        this.radioSelectedOption = radioOption;
    }

    generateRadioOptions(linearScaleQuestion: any) {
        let answerPercentages = this.taskAnswerService.range(linearScaleQuestion.From, linearScaleQuestion.To, linearScaleQuestion.Step);
        this.radioAnswerOptions = [];
        answerPercentages.forEach((percentage) => {
            console.log('percentage',percentage)
            this.radioAnswerOptions.push({
                text: percentage +'%',
                value: percentage,
                selected: false
            })
        });
    }

    /** Autocomplete start */
    get dataItems(): ObservableArray<TokenModel> {
        return this._items;
    }

    private initDataItems() {
        this._items = new ObservableArray<TokenModel>();

        for (let i = 0; i < this.countries.length; i++) {
            this._items.push(new TokenModel(this.countries[i].name, this.countries[i].image));
        }
    }

    public onDidAutoComplete(args) {
        console.log("DidAutoComplete with text: " + args.text);
    }

    public onTokenAdded(args) {
        console.log("token added event: " + args.token.text);
    }

    public onTokenRemoved(args) {
        console.log("token remove event: " + args.token.text);
    }

    public onSuggestSelected(args) {
        console.log(this.autocomplete.autoCompleteTextView.text);
        this.autocomplete.autoCompleteTextView.suggestMode = AutoCompleteSuggestMode.Suggest;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onAppendSelected(args) {
        this.autocomplete.autoCompleteTextView.suggestMode = AutoCompleteSuggestMode.Append;
        this.autocomplete.autoCompleteTextView.completionMode = AutoCompleteCompletionMode.StartsWith;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onSuggestAppendSelected(args) {
        this.autocomplete.autoCompleteTextView.suggestMode = AutoCompleteSuggestMode.SuggestAppend;
        this.autocomplete.autoCompleteTextView.completionMode = AutoCompleteCompletionMode.StartsWith;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onStartsWithSelected(args) {
        this.autocomplete.autoCompleteTextView.completionMode = AutoCompleteCompletionMode.StartsWith;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onContainsSelected(args) {
        this.autocomplete.autoCompleteTextView.completionMode = AutoCompleteCompletionMode.Contains;
        this.autocomplete.autoCompleteTextView.suggestMode = AutoCompleteSuggestMode.Suggest;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onPlainSelected(args) {
        this.autocomplete.autoCompleteTextView.displayMode = AutoCompleteDisplayMode.Plain;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    public onTokensSelected(args) {
        this.autocomplete.autoCompleteTextView.displayMode = AutoCompleteDisplayMode.Tokens;
        this.autocomplete.autoCompleteTextView.resetAutoComplete();
    }

    /** Autocomplete end */

    onCkeditorValueChange($event, formItem) {
        console.log('event', $event.message);
        formItem.setValue($event.message);
    }

    submitAnswer(){
        this.currentTask.LinearAnswer = this.answerForm.value.LinearAnswer;
        this.currentTask.TaskSchedule.TaskPercentage = this.answerForm.value.LinearAnswer;
        this.currentTask.Remarks = this.answerForm.value.Remarks;
        this.currentTask.TaskSchedule.IsCompleted = true;
        this.currentTask.LastUpdateDate = new Date().toISOString();
        this.taskAnswerService.saveUpdatedAnswer(this.currentTask.ItemId, this.currentTask).subscribe((response)=>{
            this.routerExtensions.navigate(['task-details', this.currentTaskId],{
                transition: {
                    name: "slide"
                }
            });
        });
    }

    cancelAnswer(){
        this.routerExtensions.navigate(['task-details', this.currentTaskId],{
            transition: {
                name: "slide"
            }
        });
    }

    initForm(data?) {
        this.answerForm = this.fb.group({
            LinearAnswer: [data && data.LinearAnswer, [Validators.required]],
            Remarks: [data && data.Remarks || '', [Validators.required]]
        });
    }
    
    getTaskById(taskId: string) {
        this.currentTask = this.taskDetailsService.getTaskById(taskId);
        this.linearScaleQuestion = this.currentTask.Task.LinearScaleQuestion;
        this.LinearAnswer = this.currentTask.LinearAnswer;
        this.generateRadioOptions(this.linearScaleQuestion);
    }

    goBack(taskId) {
        this.backbuttonConfig = {
            path : '/task-details/'+ taskId
        };
    }

    ngOnInit() {
        this.initForm();

        this.currentTaskId = this.activatedRoute.snapshot.params['TaskId'];
        this.getTaskById(this.currentTaskId);

        this.filterType = SCConfig.TaskStatusDefault;
        this.goBack(this.currentTaskId);
    }
}
