"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var dashboard_routing_module_1 = require("./dashboard-routing.module");
var default_page_component_1 = require("./components/default-page/default-page.component");
var tns_core_template_1 = require("@ecap3/tns-core-template");
var angular_1 = require("nativescript-localize/angular");
var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                dashboard_routing_module_1.DashboardRoutingModule,
                tns_core_template_1.CoreTemplateModule,
                angular_1.NativeScriptLocalizeModule
            ],
            declarations: [
                default_page_component_1.DefaultPageComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhc2hib2FyZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0Qsc0RBQXVFO0FBRXZFLHVFQUFvRTtBQUNwRSwyRkFBd0Y7QUFDeEYsOERBQThEO0FBQzlELHlEQUEyRTtBQWdCM0U7SUFBQTtJQUErQixDQUFDO0lBQW5CLGVBQWU7UUFkM0IsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLGlDQUF3QjtnQkFDeEIsaURBQXNCO2dCQUN0QixzQ0FBa0I7Z0JBQ2xCLG9DQUEwQjthQUM3QjtZQUNELFlBQVksRUFBRTtnQkFDViw2Q0FBb0I7YUFDdkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsdUJBQWdCO2FBQ25CO1NBQ0osQ0FBQztPQUNXLGVBQWUsQ0FBSTtJQUFELHNCQUFDO0NBQUEsQUFBaEMsSUFBZ0M7QUFBbkIsMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcclxuXHJcbmltcG9ydCB7IERhc2hib2FyZFJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9kYXNoYm9hcmQtcm91dGluZy5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRGVmYXVsdFBhZ2VDb21wb25lbnQgfSBmcm9tIFwiLi9jb21wb25lbnRzL2RlZmF1bHQtcGFnZS9kZWZhdWx0LXBhZ2UuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENvcmVUZW1wbGF0ZU1vZHVsZSB9IGZyb20gXCJAZWNhcDMvdG5zLWNvcmUtdGVtcGxhdGVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TG9jYWxpemVNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2FsaXplL2FuZ3VsYXJcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgICAgIERhc2hib2FyZFJvdXRpbmdNb2R1bGUsXHJcbiAgICAgICAgQ29yZVRlbXBsYXRlTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdExvY2FsaXplTW9kdWxlIFxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIERlZmF1bHRQYWdlQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgc2NoZW1hczogW1xyXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIERhc2hib2FyZE1vZHVsZSB7IH1cclxuIl19