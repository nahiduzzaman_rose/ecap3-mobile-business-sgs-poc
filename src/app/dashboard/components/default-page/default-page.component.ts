import { Component, OnInit, ViewContainerRef, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { localize } from "nativescript-localize";
import { Couchbase, QueryLogicalOperator } from 'nativescript-couchbase-plugin';

import {
    LoadEventData,
    WebViewExt,
    ShouldOverideUrlLoadEventData,
    LoadFinishedEventData,
    isAndroid
} from "@nota/nativescript-webview-ext";
import { LoadingIndicator } from '@nstudio/nativescript-loading-indicator';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { ActionBarConfiguration, ActionItemType, ActionType } from '@ecap3/tns-core/model/actionBar.interface';
import { ModalDialogOptions, ModalDialogService } from 'nativescript-angular/modal-dialog';
import { ModalContentComponent } from '@ecap3/tns-platform-dialog/components/modal-content/modal-content.component';
import * as app from "tns-core-modules/application/application";
import { RadSideDrawer, DrawerTransitionBase, SlideInOnTopTransition } from 'nativescript-ui-sidedrawer';
import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular';
import { DashboardService } from '../../services/dashboard.service';

@Component({
    selector: 'ns-default-page',
    templateUrl: './default-page.component.html',
    styleUrls: ['./default-page.component.css'],
    moduleId: module.id
})
export class DefaultPageComponent implements OnInit {
    webview: Array<WebViewExt> = [];
    gotMessageData: any = null;
    isWebViewLoading = true;
    private indicator: LoadingIndicator;
    public flagforuploadbutton: boolean;
    public flag: boolean;
    public filePath: any = [];

    public configuration: any = {
        fileDirectoryName:"tns-audio"
    }
    @ViewChild('modalContainer',{static:false}) modalContainer : TemplateRef<any>;
    @ViewChild(RadSideDrawerComponent, {static:false}) public drawerComponent: RadSideDrawerComponent;
    private drawer: RadSideDrawer;
    private _sideDrawerTransition: DrawerTransitionBase;

    actionBarConfiguration: ActionBarConfiguration = {
        actionItems: [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                androidActionItemPosition: ActionItemType.ACTIONBAR,
                iosActionItemPosition: ActionItemType.IOS_LEFT,
                actionItemText: 'Menu 1',
                url: null,
                callParentMethod: false,
                type: ActionType.MENU
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                androidActionItemPosition: ActionItemType.ACTIONBAR,
                iosActionItemPosition: ActionItemType.IOS_LEFT,
                actionItemText: 'Menu 2',
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                androidActionItemPosition: ActionItemType.POPUP,
                iosActionItemPosition: ActionItemType.IOS_RIGHT,
                actionItemText: 'Log Out',
                url: null,
                callParentMethod: false,
                type: ActionType.LOGOUT
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                androidActionItemPosition: ActionItemType.POPUP,
                iosActionItemPosition: ActionItemType.IOS_RIGHT,
                actionItemText: 'Go to Project',
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                androidActionItemPosition: ActionItemType.ACTIONBAR,
                actionItemText: 'Menu 5',
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                androidActionItemPosition: ActionItemType.ACTIONBAR,
                actionItemText: 'Menu 6',
                url: null,
                callParentMethod: false
            }
        ]
    };
    public audioData: any[];


    constructor(private router: Router, 
        private localPlatformDataService: LocalPlatformDataService,
        private dashboardService: DashboardService,
        private modalService: ModalDialogService,
        private viewContainerRef: ViewContainerRef) {

    }

    flagreceived(flagfromrecorder: boolean) {
        console.log("flag from recorder::", flagfromrecorder);
        this.flagforuploadbutton = flagfromrecorder;
        this.flag = !flagfromrecorder;
    }

    public received(audioFilePath: string) {
        console.log("recorded path found in manager::", audioFilePath);
        this.filePath.push(audioFilePath);
        console.log(this.filePath);
    }

    actionItemClicked(event: any) {
        console.log('dashboard event', event);
    }

    insert() {
        this.localPlatformDataService.insert({
            "id": "8ffcc386-a9d7-4fce-96ed-1eff24e481c8",
            "ItemId": "8ffcc386-a9d7-4fce-96ed-1eff24e481c8",
            "AuditName": "Amberg Group 2019 I-03",
            "ClientName": "Amberg Group",
            "ClientId": "9e8b7803-f7a2-4871-8e5b-4b5d3ff44c29",
            "ClientSiteName": null,
            "ClientSiteId": null,
            "ClientSiteFileId": null,
            "ClientSiteAddress": null,
            "StartDate": "2019-07-08T22:00:00Z",
            "EndDate": "2019-07-08T22:00:00Z",
            "TotalObservation": 0,
            "Status": "completed",
            "RegionName": null,
            "RegionKey": null,
            "SequenceNumber": 0,
            "TotalPreviousObservation": 0,
            "TotalObservationOverdue": 0,
            "IsArchived": false,
            "ProfileQuestionSetIds": null,
            "TypeOfAuditName": "Initial inspection",
            "TypeOfAuditKey": "initial_inspection",
            "SubStatus": "doc prepared",
            "Auditor": {
                "ItemId": "4f335b48-a56f-46f5-aa44-eed7870c9499",
                "CreatedBy": null,
                "FirstName": "Lina",
                "LastName": "Wyss",
                "MiddleName": null,
                "DisplayName": "Lina Wyss",
                "Email": "sccmp04@yopmail.com",
                "PhoneNumber": null,
                "ProfileImageId": null,
                "Title": null,
                "Designation": null,
                "Skills": null,
                "Active": false,
                "IsPrimaryContact": false,
                "LocationCoordinates": null
            },
            "AuditScheduleVisit": [
                {
                    "ItemId": "69108cdb-e590-45b8-8cb1-2a2cbf93a1a9",
                    "AuditVisitDate": "1970-01-01T00:00:00Z",
                    "Itineraries": [
                        {
                            "ItemId": "dee5bf04-34c7-4693-9c71-71cc982f475e",
                            "StartDateTime": "1970-01-01T07:00:00Z",
                            "EndDateTime": "1970-01-01T08:00:00Z",
                            "Location": "",
                            "Title": "Topic according to checklist ",
                            "ShortDescription": "<br>                        -Presentation of those present<br>                        -Scope/goal of the audit<br>                        -Procedure, procedure<br>                        -Answering questions<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "9b177597-72ec-45da-9862-c747589bed3c",
                            "StartDateTime": "1970-01-01T08:00:00Z",
                            "EndDateTime": "1970-01-01T09:00:00Z",
                            "Location": "",
                            "Title": "Remedy of the requirements of the last rule monitoring",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "2b39c66a-fdcb-47c4-848a-72add275cf21",
                            "StartDateTime": "1970-01-01T09:00:00Z",
                            "EndDateTime": "1970-01-01T10:00:00Z",
                            "Location": "",
                            "Title": "factory inspection",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "1bcea299-e7fa-450d-b991-134196d98fef",
                            "StartDateTime": "1970-01-01T10:00:00Z",
                            "EndDateTime": "1970-01-01T11:00:00Z",
                            "Location": "",
                            "Title": "surveillance",
                            "ShortDescription": "<br>                        -Self-monitoring<br>                        -External monitoring<br>                        -Test plan<br>                        -Proofs of conformity<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "9dac52dc-f5f5-4537-bbab-f4d8c1a2bfe6",
                            "StartDateTime": "1970-01-01T11:00:00Z",
                            "EndDateTime": "1970-01-01T12:00:00Z",
                            "Location": "",
                            "Title": "Quality documents",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "82504e7f-688f-48b8-a358-ebc7d3a450f0",
                            "StartDateTime": "1970-01-01T12:00:00Z",
                            "EndDateTime": "1970-01-01T13:00:00Z",
                            "Location": "",
                            "Title": "Final discussion preparation",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "12f6c0c2-889a-4314-bd54-30862e81dc93",
                            "StartDateTime": "1970-01-01T13:00:00Z",
                            "EndDateTime": "1970-01-01T14:00:00Z",
                            "Location": "",
                            "Title": "Final Talk",
                            "ShortDescription": "<br>                        -Announcement of the application<br>                        -Discussion of the conditions, determination of the dates for the elimination of the conditions<br>                        -Confidence-building investigations<br>                        -Next control monitoring<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "a5be4d5f-b9e0-4c8b-ba41-37edc8850cb1",
                            "StartDateTime": "1970-01-01T14:00:00Z",
                            "EndDateTime": "1970-01-01T15:00:00Z",
                            "Location": "",
                            "Title": "Conclusion of the audit",
                            "ShortDescription": "",
                            "Participents": "",
                            "Nested": [
                                {
                                    "ItemId": "69108cdb-e590-45b8-8cb1-2a2cbf93a1a9",
                                    "AuditVisitDate": "1970-01-01T00:00:00Z",
                                    "Itineraries": [
                                        {
                                            "ItemId": "dee5bf04-34c7-4693-9c71-71cc982f475e",
                                            "StartDateTime": "1970-01-01T07:00:00Z",
                                            "EndDateTime": "1970-01-01T08:00:00Z",
                                            "Location": "",
                                            "Title": "Topic according to checklist ",
                                            "ShortDescription": "<br>                        -Presentation of those present<br>                        -Scope/goal of the audit<br>                        -Procedure, procedure<br>                        -Answering questions<br>                        ",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "9b177597-72ec-45da-9862-c747589bed3c",
                                            "StartDateTime": "1970-01-01T08:00:00Z",
                                            "EndDateTime": "1970-01-01T09:00:00Z",
                                            "Location": "",
                                            "Title": "Remedy of the requirements of the last rule monitoring",
                                            "ShortDescription": "",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "2b39c66a-fdcb-47c4-848a-72add275cf21",
                                            "StartDateTime": "1970-01-01T09:00:00Z",
                                            "EndDateTime": "1970-01-01T10:00:00Z",
                                            "Location": "",
                                            "Title": "factory inspection",
                                            "ShortDescription": "",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "1bcea299-e7fa-450d-b991-134196d98fef",
                                            "StartDateTime": "1970-01-01T10:00:00Z",
                                            "EndDateTime": "1970-01-01T11:00:00Z",
                                            "Location": "",
                                            "Title": "surveillance",
                                            "ShortDescription": "<br>                        -Self-monitoring<br>                        -External monitoring<br>                        -Test plan<br>                        -Proofs of conformity<br>                        ",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "9dac52dc-f5f5-4537-bbab-f4d8c1a2bfe6",
                                            "StartDateTime": "1970-01-01T11:00:00Z",
                                            "EndDateTime": "1970-01-01T12:00:00Z",
                                            "Location": "",
                                            "Title": "Quality documents",
                                            "ShortDescription": "",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "82504e7f-688f-48b8-a358-ebc7d3a450f0",
                                            "StartDateTime": "1970-01-01T12:00:00Z",
                                            "EndDateTime": "1970-01-01T13:00:00Z",
                                            "Location": "",
                                            "Title": "Final discussion preparation",
                                            "ShortDescription": "",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "12f6c0c2-889a-4314-bd54-30862e81dc93",
                                            "StartDateTime": "1970-01-01T13:00:00Z",
                                            "EndDateTime": "1970-01-01T14:00:00Z",
                                            "Location": "",
                                            "Title": "Final Talk",
                                            "ShortDescription": "<br>                        -Announcement of the application<br>                        -Discussion of the conditions, determination of the dates for the elimination of the conditions<br>                        -Confidence-building investigations<br>                        -Next control monitoring<br>                        ",
                                            "Participents": ""
                                        },
                                        {
                                            "ItemId": "a5be4d5f-b9e0-4c8b-ba41-37edc8850cb1",
                                            "StartDateTime": "1970-01-01T14:00:00Z",
                                            "EndDateTime": "1970-01-01T15:00:00Z",
                                            "Location": "",
                                            "Title": "Conclusion of the audit",
                                            "ShortDescription": "",
                                            "Participents": "",
                                            "Nested2": [
                                                {
                                                    "ItemId": "69108cdb-e590-45b8-8cb1-2a2cbf93a1a9",
                                                    "AuditVisitDate": "1970-01-01T00:00:00Z",
                                                    "Itineraries": [
                                                        {
                                                            "ItemId": "dee5bf04-34c7-4693-9c71-71cc982f475e",
                                                            "StartDateTime": "1970-01-01T07:00:00Z",
                                                            "EndDateTime": "1970-01-01T08:00:00Z",
                                                            "Location": "",
                                                            "Title": "Topic according to checklist ",
                                                            "ShortDescription": "<br>                        -Presentation of those present<br>                        -Scope/goal of the audit<br>                        -Procedure, procedure<br>                        -Answering questions<br>                        ",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "9b177597-72ec-45da-9862-c747589bed3c",
                                                            "StartDateTime": "1970-01-01T08:00:00Z",
                                                            "EndDateTime": "1970-01-01T09:00:00Z",
                                                            "Location": "",
                                                            "Title": "Remedy of the requirements of the last rule monitoring",
                                                            "ShortDescription": "",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "2b39c66a-fdcb-47c4-848a-72add275cf21",
                                                            "StartDateTime": "1970-01-01T09:00:00Z",
                                                            "EndDateTime": "1970-01-01T10:00:00Z",
                                                            "Location": "",
                                                            "Title": "factory inspection",
                                                            "ShortDescription": "",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "1bcea299-e7fa-450d-b991-134196d98fef",
                                                            "StartDateTime": "1970-01-01T10:00:00Z",
                                                            "EndDateTime": "1970-01-01T11:00:00Z",
                                                            "Location": "",
                                                            "Title": "surveillance",
                                                            "ShortDescription": "<br>                        -Self-monitoring<br>                        -External monitoring<br>                        -Test plan<br>                        -Proofs of conformity<br>                        ",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "9dac52dc-f5f5-4537-bbab-f4d8c1a2bfe6",
                                                            "StartDateTime": "1970-01-01T11:00:00Z",
                                                            "EndDateTime": "1970-01-01T12:00:00Z",
                                                            "Location": "",
                                                            "Title": "Quality documents",
                                                            "ShortDescription": "",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "82504e7f-688f-48b8-a358-ebc7d3a450f0",
                                                            "StartDateTime": "1970-01-01T12:00:00Z",
                                                            "EndDateTime": "1970-01-01T13:00:00Z",
                                                            "Location": "",
                                                            "Title": "Final discussion preparation",
                                                            "ShortDescription": "",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "12f6c0c2-889a-4314-bd54-30862e81dc93",
                                                            "StartDateTime": "1970-01-01T13:00:00Z",
                                                            "EndDateTime": "1970-01-01T14:00:00Z",
                                                            "Location": "",
                                                            "Title": "Final Talk",
                                                            "ShortDescription": "<br>                        -Announcement of the application<br>                        -Discussion of the conditions, determination of the dates for the elimination of the conditions<br>                        -Confidence-building investigations<br>                        -Next control monitoring<br>                        ",
                                                            "Participents": ""
                                                        },
                                                        {
                                                            "ItemId": "a5be4d5f-b9e0-4c8b-ba41-37edc8850cb1",
                                                            "StartDateTime": "1970-01-01T14:00:00Z",
                                                            "EndDateTime": "1970-01-01T15:00:00Z",
                                                            "Location": "",
                                                            "Title": "Conclusion of the audit",
                                                            "ShortDescription": "",
                                                            "Participents": ""
                                                        }
                                                    ],
                                                    "IncludeItinerary": true
                                                }
                                            ]
                                        }
                                    ],
                                    "IncludeItinerary": true
                                }
                            ]
                        }
                    ],
                    "IncludeItinerary": true
                }
            ],
            "DocumentsDueDate": "0001-01-01T00:00:00Z",
            "AuditDocument": [
                {
                    "RequestedId": "9d1dd83c-139a-4b59-a738-6bce25cdd686",
                    "RequestedDocumentName": "Manual of the FCP",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "17a95638-4407-4ad2-bf57-ecd7b97e836b",
                    "RequestedDocumentName": "DoP's",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "fecce1f0-cf51-4c51-a2eb-b519ab399780",
                    "RequestedDocumentName": "Proof of conformity of the last period",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "e86f19f6-3b6a-4e34-828d-91ab51b7bf62",
                    "RequestedDocumentName": "Änderungen im Handbuch",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "8c31471b-9515-4363-98be-9b0794053446",
                    "RequestedDocumentName": "Uncategorized",
                    "UploadedDocuments": []
                }
            ],
            "Tags": [
                "Is-A-Audit"
            ],
            "AuditCarriedOutDate": "2019-06-05T00:00:00Z",
            "AuidtReportReviewByPmDate": "2019-06-05T00:00:00Z",
            "AuidtReportReviewByCmDate": "2019-06-05T00:00:00Z",
            "AuditReportApprovedDate": "2019-06-05T00:00:00Z",
            "AuditReportRejectedDate": "0001-01-01T00:00:00Z",
            "IsDocumentUploaded": false,
            "DocumentUploadDueDate": "0001-01-01T00:00:00Z",
            "CreateDate": "2019-06-04T12:47:08.576Z",
            "AllDocumentUploaded": false
        });
        console.log('======my-tutorial last inserted ');
    }

    updateData() {
        let documentId = "5b984104-d890-42c3-b1c1-ded5c4da8c15";

        this.localPlatformDataService.update(documentId, {
            "ItemId": "7ffcc386-a9d7-4fce-96ed-1eff24e481c8",
            "AuditName": "Amberg Group 2019 I-01",
            "ClientName": "Amberg Group",
            "ClientId": "9e8b7803-f7a2-4871-8e5b-4b5d3ff44c29",
            "ClientSiteName": null,
            "ClientSiteId": null,
            "ClientSiteFileId": null,
            "ClientSiteAddress": null,
            "StartDate": "2019-07-08T22:00:00Z",
            "EndDate": "2019-07-08T22:00:00Z",
            "TotalObservation": 0,
            "Status": "completed",
            "RegionName": null,
            "RegionKey": null,
            "SequenceNumber": 0,
            "TotalPreviousObservation": 0,
            "TotalObservationOverdue": 0,
            "IsArchived": false,
            "ProfileQuestionSetIds": null,
            "TypeOfAuditName": "Initial inspection",
            "TypeOfAuditKey": "initial_inspection",
            "SubStatus": "doc prepared",
            "Auditor": {
                "ItemId": "4f335b48-a56f-46f5-aa44-eed7870c9499",
                "CreatedBy": null,
                "FirstName": "Lina",
                "LastName": "Wyss",
                "MiddleName": null,
                "DisplayName": "Lina Wyss",
                "Email": "sccmp04@yopmail.com",
                "PhoneNumber": null,
                "ProfileImageId": null,
                "Title": null,
                "Designation": null,
                "Skills": null,
                "Active": false,
                "IsPrimaryContact": false,
                "LocationCoordinates": null
            },
            "AuditScheduleVisit": [
                {
                    "ItemId": "69108cdb-e590-45b8-8cb1-2a2cbf93a1a9",
                    "AuditVisitDate": "1970-01-01T00:00:00Z",
                    "Itineraries": [
                        {
                            "ItemId": "dee5bf04-34c7-4693-9c71-71cc982f475e",
                            "StartDateTime": "1970-01-01T07:00:00Z",
                            "EndDateTime": "1970-01-01T08:00:00Z",
                            "Location": "",
                            "Title": "Topic according to checklist ",
                            "ShortDescription": "<br>                        -Presentation of those present<br>                        -Scope/goal of the audit<br>                        -Procedure, procedure<br>                        -Answering questions<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "9b177597-72ec-45da-9862-c747589bed3c",
                            "StartDateTime": "1970-01-01T08:00:00Z",
                            "EndDateTime": "1970-01-01T09:00:00Z",
                            "Location": "",
                            "Title": "Remedy of the requirements of the last rule monitoring",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "2b39c66a-fdcb-47c4-848a-72add275cf21",
                            "StartDateTime": "1970-01-01T09:00:00Z",
                            "EndDateTime": "1970-01-01T10:00:00Z",
                            "Location": "",
                            "Title": "factory inspection",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "1bcea299-e7fa-450d-b991-134196d98fef",
                            "StartDateTime": "1970-01-01T10:00:00Z",
                            "EndDateTime": "1970-01-01T11:00:00Z",
                            "Location": "",
                            "Title": "surveillance",
                            "ShortDescription": "<br>                        -Self-monitoring<br>                        -External monitoring<br>                        -Test plan<br>                        -Proofs of conformity<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "9dac52dc-f5f5-4537-bbab-f4d8c1a2bfe6",
                            "StartDateTime": "1970-01-01T11:00:00Z",
                            "EndDateTime": "1970-01-01T12:00:00Z",
                            "Location": "",
                            "Title": "Quality documents",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "82504e7f-688f-48b8-a358-ebc7d3a450f0",
                            "StartDateTime": "1970-01-01T12:00:00Z",
                            "EndDateTime": "1970-01-01T13:00:00Z",
                            "Location": "",
                            "Title": "Final discussion preparation",
                            "ShortDescription": "",
                            "Participents": ""
                        },
                        {
                            "ItemId": "12f6c0c2-889a-4314-bd54-30862e81dc93",
                            "StartDateTime": "1970-01-01T13:00:00Z",
                            "EndDateTime": "1970-01-01T14:00:00Z",
                            "Location": "",
                            "Title": "Final Talk",
                            "ShortDescription": "<br>                        -Announcement of the application<br>                        -Discussion of the conditions, determination of the dates for the elimination of the conditions<br>                        -Confidence-building investigations<br>                        -Next control monitoring<br>                        ",
                            "Participents": ""
                        },
                        {
                            "ItemId": "a5be4d5f-b9e0-4c8b-ba41-37edc8850cb1",
                            "StartDateTime": "1970-01-01T14:00:00Z",
                            "EndDateTime": "1970-01-01T15:00:00Z",
                            "Location": "",
                            "Title": "Conclusion of the audit",
                            "ShortDescription": "",
                            "Participents": ""
                        }
                    ],
                    "IncludeItinerary": true
                }
            ],
            "DocumentsDueDate": "0001-01-01T00:00:00Z",
            "AuditDocument": [
                {
                    "RequestedId": "9d1dd83c-139a-4b59-a738-6bce25cdd686",
                    "RequestedDocumentName": "Manual of the FCP",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "17a95638-4407-4ad2-bf57-ecd7b97e836b",
                    "RequestedDocumentName": "DoP's",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "fecce1f0-cf51-4c51-a2eb-b519ab399780",
                    "RequestedDocumentName": "Proof of conformity of the last period",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "e86f19f6-3b6a-4e34-828d-91ab51b7bf62",
                    "RequestedDocumentName": "Änderungen im Handbuch",
                    "UploadedDocuments": null
                },
                {
                    "RequestedId": "8c31471b-9515-4363-98be-9b0794053446",
                    "RequestedDocumentName": "Uncategorized",
                    "UploadedDocuments": []
                }
            ],
            "Tags": [
                "Is-A-Audit"
            ],
            "AuditCarriedOutDate": "2019-06-05T00:00:00Z",
            "AuidtReportReviewByPmDate": "2019-06-05T00:00:00Z",
            "AuidtReportReviewByCmDate": "2019-06-05T00:00:00Z",
            "AuditReportApprovedDate": "2019-06-05T00:00:00Z",
            "AuditReportRejectedDate": "0001-01-01T00:00:00Z",
            "IsDocumentUploaded": false,
            "DocumentUploadDueDate": "0001-01-01T00:00:00Z",
            "CreateDate": "2019-06-04T12:47:08.576Z",
            "AllDocumentUploaded": false
        });

        console.log('Updated !');

    }

    viewDataById() {
        let documentId = "59b06cf0-fb47-4df6-8d59-c2a06cf22e17";
        const data = this.localPlatformDataService.getById(documentId);
        console.log('tutorial', data);
    }

    viewAllData() {
        const query = {
            select: []
        }
        const results = this.localPlatformDataService.getBySqlFilter(query);

        console.log('results', results);
        console.log('resultsCount', results.length);
    }

    viewQueryData() {
        const query = {
            select: [], // Leave empty to query for all
            where: [ //ignore logical in first element
                {
                    property: 'Tags',
                    comparison: 'in',
                    value: 'Is-A-Audit'
                }/* ,
                { 
                    property: 'tutorial.fname', 
                    comparison: 'equalTo', 
                    value: 'carl',
                    logical: QueryLogicalOperator.AND
                } */
            ]
        }
        const results = this.localPlatformDataService.getBySqlFilter(query);

        console.log('resultsQuery', results);
    }

    dropDataBase() {
        this.localPlatformDataService.dropDataBase();
        console.log('Database dropped !');
    }

    deleteDocument() {
        let documentId = "4e639cbb-f1a4-4783-9454-067fe5425196";
        const isDeleted = this.localPlatformDataService.delete(documentId);
        console.log('deleted', isDeleted);
    }

    onTap() {
        console.log('tapped!!');
        //this.router.navigate(['project']);
    }

    private webviewLoaded(args: LoadEventData, minuteData: string, instance: number) {
        this.webview.push(args.object);

        this.webview[instance].registerLocalResource("ckeditor", "~/webviews/ckef/index.html");

        if (isAndroid) {
            this.webview[instance].src = "~/webviews/ckef/index.html";
        } else {
            this.webview[instance].src = "http://localhost:8080";
        }

        this.webview[instance].on(WebViewExt.shouldOverrideUrlLoadingEvent, (args: ShouldOverideUrlLoadEventData) => {
            console.log(args.url);
            console.log(args.httpMethod);
            if (args.url.indexOf("google.com") !== -1) {
                args.cancel = true;
            }
            // let webview: WebView = this.webView.nativeElement;
            // if(webview.android) {
            //     webview.android.getSettings().setBuiltInZoomControls(false);
            // }
        });


        this.webview[instance].on(WebViewExt.loadFinishedEvent, (args: LoadFinishedEventData) => {
            console.log(`WebViewExt.loadFinishedEvent: ${args.url}`);
            this.isWebViewLoading = false;
            this.webview[instance].executeJavaScript("registerMinuteData('" + minuteData + "')");
            this.sendCurrentMinuteDetailData(minuteData, instance);
            this.indicator.hide();
            // webview.loadStyleSheetFile("local-stylesheet.css", "~/assets/test-data/css/local-stylesheet.css", false);
        });

        this.webview[instance].on("gotMessage", (msg) => {
            //console.log();
            this.gotMessageData = msg.data;
            //this.minuteUpdateForm.patchValue({MinuteDetail: this.gotMessageData["huba"]});
            console.log(`webview.gotMessage: ${minuteData}  ${JSON.stringify(msg.data)} (${typeof msg})`);
        });
    }

    sendCurrentMinuteDetailData(minuteData, instance) {
        this.webview[instance].emitToWebView(minuteData, minuteData);
    }
    

    showRecordModal() {
        /* let options = {
            title: "Multiple Sites",
            message: "Site 1",
            okButtonText: "OK"
        };

        alert(options); */

        /* let confirm = (callback) => {
            callback(true);
            console.log('call confirm !!');

        };

        let cancel = (callback) => {
            callback(true);
            console.log('call cenceled');

        };

        let options: ModalDialogOptions = {
            context: {
                promptMsg: "This is the prompt message!!!",
                template: this.modalContainer,
                okButtonText: "OK",
                cancelButtonText: "",
                functions: {
                    confirm: confirm,
                    cancel: cancel
                },
                isFullscreen: true
            },
            fullscreen: true,
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(ModalContentComponent, options)
            .then((dialogResult: string) => {
                console.log(dialogResult);
            }) */

            this.drawer.showDrawer();
    }

    closeModal(){
        this.drawer.closeDrawer();
        console.log('flagforuploadbutton',this.flagforuploadbutton);
        if(this.flagforuploadbutton){    
            this.filePath.forEach((filePath, index) => {
                let fileName = 'file'+index;
                this.dashboardService.attachAudioInEvent(fileName,filePath);            
            });
        };
    }

    getFiles(){
       let files =  this.dashboardService.getFiles();
       this.audioData = files;
       console.log('files',files.length);
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }


    ngOnInit() {
        // console.log(localize("CLM Logistics"));
        // console.log('dashboard!!!');
        this._sideDrawerTransition = new SlideInOnTopTransition();
    }
    

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
    }

}

