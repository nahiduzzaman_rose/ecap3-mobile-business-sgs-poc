"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var nativescript_localize_1 = require("nativescript-localize");
var DefaultPageComponent = /** @class */ (function () {
    function DefaultPageComponent(router) {
        this.router = router;
        this.actionBarConfiguration = [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 1',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 2',
                visible: true,
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemType: 'popup',
                actionItemText: 'Log Out',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemType: 'popup',
                actionItemText: 'Go to Project',
                visible: true,
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 5',
                visible: true,
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 6',
                visible: true,
                url: null,
                callParentMethod: false
            }
        ];
        console.log('DefaultPageComponent Biz App!!');
    }
    DefaultPageComponent.prototype.actionItemClicked = function (event) {
        console.log('dashboard event', event);
    };
    DefaultPageComponent.prototype.onTap = function () {
        console.log('tapped!!');
        this.router.navigate(['project']);
    };
    DefaultPageComponent.prototype.ngOnInit = function () {
        console.log(nativescript_localize_1.localize("SELISE Smart City"));
        console.log('dashboard!!!');
    };
    DefaultPageComponent = __decorate([
        core_1.Component({
            selector: 'ns-default-page',
            templateUrl: './default-page.component.html',
            styleUrls: ['./default-page.component.css'],
            moduleId: module.id
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], DefaultPageComponent);
    return DefaultPageComponent;
}());
exports.DefaultPageComponent = DefaultPageComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC1wYWdlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlZmF1bHQtcGFnZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsMENBQXlDO0FBQ3pDLCtEQUFpRDtBQVNqRDtJQXVESSw4QkFBb0IsTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7UUFyRGxDLDJCQUFzQixHQUFTO1lBQzNCO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxXQUFXO2dCQUMzQixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsZ0JBQWdCLEVBQUUsS0FBSztnQkFDdkIsSUFBSSxFQUFFLE1BQU07YUFDZjtZQUNEO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxXQUFXO2dCQUMzQixjQUFjLEVBQUUsUUFBUTtnQkFDeEIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsZ0JBQWdCLEVBQUUsS0FBSzthQUMxQjtZQUNEO2dCQUNJLGNBQWMsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxPQUFPO2dCQUN2QixjQUFjLEVBQUUsU0FBUztnQkFDekIsT0FBTyxFQUFFLElBQUk7Z0JBQ2IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsZ0JBQWdCLEVBQUUsS0FBSztnQkFDdkIsSUFBSSxFQUFFLFFBQVE7YUFDakI7WUFDRDtnQkFDSSxjQUFjLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxRCxjQUFjLEVBQUUsT0FBTztnQkFDdkIsY0FBYyxFQUFFLGVBQWU7Z0JBQy9CLE9BQU8sRUFBRSxJQUFJO2dCQUNiLEdBQUcsRUFBQyxVQUFVO2dCQUNkLGdCQUFnQixFQUFFLEtBQUs7YUFDMUI7WUFDRDtnQkFDSSxjQUFjLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxRCxjQUFjLEVBQUUsV0FBVztnQkFDM0IsY0FBYyxFQUFFLFFBQVE7Z0JBQ3hCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLEdBQUcsRUFBRSxJQUFJO2dCQUNULGdCQUFnQixFQUFFLElBQUk7YUFDekI7WUFDRDtnQkFDSSxjQUFjLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxRCxjQUFjLEVBQUUsV0FBVztnQkFDM0IsY0FBYyxFQUFFLFFBQVE7Z0JBQ3hCLE9BQU8sRUFBRSxJQUFJO2dCQUNiLEdBQUcsRUFBRSxJQUFJO2dCQUNULGdCQUFnQixFQUFFLEtBQUs7YUFDMUI7U0FDSixDQUFDO1FBR0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxnREFBaUIsR0FBakIsVUFBa0IsS0FBVTtRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxvQ0FBSyxHQUFMO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUNELHVDQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO1FBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQXRFUSxvQkFBb0I7UUFOaEMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztZQUMzQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDdEIsQ0FBQzt5Q0F3RDhCLGVBQU07T0F2RHpCLG9CQUFvQixDQXdFaEM7SUFBRCwyQkFBQztDQUFBLEFBeEVELElBd0VDO0FBeEVZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IGxvY2FsaXplIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1sb2NhbGl6ZVwiO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICducy1kZWZhdWx0LXBhZ2UnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RlZmF1bHQtcGFnZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kZWZhdWx0LXBhZ2UuY29tcG9uZW50LmNzcyddLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGVmYXVsdFBhZ2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGFjdGlvbkJhckNvbmZpZ3VyYXRpb24gOiBhbnkgPSBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhY3Rpb25JdGVtSWNvbjogU3RyaW5nLmZyb21DaGFyQ29kZShwYXJzZUludCgnMHgnKydmMDM5JykpLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVHlwZTogJ2FjdGlvbkJhcicsXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UZXh0OiAnTWVudSAxJyxcclxuICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgdXJsOiBudWxsLFxyXG4gICAgICAgICAgICBjYWxsUGFyZW50TWV0aG9kOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogJ21lbnUnXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YxZTAnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAnYWN0aW9uQmFyJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdNZW51IDInLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YwNjQnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAncG9wdXAnLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVGV4dDogJ0xvZyBPdXQnLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiAnbG9nb3V0J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhY3Rpb25JdGVtSWNvbjogU3RyaW5nLmZyb21DaGFyQ29kZShwYXJzZUludCgnMHgnKydmMDY0JykpLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVHlwZTogJ3BvcHVwJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdHbyB0byBQcm9qZWN0JyxcclxuICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgdXJsOicvcHJvamVjdCcsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1JY29uOiBTdHJpbmcuZnJvbUNoYXJDb2RlKHBhcnNlSW50KCcweCcrJ2YyNjQnKSksXHJcbiAgICAgICAgICAgIGFjdGlvbkl0ZW1UeXBlOiAnYWN0aW9uQmFyJyxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVRleHQ6ICdNZW51IDUnLFxyXG4gICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB1cmw6IG51bGwsXHJcbiAgICAgICAgICAgIGNhbGxQYXJlbnRNZXRob2Q6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYWN0aW9uSXRlbUljb246IFN0cmluZy5mcm9tQ2hhckNvZGUocGFyc2VJbnQoJzB4JysnZjI2MycpKSxcclxuICAgICAgICAgICAgYWN0aW9uSXRlbVR5cGU6ICdhY3Rpb25CYXInLFxyXG4gICAgICAgICAgICBhY3Rpb25JdGVtVGV4dDogJ01lbnUgNicsXHJcbiAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgIHVybDogbnVsbCxcclxuICAgICAgICAgICAgY2FsbFBhcmVudE1ldGhvZDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICBdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnRGVmYXVsdFBhZ2VDb21wb25lbnQgQml6IEFwcCEhJyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aW9uSXRlbUNsaWNrZWQoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdkYXNoYm9hcmQgZXZlbnQnLGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBvblRhcCgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCd0YXBwZWQhIScpO1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsncHJvamVjdCddKTtcclxuICAgIH1cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGxvY2FsaXplKFwiU0VMSVNFIFNtYXJ0IENpdHlcIikpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdkYXNoYm9hcmQhISEnKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbiJdfQ==