import { Injectable } from '@angular/core';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { QueryWhereItem, Query, QueryLogicalOperator } from 'nativescript-couchbase-plugin';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    constructor(public localPlatformDataService: LocalPlatformDataService) { }

    attachAudioInEvent(fileName, filePath) {
        const payload = {
            ItemId: this.localPlatformDataService.getGuid(),
            Name: fileName,
            EntityName: "File",
            MediaType: "Audio",
            LocalPath: filePath,
            IsFileUploaded: false,
            IsFileDownloaded: true,
            Tags: ['Is-Valid-Audio'],
            MetaData: {
                FileInfo: {
                    Type: "Value",
                    Value: fileName,
                },
                MediaType: {
                    Type: "MediaType",
                    Value: "Audio"
                }
            }
        };
         console.log("FilePayload::", payload);
        return this.localPlatformDataService.insert(payload);
    }

    getFiles() {
        const filter : QueryWhereItem[] = [
            {
               property: 'EntityName',
               comparison: 'equalTo',
               value: "File"
           }/* ,
           {
               logical: QueryLogicalOperator.AND,
               property: 'FilterType',
               comparison: 'equalTo',
               value: filterType,
           } */
       ];

       const query: Query = {
           select: [],
           where: filter
       };
       return this.localPlatformDataService.getBySqlFilter(query);
    }
}
