import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';

import { DefaultPageComponent } from './components/default-page/default-page.component';

const routes: Routes = [
    { path: '', component: DefaultPageComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class DashboardRoutingModule { }
