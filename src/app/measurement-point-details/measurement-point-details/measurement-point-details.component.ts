import { Component, OnInit } from '@angular/core';
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";
import {BackbuttonConfig} from "@ecap3/tns-core/model/actionBar.interface";
import {RouterExtensions} from "nativescript-angular";

@Component({
  selector: 'ns-measurement-point-details',
  templateUrl: './measurement-point-details.component.html',
  styleUrls: ['./measurement-point-details.component.scss'],
  moduleId: module.id,
})
export class MeasurementPointDetailsComponent implements OnInit {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    backbuttonConfig: BackbuttonConfig;
    constructor(
        private routerExtensions: RouterExtensions,
    ) { }

    goBack() {
        this.backbuttonConfig = {
            path : '/measurementPoint'
        };
    }

    onTap(){
        this.routerExtensions.navigate(['measurementPoint-details', 1, 'form'], {
            transition: {
                name: "slide"
            }
        });
    }

    ngOnInit() {
        this.goBack();
    }

}
