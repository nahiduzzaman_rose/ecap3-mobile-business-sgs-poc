import { Component, OnInit } from '@angular/core';
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";
import {BackbuttonConfig} from "@ecap3/tns-core/model/actionBar.interface";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'ns-measurement-point-details-form',
  templateUrl: './measurement-point-details-form.component.html',
  styleUrls: ['./measurement-point-details-form.component.scss'],
  moduleId: module.id,
})
export class MeasurementPointDetailsFormComponent implements OnInit {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    backbuttonConfig: BackbuttonConfig;
    currentMesurementItemId: any;
    measurementToleranceLevel: FormGroup;
    isYesTapped: boolean;
    isNoTapped: boolean;

    constructor(
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder
    ) { }

    goBack() {
        this.backbuttonConfig = {
            path : '/measurementPoint-details/' + this.currentMesurementItemId
        };
    }

    onButtonTap(value:string) {
        if(value === 'yes'){
            console.log('Tapped',value);
            this.isYesTapped = true;
            this.isNoTapped = false;
        } else {
            this.isNoTapped = true;
            this.isYesTapped = false;
        }
    }

    ngOnInit() {
        this.currentMesurementItemId = this.activatedRoute.snapshot.params['ItemId'];
        this.measurementToleranceLevel = this.fb.group({
            toleranceLevel: ['']
        });
        this.goBack();
    }

}
