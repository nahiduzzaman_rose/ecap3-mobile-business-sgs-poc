import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import {MeasurementPointDetailsComponent} from "~/app/measurement-point-details/measurement-point-details/measurement-point-details.component";
import {MeasurementPointDetailsFormComponent} from "~/app/measurement-point-details/measurement-point-details-form/measurement-point-details-form.component";


const routes: Routes = [
    {
        path: '',
        component: MeasurementPointDetailsComponent,
    },
    {
        path: 'form',
        component: MeasurementPointDetailsFormComponent,
    }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MeasurementPointDetailsRoutingModule { }
