import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { MeasurementPointDetailsRoutingModule } from './measurement-point-details-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { MeasurementPointDetailsComponent } from './measurement-point-details/measurement-point-details.component';
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";
import {CoreTemplateModule} from "@ecap3/tns-core-template";
import { MeasurementPointDetailsFormComponent } from './measurement-point-details-form/measurement-point-details-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {NativeScriptFormsModule} from "nativescript-angular";


@NgModule({
  declarations: [MeasurementPointDetailsComponent, MeasurementPointDetailsFormComponent],
    imports: [
        MeasurementPointDetailsRoutingModule,
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        CoreTemplateModule,
        NativeScriptFormsModule,
        ReactiveFormsModule
    ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MeasurementPointDetailsModule { }
