import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { PlatformCardModule } from '@ecap3/tns-platform-card';
import { TaskDetailsRoutingModule } from './task-details-routing.module';
import { SharedDataModule } from '../shared-data/shared-data.module';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';

@NgModule({
    declarations: [TaskDetailsComponent],
    imports: [
        NativeScriptCommonModule,
        TaskDetailsRoutingModule,
        CoreTemplateModule,
        PlatformCardModule,
        NativeScriptUIListViewModule,
        SharedDataModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class TaskDetailsModule { }
