import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { TaskAnswerService } from '~/app/task-answer/services/task-answer.service';
import { ActivatedRoute } from '@angular/router';
import { SeedDataConstant } from '~/app/sc-config/constant/seed-data.constant';
import { TaskDetailsService } from '../../services/task-details.service';
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';
import { device } from 'tns-core-modules/platform/platform';
import { DeviceType } from 'tns-core-modules/ui/enums/enums';

@Component({
    selector: 'ns-task-details',
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.scss'],
    moduleId: module.id
})
export class TaskDetailsComponent implements OnInit {

    actionBarConfiguration : any = '';
    circles: any[];
    currentTask: any;
    currentTaskId: string;
    tasks = [];
    linearScaleQuestion: any;
    LinearAnswer: any;
    isTaskCompleted: boolean;
    backbuttonConfig: BackbuttonConfig;
    spanCount: number;


    constructor(
        private routerExtensions: RouterExtensions,
        private taskAnswerService: TaskAnswerService,
        private taskDetailsService: TaskDetailsService,
        private activatedRoute: ActivatedRoute
    ) { }

    actionItemClicked(event: any) {
        if (event.actionItemType === 'sync') {
            this.routerExtensions.navigate(["/sync", "audit-list", false], {transition: {name: "fade"}, clearHistory: true});
        }
    }

    answerQuestion() {
        this.routerExtensions.navigate(['task-answer', this.currentTaskId],{
            transition: {
                name: "slide"
            }
        });
    }

    editAnswer() {
        this.routerExtensions.navigate(['task-answer', this.currentTaskId, 'task-update-answer'],{
            transition: {
                name: "slide"
            }
        });
    }

    generateRedToGreenColorWithRange(index: number, splitBy: number) {
        const division = 255 / splitBy;
        const color = [255, 255, 0];
        if (splitBy / 2 > index) {
            color[0] = 255;
            color[1] = (index * 2 * division);
        } else {
            color[0] = 255 - (index * division);
        }
        return `rgb(${color[0]},${color[1]},0)`;
    }

    generateLinearScaleQuestion(linearScaleQuestion: any) {
        this.circles = this.taskAnswerService.range(linearScaleQuestion.From, linearScaleQuestion.To, linearScaleQuestion.Step);
    }

    getTaskById(taskId: string) {
        this.currentTask = this.taskDetailsService.getTaskById(taskId);
        this.isTaskCompleted = this.currentTask.TaskSchedule.IsCompleted;
        this.linearScaleQuestion = this.currentTask.Task.LinearScaleQuestion;
        this.LinearAnswer = this.currentTask.LinearAnswer;
        this.generateLinearScaleQuestion(this.linearScaleQuestion);
    }

    
    goBack() {
        this.backbuttonConfig = {
            path : '/tasklist'
        };
    }

    ngOnInit() {
        this.currentTaskId = this.activatedRoute.snapshot.params['TaskId'];
        this.getTaskById(this.currentTaskId);
        this.spanCount = device.deviceType === DeviceType.Tablet ? 2 : 1;
        this.goBack();
    }

}
