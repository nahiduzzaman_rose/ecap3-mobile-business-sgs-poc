import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TaskDetailsComponent } from './components/task-details/task-details.component';


const routes: Routes = [
    { 
        path: '', 
        redirectTo: 'task-details', 
        pathMatch: 'full' 
    },
    { 
        path: 'task-details', 
        component: TaskDetailsComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class TaskDetailsRoutingModule { }
