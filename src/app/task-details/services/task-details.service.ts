import { Injectable } from '@angular/core';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { Observable } from 'rxjs';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { QueryWhereItem, QueryLogicalOperator, Query } from 'nativescript-couchbase-plugin';

@Injectable({
    providedIn: 'root'
})
export class TaskDetailsService {

    constructor(
        private localPlatformDataService : LocalPlatformDataService
    ) { }

    getTaskById(taskId): any {
        let tag = SCConfig.Tags.IsValidPraxisTask;
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.PraxisTask,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'ItemId',
                comparison: 'equalTo',
                value: taskId,
            }
        ];

        const query: Query = {
            select: [],
            where: filter,
            order: [{property: 'CreateDate', direction: "desc"}],
        };

        let results = this.localPlatformDataService.getBySqlFilter(query);

        return results[0];
    }
}
