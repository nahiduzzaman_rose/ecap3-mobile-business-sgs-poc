import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { Routes } from '@angular/router';
import { ShipmentDetailsComponent } from './components/shipment-details/shipment-details.component';
import { ShipmentNotesComponent } from './components/shipment-notes/shipment-notes.component';

const routes: Routes = [
  { 
      path: '', 
      component: ShipmentDetailsComponent,
  },
  { 
    path: 'shipment-notes', 
    component: ShipmentNotesComponent,
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ShipmentDetailsRoutingModule { }
