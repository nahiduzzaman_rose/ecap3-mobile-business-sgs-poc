import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { ShipmentDetailsComponent } from './components/shipment-details/shipment-details.component';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { PlatformCardModule } from '@ecap3/tns-platform-card';
import { ShipmentDetailsRoutingModule } from './shipment-details-routing.module';
import { ModalDialogService, NativeScriptFormsModule } from 'nativescript-angular';
import { PlatformDialogModule } from '@ecap3/tns-platform-dialog/platform-dialog.module';
import { ShipmentNotesComponent } from './components/shipment-notes/shipment-notes.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptLocalizeModule } from 'nativescript-localize/angular';
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular';
import { FormRecordingImageComponent } from './components/form-recording-image/form-recording-image.component';
import { PlatformImageModule } from '@ecap3/tns-platform-image';

@NgModule({
  declarations: [ShipmentDetailsComponent, ShipmentNotesComponent, FormRecordingImageComponent],
  imports: [
    NativeScriptCommonModule,
    CoreTemplateModule,
    PlatformCardModule,
    ShipmentDetailsRoutingModule,
    PlatformDialogModule,
    NativeScriptFormsModule,
    ReactiveFormsModule,
    NativeScriptLocalizeModule,
    NativeScriptUISideDrawerModule,
    PlatformImageModule
  ],
  providers: [ModalDialogService],
  schemas: [NO_ERRORS_SCHEMA]
})

export class ShipmentDetailsModule { }
