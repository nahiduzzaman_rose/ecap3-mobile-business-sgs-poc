import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RouterExtensions } from 'nativescript-angular';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';
import {ShipmentDetailsService} from "~/app/shipment-details/services/shipment-details.service";
import {ShipmentsService} from "~/app/shipments/services/shipments.service";
import {SnackBarService} from "@ecap3/tns-core";

@Component({
    selector: 'ns-shipment-notes',
    templateUrl: './shipment-notes.component.html',
    styleUrls: ['./shipment-notes.component.scss'],
    moduleId: module.id,
})
export class ShipmentNotesComponent implements OnInit {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    shipmentNotes: FormGroup;
    currentShipmentId: any;
    backbuttonConfig: BackbuttonConfig;
    currentSiteId: string;
    currentShipmentNumber: any;
    currentShipment: any = [];
    note: string;
    _id : any;
    textLength: number;
    isUpdated: boolean = false;
    
    constructor(
        private fb: FormBuilder,
        private routerExtensions: RouterExtensions,
        private shipmentDetailsService: ShipmentDetailsService,
        private shipmentService : ShipmentsService,
        private activatedRoute: ActivatedRoute,
        private snackBarService: SnackBarService
    ) { }

    onSave() {
        this._id = this.currentShipmentId;
        this.note = this.shipmentNotes.value["note"];
        let newNotes={
            "ItemId" :this._id,
            "Notes" : this.note
        }
        this.shipmentDetailsService.updateNote(newNotes).subscribe(response => {
            console.log("update",response);
            if(this.isUpdated) {
                this.snackBarService.show('Note has been updated successfully.', 'white');
            }
            else {
                this.snackBarService.show('Note has been added successfully.', 'white');
            }
            this.routerExtensions.navigate(['shipment-details', this.currentShipmentId, this.currentSiteId], {
                transition: {
                    name: "slide"
                }
            });
        });
    }

    onCancel() {
        this.routerExtensions.navigate(['shipment-details', this.currentShipmentId, this.currentSiteId], {
            transition: {
                name: "slide"
            }
        });
    }

    onTextChange(event) {
        this.textLength = event.value.length;
    }

    goBack() {
        debugger;
        this.backbuttonConfig = {
            path : '/shipment-details/'+ this.currentShipmentId + '/' + this.currentSiteId
        };
    }
    getShipment() {
        this.shipmentService.getShipmentById(this.currentShipmentId).subscribe(response  => {
            this.currentShipment = response['Data'][0];
            this.currentShipmentNumber = this.currentShipment.ShipmentNumber;
            this.shipmentNotes.get('note').patchValue(this.currentShipment.Notes);
            if(this.currentShipment.Notes !== null) {
                this.textLength = this.currentShipment.Notes.length;
                this.isUpdated = true;
            }
        });
    }

    ngOnInit() {
        this.textLength = 0;
        this.currentShipmentId = this.activatedRoute.snapshot.params['ItemId'];
        this.currentSiteId = this.activatedRoute.snapshot.params['SiteId'];
        this.shipmentNotes = this.fb.group({
            note: ['']
        });
        this.getShipment();
        this.goBack();
    }

}
// export interface Notes {
//     _id: any;
//     Notes: string;
// }
