import {
    AfterViewInit,
    ChangeDetectionStrategy, ChangeDetectorRef,
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
    Output,
    TemplateRef,
    ViewChild,
    Input
} from '@angular/core';
import { ImageControlPanel, ImagePickerPayload } from "@ecap3/tns-platform-image/types";
import localize from 'nativescript-localize';


@Component({
    selector: 'tns-form-recording-image',
    templateUrl: './form-recording-image.component.html',
    /*styleUrls: ['./form-recording-image.component.scss'],*/
    moduleId: module.id,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormRecordingImageComponent implements OnInit, AfterViewInit, OnDestroy {
    @Output()
    onImageSelected: EventEmitter<ImagePickerPayload> = new EventEmitter();
    @Output()
    onCloseEvent: EventEmitter<any> = new EventEmitter();

    @Input()
    imagePickerTemplate: TemplateRef<any>;

    imageManagerConfig: ImageControlPanel;
    public selectedIndex: number = null;
    isForwardActive: boolean = false;
    isBackwardActive: boolean = false;
    screenRationWidth: any;
    screenRationHeight: any;
    isTabWidthIsNineSixty: any;
    saveButtonToggle: (status: boolean) => {};
    @ViewChild("filterTemplate", {static: false}) filterTemplate: TemplateRef<any>;
    @ViewChild("imageDetailsDataCapture", {static: false}) imageDetailsCaptureTemplate: TemplateRef<any>;

    constructor(
        private cdrf: ChangeDetectorRef,
    ) {
    }


    ngOnInit() {
        /*this.screenRationWidth=(screen.mainScreen.widthDIPs/1280);
        this.screenRationHeight=(screen.mainScreen.heightDIPs/800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }*/

        this.isForwardActive = false;
        this.isBackwardActive = false;
        this.selectedIndex = null;
        this.imageManagerConfig = {
            title: localize("tns.photo"),
            hideGallery: true,
            hideImagePicker: false,
            hideMultiSelectToggle: true,
            hideGalleryFilter: true,
            galleryFilterTemplate: this.filterTemplate,
            imageDetailsCaptureTemplate: this.imageDetailsCaptureTemplate
        };
    }



    ngAfterViewInit() {

    }

    received(filePath: string, type) {
        console.log("file path found in manager::", filePath);
    }

    imageCaptured(data) {
        console.log("event camera Image Captured", data);
    }

    imageUploaded(data) {
        console.log("Image Uploaded", data);
    }

    onImagePickerEventData(imageFile: ImagePickerPayload) {
        console.log("event camera Image Data", imageFile);
    }

    saveAndBackToCamera(imagePath) {
        console.log("saveAndBackToCamera");
        this.storeImage(imagePath);
        this.resetSavePanelStatus();

        this.updateDetectChange();
    }

    saveImage(imageFile) {
        this.storeImage(imageFile);
        console.log("saveImage::", imageFile);
        this.updateDetectChange();
    }

    private storeImage(passData: any) {
        passData.direction = this.isForwardActive ? "FORWARD" : "BACKWARD";
        this.onImageSelected.emit(passData);
        this.updateDetectChange();
    }

    listenTemplateEventData(data) {
        console.log("data", data);
        if (data === "FORWARD") {
            this.isForwardActive = true;
            this.isBackwardActive = false;
        } else {
            this.isForwardActive = false;
            this.isBackwardActive = true;
        }

        this.updateDetectChange();
    }

    private resetSavePanelStatus() {
        this.isForwardActive = false;
        this.isBackwardActive = false;
        this.selectedIndex = null;
        if (this.saveButtonToggle)
            this.saveButtonToggle(false);
    }

    closeModal() {
        this.onCloseEvent.emit(true);
    }


    updateDetectChange() {
        try {
            if (!this.cdrf['destroyed']) {
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
            }
        } catch (e) {
            console.error('updateDetectChange', e);
        }
    }

    ngOnDestroy() {
        this.cdrf.detach();
    }
}
