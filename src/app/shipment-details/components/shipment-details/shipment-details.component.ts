import {
    Component,
    OnInit,
    ViewContainerRef,
    ChangeDetectorRef, OnDestroy, ViewChild, AfterViewInit
} from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions, ModalDialogOptions, ModalDialogService, ModalDialogParams} from 'nativescript-angular';
import { ShipmentDetailsService } from '../../services/shipment-details.service';
import {
    SpinnerService,
    SpinnerOptionInterface,
    StorageDataService,
    SnackBarService,
    LocalPlatformDataService
} from '@ecap3/tns-core';
import { SeedDataConstant } from '~/app/sc-config/constant/seed-data.constant';
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';
import {ShipmentsService} from "~/app/shipments/services/shipments.service";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import { knownFolders, Folder, File } from "tns-core-modules/file-system";
import * as bgHttp from "nativescript-background-http";
import {RadSideDrawerComponent} from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import {Query, QueryWhereItem} from "nativescript-couchbase-plugin";



@Component({
    selector: 'ns-shipment-details',
    templateUrl: './shipment-details.component.html',
    styleUrls: ['./shipment-details.component.scss'],
    moduleId: module.id,
})

export class ShipmentDetailsComponent implements OnInit,AfterViewInit, OnDestroy {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
    private drawer: RadSideDrawer;
    shipment: any;
    currentShipmentId: string;
    backbuttonConfig: BackbuttonConfig;
    photosLocalObjects: any[] = [];
    imageFile: File;
    shipmentFileModel: any = {
        ShipmentFiles: []
    };
    private session: any;
    public tasks: bgHttp.Task[] = [];
    isUploadCompleted: boolean;
    imgSrc: string;
    @ViewChild(RadSideDrawerComponent, { static: false }) public drawerComponent: RadSideDrawerComponent;

    constructor(
        private activatedRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private modalService: ModalDialogService,
        private viewContainerRef: ViewContainerRef,
        private shipmentDetailsService: ShipmentDetailsService,
        private spinnerService: SpinnerService,
        private localPlatformDataService : LocalPlatformDataService,
        private shipmentService : ShipmentsService,
        private storageDataService : StorageDataService,
        private cdrf: ChangeDetectorRef,
        private snackBarService: SnackBarService
    ) { }

    getShipmentData() {
        for(let item of SeedDataConstant.Shipments) {
            if(item.ItemId === this.currentShipmentId) {
                this.shipment = item;
            }
        }
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.File
            }
        ];
        const query: Query = <Query>{where: filter};
        this.photosLocalObjects = this.localPlatformDataService.getBySqlFilter(query);
        console.log("Photo data", this.photosLocalObjects);

    }

    openCamera() {
        this.drawer.showDrawer();
    }

    closeModal(){
        this.drawer.closeDrawer();
    }

    onImageClose(item) {
        this.localPlatformDataService.delete(item.ItemId);
        this.getShipmentData();
    }

    public receivedImage(filePayload: any) {
        console.log("Image file received:", filePayload);
        this.imageFile = File.fromPath(filePayload.SaveFilePath);
        const imageFileId = this.storageDataService.getNewGuid();
        const imagePath = filePayload.SaveFilePath;
        const imageFileName = this.imageFile.name;
        filePayload['UploadPercentage'] = 0;
        console.log("Photo data", this.photosLocalObjects);
        let imageData = {
            ItemId : this.localPlatformDataService.getGuid(),
            EntityName : SCConfig.Entity.File,
            LastUpdatedDate : new Date().toISOString(),
            FileData : filePayload
        }
        this.photosLocalObjects.unshift(imageData);
        this.localPlatformDataService.insert(imageData);
        //
        // let shipmentFilePayload = {
        //     FileId : imageFileId,
        //     FileName : imageFileName,
        //     FileSize : this.imageFile.size.toString(),
        //     FileType : this.imageFile.extension,
        //     Thumbnails : []
        // }
        //
        // this.shipmentDetailsService.getPreSignedUrl(imageFileId, imageFileName).subscribe((response) => {
        //     const uploadUrl = response;
        //     this.uploadFile(imagePath,  shipmentFilePayload, uploadUrl, filePayload, false);
        // })
        this.closeModal();
    }

    updateShipmentFileInfo(shipmentFilePayload){
        if(this.shipment.ShipmentFiles && this.shipment.ShipmentFiles.length){
            this.shipment.ShipmentFiles.push(shipmentFilePayload);
            this.shipmentFileModel.ShipmentFiles = this.shipment.ShipmentFiles;
        }else{
            this.shipmentFileModel.ShipmentFiles.push(shipmentFilePayload);
        }
        // this.shipmentDetailsService.updateShipment(this.currentShipmentId, this.shipmentFileModel).subscribe((response)=>{
        //         //     console.log('Successfully uploaded!')
        //         // })
        console.log("Image", this.shipmentFileModel);
    }

    uploadFile(filePathForUpload, shipmentFilePayload, uploadUrl, filePayload, needToDelete?) {
        console.log(`Starting uploadFile Media file: ${filePathForUpload}, MediaFileName: ${shipmentFilePayload.FileName}, Url: ${uploadUrl} `);
        const request = {
            url: uploadUrl,
            method: "PUT",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": shipmentFilePayload.FileName
            },
            description: "Uploading file:" + shipmentFilePayload.FileId,
            androidAutoDeleteAfterUpload: needToDelete,
            androidRingToneEnabled: true,
            androidNotificationTitle: "Upload local mediafiles",
        };
        let task: bgHttp.Task;
        let lastEvent = "";
        task = this.session.uploadFile(filePathForUpload, request);
        const that = this;

        task.on("progress", (e) => {
            filePayload['UploadPercentage'] = Math.round((e.currentBytes  / e.totalBytes) * 100);
            //console.log("progress",filePayload['UploadPercentage']);
            this.cdrf.detectChanges();
        });
        task.on("error",  (e) => {
            console.error('Error uploading FileId::', shipmentFilePayload.FileId, e);
        });
        task.on("cancelled",  (e) => {
            console.log('Cancelled FileId::', shipmentFilePayload.FileId, e);
        });
        task.on("complete",  (e) => {
            this.isUploadCompleted = true;
            this.updateShipmentFileInfo(shipmentFilePayload);
            this.cdrf.detectChanges();
            //console.log('Complete FileId::', shipmentFilePayload.FileId, e);
        });
        lastEvent = "";
        this.tasks.push(task);
    }

    goBack() {
        this.backbuttonConfig = {
            path : '/shipments/'
        };
    }

    ngOnInit() {
        this.imgSrc = "res://placeholder";
        this.currentShipmentId = this.activatedRoute.snapshot.params['ItemId'];

        this.getShipmentData();
        this.goBack();

    }

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.cdrf.detach();
    }
}

