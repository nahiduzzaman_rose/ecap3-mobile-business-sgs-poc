import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlatformDataService, SqlQueryBuilderService, StorageDataService } from '@ecap3/tns-core';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { GqlQueryBuilderService } from '@ecap3/tns-core/service/query-builder/gql-query-builder.service';
import { GraphqlDataService } from '@ecap3/tns-platform-graphql';

@Injectable({
    providedIn: 'root'
})
export class ShipmentDetailsService {
    private url = ' https://my-json-server.typicode.com/Rashid46/demo/shipments';
    constructor(
        private http: HttpClient,
        private platformDataService: PlatformDataService,
        private sqlQueryBuilderService: SqlQueryBuilderService,
        private storageDataService: StorageDataService,
        private gqlQueryBuilderService: GqlQueryBuilderService,
        private graphqlDataService: GraphqlDataService,
    ) { }

    updateNote(note): Observable<Response> {
        return this.platformDataService.update('Shipment', note).pipe(map((response: Response) => {
            return response;
        }));
    }

    updateShipment(itemId, model): Observable<any> {
        model['Tags'] = ['Is-Valid-Shipment'];
        return this.graphqlDataService.mutate({
            mutation: this.gqlQueryBuilderService.prepareUpdateModel('Shipment', model, itemId)
        }).pipe(
            map(result => {
                return result;
            })
        );
    }

    getShipmentFiles(fileIds): Observable<any> {
        return this.storageDataService.getFilesInfo(fileIds)
            .pipe(
                map(res => {
                    if (res && res.body && res.body.length) {
                        return res.body;
                    }else {
                        return false;
                    }
                }),
                catchError(err => {
                    console.error('files getting error', err);
                    return of(false);
                })
            );
    }

    getPreSignedUrl(fileId, fileName): Observable<Response> {
        return this.storageDataService.getPreSignedUrl(fileId,  fileName, null, ['File'], null)
            .pipe(
                map(res => {
                    if (res.body.FileId === fileId) {
                        const preSignedUploadUrl = res.body.UploadUrl;
                        return preSignedUploadUrl;
                    }else {
                        return false;
                    }
                }),
                catchError(err => {
                    console.error('getPreSignedUrl error', err);
                    return of(false);
                })
            );
    }
}
