import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {knownFolders, Folder, File} from "tns-core-modules/file-system";
import {RouterExtensions} from "nativescript-angular";
import { Downloader} from "nativescript-downloader";
import {Subscription} from "rxjs";
import { BusinessSyncService } from "~/app/sync/services/business-sync.service";
import {LocalPlatformDataService, UtilityService, SpinnerService} from "@ecap3/tns-core";
import {PlatformSyncService} from "@ecap3/tns-platform-sync/services/platform-sync.service";
//import {AuditSyncService} from "~/app/sync/services/audit-sync.service";


@Component({
    selector: "app-sync-audit-details",
    moduleId: module.id,
    template: ``
})
export class SyncAuditDetailsComponent implements OnInit, OnDestroy {
    subscription: Subscription[] = [];
    timeouts: any[] = [];


    constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private businessSyncService: BusinessSyncService,
                private utilityService: UtilityService, private spinnerService: SpinnerService,
                ) {
    }

    ngOnInit() {
        /* this.spinnerService.hide();
        const auditId = this.activatedRoute.snapshot.params["auditId"];
        this.spinnerService.show({message: 'Synchronizing Audit Details...'});
        this.subscription.push(
            this.businessSyncService.syncAuditDetails(auditId)
                .subscribe(res => {
                    this.spinnerService.hide();
                    if (res) {
                        this.timeouts.push(
                            setTimeout(() => {
                                this.routerExtensions.navigate(["/audit-details", auditId], {
                                    transition: {name: 'fade'},
                                    clearHistory: true
                                });
                            }, 1)
                        );
                    }
                })
        ); */

    }

    // New Sync Methods Start
    private auditDetailSync() {

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.forEach(item => item.unsubscribe());
        this.timeouts.forEach(item => clearTimeout(item));
    }
}
