import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {File, Folder, knownFolders} from "tns-core-modules/file-system";
import {RouterExtensions} from "nativescript-angular";
import {forkJoin, from, Observable, of, Subscription} from "rxjs";
import {BusinessSyncService} from "~/app/sync/services/business-sync.service";
import {
    LocalPlatformDataService,
    SnackBarService,
    SpinnerService,
    TokenProvider,
    UtilityService
} from "@ecap3/tns-core";
import {PlatformSyncService} from "@ecap3/tns-platform-sync/services/platform-sync.service";
//import {AuditSyncService} from "~/app/sync/services/audit-sync.service";
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";
import {catchError, map, switchMap} from "rxjs/operators";
import {FileDownloaderService} from "~/app/sync/services/file-downloader.service";
import {SyncLog} from "~/app/shared-data/model/sync-log.model";
import { PraxisTaskSyncService } from "../services/praxis-task-sync.service";

@Component({
    selector: "app-sync",
    moduleId: module.id,
    templateUrl: "./sync.component.html",
    styleUrls: ["./sync-common.css", "./sync.component.css"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SyncComponent implements OnInit, OnDestroy {
    subscription: Subscription[] = [];
    timeouts: any[] = [];

    isInitialSync: boolean = false;
    isSyncFilesReadyForZip: boolean = false;
    isSyncFilesUploaded: boolean = false;
    isServerNotificationReceived: boolean = false;
    isLocalInsertDone: boolean = false;

    indexFile: File;
    appFolder: Folder;
    syncFilesDownFolder: Folder;
    syncFilesUpFolder: Folder;
    operationFile: File;
    public outputSyncBulkArchiveFileId: any = "";
    public syncPraxisTaskId: string = "";
    public isFilesNeedToDownload: boolean = false;
    public filesToUpload = [];
    public filesToUploadResponse: any;
    public filesToDownload = [];
    public skipLocalUpdate = false;
    private readOnlySyncResponsetoWrite = {};
    public praxisTaskSyncPrepData = {
        PraxisTask: {
            "BulkProcessors": [
                {
                    Name: 'PraxisTaskSyncBulkProcessor',
                    From: '2019-10-15T00:00:00'
                }
            ],
            "Tags": []
        }
    };
    public readonly SYNC_TYPE_PRAXIS_TASK = 'praxisTask';
    public syncType = '';
    public syncLogName = '';
    public responseIndexData: any;

    public isInternetFails: boolean = false;
    private praxisTaskSyncLog: SyncLog;
    private listSyncTime = "2017-06-01T00:00:00";
    public syncProgressStepArray = [];
    public syncHasError = false;
    private isUpdateSyncLog = true;
    public currentActiveStep: number;
    syncIcon = String.fromCharCode(parseInt('0x'+'ebda'));
    syncDone = String.fromCharCode(parseInt('0x'+'ebdc'));

    constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private router: Router,
        private businessSyncService: BusinessSyncService,
        private utilityService: UtilityService, private spinnerService: SpinnerService, private sb: SnackBarService,
        private praxisTaskSyncService: PraxisTaskSyncService, 
        private fileDownloader: FileDownloaderService,
        private cdrf: ChangeDetectorRef, private tokenProvider: TokenProvider,
        private localPlatformDataService: LocalPlatformDataService, 
        public platformSyncService: PlatformSyncService) {

        this.appFolder = knownFolders.currentApp();
    }

    ngOnInit() {
        console.log("Starting Sync Synchronisation!");
        console.log(this.localPlatformDataService.getDatabaseInfo());

        this.spinnerService.hide();
        this._subscribeToSyncEvents();

        // Set initial Sync step
        this.platformSyncService.resetSync();
        this.isInitialSync = this.activatedRoute.snapshot.params["isInitialSync"] === "true";
        this.syncPraxisTaskId = this.activatedRoute.snapshot.params["TaskId"];
        this.outputSyncBulkArchiveFileId = this.utilityService.getNewGuid();

        this.syncType = this.SYNC_TYPE_PRAXIS_TASK;
        this.syncLogName = SCConfig.Entity.PraxisTaskSyncSettings;

        this.updateDetectChange();

        // Clear existing data for Initial sync
        /*if (this.isInitialSync) {
            this.businessSyncService.clearAllData().subscribe(_ => _);
        }*/

        this.subscription.push(
            this.tokenProvider.refreshTokenObservable().subscribe(token => {
                this.businessSyncService.getSyncLog(this.syncLogName, this.syncPraxisTaskId).subscribe(praxisTaskSyncLog => {
                    // Get previous sync log setting
                    this.praxisTaskSyncLog = praxisTaskSyncLog;
                    if (!this.praxisTaskSyncLog) {
                        this.isInitialSync = true;
                        this.praxisTaskSyncLog = new SyncLog();
                        this.praxisTaskSyncLog.ItemId = this.utilityService.getNewGuid();
                        this.praxisTaskSyncLog.BulkProcessors = this.praxisTaskSyncPrepData.PraxisTask.BulkProcessors;
                    }
                    //

                    this.subscription.push(
                        this.prepareSourceSyncFile().subscribe(isSyncFilesReadyForZip => {
                            // Notify Core Sync component to start the Sync process, then wait for Sync server outfile file response
                            if (isSyncFilesReadyForZip) {
                                this.isSyncFilesReadyForZip = true;
                                this.updateDetectChange();
                            }
                        }, error1 => {
                            console.error('Unable to upload source file', error1);
                        })
                    );
                })
            })
        );
    }

    _subscribeToSyncEvents() {
        //debugger
        this.subscription.push(
            // Subscribe to Sync server outfile file response
            this.platformSyncService.getFileUnzipCompleted().subscribe((folder: any) => {
                console.log('Got Notification!');
                this.isServerNotificationReceived = true;
                this.processServerOutputFile(folder);
            }),
            // Subscribe to Sync Step change event
            this.platformSyncService.syncStepsStatusArray.subscribe(syncProgressStepArray => {
                // this.syncProgressStepArray = [];
                this.syncProgressStepArray = syncProgressStepArray;
                this.updateDetectChange();
            }),
            // Subscribe to Current Step Number
            this.platformSyncService.currentActiveStep.subscribe(currentActiveStep => {
                // this.syncProgressStepArray = [];
                this.currentActiveStep = currentActiveStep;
                this.updateDetectChange();
            }),
            // Check if Sync is canceled/Error
            this.platformSyncService.isSyncCancelled.subscribe(isSyncCancelled => {
                if (isSyncCancelled) {
                    this.syncHasError = true;
                    this.updateDetectChange();
                    console.error('Sync error');
                }
            }),
            this.platformSyncService.getInternetFailureFlag().subscribe((internetFailFlag: boolean) => {
                console.log("::internetFailFlag::", internetFailFlag);
                this.isInternetFails = internetFailFlag;
                this.updateDetectChange();
            })
        );
    }

    // New Sync Methods Start
    private prepareSourceSyncFile(): Observable<any> {
        return this.platformSyncService.createSyncFilesFolder().pipe(switchMap(syncFilesUpFolder => {
            this.syncFilesUpFolder = syncFilesUpFolder;
            const praxisTaskId = this.syncPraxisTaskId;

            return this.preparePraxisTaskFilterSyncFile(SCConfig.Entity.PraxisTask, praxisTaskId).pipe(switchMap(res => {
                // Prepare local Updates
                this.filesToUpload = <any[]>this.praxisTaskSyncService.getLocalUploadedFiles(praxisTaskId);
                return this.prepareLocalUpdateSyncFile().pipe(map(res2 => {
                    return res2;
                }));
            }));
        }));
    }

    private preparePraxisTaskFilterSyncFile(entityName, praxisTaskId?): Observable<any> {
        let obsCalls$ = [];

        if (this.syncType === this.SYNC_TYPE_PRAXIS_TASK) {
            // For List sync, we need only Audit entity filter file
            const filterFile = SCConfig.Entity.PraxisTask + "Filter" + ".json";
            const operationFile = this.syncFilesUpFolder.getFile(filterFile);
            let filterObj = {};
            obsCalls$.push(this._writeToFile(operationFile, filterObj));
        }

        return forkJoin(obsCalls$).pipe(map(res => {
            return res;
        }));
    }

    private prepareLocalUpdateSyncFile(): Observable<any> {
        let batchCall$ = [];
        // Upload local files only for sync
        this.praxisTaskSyncLog.BulkProcessors.forEach(x => {
            // Currently we are only syncing `AuditPeriod` to server
            if (x.Name === 'PraxisTaskSyncBulkProcessor') {
            }
            batchCall$.push(this.praxisTaskSyncService.getUpdatedEntity(SCConfig.Entity.PraxisTask, x.From));
            /*  Add conditions here for future local updates */
        });

        if (batchCall$.length > 0) {
            return forkJoin(batchCall$).pipe(switchMap((data: any[]) => {
                if (data && data.length > 0) {
                    const batchCallWriteFile$ = [];
                    data.forEach((updatedEntities: any[]) => {
                        if (updatedEntities && updatedEntities.length > 0) {
                            const updateFile = updatedEntities[0]['EntityName'] + ".json";
                            this.operationFile = this.syncFilesUpFolder.getFile(updateFile);
                            batchCallWriteFile$.push(from(this._writeToFile(this.operationFile, updatedEntities)));
                        }
                    });

                    if (batchCallWriteFile$.length > 0) {
                        return forkJoin(batchCallWriteFile$).pipe(map(res => {
                            return true;
                        }));
                    }
                    return of(true);
                }
                return of(true);
            }));
        }

        return of(true);
    }

    _writeToFile(filePath, data): Promise<any> {
        return filePath.writeText(JSON.stringify(data))
            .then(result => {
                return true;
            }).catch(err => {
                console.error(`Error creating file ${filePath}: `, err);
                return false;
            });
    }

    //Event
    finishedLocalMediaUpload($event) {
        console.log('finishedLocalMediaUpload complete event', $event);
        // After successful upload, remove the records from local database
        this.filesToUploadResponse = $event; // {"FailedUploadFiles": [], "SuccessUploadFiles": [ ]}
        if (this.filesToUploadResponse.SuccessUploadFiles && this.filesToUploadResponse.SuccessUploadFiles.length > 0) {
            this.filesToUploadResponse.SuccessUploadFiles.forEach(id => {
                const fileEntity = this.localPlatformDataService.getById(id);
                if (fileEntity) {
                    fileEntity.SyncStatus = true;
                    fileEntity.isLocalUpload = false;
                    fileEntity.RetryCountUpload = 0;
                    this.localPlatformDataService.update(id, fileEntity);
                    console.log('updated sync status for local file ', id);
                }
            });

            this.filesToUploadResponse.SuccessUploadFiles = [];
        }

        if (this.filesToUploadResponse.FailedUploadFiles && this.filesToUploadResponse.FailedUploadFiles.length > 0) {
            this.filesToUploadResponse.FailedUploadFiles.forEach(id => {
                const fileEntity = this.localPlatformDataService.getById(id);
                if (fileEntity) {
                    fileEntity.RetryCountUpload = typeof fileEntity.RetryCountUpload === 'undefined' ? 1 : parseInt(fileEntity.RetryCountUpload) + 1;
                    this.localPlatformDataService.update(id, fileEntity);
                    console.log('updated failed upload retry count for local file ', id);
                }
            });

            this.filesToUploadResponse.FailedUploadFiles = [];
        }

    }

    //Event: When Upload Finished
    goForSync(sourceFileId) {
        this.subscription.push(
            this.businessSyncService.genericSyncBiz(sourceFileId, this.outputSyncBulkArchiveFileId, this.praxisTaskSyncLog.BulkProcessors)
                .subscribe(response => {
                    // No local files to upload
                    console.log("Sync file successfully Uploaded....waiting for server notification");
                    this.isSyncFilesUploaded = true;
                    this.platformSyncService.setStepDescription(2,'Waiting for Server response');
                }, error1 => {
                    console.error('Sync Error: unable to upload source sync file ', error1);
                    this.syncHasError = true;
                    this.platformSyncService.setSyncCancelledFlag(true);
                })
        );
    }

    // Get Downloaded Sync Files Tasks (Event Coming From Platform Sync App) Start //
    processServerOutputFile(syncFilesFolder: Folder) {
        this.platformSyncService.setStepDescription(2,'Received Server response');

        this.syncFilesDownFolder = syncFilesFolder;
        this.indexFile = this.appFolder.getFile("sync_files" + "/" + "Index.json");
        const indexData = JSON.parse(this.indexFile.readTextSync());
        this.responseIndexData = indexData;
        if (this.responseIndexData && indexData.EndTime) {
            console.log("Serve response indexData::", this.responseIndexData);
            let localInsert = 0;
            let localUpdate = 0;
            this.readOnlySyncResponsetoWrite = {"reqDateTime": this.responseIndexData.EndTime};

            if (this.responseIndexData.BulkProcessors) {
                const batchCall$ = [];

                // Bulk Insert Data
                this.responseIndexData.BulkProcessors.forEach((syncEntityObj, i) => {
                    if (syncEntityObj.SyncError) {
                        // Do not update Sync log if sync has errors
                        this.isUpdateSyncLog = false;
                        console.error('Server sync error');

                        this._handleServerSyncError(syncEntityObj);
                    } else {
                        if (syncEntityObj.LocalInsert) {
                            localInsert++;
                            batchCall$.push(this._processBulkProcessorsSyncData(syncEntityObj.EntryName, 'LocalInsert'));
                        }
                        if (syncEntityObj.LocalUpdate) {
                            localUpdate++;
                            batchCall$.push(this._processBulkProcessorsSyncData(syncEntityObj.EntryName, 'LocalUpdate'));
                        }
                    }
                });

                if (batchCall$.length > 0) {
                    forkJoin(batchCall$).subscribe(response => {
                        console.log("All Local Batch Insert Done::");

                        // Bulk Insert Media
                        const batchImageCall$ = [];
                        /* indexData.BulkProcessors.forEach((syncEntityObj, i) => {
                            if (syncEntityObj.LocalInsert) {
                                batchImageCall$.push(this._insertBatchSyncMedia(syncEntityObj.EntryName, 'LocalInsert'))
                            }
                            if (syncEntityObj.LocalUpdate) {
                                batchImageCall$.push(this._insertBatchSyncMedia(syncEntityObj.EntryName, 'LocalUpdate'))
                            }
                        }); */

                        // re-try previously failed downloads
                        // batchImageCall$.push(this.praxisTaskSyncService.retryFailedDownloads());

                        // Update Progress Status
                        /*this.syncProgressStepArray[2].StatusCode = this.platformSyncService.syncStatusTypes.Finished;
                        this.syncProgressStepArray[3].StatusCode = this.platformSyncService.syncStatusTypes.Progress;
                        this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/
                        this.platformSyncService.finishCurrentStep('Downloading_Data');

                        //forkJoin(batchImageCall$).subscribe(responseImage => {
                          //  console.log("All Local Batch Media Insert Done::");
                        this.finishSyncProcess();
                        //}, error1 => {
                            // handle errors here
                        //    console.error('BulkProcess Media download Error', error1);
                        //});
                    }, error1 => {
                        // handle errors here
                        console.error('BulkProcess Insert Error', error1);
                    })
                } else {
                    this.finishSyncProcess();
                }
            }
        }
    }

    private _processBulkProcessorsSyncData(entityName, operationName): Observable<any> {
        console.log(`Processing Sync data: '${entityName}' for Operation: '${operationName}' `);
        const operationFile = this.appFolder.getFile("sync_files" + "/" + entityName + "_" + operationName + ".json");
        try{
            const fileContents = operationFile.readTextSync();
            const operationFileData = JSON.parse(fileContents);

            if (operationFileData && entityName === SCConfig.Entity.PraxisTask) {
                // Sync Entity: Audit list
                this.platformSyncService.setStepDescription(2,`Downloading ${operationFileData.length} Tasks`);
                return this.praxisTaskSyncService.syncPraxisTask(operationFileData, operationName).pipe(map(res => {
                    return res;
                }));
            } else {
                console.log("`Unknown Entity ${entityName}`...");
                return of (false);
            }
        } catch (e) {
            console.error(`Error reading JSON file: `, e);
            return of (false);
        }
    }

    /* private _insertBatchSyncMedia(entityName, operationName): Observable<any> {
        const operationFile = this.appFolder.getFile("sync_files" + "/" + entityName + "_" + operationName + ".json");
        const operationFileData = JSON.parse(operationFile.readTextSync());
        // Sync Entity: Audit
        if (operationFileData && entityName === SCConfig.Entity.PraxisTask) {
            console.log("Starting downloading `Audit`... Media");
            return this.praxisTaskSyncService.downloadAuditMedia(operationFileData).pipe(
                map(resMedia => {
                    // console.log("Finished downloading `Audit`... Media");
                    return resMedia;
                }),
                catchError(err => {
                    console.log("Error downloading `Praxis task`... Media");
                    return of(null);
                })
            );
        } else {
            console.log(`Unknown Entity ${entityName}...`);
            return of (false);
        }
    } */

    private _handleServerSyncError(syncEntityObj) {
        try {
            const operationFile = this.appFolder.getFile("sync_files" + "/" + syncEntityObj.EntryName + "_SyncError" + ".json");
            if (operationFile) {
                const operationFileData = JSON.parse(operationFile.readTextSync());
                console.error('Server Sync Error', operationFileData);
            }
        } catch (e) {

        }
    }

    finishSyncProcess() {
        // Update Progress Status
        /*this.syncProgressStepArray[3].StatusCode = this.platformSyncService.syncStatusTypes.Finished;
        this.syncProgressStepArray[4].StatusCode = this.platformSyncService.syncStatusTypes.Progress;
        this.platformSyncService.setSyncStepStatusArray(this.syncProgressStepArray);*/
        this.platformSyncService.finishCurrentStep('Downloading_Media');

        // Save/Update SyncLog for next sync request
        const bulkProcessor = [];
        if (this.responseIndexData && this.responseIndexData.BulkProcessors) {
            this.responseIndexData.BulkProcessors.forEach(x => {
                if (x.EntryName === SCConfig.Entity.PraxisTask) {
                    bulkProcessor.push(
                        {Name: 'PraxisTaskSyncBulkProcessor', From: x.EndTime}
                    );
                }
            });
        }

        if (this.isUpdateSyncLog) {
            if (this.syncType === this.SYNC_TYPE_PRAXIS_TASK) {
                this.businessSyncService.updateSyncLog(this.praxisTaskSyncLog.ItemId, bulkProcessor);
            } else {
                console.log("unknown sync log");
            }
        }

        this.platformSyncService.removeSyncDownFolderFiles();
        this.isLocalInsertDone = true;
        // this.platformSyncService.finishSync();
        this._unsubscribeValues();

        // if (this.syncType === this.SYNC_TYPE_AUDIT_DETAILS) {
        //     this.gotoAuditDetails(this.syncPraxisTaskId);
        // } else {
            this.gotoPraxisTaskList();
        //}
    }

    isPraxisTaskListSync(): boolean {
        return this.syncType === this.SYNC_TYPE_PRAXIS_TASK;
    }

    cancelSync() {
        this.platformSyncService.setSyncCancelledFlag(true);
        this.syncHasError = true;

        setTimeout(() => {
            this.gotoPraxisTaskList();
        }, 1000)
    }

    gotoPraxisTaskList() {
        // this.router.navigate(["/audit"], {replaceUrl: true});
        this.spinnerService.show();
        setTimeout(() => {
            this.routerExtensions.navigate(["tasklist"], {transition: {name: "fade"}, clearHistory: true});
            this.spinnerService.hide();
        }, 300);
    }

    startProgressAnimation(pId) {
        pId.animate({
            rotate: -360,
            delay: 1,
            // iterations: 5,
            // curve: enums.AnimationCurve.easeIn
            // curve: AnimationCurve.easeInOut,
            duration: 7000,
            iterations: Number.POSITIVE_INFINITY
        });
    }

    updateDetectChange() {
        try {
            if (!this.cdrf['destroyed']) {
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
            }
        } catch (e) {
            console.error('updateDetectChange', e);
        }
    }

    _unsubscribeValues() {
        this.subscription.forEach(item => item.unsubscribe());
        this.timeouts.forEach(item => clearTimeout(item));
    }

    // @HostListener('unloaded')
    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this._unsubscribeValues();
        this.cdrf.detach();
    }
}
