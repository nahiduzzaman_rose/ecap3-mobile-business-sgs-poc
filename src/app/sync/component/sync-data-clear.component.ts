import {Component, OnDestroy, OnInit} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {Subscription} from "rxjs";
import {BusinessSyncService} from "~/app/sync/services/business-sync.service";
import {SpinnerService} from "@ecap3/tns-core";


@Component({
    selector: "app-sync-data-clear",
    moduleId: module.id,
    template: ``
})
export class SyncDataClearComponent implements OnInit, OnDestroy {
    subscription: Subscription[] = [];
    timeouts: any[] = [];


    constructor(private routerExtensions: RouterExtensions,
                private businessSyncService: BusinessSyncService,
                private spinnerService: SpinnerService,
                ) {
    }

    ngOnInit() {
        this.spinnerService.show({message: 'Clearing sync data.....'});
        this.subscription.push(
            this.businessSyncService.clearAllData()
                .subscribe(res => {
                    setTimeout(() => {
                        this.spinnerService.hide();
                        this.routerExtensions.navigate(["/tasklist"], {
                            transition: {name: 'fade'},
                            clearHistory: true
                        });
                    }, 1)
                })

        );

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.forEach(item => item.unsubscribe());
        this.timeouts.forEach(item => clearTimeout(item));
    }
}
