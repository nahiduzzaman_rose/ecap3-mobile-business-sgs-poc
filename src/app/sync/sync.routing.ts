import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { SyncComponent } from "~/app/sync/component/sync.component";
/*import { SyncAuditDetailsComponent } from "~/app/sync/component/sync-audit-details.component";*/
import { SyncDataClearComponent } from "~/app/sync/component/sync-data-clear.component";

const routes: Routes = [
    { path: "", redirectTo: "task-list", pathMatch: 'full' },
    { path: "task-list/:isInitialSync", component: SyncComponent },
    // { path: "audit-details/:auditId", component: SyncComponent },
    { path: "clear-data", component: SyncDataClearComponent }
];
@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SyncRoutingModule { }
