import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { PlatformSyncModule } from "@ecap3/tns-platform-sync";
import { PlatformSyncService } from "@ecap3/tns-platform-sync/services/platform-sync.service";

// Routes
import { SyncRoutingModule } from "./sync.routing";

// Modules
import { SharedDataModule } from "~/app/shared-data/shared-data.module";

// Components
import { SyncComponent } from "./component/sync.component";
/*import { SyncAuditDetailsComponent } from "~/app/sync/component/sync-audit-details.component";*/
import { SyncDataClearComponent } from "~/app/sync/component/sync-data-clear.component";

// Services
import { StorageDataService } from "@ecap3/tns-core";
import { BusinessSyncService } from "./services/business-sync.service";
import { PraxisTaskSyncService } from "./services/praxis-task-sync.service";


@NgModule({
    providers: [
        PlatformSyncService,
        StorageDataService,
        BusinessSyncService,
        PraxisTaskSyncService
    ],
    imports: [
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        SyncRoutingModule,
        PlatformSyncModule,
        SharedDataModule
    ],
    declarations: [
        SyncComponent,
        /*SyncAuditDetailsComponent,*/
        SyncDataClearComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SyncModule { }
