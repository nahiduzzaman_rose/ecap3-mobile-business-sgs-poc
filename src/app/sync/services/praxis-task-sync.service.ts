import {forkJoin, from, Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {
    LocalPlatformDataService,
    PlatformDataService,
    SqlQueryBuilderService,
    StorageDataService,
    UtilityService
} from "@ecap3/tns-core";
import {BusinessSyncService} from "~/app/sync/services/business-sync.service";
import {catchError, concatMap, map, switchMap} from "rxjs/operators";
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";
import {FileDownloaderService} from "~/app/sync/services/file-downloader.service";
import * as fs from "tns-core-modules/file-system";
import {PlatformSyncService} from "@ecap3/tns-platform-sync/services/platform-sync.service";
import {Query, QueryLogicalOperator, QueryWhereItem} from "nativescript-couchbase-plugin";
import {SyncSetting} from "~/app/shared-data/model/sync-setting.model";
import {FileLocal} from "~/app/shared-data/model/file-local";
import { PraxisTaskModel } from "~/app/shared-data/model/praxis-task.model";

@Injectable()
export class PraxisTaskSyncService {
    constructor(public localDataService: LocalPlatformDataService,
                private businessSyncService: BusinessSyncService,
                private fileDownloader: FileDownloaderService,
                private storageDataService: StorageDataService,
                private sqlQueryBuilderService: SqlQueryBuilderService,
                private platformDataService: PlatformDataService,
                private localPlatformDataService : LocalPlatformDataService,
                private platformSyncService: PlatformSyncService,
                private utilityService: UtilityService) {
    }

    syncPraxisTask(data: any[], operationName, lastSyncDate?): Observable<any> {
        const praxisTasks = data;
        return this.localDataService.batchInsert(praxisTasks, SCConfig.Entity.PraxisTask).pipe(map(res => {
            console.log(`Finished batch insert of (${praxisTasks.length}) Audits`);
            return res;
        }));
    }

    getUpdatedEntity(entityName, LastReqEndTime?: any) {
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: entityName,
            }
        ];
        if (LastReqEndTime) {
            filter.push(
                {
                    logical: QueryLogicalOperator.AND,
                    property: 'LastUpdateDate',
                    comparison: 'greaterThan',
                    value: LastReqEndTime,
                }
            );
        }

        // TODO: fix multiple filter not working for Couchbase plugin

        const query: Query = {
            select: [],
            where: filter
        };
        let results = this.localPlatformDataService.getBySqlFilter(query);

        // Update only specific fields
        let resultsFilter = results.map((praxisTask: PraxisTaskModel) => {
            return {
                ItemId: praxisTask.ItemId,
                LinearAnswer: praxisTask.LinearAnswer,
                Remarks: praxisTask.Remarks,
                TaskSchedule: praxisTask.TaskSchedule,
                EntityName: praxisTask.EntityName,
                Tags: praxisTask.Tags,
                Language : "en-US"
            }
        });

        return of(resultsFilter);
    }

    getLocalUploadedFiles(entityId?) {
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.File,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'SyncStatus',
                comparison: 'equalTo',
                value: false,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'isLocalUpload',
                comparison: 'equalTo',
                value: true,
            }
        ];

        const query: Query = {
            select: [],
            where: filter
        };

        let results = this.localPlatformDataService.getBySqlFilter(query) || [];
        // Skip file if Retry count less than 3
        if (results.length > 0) {
            results = results.filter(fileEntity => (typeof fileEntity.RetryCountUpload === 'undefined') || (parseInt(fileEntity.RetryCountUpload) < 3) );
        }

        return results;
    }

    /* downloadAuditMedia(audits): Observable<any> {
        // Download file One by One
        return from(audits)
            .pipe(
                concatMap(audit => this.getAuditImage(audit))
            );

        // Download file parallel
        // const insertMultiCall$ = [];
        // audits.forEach((audit, i) => {
        //     insertMultiCall$.push(
        //         this.getAuditImage(audit).pipe(
        //             catchError(err => {
        //                 console.error('Audit Image download error', err);
        //                 return of(null)
        //             })
        //         )
        //     );
        // });

        // // Download Audit images
        // return forkJoin(insertMultiCall$).pipe(map(uploadedImages => {
        //     console.log(`Downloaded Audit Images successfully`);
        //     return uploadedImages;
        // }));
    } */

    /* getAuditImage(audit): Observable<any> {
        return this.getAuditClient(audit).pipe(switchMap(client => {
            if (client && client.FileId) {
                return this.platformDataService.getResource(SCConfig.Entity.Client, client.ItemId, 'File-Of-Client-546-294').pipe(
                    switchMap(resFile => {
                        const data = resFile.body;
                        if (data && data.Result !== '') {
                            // New file
                            const localFile = {
                                ItemId: client.FileId,
                                LocalPath: null,
                                EntityName: SCConfig.Entity.File,
                                Tags: [[SCConfig.Tags.IsFile]],
                                ParentEntityId: audit.ItemId,
                                ParentEntityName: SCConfig.Entity.Audit,
                                SyncStatus: false,
                            };
                            return this.downloadEntityFile(client.FileId, localFile, audit.ItemId, SCConfig.Entity.Audit)
                                .pipe(switchMap(path => {
                                    // Save the local path to the Audit entity
                                    return this.localDataService.update(audit.ItemId, {LocalPath: path}).pipe(map(resUpdate => {
                                        return path;
                                    }));
                                }),
                                catchError(err => {
                                    console.error('Audit Image download error', err);
                                    return of(null)
                                })
                            )
                        }

                        return of(null);
                    }),
                    catchError(err2 => {
                        console.error('Audit 2 Image download error', err2);
                        return of(null)
                    })
                );
            }

            return of(null);
        }));
    } */

    /* syncAuditPeriod(data, auditId, operationName, isInitialSync = true): Observable<any> {
        return this.auditService.getAuditById(auditId).pipe(switchMap(audit => {
            if (audit) {
                const auditPeriods = data.map(x => {
                    let auditPeriod = new AuditPeriodModel(x);
                    if (operationName === 'LocalInsert') {
                        auditPeriod.SyncSetting = new SyncSetting();
                        auditPeriod.SyncSetting.LastReqEndTime = this.detailsSyncTime;
                    }

                    return auditPeriod
                });

                if (auditPeriods.length === 0) {
                    console.error('No new Periods found for this Audit ' + audit.ItemId);
                }

                return this.localDataService.batchInsert(auditPeriods, SCConfig.Entity.AuditPeriod).pipe(switchMap(res => {
                    console.log(`Finished batch insert of (${auditPeriods.length}) AuditPeriods`);
                    
                    return this.auditService.updateAudit(auditId, audit).pipe(map(resAudit => {
                        return res;
                    }));
                }));
            }

            return of(false);
        }));
    } */

    /* downloadAuditDocuments(auditId): Observable<any> {
        return this.auditService.getAuditById(auditId).pipe(switchMap(audit => {
            if (audit) {
                console.log('Downloading Audit Documents');
                const allAuditDocuments = this.listAuditDocuments(audit);
                let obsCalls$ = [];
                if (allAuditDocuments.length > 0) {
                    allAuditDocuments.forEach(document => {
                        obsCalls$.push(
                            this.downloadEntityFile(document.FileId, document, auditId, SCConfig.Entity.Audit)
                        )
                    });

                    return forkJoin(obsCalls$).pipe(map(obsArray => {
                        return true;
                    }), catchError(err => {
                        console.error('Error downloading batch Audit Documents', err);
                        return of(null);
                    } ));
                }

                return of(null);
            }

            return of(null);
        }));
    }

    downloadEntityFile(id, file, parentEntityId?, parentEntityName?): Observable<any> {
        let localFile = <FileLocal>this.localDataService.getById(id);
        if (!localFile) {
            // New file
            localFile = {
                ItemId: id,
                LocalPath: null,
                EntityName: SCConfig.Entity.File,
                Tags: [[SCConfig.Tags.IsFile]],
                ParentEntityId: parentEntityId || null,
                ParentEntityName: parentEntityName || null,
                SyncStatus: false,
            };
            this.localDataService.insert(localFile);
        }

        return this.businessSyncService.downloadStorageFileByFileId(id, this.businessSyncService.getAuditFolderLocation())
            .pipe(
                map(path => {
                    localFile.LocalPath = path;
                    localFile.SyncStatus = true;
                    this.localDataService.update(id, localFile);

                    return path;
                }),
                catchError(err => {
                    console.error(`Error downloading file ${id}`, err);
                    return of(null);
                })
            )
    }

    downloadAuditPeriodMedia(auditPeriods, auditId): Observable<any> {
        return this.downloadAuditDocuments(auditId).pipe(switchMap(res => {
            return this.downloadPeriodAllQuestionAnswersFiles(auditPeriods).pipe(map(resFiles => {
                return resFiles;
            }));
        }));
    } */

    /* downloadPeriodAllQuestionAnswersFiles(auditPeriods: any[]): Observable<any> {
        console.log('Downloading Period Question Answers Documents');
        let answerFiles = [];
        if (auditPeriods && auditPeriods.length > 0) {
            auditPeriods.forEach(period => {
                period = this.auditDetailsService.createChildItems(period);
                if (period.Chapters && period.Chapters.length > 0) {
                    period.Chapters.forEach((chapter, chapterIndex) => {
                        if (chapter['QuestionListsAll'] && chapter['QuestionListsAll'].length > 0) {
                            chapter['QuestionListsAll'].forEach(questionItem => {
                                const question = questionItem;
                                if (question.Answer && question.Answer.Recordings) {
                                    // Photos
                                    if (question.Answer.Recordings.CapturePhotos && question.Answer.Recordings.CapturePhotos.length > 0) {
                                        question.Answer.Recordings.CapturePhotos.forEach(file => {
                                            file.ParentEntityId = period.ItemId;
                                            answerFiles.push(file);
                                        })
                                    }

                                    // Videos
                                    if (question.Answer.Recordings.Videos && question.Answer.Recordings.Videos.length > 0) {
                                        question.Answer.Recordings.Videos.forEach(file => {
                                            file.ParentEntityId = period.ItemId;
                                            answerFiles.push(file);
                                        })
                                    }

                                    // Voices
                                    if (question.Answer.Recordings.Voices && question.Answer.Recordings.Voices.length > 0) {
                                        question.Answer.Recordings.Voices.forEach(file => {
                                            file.ParentEntityId = period.ItemId;
                                            answerFiles.push(file);
                                        })
                                    }
                                }
                            })
                        }

                        // Reset extra field
                        delete chapter['QuestionListsAll'];
                    })
                }

                // Reset extra field
                delete period['ChildItems'];
            });

            if (answerFiles.length > 0) {
                let obsCalls$ = [];
                answerFiles.forEach(document => {
                    obsCalls$.push(
                        this.downloadEntityFile(document.ItemId, document, document.ParentEntityId, SCConfig.Entity.AuditPeriod)
                    )
                });

                if (obsCalls$.length > 0) {
                    return forkJoin(obsCalls$).pipe(map(obsArray => {
                        return obsArray;
                    }), catchError(err => {
                        console.error('Error downloading batch Audit Documents', err);
                        return of(null);
                    } ));
                } else {
                    return of(null);
                }
            }
        }

        return of(null);
    } */

    //Untested
    /* retryFailedDownloads(): Observable<any> {
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.File,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'SyncStatus',
                comparison: 'equalTo',
                value: false,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'LocalPath',
                comparison: 'isNullOrMissing',
                value: null,
            }
        ];

        const query: Query = {
            select: [],
            where: filter
        };

        let retryFiles = this.localPlatformDataService.getBySqlFilter(query) || [];
        // Skip file if Retry count less than 3
        // if (results.length > 0) {
        //     results = results.filter(fileEntity => (typeof fileEntity.RetryCountUpload === 'undefined') || (parseInt(fileEntity.RetryCountUpload) < 3) );
        // }

        if (retryFiles.length > 0) {
            let obsCalls$ = [];
            retryFiles.forEach(document => {
                obsCalls$.push(
                    this.downloadEntityFile(document.ItemId, document)
                )
            });

            if (obsCalls$.length > 0) {
                console.log('Starting downloading `Previous Failed download Media`...');
                return forkJoin(obsCalls$).pipe(map(obsArray => {
                    return obsArray;
                }), catchError(err => {
                    console.error('Error downloading batch Retry files', err);
                    return of(null);
                } ));
            } else {
                return of(null);
            }
        }

        return of(null);
    } */

    

    

    /* listAuditDocuments(audit) {
        let documents = [];
        if (audit.AuditDocument && audit.AuditDocument.length > 0) {
            audit.AuditDocument.forEach(request => {
                if (request.UploadedDocuments && request.UploadedDocuments.length > 0) {
                    request.UploadedDocuments.forEach(document => {
                        documents.push(document);
                    })
                }
            })
        }

        return documents;
    }

    getAuditClient(audit): Observable<any> {
        let filters = [
            {
                property: 'ItemId',
                operator: '=',
                value: audit.ClientId
            }
        ];
        const fields = [SCConfig.AllFieldsOfEntity.Client];
        const query = this.sqlQueryBuilderService.prepareQuery(SCConfig.Entity.Client, fields, filters, null, 0, 100);
        return this.platformDataService.getBySqlFilter(SCConfig.Entity.Connection, query).pipe(
            map((response) => {
                return response.body.Results ? response.body.Results[0] : false;
            }), catchError(err => {
                console.error('Error client', err);
                return of(null);
            })
        );
    }

    getAuditFolderLocation() {
        return fs.knownFolders.currentApp().getFolder('audit').path;
    }

    private clearAuditPeriods(auditId: any): Observable<any> {
        return of(null);
    } */
}
