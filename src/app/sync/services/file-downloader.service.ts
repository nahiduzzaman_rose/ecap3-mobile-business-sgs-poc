import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Downloader, ProgressEventData, DownloadEventData} from 'nativescript-downloader';
import {getFile} from "tns-core-modules/http";
import * as fs from "tns-core-modules/file-system";


@Injectable({
    providedIn: 'root',
})
export class FileDownloaderService {
    downloader: Downloader = new Downloader();
    private headerHttp = {
        "Content-Type": "application/json",
        "Authorization": 'Bearer ' + 'Token...'
    };

    constructor() {
    }


    /**
     * @ngdoc method
     * @name downloadDb
     * @description  download generic method for nay file
     * @memberof DataDownLoadService
     * @param apiUrl : - https://myserver.com/mypath
     * @param filename :- myfile.zip ...
     * @param downloadlocation : mobile local location
     */
    downloadFileWithProgress(apiUrl, filename, downloadLocation): Promise<Observable<any>> {
        const zipDownloaderId = this.downloader.createDownload({
            url: apiUrl,
            headers: this.headerHttp,
            path: downloadLocation,
            fileName: filename
        });


        return this.downloader
            .start(zipDownloaderId, (progressData: ProgressEventData) => {
                console.log(`Download Progress : ${progressData.value}%`);
                // console.log(`Current Size : ${progressData.currentSize}%`);
                // console.log(`Total Size : ${progressData.totalSize}%`);
                // console.log(`Download Speed in bytes : ${progressData.speed}%`);
            })
            .then((completed: DownloadEventData) => {
                    console.log(`file successfully downloaded at : ${completed.path}`);
                    return of(completed.path);
                },
                (res) => {
                    return of(false);
                });
    }

    downloadFileByUrl(apiUrl, filename, downloadLocation): Promise<any> {
        let savedPath = fs.path.join(downloadLocation, filename);
        return getFile(apiUrl, savedPath).then(function (r) {
            console.log(`file downloaded at : ${r.path}`);
            return r.path;
        }, function (e) {
            console.error("err download file", e, 'URL: ', apiUrl);
            return false;
        });
    }
}
