import {Injectable} from '@angular/core';
import {
    AppSettingsProvider,
    LocalPlatformDataService,
    PlatformDataService,
    SpinnerService,
    SqlQueryBuilderService,
    StorageDataService,
    TokenProvider,
    UtilityService
} from '@ecap3/tns-core';
import {catchError, map, switchMap} from 'rxjs/operators';
import {forkJoin, from, Observable, of} from "rxjs";
import {SeedDataConstant} from "~/app/sc-config/constant/seed-data.constant";
import {Query, QueryLogicalOperator, QueryWhereItem} from 'nativescript-couchbase-plugin';
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import * as fs from "tns-core-modules/file-system";
//import {AuditService} from "~/app/audit/services/audit.service";
import {FileDownloaderService} from "~/app/sync/services/file-downloader.service";
import {PlatformSyncService} from "@ecap3/tns-platform-sync/services/platform-sync.service";
import {Folder} from "tns-core-modules/file-system";


@Injectable(/*{
    providedIn: 'root'
}*/)
export class BusinessSyncService {
    constructor(
        private http: HttpClient,
        private appSettingsProvider: AppSettingsProvider,
        private tokenProvider: TokenProvider,
        private spinnerService: SpinnerService,
        private utilityService: UtilityService,
        //private auditService: AuditService,
        private fileDownloader: FileDownloaderService,
        private platformSyncService: PlatformSyncService,
        private storageDataService: StorageDataService,
        private sqlQueryBuilderService: SqlQueryBuilderService,
        private platformDataService: PlatformDataService,
        private localPlatformDataService : LocalPlatformDataService,
        /*private gqlDataService: GqlQueryBuilderService*/) {
    }

    getAuditFolderLocation() {
        return fs.knownFolders.currentApp().getFolder('audit').path;
    }

    public removeAllFolderImages(folderName) {
        let folder = fs.knownFolders.documents();
        let auditFolder = folder.getFolder(folderName);
        if (auditFolder) {
            // >> fs-delete-folder-code
            // Remove a folder and recursively its content.
            auditFolder.clear()
                .then(fres => {
                    // Success removing the folder.
                    console.log("resultMessage", "Audit Folder successfully cleared!");
                }).catch(err => {
                console.log(err.stack);
            });
        } else {
            console.log("Already deleted folder!: " + folderName);
        }
    }

    header: any = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": "bearer " + this.tokenProvider.getAccessToken(),
        "Origin": this.appSettingsProvider.getAppSettings().Origin
    });

    /* saveAudit(audit): Observable<any> {
        return this.getAuditById(audit.ItemId).pipe(map(existingAudit => {
            const obsCall$ = [];
            if (existingAudit) {
                obsCall$.push(this.localPlatformDataService.update(existingAudit.id, audit));
            } else {
                obsCall$.push(this.localPlatformDataService.insert(audit));
            }

            return forkJoin(obsCall$).pipe(map(res => {
                return res;
            }))
        }));
    } */

    getPraxisTaskById(praxisTaskItemId): Observable<any> {
        const filter : QueryWhereItem[] = [
            /*{
               property: 'Tags',
               comparison: 'in',
               value: 'Is-A-Audit',
           },*/
            {
                // logical: QueryLogicalOperator.AND,
                property: 'ItemId',
                comparison: 'equalTo',
                value: praxisTaskItemId,
            }
        ];

        const query: Query = {
            select: [],
            where: filter,
            limit: 1,
            offset: 0
        };
        const results = this.localPlatformDataService.getBySqlFilter(query);
        let audit = results[0] ? results[0] : null;

        return of(audit)
    }

    /* syncAll(useSeedData = false): Observable<any> {
        return this.clearAllData().pipe(switchMap(resClear => {
            console.log('Clearing existing data...');
            return this.getAuditList().pipe(switchMap(resAuditResult => {
                if (useSeedData) {
                    console.log('Seed data sync...');
                    SeedDataConstant.Audits.forEach((item)=>{
                        this.localPlatformDataService.insert(item);
                    }, );

                    // Insert Sync settings
                    this.localPlatformDataService.insert({
                        ItemId: this.utilityService.getNewGuid(),
                        Tags: SCConfig.Tags.IsAAuditListSyncSettings,
                        EntityName: SCConfig.Entity.AuditListSyncSettings,
                        LastReqEndTime: new Date().toISOString(),
                        CountPlannedAudit: 4,
                        CountCarriedOut: 15
                    });
                    return of(true);
                } else {
                    console.log('Live data sync...');
                    if (resAuditResult.Data && resAuditResult.Data.length > 0) {
                        this.removeAllFolderImages('audit'); // Clear Audit folder
                        const multiCallInsert = [];
                        let countPlannedAudit = 0;
                        let countCarriedOut = 0;
                        resAuditResult.Data.forEach( (audit, auditIndex) => {
                            multiCallInsert.push(
                                from(this.fileDownloader.downloadFileByUrl(
                                    'https://picsum.photos/800/600',
                                    audit.ItemId + '.jpeg',
                                    this.getAuditFolderLocation()
                                )).pipe(
                                    catchError(err => of(err))
                                )
                            );

                            audit.SyncSetting = {Details: null, LastSyncDate: new Date().toISOString()};
                            if (audit.Status === SCConfig.AuditStatus.PLANNED) {
                                countPlannedAudit = countPlannedAudit + 1;
                            } else if (audit.Status === SCConfig.AuditStatus.CARRIED_OUT) {
                                countCarriedOut = countCarriedOut + 1;
                            }
                            multiCallInsert.push(this.localPlatformDataService.insert(audit));
                        });
                        // Insert Sync log
                        multiCallInsert.push(
                            this.localPlatformDataService.insert({
                                Tags: SCConfig.Tags.IsAAuditListSyncSettings,
                                EntityName: SCConfig.Entity.AuditListSyncSettings,
                                CountPlannedAudit: countPlannedAudit,
                                CountCarriedOut: countCarriedOut
                            })
                        );

                        return forkJoin(multiCallInsert ).pipe(map((resAuditInserts: any) => {
                            console.log('Audit list sync done');
                            return resAuditInserts;
                        }));
                    }
                }
            }));

            // return resClear;
        }));
    } */

    /* syncAuditDetails(auditId, seedData = false): Observable<any> {
        return this.auditService.getAuditById(auditId).pipe(switchMap(audit => {
            if (!audit) {
                return of (null);
            }
            return this.getAuditPeriods(auditId, seedData).pipe(switchMap(auditPeriods => {
                const multiCall$ = [];
                if (auditPeriods && auditPeriods.Data.length > 0) {
                    auditPeriods.Data.forEach((period, periodDataIndex) => {
                        multiCall$.push(this.localPlatformDataService.insert(period));
                    });

                    // Also update AuditDetails Sync Settings
                    audit.SyncSetting = {Details: true, LastSyncDate: new Date().toISOString()};
                    multiCall$.push(this.localPlatformDataService.update(audit.id, audit));

                    return forkJoin(multiCall$ ).pipe(map((resAuditInserts: any) => {
                        return resAuditInserts;
                    }));
                }

                return of (null);
            }));
        }));
    } */

    clearAllData(): Observable<any> {
        const result = this.localPlatformDataService.clearData();
        return from(this._clearAllSyncFolder()).pipe(map(res => {
            return res;
        }));
    }

    _clearAllSyncFolder(): Promise<any> {
        let syncFilesFolder: Folder = fs.knownFolders.currentApp().getFolder('audit');
        return syncFilesFolder.clear()
            .then(fres => {
                // Success removing the folder.
                console.log("resultMessage", "Sync Files Folder successfully cleared!");
                return true;
            }).catch(err => {
                console.log(err.stack);
                return false;
            });
    }

    /* getAuditList(){
        const fields = 'Data {\n      ItemId\n      AuditName\n      ClientName\n      ClientId\n      ClientSiteName\n      ClientSiteId\n      ClientSiteFileId\n      ClientSiteAddress\n      StartDate\n      EndDate\n      TotalObservation\n      Status\n      RegionName\n      RegionKey\n      SequenceNumber\n      TotalPreviousObservation\n      TotalObservationOverdue\n      IsArchived\n      ProfileQuestionSetIds\n      TypeOfAuditName\n      TypeOfAuditKey\n      SubStatus\n      Auditor\n      AuditScheduleVisit\n      DocumentsDueDate\n      AuditDocument\n      Tags\n      AuditCarriedOutDate\n      AuidtReportReviewByPmDate\n      AuidtReportReviewByCmDate\n      AuditReportApprovedDate\n      AuditReportRejectedDate\n      IsDocumentUploaded\n      DocumentUploadDueDate\n    }';
        return this.getBySqlFilter(SCConfig.Entity.Audit, `{}`, fields)
            .pipe(map((response: HttpResponse<any>) => {
                return this.extractGqlResponseResult(SCConfig.Entity.Audit, response)
            }));

    //     SeedDataConstant.Audits.forEach((item)=>{
    //         this.localPlatformDataService.insert(item);
    //     }, );

    //    // Insert Sync settings
    //    this.localPlatformDataService.insert({
    //        Tags: ['Is-A-SyncSettings'],
    //        Type: 'audit_list',
    //        CountPlannedAudit: 4,
    //        CountCarriedOut: 15
    //    });
    } */

    /* getAuditPeriods(auditId, seedData) {
        if (!seedData) {
            // Live data
            const fields = 'Data {\n      ItemId\n      AuditId\n      PeriodId\n      ChapterIds\n      ProjectId\n      Chapters\n      Project\n    }';
            return this.getBySqlFilter(SCConfig.Entity.AuditPeriod, `{AuditId: '${auditId}'}`, fields)
                .pipe(map((response: HttpResponse<any>) => {
                    return this.extractGqlResponseResult(SCConfig.Entity.AuditPeriod, response)
                }));
        } else {
            // Seed data
            return of(
                {Data: SeedDataConstant.AuditPeriods.filter(x => x.AuditId === auditId)}
            );
        }
    } */

    /* getAuditListSyncLog(): Observable<any> {
        const tag = SCConfig.Tags.IsAAuditListSyncSettings;
        const filter : QueryWhereItem[] = [
        //     {
        //        property: 'Tags',
        //        comparison: 'in',
        //        value: 'Is-A-SyncSettings',
        //    }
            {
                // logical: QueryLogicalOperator.AND,
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.AuditListSyncSettings,
            }
        ];

        const query: Query = {
            select: [],
            where: filter,
            limit: 1,
            offset: 0
        };
        const results = this.localPlatformDataService.getBySqlFilter(query);
        const resultsSingle = results.length > 0 ? results.filter(x => x.Tags.includes(tag)) : results;

        return of(resultsSingle[0]);
    } */

    /* isSynced(): boolean {
        const tag = SCConfig.Tags.IsAAuditListSyncSettings;
        const filter : QueryWhereItem[] = [
        //     {
        //        property: 'Tags',
        //        comparison: 'in',
        //        value: 'Is-A-SyncSettings',
        //    }
            {
                // logical: QueryLogicalOperator.AND,
                property: 'EntityName',
                comparison: 'equalTo',
                value:  SCConfig.Entity.AuditListSyncSettings,
            }
        ];

        const query: Query = {
            select: [],
            where: filter,
            limit: 1,
            offset: 0
        };
        const results = this.localPlatformDataService.getBySqlFilter(query);
        const resultsSingle = results.length > 0 ? results.filter(x => x.Tags.includes(tag)) : results;

        return !!resultsSingle[0];
    } */

    getBySqlFilter(entityName, filter = `{}`, fields, pageNumber = 1): Observable<any> {
        return this.http.post(this.appSettingsProvider.getAppSettings().GQLQueryUri, {
                "operationName": "findData",
                "variables": {},
                "query":"query findData {\n  "+entityName+"s(Model: {PageNumber: "+pageNumber+", Filter: \" "+filter+" \", Sort: \"{CreateDate: 1}\"}) {\n  "+fields+"  \n    ErrorMessage\n    Success\n    ValidationResult\n    TotalCount\n  }\n}\n"
            },
            {headers: this.header, observe: "response"})
            .pipe(map((response) => response));
    }

    extractGqlResponseResult(entityName, response: HttpResponse<any>): any {
        return response.body && response.body.data  && response.body.data[entityName + 's'] ? response.body.data[entityName + 's'] : {}
    }

    getSyncLog(entityName, id?): Observable<any> {
        debugger
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: entityName,
            }
        ];

        if (id) {
            filter.push(
                {
                    logical: QueryLogicalOperator.AND,
                    property: 'ItemId',
                    comparison: 'equalTo',
                    value: id,
                }
            );
        }

        const query: Query = <Query>{where: filter};
        const results = this.localPlatformDataService.getBySqlFilter(query);
        const log = results && results.length > 0 ? results[0] : null;

        return of (log);
    }

    updateSyncLog(id, data): Observable<any> {
        const filter : QueryWhereItem[] = [
            {
                property: 'id',
                comparison: 'equalTo',
                value: id,
            }
        ];

        const query: Query = <Query>{where: filter};
        const results = this.localPlatformDataService.getBySqlFilter(query);
        let log = results && results.length > 0 ? results[0] : null;

        // Create new sync log
        if (!log) {
            const syncLog = {
                ItemId: id,
                EntityName: SCConfig.Entity.PraxisTaskSyncSettings,
                BulkProcessors: data
            };

            this.localPlatformDataService.insert(syncLog);
            console.log('New Sync log created: ', syncLog);
        } else {
            const updateSyncLog = {
                BulkProcessors: data
            };

            console.log('Updated Sync log', updateSyncLog);
            return this.localPlatformDataService.update(id, updateSyncLog);
        }
    }

    /* getListSyncLog(): Observable<any> {
        const entityName: string = SCConfig.Entity.AuditListSyncSettings;
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value: entityName,
            }
        ];

        const query: Query = <Query>{where: filter};
        const results = this.localPlatformDataService.getBySqlFilter(query);
        const log = results && results.length > 0 ? results[0] : null;
        return of (log);
    } */

    /* setListSyncLog(data): Observable<any> {
        const syncLog = {
            ItemId: this.utilityService.getNewGuid(),
            Tags: [SCConfig.Tags.IsAAuditListSyncSettings],
            SyncType: SCConfig.Entity.AuditListSyncSettings,
            EntityName: SCConfig.Entity.AuditListSyncSettings,
            LastReqEndTime: data.reqDateTime,
        };

        return this.localPlatformDataService.insert(syncLog);
    } */

    /* updateListSyncLog(itemID, data): Observable<any> {
        const updateSyncLog = {
            ItemId: itemID,
            LastReqEndTime: data.reqDateTime,
        };
        return this.localPlatformDataService.update(SCConfig.Entity.AuditListSyncSettings, updateSyncLog);
    } */

    readOnlySyncBiz(syncId, syncReqDateTime): Observable<any> {
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });

        let body = {
            SyncSessionId: syncId,
            SyncAdapterId: "Selise.SC.GermanRailway.SyncAdapter",
            OutputSyncBulkArchiveFileId: syncId,
            From: "2017-01-01T00:00:00",
            To: syncReqDateTime
        };
        return this.http.post(this.appSettingsProvider.getAppSettings().SyncUrl + "DataSyncCommand/ReadonlySync", body, {headers: header}).pipe(map((response: any) => {
            return {reqResult: response, reqDateTime: syncReqDateTime};
        }, (error) => {
            return error;
        }));
    }

    genericSyncBiz(sourceFileId, outputFileId, bulkProcesssors, syncFromTime?): Observable<any> {
        const header = new HttpHeaders({
            "Content-Type": "application/json"
        });
        let body = {
            SyncSessionId: outputFileId,
            SyncAdapterId: this.appSettingsProvider.getAppSettings().SyncAdapterId,
            OutputSyncBulkArchiveFileId: outputFileId,
            SourceSyncBulkArchiveFileId: sourceFileId,
            /*From: syncFromTime,*/
            BulkProcessors: bulkProcesssors
        };
        console.log('Sync request Payload', body);

        return this.http.post(this.appSettingsProvider.getAppSettings().SyncUrl + "DataSyncCommand/Sync", body, {headers: header}).pipe(map((response: any) => {
            return {reqResult: response};
        }, (error) => {
            return error;
        }));
    }

    downloadStorageFileByFileId(fileId, location): Observable<any> {
        if (fileId) {
            return this.storageDataService.getFileInfo(fileId).pipe(switchMap(resFile => {
                const data = resFile.body;
                if (data && data.Url) {
                    const imageName = `${fileId}_${data.Name}`;
                    return from(this.fileDownloader.downloadFileByUrl(
                        data.Url,
                        imageName,
                        location
                    )).pipe(map(path => path),
                        catchError(err => of(null))
                    )
                }
            }));
        } else {
            return of(null);
        }
    }

    /* getAuditImage(audit): Observable<any> {
        return this.getAuditClient(audit).pipe(switchMap(client => {
            if (client && client.FileId) {
                return this.storageDataService.getFileInfo(client.FileId).pipe(switchMap(resFile => {
                    const data = resFile.body;
                    if (data && data.Url) {
                        const imageName = audit.ItemId + `.jpeg`;
                        return from(this.fileDownloader.downloadFileByUrl(
                            data.Url,
                            imageName,
                            this.getAuditFolderLocation()
                        )).pipe(map(path => path),
                            catchError(err => of(null))
                        )
                    }
                }));
            } else {
                return of(null);
            }
        }));
    } */

    /* getAuditClient(audit): Observable<any> {
        let filters = [
            {
                property: 'ItemId',
                operator: '=',
                value: audit.ClientId
            }
        ];
        const fields = [SCConfig.AllFieldsOfEntity.Client];
        const query = this.sqlQueryBuilderService.prepareQuery(SCConfig.Entity.Client, fields, filters, null, 0, 100);
        return this.platformDataService.getBySqlFilter(SCConfig.Entity.Connection, query).pipe(
            map((response) => {
                return response.body.Results ? response.body.Results[0] : false;
            }), catchError(err => null)
        );
    } */
}
