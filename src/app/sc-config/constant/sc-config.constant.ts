export class SCConfig {
    public static Pagination = {
        PageNumber: 0,
        PageSize: 100
    };

    public static ScertDateFormat = 'dd.MM.yyyy';

    public static Entity = {
        Connection: 'Connection',
        File: 'File',
        PraxisTask: 'PraxisTask',
        PraxisTaskSyncSettings: 'PraxisTaskSyncSettings'
    };

    public static Tags = {
        IsValidPraxisTask: 'Is-Valid-PraxisTask'
    };

    public static LanguageKey = {
        'English': 'en-US',
        'French': 'fr-FR',
        'German': 'de-DE'
    };

    public static UserLanguageSkills = [
        'English',
        'German',
        'French',
        'Italian'
    ];

    //Copied from web 
    public static ThumbnailFields = ['FileId', 'Height', 'Width', 'Tag', 'Dimension'];
    public static ImageFields = ['FileId', 'FileName', 'FileSize', {Thumbnails: SCConfig.ThumbnailFields}];
    public static AddressFields = ['ItemId', 'AddressLine1', 'AddressLine2', 'Street', 'StreetNo', 'PostalCode', 'CountryName',
        'CountryShortCode', 'CityName', 'AreaCode', 'AreaCode'];
    public static AdditionalInfoFields = ['ItemId', 'CategoryKey', 'CategoryName', {Address: SCConfig.AddressFields},
        'Name', 'ContactPerson', 'PhoneNumber', 'Email'];
    public static SpecialityFields = ['ItemId', 'Title', 'Priorities'];
    public static CertificateFields = ['Title', 'Descriptions'];
    public static PraxisUserFields = [
        'ItemId', 'CreatedBy', 'CreateDate', 'Language', 'Tags', 'LastUpdatedBy', 'UserId', {Image: SCConfig.ImageFields},
        'Salutation', 'FirstName', 'LastName', 'DisplayName', 'Gender', 'DateOfBirth', 'Nationality',
        'MotherTongue', 'OtherLanguage', 'ClientId', 'ClientName', 'Designation', 'Email',
        'Phone', 'PhoneExtensionNumber', 'AcademicTitle', 'WorkLoad', {Specialities: SCConfig.SpecialityFields}, 'Skills', 'Roles', 'KuNumber', 'NumberOfChildren',
        {CertificateOfCompetence: SCConfig.CertificateFields}, 'DateOfJoining', 'NumberOfPatient', 'Telephone', 'GlnNumber',
        'ZsrNumber', 'KNumber', 'Remarks'
    ];
    public static PraxisClientCategoryBaseFields = [
        'ItemId', 'CreatedBy', 'CreateDate', 'Language', 'Tags', 'LastUpdatedBy', 'ClientId', 'Name', 'ParentId',
        {ControllingGroup: SCConfig.PraxisUserFields}, {ControlledGroup: SCConfig.PraxisUserFields}
    ];
    public static PraxisSubCategoryFields = [{SubCategories: SCConfig.PraxisClientCategoryBaseFields}];
    public static PraxisClientCategoryFields = [...SCConfig.PraxisClientCategoryBaseFields, ...SCConfig.PraxisSubCategoryFields];

    private static PraxisQuestionOptionFields: any = [
        'OptionId', 'OptionTitle', 'IsCorrectOption'
    ];

    private static LinearScaleQuestionFields: any = [
        'From', 'To', 'Step', 'FromLabel', 'ToLabel', 'Direction'
    ];

    private static FileUploadQuestion: any = [
        'FileTypes', 'NumberOfFiles', 'MaximumFileSize'
    ];

    private static PraxisQuestionFields: any = [
        'QuestionId', 'QuestionTypeKey', 'Title', 'Description',
        'QuestionTypeKey', 'QuestionTypeValue', 'Remarks',
        {QuestionOptions: SCConfig.PraxisQuestionOptionFields},
        {LinearScaleQuestion: SCConfig.LinearScaleQuestionFields},
        {FileUploadQuestion: SCConfig.FileUploadQuestion}
    ];


    private static PraxisTaskNotifyConfig: any = [
        'IsEnable', 'Members'
    ];

    private static PraxisTaskNotify: any = [
        {TaskNotFullFilled: SCConfig.PraxisTaskNotifyConfig},
        {TaskNotCompleted: SCConfig.PraxisTaskNotifyConfig},
        {TaskToNextDay: SCConfig.PraxisTaskNotifyConfig},
    ];

    private static RepeatingDateProp: any = [
        'Day', 'Month', 'Year'
    ];

    private static PraxisTaskTimetable: any = [
        'IsRecurrent', 'SubmissionDates', 'ScheduleType', 'ScheduleTypeKey', 'ScheduleTypeValue',
        'AvoidHoliday', 'AvoidWeekend', 'StartTime', 'EndTime', 'RepeatingDays',
        {RepeatingDates: SCConfig.RepeatingDateProp}, 'RepeatEvery'
    ];

    private static TaskSchedule: any = [
        "ItemId",
        "CreatedBy",
        "CreateDate",
        "Language",
        "Tags",
        "LastUpdatedBy",
        "LastUpdateDate",
        "FromDateTime",
        "ToDateTime",
        "ReserveForEver",
        "Title",
        "TaskSummaryId",
        "TaskDateTime",
        "RelatedEntityName",
        "RelatedEntityId",
        "TaskPercentage",
        "IsOverdue",
        "IsCompleted",
        "Status"
        ];

    public static AllFieldsOfEntity = {
        Client: [
            'ItemId', 'CreatedBy', 'ClientName', 'CreateDate', 'Language', 'Tags', 'LastUpdatedBy', 'Address',
            'Street', 'StreetNo', 'City', 'Zip', 'CountryName', 'FileId', 'DisplayEnable', 'CompanyTypes',
            'ClientIdentifier'
        ],
        User: ['Active', 'CreateDate', 'CreatedBy', 'DisplayName', 'Email', 'EmailVarified', 'Language',
            'LastUpdateDate', 'LastUpdatedBy', 'PhoneNumber', 'Platform', 'ProfileImageUrl', 'Roles',
            'TenantId', 'UserName', 'FirstName', 'LastName', 'UserSignup', 'ExternalProviderUserId',
            'ProfileImageId', 'DefaultPassword', 'EverLoggedIn', 'ItemId', 'Tags', 'CountryCode',
            'Salutation', 'TwoFactorEnabled', 'IsSystemBlocked'],
        Person: ['ItemId', 'CreatedBy', 'Tags', 'DateOfBirth', 'Interests', 'ShortBio', 'Sex',
            'FirstName', 'LastName', 'MiddleName', 'ProfileImage', 'Email', 'DisplayName', 'Countires',
            'ProfileImageId', 'Currency', 'IsVerified', 'Skills', 'Salutation', 'CompanyName',
            'AddressLine1', 'CustomerId', 'AddressLine2', 'Street', 'PostalCode', 'City', 'Country', 'State', 'Nationality',
            'PhoneNumber', 'AccountNumber', 'Designation', 'Organization', 'LastLogInTime', 'LogInCount', 'OfficialPhoneNumber',
            'OfficeCountryCode', 'MarketName', 'ColorCode', 'ContactEmails', 'ContactPhoneNumbers', 'ManagerNr'],
        CountryInfo: ['ItemId', 'Tags', 'Name', 'Code', 'CurrencyCode', 'CurrencyName', 'DialingCode'],
        PlatformDictionary: ['Name', 'Key', 'Value'],
        GdprContent: ['ItemId', 'Language', 'TermAndConditionType', 'TermAndConditionContent', 'SiteId', 'PlatformType',
            'ContentEn', 'ContentDe', 'ContentCz'],
        GdprUser: ['ItemId', 'UserId', 'UserPnr', 'IsMarktforschung', 'IsKundenveranstaltung', 'IsNewsletter', 'IsPost', 'LastUpdateDate'],
        Connection: ['ItemId', 'ParentEntityName', 'ParentEntityID', 'ChildEntityName', 'ChildEntityID'],
        PraxisClient: [
            'ItemId', 'CreatedBy', 'CreateDate', 'Language', 'Tags', 'LastUpdatedBy', 'ClientName', 'ClientNumber', 'MemberPhysicianNetwork', 'WebPageUrl',
            'MedicalSoftware', 'ComputerSystem', {Logo: SCConfig.ImageFields}, {Address: SCConfig.AddressFields}, 'ContactEmail', 'ContactPhone',
            {AdditionalInfos: SCConfig.AdditionalInfoFields}, 'CompanyTypes',
        ],
        PraxisUser: SCConfig.PraxisUserFields,
        PraxisClientCategory: SCConfig.PraxisClientCategoryFields,
        PraxisForm: ['ItemId', 'CreatedBy', 'CreateDate', 'Language', 'Tags', 'LastUpdateDate', 'LastUpdatedBy', 'Title',
            'Description', 'PurposeOfFormKey', 'PurposeOfFormValue', 'TopicKey', 'TopicValue',
            'Qualification', {QuestionsList: SCConfig.PraxisQuestionFields}],
        PraxisTask: ['ItemId', 'CreatedBy', 'CreateDate', 'Language', 'LastUpdateDate', 'LastUpdatedBy',
            'Tags', 'TaskConfigId', 'ClientId', 'CategoryId', 'CategoryName', 'SubCategoryName',
            'TopicValue', 'SubCategoryId', 'TopicKey', 'Task', 'TaskSchedule', 'ControllingMembers', 'ControlledMembers', 'LinearAnswer', 'TaskDate', 'IsOverdue', 'Remarks'],
        PraxisTaskConfig: [
            'ItemId', 'ClientId', 'ClientName', 'CategoryId', 'CategoryName', 'SubCategoryId', 'SubCategoryName',
            'SpecificControllingMembers', 'SpecificControlledMembers', 'TopicKey', 'TopicValue',
            'FormId', 'FormTitle', 'QuestionIds', {TaskTimetable: SCConfig.PraxisTaskTimetable}, {TaskNotification: SCConfig.PraxisTaskNotify}, 'Tags', 'Language'
        ],

    };

    //Copied from web End

    static TaskStatus = {
        PENDING: {
            Key: 'pending',
            Value: 'PENDING'
        },
        COMPLETED: {
            Key: 'completed',
            Value: 'COMPLETED'
        },
    };

    static TaskStatusDefault = SCConfig.TaskStatus.PENDING.Key;

    static CKEditorConfig: any = {
        toolbar: [
            {name: 'clipboard', items: ['NewPage', 'Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo']},
            {name: 'links', items: ['Link', 'Unlink']},
            {
                name: 'basicstyles',
                items: ['FontSize', 'Bold', 'Italic', 'Strike', '-', 'TextColor', 'BGColor', '-']
            },
            {
                name: 'paragraph',
                items: ['NumberedList', 'BulletedList', 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            },
            {name: 'styles', items: ['RemoveFormat', 'Format']}
        ],
        resize_enabled: false,
        extraPlugins: 'colorbutton,colordialog',
        entities: false,
        basicEntities: true,
        entities_greek: false,
        entities_latin: false,
        // height : '25em',
        height: '100%',
        font_names: 'Sans-Regualr',
        format_h5 :{ element : 'h5' ,attributes: { 'class': 'mat-h5' }, styles: { font: '400 25px/25px Lato'}},
        format_h3 :{ element : 'h3' ,attributes: { 'class': 'mat-h3' }, styles: { fontSize: '15px'}},
        format_p :{ element : 'p' , attributes: { 'class': 'mat-caption' } , styles: { fontSize: '13px'}},
        extraCss: ['body {font-family:Sans-Regular;}'],
        font_defaultLabel: 'Times New Roman/Times New Roman, Times, serif;',
        removePlugins: 'elementspath'/*,
        on: {
            // maximize the editor on startup
            'instanceReady': function (evt) {
                evt.editor.resize('100%', document.querySelector('.ckEditor').clientHeight - 64);
            }
        }*/
    };

    static ActionBarConfiguration : any = {
        showMenuButton: false,
        actionItems: [
            /*{
                actionItemIcon: String.fromCharCode(parseInt('0x'+'ebda')),
                actionItemType: 'sync',
                actionItemText: 'Sync',
                visible: true,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f039')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 1',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f1e0')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 2',
                visible: true,
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f064')),
                actionItemType: 'popup',
                actionItemText: 'Log Out',
                visible: true,
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f064')),
                actionItemType: 'popup',
                actionItemText: 'Go to Project',
                visible: true,
                url:'/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f264')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 5',
                visible: true,
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'f263')),
                actionItemType: 'actionBar',
                actionItemText: 'Menu 6',
                visible: true,
                url: null,
                callParentMethod: false
            }*/
        ],
        menuIconSrc: 'res://ic_action_menu_black',
        logo: {
            src: 'res://logo',
            width: '100'
        }
    };

    static ActionBarAuditDetailsConfiguration : any = {
        showMenuButton: false,
        actionItems: [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x'+'ebda')),
                actionItemType: 'sync-details',
                actionItemText: 'Sync',
                visible: true,
                callParentMethod: true
            }
        ]
    };
}
