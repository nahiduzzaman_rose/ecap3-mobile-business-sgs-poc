import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import {MeasurementPointDefaultComponent} from "~/app/measurement-point-list/measurement-point-default/measurement-point-default.component";


const routes: Routes = [
    {
        path: '',
        component: MeasurementPointDefaultComponent,
    }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MeasurementPointListRoutingModule { }
