import { Component, OnInit } from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";

@Component({
  selector: 'ns-measurement-point-default',
  templateUrl: './measurement-point-default.component.html',
  styleUrls: ['./measurement-point-default.component.scss'],
  moduleId: module.id,
})
export class MeasurementPointDefaultComponent implements OnInit {
    actionBarConfiguration: any = SCConfig.ActionBarConfiguration;
  constructor(
      private routerExtensions: RouterExtensions,
  ) { }

  onTap() {
      this.routerExtensions.navigate(['measurementPoint-details', 1], {
          transition: {
              name: "slide"
          }
      });
  }

  ngOnInit() {
  }

}
