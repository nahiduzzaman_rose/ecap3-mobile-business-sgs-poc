import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { MeasurementPointListRoutingModule } from './measurement-point-list-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { MeasurementPointDefaultComponent } from './measurement-point-default/measurement-point-default.component';
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";
import {CoreTemplateModule} from "@ecap3/tns-core-template";


@NgModule({
  declarations: [MeasurementPointDefaultComponent],
  imports: [
      MeasurementPointListRoutingModule,
      NativeScriptCommonModule,
      NativeScriptUIListViewModule,
      CoreTemplateModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MeasurementPointListModule { }
