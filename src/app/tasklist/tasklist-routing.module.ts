import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { TasklistDefaultComponent } from './components/tasklist-default/tasklist-default.component';


const routes: Routes = [
    { 
        path: '', 
        component: TasklistDefaultComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class TasklistRoutingModule { }
