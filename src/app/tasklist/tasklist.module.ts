import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { TasklistDefaultComponent } from './components/tasklist-default/tasklist-default.component';
import { TasklistRoutingModule } from './tasklist-routing.module';
import { CoreTemplateModule } from '@ecap3/tns-core-template';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { PlatformCardModule } from '@ecap3/tns-platform-card';

@NgModule({
    declarations: [TasklistDefaultComponent],
    imports: [
        NativeScriptCommonModule,
        TasklistRoutingModule,
        CoreTemplateModule,
        PlatformCardModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class TasklistModule { }
