import { Component, OnInit } from '@angular/core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { RouterExtensions } from 'nativescript-angular/router';
import { ItemEventData } from 'tns-core-modules/ui/list-view/list-view';
import { SeedDataConstant } from '~/app/sc-config/constant/seed-data.constant';
import { TasklistService } from '../../services/tasklist.service';
import { screen } from "tns-core-modules/platform";
import { BackbuttonConfig } from '@ecap3/tns-core/model/actionBar.interface';


@Component({
    selector: 'ns-tasklist-default',
    templateUrl: './tasklist-default.component.html',
    styleUrls: ['./tasklist-default.component.scss'],
    moduleId: module.id
})
export class TasklistDefaultComponent implements OnInit {
    filterType = null;
    filterTypes = {
        Pending: SCConfig.TaskStatus.PENDING.Key,
        Completed: SCConfig.TaskStatus.COMPLETED.Key,
    };
    currentDate: Date;
    public pageData = SCConfig.Pagination;
    tasks = [];
    actionBarConfiguration : any = SCConfig.ActionBarConfiguration;
    screenwidthDIPs: number;
    screenheightDIPs: number;
    screenRationHeight: number;
    backbuttonConfig: BackbuttonConfig;

    
    constructor(
        private routerExtensions: RouterExtensions,
        private tasklistService: TasklistService
    ) { }

    actionItemClicked(event: any) {
        if (event.actionItemType === 'sync') {
            this.routerExtensions.navigate(["/sync", "task-list", false], {transition: {name: "fade"}, clearHistory: true});
        }
    }

    toggleFilter(filterType: string) {
        console.log(filterType);
        this.filterType = filterType;
    }

    onItemTap(args: ItemEventData) {
        //console.log(`Index: ${args.index}; View: ${args.view}`);x
        const selectedTask = this.tasks[args.index];
        this.routerExtensions.navigate(['task-details',selectedTask.ItemId],{
            transition: {
                name: "slide"
            }
        });
    }

    getTasklist() {
        const response = this.tasklistService.getData(this.pageData);
        this.tasks = response.Results;
    }

    ngOnInit() {
        this.getTasklist()
        this.filterType = SCConfig.TaskStatusDefault;
        this.currentDate = new Date();

        this.screenwidthDIPs =  screen.mainScreen.widthDIPs;
        this.screenheightDIPs =  screen.mainScreen.heightDIPs;
        this.screenRationHeight = (screen.mainScreen.heightDIPs / this.screenheightDIPs);

        console.log('screen class height', this.screenheightDIPs);
        console.log('screen class width', this.screenwidthDIPs);

    }

}
