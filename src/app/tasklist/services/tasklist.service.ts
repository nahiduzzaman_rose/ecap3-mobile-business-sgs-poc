import { Injectable } from '@angular/core';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { SCConfig } from '~/app/sc-config/constant/sc-config.constant';
import { QueryWhereItem, QueryLogicalOperator, Query } from 'nativescript-couchbase-plugin';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TasklistService {

    constructor(
        private localPlatformDataService : LocalPlatformDataService
    ) { }

    formatDate(dateString, type?){
        if(type == "from"){
            var d = new Date(dateString);
            d.setHours(0);
            d.setMinutes(0);
            d.setSeconds(0);
            d.setMilliseconds(0);
            var fromDate = d.toISOString();
            return fromDate;
        }else{
            var d = new Date(dateString);
            d.setHours(23);
            d.setMinutes(59);
            var toDate = d.toISOString();
            return toDate;
        }
    }

    getData(pageData, filterType?): any {
        const date = new Date().toISOString();
        const fromDate = this.formatDate(date, 'from');
        const toDate = this.formatDate(date);
        const filter : QueryWhereItem[] = [
             {
                property: 'EntityName',
                comparison: 'equalTo',
                value: SCConfig.Entity.PraxisTask,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'TaskSchedule.TaskDateTime',
                comparison: 'greaterThanOrEqualTo',
                value: fromDate,
            },
            {
                logical: QueryLogicalOperator.AND,
                property: 'TaskSchedule.TaskDateTime',
                comparison: 'lessThanOrEqualTo',
                value: toDate,
            }
        ];

        if (filterType) {
            filter.push(
                {
                    logical: QueryLogicalOperator.AND,
                    property: 'Status',
                    comparison: 'equalTo',
                    value: filterType,
                }
            );
        }

        const query: Query = {
            select: [],
            where: filter,
            limit: pageData.PageSize,
            order: [{property: 'CreateDate', direction: "desc"}],
            offset: pageData.PageSize * pageData.PageNumber
        };

        const response = this.localPlatformDataService.getPaginateData(query);
        return response;
    }
}
