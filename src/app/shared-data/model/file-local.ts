import {SCConfig} from "~/app/sc-config/constant/sc-config.constant";

export class FileLocal {
    ItemId: string;
    Url?: string;
    Name?: string;
    SystemName?: string;
    LocalPath?: string;
    TypeString?: string;
    EntityName?: string;
    ParentEntityId: string;
    ParentEntityName: string;
    Tags?: any[];
    SyncStatus: boolean;
    isLocalUpload?: boolean;
    RetryCountUpload?: number;
    RetryCountDownload?: number;

    constructor(data?) {
        this.EntityName = SCConfig.Entity.File;
        if (data) {

        }
    }
}
