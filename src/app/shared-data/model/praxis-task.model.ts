import { SyncSetting } from "./sync-setting.model";
import { SCConfig } from "~/app/sc-config/constant/sc-config.constant";

export class PraxisTaskModel {
    ItemId: string;
    Tags: string[];
    LastUpdateDate: string;
    TaskConfigId: string;
    ClientId: string;
    CategoryId: string;
    CategoryName?: any;
    SubCategoryId: string;
    SubCategoryName?: any;
    TopicKey: string;
    TopicValue?: any;
    Task: Task;
    Remarks: string;
    ControllingMembers: string[];
    ControlledMembers: string[];
    LinearAnswer: number;
    TaskDate: string;
    IsOverdue: boolean;
    TaskSchedule: TaskSchedule;

    //Local
    id?: string;
    EntityName?: string;
    SyncSetting?: SyncSetting;
    BulkProcessors?: any[];
    LocalLastUpdateDate?: string;

    constructor(data, showLocalData = false) {
        if (data) {
            SCConfig.AllFieldsOfEntity.PraxisTask.forEach(fieldName => {
                this[fieldName] = data[fieldName] ? data[fieldName] : null;
            });

            this.Task = data['Task'] ? new Task(data['Task']) : null;
            this.TaskSchedule = data['TaskSchedule'] ? new TaskSchedule(data['TaskSchedule']) : null;

            // Local Data
            this.EntityName = SCConfig.Entity.PraxisTask;
            this.BulkProcessors = data['BulkProcessors'] || null;
            this.LocalLastUpdateDate = data['LocalLastUpdateDate'] || null;
        }
    }
}

export class Task {
    QuestionId: string;
    Title: string;
    Description: string;
    QuestionTypeKey: string;
    QuestionTypeValue: string;
    Remarks: string;
    QuestionOptions?: any;
    LinearScaleQuestion: LinearScaleQuestion;
    FileUploadQuestion: FileUploadQuestion;

    constructor(data) {
        if (data) {
            this.QuestionId = data['QuestionId'] ? data['QuestionId'] : null;
            this.Title = data['Title'] ? data['Title'] : null;
            this.Description = data['Description'] ? data['Description'] : null;
            this.QuestionTypeKey = data['QuestionTypeKey'] ? data['QuestionTypeKey'] : null;
            this.QuestionTypeValue = data['QuestionTypeValue'] ? data['QuestionTypeValue'] : null;
            this.Remarks = data['Remarks'] ? data['Remarks'] : null;
            this.QuestionOptions = data['QuestionOptions'] ? data['QuestionOptions'] : null;
            this.LinearScaleQuestion = data['LinearScaleQuestion'] ? data['LinearScaleQuestion'].map(x => new LinearScaleQuestion(x)) : null;
            this.FileUploadQuestion = data['FileUploadQuestion'] ? data['FileUploadQuestion'].map(x => new FileUploadQuestion(x)) : null;
        }
    }
}

export class LinearScaleQuestion {
    From: number;
    To: number;
    Step: number;
    FromLabel: string;
    ToLabel: string;
    Direction: string;

    constructor(data) {
        if (data) {
            this.From = data['From'] ? data['From'] : null;
            this.To = data['To'] ? data['To'] : null;
            this.Step = data['Step'] ? data['Step'] : null;
            this.FromLabel = data['FromLabel'] ? data['FromLabel'] : null;
            this.ToLabel = data['ToLabel'] ? data['ToLabel'] : null;
            this.Direction = data['Direction'] ? data['Direction'] : null;
        }
    }
}

export class FileUploadQuestion {
    FileTypes: any[];
    NumberOfFiles: number;
    MaximumFileSize: string;
    constructor(data) {
        if (data) {
            this.FileTypes = data['FileTypes'] ? data['FileTypes'] : null;
            this.NumberOfFiles = data['NumberOfFiles'] ? data['NumberOfFiles'] : null;
            this.MaximumFileSize = data['MaximumFileSize'] ? data['MaximumFileSize'] : null;
        }
    }
}

export class TaskSchedule {
    FromDateTime: string;
    ToDateTime: string;
    ReserveForEver: boolean;
    Title?: any;
    TaskSummaryId: string;
    TaskDateTime: string;
    RelatedEntityName: string;
    RelatedEntityId: string;
    TaskPercentage: number;
    IsOverdue: boolean;
    IsCompleted: boolean;
    Status?: any;
    ItemId: string;
    Language: string;
    LastUpdateDate: string;
    LastUpdatedBy: string;
    Tags: string[];
    TenantId: string;
    IsMarkedToDelete: boolean;

    constructor(data) {
        if (data) {
            this.FromDateTime = data['FromDateTime'] ? data['FromDateTime'] : null;
            this.ToDateTime = data['ToDateTime'] ? data['ToDateTime'] : null;
            this.ReserveForEver = data['ReserveForEver'] ? data['ReserveForEver'] : null;
            this.Title = data['Title'] ? data['Title'] : null;
            this.TaskSummaryId = data['TaskSummaryId'] ? data['TaskSummaryId'] : null;
            this.TaskDateTime = data['TaskDateTime'] ? data['TaskDateTime'] : null;
            this.RelatedEntityName = data['RelatedEntityName'] ? data['RelatedEntityName'] : null;
            this.RelatedEntityId = data['RelatedEntityId'] ? data['RelatedEntityId'] : null;
            this.TaskPercentage = data['TaskPercentage'] ? data['TaskPercentage'] : null;
            this.IsOverdue = data['IsOverdue'] ? data['IsOverdue'] : null;
            this.IsCompleted = data['IsCompleted'] ? data['IsCompleted'] : null;
            this.Status = data['Status'] ? data['Status'] : null;
            this.ItemId = data['ItemId'] ? data['ItemId'] : null;
            this.Language = data['Language'] ? data['Language'] : null;
            this.LastUpdateDate = data['LastUpdateDate'] ? data['LastUpdateDate'] : null;
            this.LastUpdatedBy = data['LastUpdatedBy'] ? data['LastUpdatedBy'] : null;
            this.Tags = data['Tags'] ? data['Tags'] : null;
            this.TenantId = data['TenantId'] ? data['TenantId'] : null;
            this.IsMarkedToDelete = data['IsMarkedToDelete'] ? data['IsMarkedToDelete'] : null;
        }
    }
}







