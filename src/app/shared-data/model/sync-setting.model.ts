export class SyncSetting {
    NeedToSync: boolean;
    InitDetailSynced: boolean;
    LastSyncReqStatus: boolean;
    LastReqEndTime: string;

    constructor(data?) {
        this.NeedToSync = false;
        this.InitDetailSynced = false;
        this.LastSyncReqStatus = false;
        this.LastReqEndTime = '';
    }
}
