export interface IUserInfo {
    email: string;
    displayName: string;
    phoneNumber: string;
    firstName: string;
    lastName: string;
}