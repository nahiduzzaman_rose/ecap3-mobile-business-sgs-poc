export class SyncLog {
    ItemId?: string;
    Tags?: string[];
    EntityName?: string;
    BulkProcessors: any[];

    constructor(data?) {
        this.ItemId = null;
        this.Tags = [];
        this.EntityName = null;
        this.BulkProcessors = [];
    }
}
