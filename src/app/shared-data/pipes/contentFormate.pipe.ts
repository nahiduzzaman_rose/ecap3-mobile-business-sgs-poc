import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "ContentFormate" 
})

export class ContentFormating implements PipeTransform {
   transform(content: string): string {
       return content ? content.replace(/[<]br[^>]*[>]/gi, "") : "";
   }
}