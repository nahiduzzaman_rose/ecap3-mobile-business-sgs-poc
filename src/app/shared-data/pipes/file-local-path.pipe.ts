import { Pipe, PipeTransform } from '@angular/core';
import * as fs from "tns-core-modules/file-system";
import {LocalPlatformDataService} from "@ecap3/tns-core";
import {FileLocal} from "~/app/shared-data/model/file-local";

@Pipe({ name: 'fileLocalPath' })
export class FileLocalPathPipe implements PipeTransform {
    constructor(
        private localPlatformDataService : LocalPlatformDataService) {

    }

    transform(fileId: string): string {
        let localPath = 'res://company';
        if (fileId) {
            const file = <FileLocal>this.localPlatformDataService.getById(fileId);
            if (file && file.LocalPath && fs.File.exists(file.LocalPath)) {
                localPath = file.LocalPath;
            }
        }

        return localPath;
    }
}
