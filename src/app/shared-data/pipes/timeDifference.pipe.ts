import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'timeDifference'
})
export class TimeDifferencePipe implements PipeTransform{
    transform(args: any) {
        let time = +new Date(args.ScheduleEndDateTime) - +new Date(args.ScheduleStartDateTime);
        return time/60000;
    }
}
