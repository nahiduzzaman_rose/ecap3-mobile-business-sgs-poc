import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';


@Pipe({
    name: 'findValue'
})
export class FindValuePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        const result = _.find(args, { "key": value }).value;
        return result;
    }
}
