import {
    Component,
    OnInit,
    AfterViewInit,
    Input,
    ViewChild,
    ElementRef,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from "@angular/core";
import { Color } from 'tns-core-modules/color';
import { TruncatePipe } from "~/app/shared-data/pipes/truncate.pipe";
import { ContentFormating } from "~/app/shared-data/pipes/contentFormate.pipe";

@Component({
    selector: "HtmlViewer",
    moduleId: module.id,
    templateUrl: 'html-viewer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HtmlViewerComponent implements OnInit, AfterViewInit {

    @Input('html') html: string;

    @Input('textSize') textSize: number;

    @Input('textColor') textColor: string;

    @Input('textLinkColor') textLinkColor: string;

    @Input('truncateSize') truncateSize: number;

    @Input('truncateSuffix') truncateSuffix: string = '...';

    @ViewChild('htmlView',{static: false}) htmlView: ElementRef;

    htmlString: string;


    constructor(private truncate: TruncatePipe,
        private contentFormating: ContentFormating,
    private cdrf: ChangeDetectorRef,) {
        this.htmlString = '';
    }

    ngOnInit(): void {
        // this.htmlString = this.html ? this.html : '';
        // if (this.truncateSize > 0) {
        //     this.htmlString = this.truncate.transform(this.htmlString, [this.truncateSize.toString(), this.truncateSuffix]);
        // }
        // this.updateDetectChange();
    }


    ngAfterViewInit() {
        //const formatedContent = this.contentFormating.transform(this.html);
        this.html = this.contentFormating.transform(this.html);
        this.htmlString = this.html ? this.html : '';
        if (this.truncateSize > 0) {
            this.htmlString = this.truncate.transform(this.htmlString, [this.truncateSize.toString(), this.truncateSuffix]);
        }
        this.updateDetectChange();
        this.onLoaded();
    }

    checkLastNewLine(questionTitle : string) {
        const lastFour = questionTitle.substr(questionTitle.length - 4);
        if(lastFour == '<br>') {
            const trimmedQuestionTitle = questionTitle.slice(0, -4);
            return trimmedQuestionTitle;
        } else {
            return questionTitle;
        }
    }


    onLoaded() {
        let htmlView = this.htmlView.nativeElement.android;

        if (htmlView) {
            htmlView.setTextSize(this.textSize);
            htmlView.setTextColor(this.textColor ? new Color(this.textColor).android : new Color('#999999').android);
            htmlView.setLinkTextColor(this.textLinkColor ? new Color(this.textLinkColor).android : new Color('#0000FF').android);

            this.updateDetectChange();
        }
    }

    updateDetectChange() {
        try {
            if (!this.cdrf['destroyed']) {
                this.cdrf.detectChanges();
                this.cdrf.markForCheck();
            }
        } catch (e) {
            console.error('updateDetectChange', e);
        }
    }
}
