import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { FileLocalPathPipe } from './pipes/file-local-path.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { HtmlViewerComponent } from './components/html-viewer.component';
import { FindValuePipe } from './pipes/find-value.pipe';
import { ContentFormating } from './pipes/contentFormate.pipe';
import {TimeDifferencePipe} from './pipes/timeDifference.pipe';


@NgModule({
    declarations: [
        FileLocalPathPipe,
        TruncatePipe,
        HtmlViewerComponent,
        FindValuePipe,
        ContentFormating,
        TimeDifferencePipe

    ],
    providers: [
        TruncatePipe,
        ContentFormating
    ],
    imports: [
        NativeScriptCommonModule
    ],
    exports: [
        FileLocalPathPipe,
        TruncatePipe,
        FindValuePipe,
        HtmlViewerComponent,
        ContentFormating,
        TimeDifferencePipe
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedDataModule { }
