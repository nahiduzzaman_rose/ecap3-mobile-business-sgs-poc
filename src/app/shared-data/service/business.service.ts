import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {AppSettingsProvider} from "@ecap3/tns-core";
import {Observable} from "rxjs";
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BusinessService {
    header: any = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    commonOptions: any = { headers: this.header, withCredentials: true, observe: 'response' };


    constructor(
        private http: HttpClient,
        private appSettingsProvider: AppSettingsProvider,
    ) { }

    getSiteLocations(siteId: string): Observable<any> {
        const url: any = this.appSettingsProvider.getAppSettings().ClmApi + '/ClmQuery/GetSiteLocations';
        const params = new HttpParams().append('siteId', siteId);
        const options = {...this.commonOptions};
        options['params'] = params;
        return this.http.get(url, options).pipe(map((response) => {
            return response;
        }));
    }

    getShipmentTime(endTime: any , startTime: any) {
        debugger;
        let time = +new Date(endTime) - +new Date(startTime);
        console.log('Time', time);
        return time/60000;
    }
}
