import { ReactiveFormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { EcapCoreModule, NavigationProvider, AppSettingsProvider } from "@ecap3/tns-core";
import { CoreTemplateModule } from "@ecap3/tns-core-template";
import { RootRoutingModule } from "./root-routing.module";
import { RootDefaultComponent } from "./components/root-default/root-default.component";
import { DropDownModule } from "nativescript-drop-down/angular";
import { navigations } from "./navigation";
import { AppSettingService } from "./services/configs/app-setting.service";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { TNSCheckBoxModule } from '@nstudio/nativescript-checkbox/angular';
import { ModalDialogService } from "nativescript-angular";
import { SiteidModalViewComponent } from './components/siteid-modal-view/siteid-modal-view.component';
import { GraphQLModule } from '@ecap3/tns-platform-graphql';
import { environment } from '../environments/environment';

export const config: any = {
    queryUri: environment['GQLQueryUri'],
    mutationUri: environment['GQLMutationUri']
};

@NgModule({
    bootstrap: [
        RootDefaultComponent
    ],
    imports: [
        NativeScriptModule,
        RootRoutingModule,
        ReactiveFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptLocalizeModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        DropDownModule,
        EcapCoreModule.forRoot(),
        GraphQLModule.forRoot(config),
        CoreTemplateModule,
        TNSCheckBoxModule,
        TNSFontIconModule.forRoot({
            'fa': './../fonts/font-awesome.css',
            'md-icon': './../fonts/material-icons.css'
        })
    ],
    providers: [
        AppSettingService,
        ModalDialogService,
    ],
    declarations: [
        RootDefaultComponent,
        SiteidModalViewComponent
    ],
    entryComponents: [
        SiteidModalViewComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RootModule {

    constructor(
        private navigationProvider: NavigationProvider,
        private appSettingsProvider: AppSettingsProvider,
        private appSettingService: AppSettingService
    ) {
        console.log("============= RootModule SET Navigations ============");
        this.navigationProvider.setCurrentNavigations(navigations);

        console.log("============= RootModule SET AppSettings ===========");
        this.appSettingsProvider.setAppSettings(this.appSettingService.getAppSettings());
    }
}
