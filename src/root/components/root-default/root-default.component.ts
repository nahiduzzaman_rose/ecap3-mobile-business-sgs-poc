import {Component, OnInit, OnDestroy, ViewContainerRef} from "@angular/core";
import {Subscription} from "rxjs";
import {RouterExtensions} from "nativescript-angular/router";
import {registerElement} from "nativescript-angular/element-registry";
import {DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition} from "nativescript-ui-sidedrawer";
import {NavigationProvider, UserInfoService, EcapLoginService, LocalPlatformDataService, AppSettingsProvider} from "@ecap3/tns-core";
import { IUserInfo } from "../../models/user.info";
import { navigations } from "~/root/navigation";
import {Page} from "tns-core-modules/ui/page";
import { CardView } from '@nstudio/nativescript-cardview';
import { Video } from 'nativescript-videoplayer';
import * as permissions from "nativescript-permissions";
import { QueryWhereItem, Query } from "nativescript-couchbase-plugin";
import { SCConfig } from "~/app/sc-config/constant/sc-config.constant";
import * as firebase from "nativescript-plugin-firebase";
import { device } from "tns-core-modules/platform";
import { ModalDialogOptions, ModalDialogService } from "nativescript-angular";
import {NavigationExtras} from "@angular/router";
import { CommonService } from "~/root/services/common/common.service";
import { SiteidModalViewComponent } from "../siteid-modal-view/siteid-modal-view.component";
import * as app from "tns-core-modules/application";
import {connectionType, getConnectionType} from "tns-core-modules/connectivity";

registerElement('AnimatedCircle', () => require('nativescript-animated-circle').AnimatedCircle);
registerElement('CardView', () => CardView);
registerElement("VideoPlayer", () => Video);

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "root-default.component.html"
})
export class RootDefaultComponent implements OnInit, OnDestroy{

    userInfo: IUserInfo;
    navigations: any[];
    currentNavigations: any[];
    showNavigation = true;
    subscription: Subscription;
    private _sideDrawerTransition: DrawerTransitionBase;

    device_css: any;
    siteData: any[];
    site: any;
    userDetails: any = null;

    constructor(
        private routerExtensions: RouterExtensions,
        private modal: ModalDialogService,
        private navigationProvider: NavigationProvider,
        public ecapLoginService: EcapLoginService,
        private userInfoService: UserInfoService,
        private appSettingsProvider: AppSettingsProvider,
        private localPlatformDataService: LocalPlatformDataService,
        private commonService: CommonService,
        private vcRef: ViewContainerRef,
        private page: Page
    ) {
        console.log("!!! Fire RootDefaultComponent !!!");
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.navigations = navigations;
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    ngOnInit(): void {
        console.log("root default");
        // this.page.actionBarHidden = true;
        this.device_css = device.deviceType + '_class';



        this.navigationProvider.getCurrentNavigationObservable().subscribe(response => {
            this.setEventListenerForUserInfo();
            this.currentNavigations = response;
            this.getSites();
        });
    }

    getSites(){
        this.commonService.getSites().subscribe((response) => {
            this.siteData = response;
            if(this.siteData && this.siteData.length){
                this.site = this.siteData[0].SiteName;
                this.commonService.changeSiteId(this.siteData[0].ItemId);
            }
        })
    }

    selectSite(item, data){
        console.log(item, data);
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        //this.subscription.unsubscribe();
    }

    // Set Event Listener
    setEventListenerForUserInfo() {
        console.log("===== GetUpdatedUserData ==============");
        if (this.userInfoService.getUserInfo()) {
            //this.showNavigation = true;
            this.userInfoService.updateUserDetailsObserved();
            this.userDetails = this.userInfoService.getUserInfo();
            let dbName = this.appSettingsProvider.getAppSettings().LocalDBName;
            this.localPlatformDataService.initDatabase(dbName + '_' + this.userDetails.UserId);
            console.log('Initialized Couchbase database: ', dbName + '_' + this.userDetails.UserId);
            if (this.userDetails && this.userDetails.UserId) {
                firebase.subscribeToTopic(this.userDetails.UserId).then(() => {
                    console.log("Firbase subscribe to userid");
                });
            }

            console.log("============ RootDefaultComponent GetUserInfoEvent ===========");
            permissions
                .requestPermission([
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ]).then(output => {
                this.initiateRoute();
            });
        }
    }

    isSynced(): boolean {
        const filter : QueryWhereItem[] = [
            {
                property: 'EntityName',
                comparison: 'equalTo',
                value:  SCConfig.Entity.PraxisTaskSyncSettings
            }
        ];

        const query: Query = <Query>{where: filter};
        const results = this.localPlatformDataService.getBySqlFilter(query);
        const resultsSingle = results && results.length > 0 ? results[0] : null;

        return !!resultsSingle;
    }

    logoutFromPlatform(){
        if (getConnectionType() === connectionType.none) {
            alert("Internet disconnected, please try again later.");
            return;
        }
        this.ecapLoginService.logoutFromPlatform().subscribe(() => {
            firebase.unsubscribeFromTopic(this.userDetails.UserId).then(() => {
                console.warn("Firebase unsubscribed from topic user id", this.userDetails.UserId)
            });
            console.log("Successfully logout!!!");
        },(error) => {
            if (error.status === 401) {
                this.ecapLoginService.clearAllCacheData();
            }
        });
    };

    logoutEvent(event){
        console.log('logoutEvent', event);
        this.logoutFromPlatform();
    }

    // Goto Initial route
    initiateRoute() {
        this.routerExtensions.navigate(["/measurementPoint"]);

        /* if (!this.isSynced()) {
            this.routerExtensions.navigate(["/sync", "task-list", true], {transition: {name: "fade"}, clearHistory: true});
        } else {
            this.routerExtensions.navigate(["/tasklist"]);
        } */
    }

    showModal() {
        let option: ModalDialogOptions = {
            context: {
                data: this.siteData
            },
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(SiteidModalViewComponent, option).then(response => {
            if(response && response.SiteName) {
                this.site = response.SiteName;
                this.commonService.changeSiteId(response.ItemId);
                const sideDrawer = <RadSideDrawer>app.getRootView();
                sideDrawer.closeDrawer();
            }
        });
    }
 }
