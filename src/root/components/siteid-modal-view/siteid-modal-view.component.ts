import { ModalDialogParams } from 'nativescript-angular/modal-dialog';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ItemEventData} from "ui/list-view";

@Component({
    selector: 'ns-siteid-modal-view',
    templateUrl: './siteid-modal-view.component.html',
    styleUrls: ['./siteid-modal-view.component.scss'],
    moduleId: module.id,
})
export class SiteidModalViewComponent implements OnInit {
    siteData: any = [];
    currentShipmentId: any;
    constructor(
        private activatedRoute: ActivatedRoute,
        private params: ModalDialogParams
    ) {
        this.siteData = params.context.data;
    }

    public onTap(args: ItemEventData) {
        const selectedTask = this.siteData[args.index];
        this.params.closeCallback(selectedTask);
    }

    public onClose() {
        this.params.closeCallback();
    }

    ngOnInit() {
        //this.currentShipmentId = this.activatedRoute.snapshot.params['ItemId'];
        // this.getSiteLocations(this.siteId);
    }

}
