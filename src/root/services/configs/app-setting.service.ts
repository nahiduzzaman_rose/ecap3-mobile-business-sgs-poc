import { Injectable } from '@angular/core';
import { IAppSettings } from "@ecap3/tns-core";
import { environment } from "~/environments/environment";

@Injectable()
export class AppSettingService {

    constructor() {
    }

    getAppSettings(): any {

        var appSettings: any = {
            Apps: environment.Apps,
            Token: environment.Token,
            Security: environment.Security,
            Captcha: environment.Captcha,
            Identity: environment.Identity,
            Navigation: environment.Navigations,
            TenantId: environment.TenantId,
            Origin: environment.Origin,
            DomainName: environment.DomainName,
            SubDomainName: environment.SubDomainName,
            Authentication: environment.Authentication,
            DataCore: environment.DataCore,
            IsMock: false,
            LocalDBName: environment.LocalDBName,
            Storage: environment.Storage,
            SyncUrl: environment.SyncUrl,
            GQLQueryUri: environment.GQLQueryUri,
            GQLMutationUri: environment.GQLMutationUri,
            SyncAdapterId: environment.SyncAdapterId,
            ClmApi: environment.ClmApi
        }

        return appSettings;
    }
}
