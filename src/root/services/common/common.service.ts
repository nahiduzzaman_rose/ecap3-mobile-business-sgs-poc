import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PlatformDataService, SqlQueryBuilderService } from '@ecap3/tns-core';
import { BusinessService } from '~/app/shared-data/service/business.service';
import { GraphqlDataService } from '@ecap3/tns-platform-graphql';
import { GqlQueryBuilderService } from '@ecap3/tns-core/service/query-builder/gql-query-builder.service';
import { map, take } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class CommonService {

    private siteId = new BehaviorSubject('');
    currentSiteId = this.siteId.asObservable();

    constructor(
        private platformDataService: PlatformDataService,
        private sqlQueryBuilderService: SqlQueryBuilderService,
        private businessService : BusinessService,
        private graphqlDataService : GraphqlDataService,
        private gqlQueryBuilderService : GqlQueryBuilderService
    ) { }

    changeSiteId(siteId: string) {
        this.siteId.next(siteId)
    }

    getSites(){
        const filters = [];

        filters.push({
            property: 'Tags',
            operator: 'contains',
            value: "Is-Valid-ProjectSite"
        });

        const fields = ['SiteName', 'ProjectName'];
        const query = this.sqlQueryBuilderService.prepareQuery("ProjectSite", fields, filters,
            'SiteName' + ' __' + 'asc', 0, 100);

        return this.platformDataService.getBySqlFilter('ProjectSite', query).pipe(map((response: Response) => {

            return response.body['Results'];
        }));

        /* const filters = [];
        const  orderBy = 'SiteName: 1';
        const  pageNumber = 0;
        const  pageSize = 100;
        filters.push({
            operator: "raw",
            value: `'Tags': { $in: [ 'Is-Valid-ProjectSite' ] }` 
        });

        const fields = ['ItemId', 'SiteName', 'ProjectName'];
      
        return this.graphqlDataService.watchQuery<any>({
            query: this.gqlQueryBuilderService.prepareQuery('ProjectSite', fields, filters, orderBy , pageNumber, pageSize),
        })
            .valueChanges
            .pipe(
                take(1),
                map(result => {
                    console.warn("Site found");
                    if (result.data && result.data.ProjectSites && result.data.ProjectSites.Data) {
                        return result.data.ProjectSites.Data;
                    }
                    return null;
                })
            ); */
    }
}
