import { INavigationItem, NavigationType } from "@ecap3/tns-core/model/navigation.interface";

export const navigations : INavigationItem[] = [
    /* {
        'id': 'dashboard',
        'title': 'Dashboard',
        'translate': 'DASHBOARD',
        'type': NavigationType.ITEM,
        'url': '/dashboard',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    }, */
    /*{
        'id': 'taslklist',
        'title': 'Tasks',
        'translate': 'TASKS',
        'type': NavigationType.ITEM,
        'url': '/tasklist',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    },
    {
        'id': 'employee',
        'title': 'Employees',
        'translate': 'EMPLOYEES',
        'type': NavigationType.ITEM,
        'url': '/employee',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    },*/
    // {
    //     'id': 'shipments',
    //     'title': 'Shipments',
    //     'translate': 'SHIPMENTS',
    //     'type': NavigationType.ITEM,
    //     'url': '/shipments',
    //     'icon': String.fromCharCode(parseInt('0x'+'f042')),
    //     'iconType': 'SLIcon',
    //     'isVisible': true,
    //     'isHidden': false
    // },
    {
        'id': 'platform.login',
        'title': 'Log out',
        'translate': 'LOG_OUT',
        'type': 'logout',
        'url': '',
        'icon': String.fromCharCode(parseInt('0x'+'f045')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    },
    {
        'id': 'clear-data',
        'title': 'Clear Data',
        'translate': 'Clear Data',
        'type': 'item',
        'url': '/sync/clear-data',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    },
    {
        'id': 'project',
        'title': 'Project',
        'translate': 'Project',
        'type': 'item',
        'url': '/project',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false
    }
];
