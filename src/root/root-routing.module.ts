import { NgModule } from "@angular/core";
import { Routes, PreloadAllModules } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthGuard } from "@ecap3/tns-core";

// const routes: Routes = [
//     { path: "", redirectTo: "/dashboard", pathMatch: "full" },
//     { path: "dashboard", loadChildren: "~/app/dashboard/dashboard.module#DashboardModule" }
// ];

const routes: Routes = [
    { path: "", redirectTo: "/measurementPoint", pathMatch: "full" },
    {
        path: "login",
        loadChildren: "~/app/login/login.module#LoginModule",
        data: {
            requiredFeature: 'login',
            authFailRedirection: '/login'
        }
    },
    {
        path: "measurementPoint",
        loadChildren: "~/app/measurement-point-list/measurement-point-list.module#MeasurementPointListModule",
        data: {
            requiredFeature: 'login',
            authFailRedirection: '/login'
        }
    },
    {
        path: "measurementPoint-details/:ItemId",
        loadChildren: "~/app/measurement-point-details/measurement-point-details.module#MeasurementPointDetailsModule",
        data: {
            requiredFeature: 'login',
            authFailRedirection: '/login'
        }
    },
    {
        path: "project",
        loadChildren: "~/app/project/project.module#ProjectModule",
        canActivate: [AuthGuard],
        data: {
            requiredFeature: 'clmlogistics-mobile',
            authFailRedirection: '/login'
        }
    }
    // },
    // {
    //     path: "shipments",
    //     loadChildren: "~/app/shipments/shipments.module#ShipmentsModule",
    //     canActivate: [AuthGuard],
    //     data: {
    //         requiredFeature: 'clmlogistics-mobile',
    //         authFailRedirection: '/login'
    //     }
    // },
    // {
    //     path: "shipment-details/:ItemId",
    //     loadChildren: "~/app/shipment-details/shipment-details.module#ShipmentDetailsModule",
    //     canActivate: [AuthGuard],
    //     data: {
    //         requiredFeature: 'clmlogistics-mobile',
    //         authFailRedirection: '/login'
    //     }
    // },
    // {
    //     path: "shipments/:ItemId",
    //     loadChildren: "~/app/shipments/shipments.module#ShipmentsModule",
    //     canActivate: [AuthGuard],
    //     data: {
    //         requiredFeature: 'clmlogistics-mobile',
    //         authFailRedirection: '/login'
    //     }
    // },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [NativeScriptRouterModule]
})
export class RootRoutingModule {

}
