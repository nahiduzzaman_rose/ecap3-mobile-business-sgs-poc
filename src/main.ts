// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
// import { setCssFileName, start as applicationStart } from "tns-core-modules/application";
// setCssFileName("theme-style/styles.sass");

import { RootModule } from "./root/root.module";
var nsPlatformCss = require("nativescript-platform-css");
nsPlatformCss.sizeGroupings([1280,1024,800,600,540,480,400,393,360,320]);
platformNativeScriptDynamic({createFrameOnBootstrap: false}).bootstrapModule(RootModule);