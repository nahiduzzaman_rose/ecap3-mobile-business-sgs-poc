export const data = [
    {
        "tutorial": {
            "age": 46,
            "children": [
                {
                    "age": 17,
                    "fname": "Aiden",
                    "gender": "m"
                },
                {
                    "age": 2,
                    "fname": "Bill",
                    "gender": "f"
                }
            ],
            "email": "dave@gmail.com",
            "fname": "Dave",
            "hobbies": [
                "golf",
                "surfing"
            ],
            "lname": "Smith",
            "relation": "friend",
            "title": "Mr.",
            "type": "contact"
        }
    },
    {
        "tutorial": {
            "age": 46,
            "children": [
                {
                    "age": 17,
                    "fname": "Xena",
                    "gender": "f"
                },
                {
                    "age": 2,
                    "fname": "Yuri",
                    "gender": "m"
                }
            ],
            "email": "earl@gmail.com",
            "fname": "Earl",
            "hobbies": [
                "surfing"
            ],
            "lname": "Johnson",
            "relation": "friend",
            "title": "Mr.",
            "type": "contact"
        }
    },
    {
        "tutorial": {
            "age": 18,
            "children": null,
            "email": "fred@gmail.com",
            "fname": "Fred",
            "hobbies": [
                "golf",
                "surfing"
            ],
            "lname": "Jackson",
            "relation": "coworker",
            "title": "Mr.",
            "type": "contact"
        }
    },
    {
        "tutorial": {
            "age": 20,
            "email": "harry@yahoo.com",
            "fname": "Harry",
            "lname": "Jackson",
            "relation": "parent",
            "title": "Mr.",
            "type": "contact"
        }
    },
    {
        "tutorial": {
            "age": 56,
            "children": [
                {
                    "age": 17,
                    "fname": "Abama",
                    "gender": "m"
                },
                {
                    "age": 21,
                    "fname": "Bebama",
                    "gender": "m"
                }
            ],
            "email": "ian@gmail.com",
            "fname": "Ian",
            "hobbies": [
                "golf",
                "surfing"
            ],
            "lname": "Taylor",
            "relation": "cousin",
            "title": "Mr.",
            "type": "contact"
        }
    },
    {
        "tutorial": {
            "age": 40,
            "contacts": [
                {
                    "fname": "Fred"
                },
                {
                    "fname": "Sheela"
                }
            ],
            "email": "jane@gmail.com",
            "fname": "Jane",
            "lname": "Edwards",
            "relation": "cousin",
            "title": "Mrs.",
            "type": "contact"
        }
    }
]
