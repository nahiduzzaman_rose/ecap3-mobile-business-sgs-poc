import { microServiceVersions } from "./micro.service.version";

export const environment = {
    isMock: false,
    production: false,

    TenantId: "8139EEA6-469A-40AB-92B7-F4DC33DA2D06",
    Client_ID: "8139EEA6-469A-40AB-92B7-F4DC33DA2D06",
    DomainName: "fakedomain.com",
    SubDomainName: "http://clmtestweb3.fakedomain.com",
    Origin: "http://clmtestweb3.fakedomain.com",
    LocalDBName: "clm-logistics-db",
    SyncAdapterId: "SC.SyncAdapters.Business.PraxisMonitor",
    
    // Token: 'http://Security.fakedomain.com/security/token',
    // Security: "http://Security.fakedomain.com/Security/",
    Token: `http://identity.fakedomain.com/${microServiceVersions.identity.dev}identity/token`,
    Captcha: `http://captcha.fakedomain.com/Captcha/CaptchaCommand/`,
    Security: `http://uam.fakedomain.com/${microServiceVersions.uam.dev}UserAccessManagement/`,
    Authentication: `http://identity.fakedomain.com/${microServiceVersions.identity.dev}identity/Authentication/`,
    Apps: `http://Catalogue.fakedomain.com/${microServiceVersions.appcatalogue.dev}AppCatalogue/AppManagerQuery/GetApps`,
    Navigations: `http://Tenant.fakedomain.com/${microServiceVersions.tenant.dev}TenantManager/SiteManagerQuery/GetNavigations`,
    Identity: `http://Tenant.fakedomain.com/${microServiceVersions.tenant.dev}TenantManager/TenantManagerQuery/GetIdentity`,
    Sites: `http://Tenant.fakedomain.com/${microServiceVersions.tenant.dev}TenantManager/SiteManagerQuery/GetSites`,

    DataCore: `http://pds.fakedomain.com/${microServiceVersions.pds.dev}AppSuiteDataCore/`,
    GQLQueryUri: `http://graphqlquery.fakedomain.com/${microServiceVersions.graphQlQuery.dev}graphql`,
    GQLMutationUri: `http://graphqlcommand.fakedomain.com/${microServiceVersions.graphQlCommand.dev}graphql`,

    AggregateService: `http://aggregate.fakedomain.com/${microServiceVersions.aggregator.dev}Service-Aggregator/ServiceAggrigation/`,
    NotificationHubName: "NotifierServerHub",
    NotificationService: `http://Notification.fakedomain.com/${microServiceVersions.notification.dev}`,
    StorageBaseUrl: "https://cdn.selise.biz/",
    StorageImageUrl: "grandbasel/assets/",
    StorageFlagUrl: "common/flag/icon/",
    CdnTenantBaseUrl: "https://cdn.selise.biz/slnetwork/",
    StaticResourceCDN: "https://stg-ecap.s3.eu-central-1.amazonaws.com/",
    SecureStorage: "https://securestorage.fakedomain.com/StorageService/",
    Storage: `http://storage.fakedomain.com/${microServiceVersions.storageservice.dev}StorageService/`,
    Test:`test`,
    
    // EmailService: `http://mail.fakedomain.com/${microServiceVersions.mailservice.dev}MailService`,
    // MessaginService: `http://MessagingService.fakedomain.com/${microServiceVersions.messagingservice.dev}MessagingService/`,
    
    // DictionaryService: `http://dictionary.fakedomain.com/${microServiceVersions.dictionaryservice.dev}DictionaryService/`,
    
    // PdfGeneratorService: `http://pdfgenerator.fakedomain.com/${microServiceVersions.pdfgenerator.dev}PdfGeneratorService/`,
    // TemplateEngineService: "http://templateengine.fakedomain.com/TemplateEngineService/",
    
    PersonFields: [
        "ItemId", "CreatedBy", "Tags", "DateOfBirth", "Interests", "ShortBio", "Sex",
        "FirstName", "LastName", "MiddleName", "ProfileImage", "Email", "DisplayName", "Countires",
        "ProfileImageId", "Currency", "IsVerified", "Skills", "Salutation", "CompanyName",
        "AddressLine1", "CustomerId", "AddressLine2", "Street", "PostalCode", "City", "Country", "State", "Nationality",
        "PhoneNumber", "AccountNumber", "Designation", "Organization", "LastLogInTime", "LogInCount", "OfficialPhoneNumber",
        "OfficeCountryCode", "MarketName", "ColorCode", "ContactEmails", "ContactPhoneNumbers", "ManagerNr"
    ],
    SyncUrl: `http://sync.fakedomain.com/${microServiceVersions.syncservice.dev}DataSyncApi/`, /* Dev Server */
    // SyncUrl: `http://stg-sync.selise.biz/${microServiceVersions.syncservice.dev}DataSyncApi/`, /* Staging Server */
    // SyncUrl: `http://172.16.0.119:7070/DataSyncApi/`, /* Kawser PC */
    // SyncUrl: `http://172.16.0.123:7070/DataSyncApi/`, /* Nazmul PC */
    // SyncUrl : `http://172.16.0.75:7070/DataSyncApi/` /*Rose PC*/
    ClmApi: 'http://clmbusiness.fakedomain.com/ClmBusinessService',

};