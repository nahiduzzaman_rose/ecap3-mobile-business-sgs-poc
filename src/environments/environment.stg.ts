    import { microServiceVersions } from "./micro.service.version";

export const environment = {
    isMock: false,
    production: false,

    TenantId: "8139EEA6-469A-40AB-92B7-F4DC33DA2D06",
    Client_ID: "8139EEA6-469A-40AB-92B7-F4DC33DA2D06",
    DomainName: "selise.biz",
    SubDomainName: "https://stg-clmtest.selise.biz/",
    Origin: "https://stg-clmtest.selise.biz/",
    LocalDBName: "clm-logistics-db",
    SyncAdapterId: "SC.SyncAdapters.Business.ClmLogistics",

    // Token: 'http://Security.selise.biz/security/token',
    // Security: "http://Security.selise.biz/Security/",
    Token: `https://stg-identity.selise.biz/${microServiceVersions.identity.stg}identity/token`,
    Captcha: `https://stg-captcha.selise.biz/Captcha/CaptchaCommand/`,
    Security: `https://stg-uam.selise.biz/${microServiceVersions.uam.stg}UserAccessManagement/`,
    Authentication: `https://stg-identity.selise.biz/${microServiceVersions.identity.stg}identity/Authentication/`,
    Apps: `https://stg-catalogue.selise.biz/${microServiceVersions.appcatalogue.stg}AppCatalogue/AppManagerQuery/GetApps`,
    Sites: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/SiteManagerQuery/GetSites`,
    Navigations: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/SiteManagerQuery/GetNavigations`,
    Identity: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/TenantManagerQuery/GetIdentity`,
    
    AggregateService: `https://stg-aggregate.selise.biz/${microServiceVersions.aggregator.stg}Service-Aggregator/ServiceAggrigation/`,

    DataCore: `https://stg-pds.selise.biz/${microServiceVersions.pds.stg}AppSuiteDataCore/`,
    GQLQueryUri: `https://stg-graphqlquery.selise.biz/${microServiceVersions.graphQlQuery.stg}graphql`,
    GQLMutationUri: `https://stg-graphqlcommand.selise.biz/${microServiceVersions.graphQlCommand.stg}graphql`,

    NotificationHubName: "NotifierServerHub",
    NotificationService: `https://stg-notification.selise.biz/${microServiceVersions.notification.stg}`,

    StorageBaseUrl: "https://cdn.selise.biz/",
    StorageFlagUrl: "common/flag/icon/",
    StorageImageUrl: "ClmLogistics/images/",
    CdnTenantBaseUrl: "https://cdn.selise.biz/ClmLogistics/",
    StaticResourceCDN: "https://stg-ecap.s3.eu-central-1.amazonaws.com/",
    SecureStorage: "https://stg-securestorage.selise.biz/StorageService/",
    Storage: `https://stg-storage.selise.biz/${microServiceVersions.storageservice.stg}StorageService/`,

    // EmailService: `https://stg-mail.selise.biz/${microServiceVersions.mailservice.stg}MailService`,
    // MessaginService: `https://stg-MessagingService.selise.biz/${microServiceVersions.messagingservice.stg}MessagingService/`,

    // DictionaryService: `https://stg-dictionaryservice.selise.biz/${microServiceVersions.dictionaryservice.stg}DictionaryService/`,

    // PdfGeneratorService: `https://stg-pdfgenerator.selise.biz/${microServiceVersions.pdfgenerator.dev}PdfGeneratorService/`,
    // TemplateEngineService: "https://stg-templateengine.selise.biz/TemplateEngineService/",

    PersonFields: [
        "ItemId", "CreatedBy", "Tags", "DateOfBirth", "Interests", "ShortBio", "Sex",
        "FirstName", "LastName", "MiddleName", "ProfileImage", "Email", "DisplayName", "Countires",
        "ProfileImageId", "Currency", "IsVerified", "Skills", "Salutation", "CompanyName",
        "AddressLine1", "CustomerId", "AddressLine2", "Street", "PostalCode", "City", "Country", "State", "Nationality",
        "PhoneNumber", "AccountNumber", "Designation", "Organization", "LastLogInTime", "LogInCount", "OfficialPhoneNumber",
        "OfficeCountryCode", "MarketName", "ColorCode", "ContactEmails", "ContactPhoneNumbers", "ManagerNr"
    ],
    SyncUrl: `https://stg-sync.selise.biz/${microServiceVersions.syncservice.stg}DataSyncApi/`,
    ClmApi: `https://stg-clmbusinesstest.selise.biz/ClmBusinessServiceTest`,
};
