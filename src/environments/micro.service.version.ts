export const microServiceVersions = {
    identity: {
        dev: "v12/",
        stg: "v12/",
        prod: "/"
    },
    uam: {
        dev: "v1/",
        stg: "v1/",
        prod: "/"
    },
    tenant: {
        dev: "v5/",
        stg: "v5/",
        prod: "/"
    },
    messagingservice: {
        dev: "v8/",
        stg: "v8/",
        prod: "/"
    },
    mailservice: {
        dev: "v7/",
        stg: "v6/",
        prod: "/"
    },
    storageservice: {
        dev: "v47/",
        stg: "v59/",
        prod: "/"
    },
    securestorage: {
        dev: "v1/",
        stg: "/",
        prod: "/"
    },
    shortmessage: {
        dev: "v6/",
        stg: "v6/",
        prod: "/"
    },
    notification: {
        dev: "v8",
        stg: "",
        prod: ""
    },
    appcatalogue: {
        dev: "v12/",
        stg: "v9/"
    },
    sequencenumber: {
        dev: "v1/",
        stg: "v1/",
    },
    aggregator: {
        dev: "v3/",
        stg: "v3/",
    },
    dictionaryservice: {
        dev: "v1/",
        stg: "v1/",
        prod: "/"
    },
    pdfgenerator: {
        dev: "v15/",
        stg: "/",
        prod: "/"
    },
    pds: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/',
    },
    graphQlCommand: {
      dev: 'v27/',
      stg: 'v27/',
      prod: 'v1/'
    },
    graphQlQuery: {
      dev: 'v27/',
      stg: 'v27/',
      prod: 'v1/'
    },
    syncservice: {
        dev: "v16/",
        stg: "v16/",
        prod: "/"
    }
};
